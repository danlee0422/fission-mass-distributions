/* CubeDet.cxx

   For src_3det_v5 - Yun Jeung, May 2019 update

   Any code used for position and time calculations for each detector has been placed here.

   Liz Williams May 2014 update

 */

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

//specialized header file for CubeDet
#include "CubeDet.h"

using namespace std;

/*------------------------------------------------------------------------------------
  derivTcal
  : Attempted derivation of T0, dT - AC beam only
  Method used: Assume vpar=Vcn and vperp=0.
  Work backwards to solve for T0, dT based on these assumptions and the observed event position and time data for each event.
  - Yun Jeung, May 2019 update
  -------------------------------------------------------------------------------------*/
void derivTcal(reactPar* rxn, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis){

    float T0, dT, dT3;
    float Vcn=rxn->Vcn;

    if (rxn->beamType < 3){

        T0 = -mwpcEvt[1].t      + ( mwpcEvt[1].r * fabs(cos(mwpcEvt[1].theta)) ) / Vcn; // only back detector
        dT = -mwpcEvt[0].t - T0 + ( mwpcEvt[0].r * fabs(cos(mwpcEvt[0].theta)) ) / Vcn;
        dT3 = -mwpcEvt[2].t - T0 + ( mwpcEvt[2].r * fabs(cos(mwpcEvt[2].theta)) ) / Vcn;
        //without the absolute values, you get a second set of solutions
    }
    else{

        T0 = rxn->T0;
        dT = rxn->dT;
        dT3 = rxn->dT3;

    }
    mwpcFis->T0der = T0;
    mwpcFis->dTder = dT;
    mwpcFis->dT3der = dT3; //src_v5.0

}

/*------------------------------------------------------------------------------------
  calcTOF
  : time calculations including T0, dT params
  Modified tdiff definition for time difference method.

  - Yun Jeung, May 2019 update
  Before 2015, raw_T1 is actually raw_T1 - raw_T2.
  Therefore, tdiff = dT - qc1 + mwpcEvt[0].t. (-qc1 is for quarant correction for the front detector)
  But now, we collect inidiviual signals, we defined as
  tdiff = dT + ( mwpcEvt[1].t - mwpcEvt[0].t )

  In src_2det code, time parameters are defined as
  mwpcEvt[0].t += T0 ;         // back
  mwpcEvt[1].t += T0 + dT;     // front

  In src_3det code, time parameters are defined as
  mwpcEvt[0].t += T0 + dT; // back
  mwpcEvt[1].t += T0;      // front
  mwpcEvt[2].t += T0 + dT3 ; // small back

  It dosen't matter swapping back and front detector times! The definition of T0, dT and dT are same.
  -------------------------------------------------------------------------------------*/
void calcTOF(reactPar* rxn, MWPCEvt* mwpcEvt, float qc1, MWPCfis* mwpcFis, int choice){

    float T0 = rxn->T0;
    float dT = rxn->dT;
    float T03 = rxn->T03;
    float dT3 = rxn->dT3;
    float tdiff = 0;
    float tdiff12 = 0;//Y
    float tdiff32 = 0;//Y
    float p1 = 0, p0 = 0;
    float cotx;
    float theta[N_MWPC]={};

    (void)T03; // Ideally T03 should be the same as T0
    (void)qc1; // Data collected > 2015, no need qc1.

    for (int i=0; i<N_MWPC; i++){
        theta[i] = mwpcEvt[i].theta * d2r; //HERE
    }

    if (rxn->beamType == 1){

        tdiff = 0;//tdiff not used, so set to zero

    }//end beamtype 1 (AC, usual method)

    if (rxn->beamType>1){

        cotx = 1.0/tan( theta[choice] );
        p1 = ((float)rxn->Acn * rxn->Vcn) / ( cos( theta[1] ) + sin( theta[1] )*cotx);
        p0 = p1 * sin( theta[1] ) / sin( theta[choice] );
    }

    if (rxn->beamType == 2){

        // mwpcFis->dTder = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[0].r/p0) - ( mwpcEvt[1].t - mwpcEvt[0].t );
        mwpcFis->dTder = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[0].r/p0) - ( mwpcEvt[1].t - mwpcEvt[0].t );
        mwpcFis->dT3der = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[2].r/p0) - ( mwpcEvt[1].t - mwpcEvt[2].t );

        if (rxn->niTag==1){
            dT = mwpcFis->dTder;
        }

        // tdiff = dT + ( mwpcEvt[1].t - mwpcEvt[0].t );

        if (choice == 2){
            tdiff = dT3 + ( mwpcEvt[1].t - mwpcEvt[2].t );
        }else{
            tdiff = dT + ( mwpcEvt[1].t - mwpcEvt[0].t );
        }


    }//end beamtype 2 (AC, tdiff method)
    /*
Note: Discussion with David: In the electronics setup for DC beams/time difference runs,
the time for each detector is no longer CF time versus RF (or if it is, this is a MOTT + pre-MOTT era run).
The back detector TAC is changed such that the CF time for one detector is the start, while the CF time (delayed) for the second detector is the stop.
This is why the front detector time is ignored completely in the logic below.
mwpcEvt[0].t is actually,  dTBack = T1 - T2 = raw.TBack (see convTree.cxx) --> Modified.
     ****
     Check year of data collection and experimental defintion! Modify codes depneding on your situation.
     */

    if (rxn->beamType == 3){

        // mwpcFis->dTder = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[0].r/p0) - qc1 + mwpcEvt[0].t;
        mwpcFis->dTder = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[0].r/p0) - ( mwpcEvt[1].t - mwpcEvt[0].t );
        mwpcFis->dT3der = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[2].r/p0) - ( mwpcEvt[1].t - mwpcEvt[2].t );

        if (rxn->niTag==1){
            dT = mwpcFis->dTder;
        }
        /*
        // Only for old data (before 2015). signals collected using ADCs.
        if (choice == 2){
        tdiff = dT3 + qc1 - mwpcEvt[2].t;
        }else{
        tdiff = dT + qc1 - mwpcEvt[0].t;
        }

        tdiff12 = dT + qc1 - mwpcEvt[0].t;//Y
        tdiff32 = dT3 + qc1 - mwpcEvt[2].t;//Y
         */

        if (choice == 2){
            tdiff = dT3 - ( mwpcEvt[2].t  - mwpcEvt[1].t );// T2-T3
        }else{
            tdiff = dT - ( mwpcEvt[0].t - mwpcEvt[1].t );//T2-T1
        }

        tdiff12 = dT - ( mwpcEvt[0].t  - mwpcEvt[1].t );// T2-T1 <--only this works
        tdiff32 = dT3 - ( mwpcEvt[2].t - mwpcEvt[1].t );// T2-T3

    }// end beamtype 3 (DC beam, tdiff-)

    if (rxn->beamType == 4){

        // mwpcFis->dTder = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[0].r/p0)+qc1-mwpcEvt[0].t;
        mwpcFis->dTder = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[0].r/p0) - ( mwpcEvt[1].t - mwpcEvt[0].t );
        mwpcFis->dT3der = 0.5*rxn->Acn*(mwpcEvt[1].r/p1 - mwpcEvt[2].r/p0) - ( mwpcEvt[1].t - mwpcEvt[2].t );

        if (rxn->niTag==1){
            dT = mwpcFis->dTder;
        }

        /*
        // Only for old data (before 2015). signals collected using ADCs.
        if (choice == 2){
        tdiff = dT3 - qc1 + mwpcEvt[2].t;
        }else{
        tdiff = dT - qc1 + mwpcEvt[0].t;
        }

        tdiff12 = dT - qc1 + mwpcEvt[0].t;//Y
        tdiff32 = dT3 - qc1 + mwpcEvt[2].t;//Y
         */

        if (choice == 2){
            tdiff = dT3 + ( mwpcEvt[2].t  - mwpcEvt[1].t );//T3-T2
        }else{
            tdiff = dT + ( mwpcEvt[0].t - mwpcEvt[1].t );//T1-T2
        }

        tdiff12 = dT + ( mwpcEvt[0].t  - mwpcEvt[1].t );// T1-T2
        tdiff32 = dT3 + ( mwpcEvt[2].t - mwpcEvt[1].t );// T3-T2

    }//end beamtype 4 (DC beam, tdiff+)

    rxn->tdiff=tdiff;
    rxn->tdiff12=tdiff12;
    rxn->tdiff32=tdiff32;

    if (rxn->beamType > 4 || rxn->beamType<1){
        cout << "calcTime: error: beamType invalid!" << endl;
    }

    if (rxn->beamType < 3){

        mwpcEvt[0].t += T0 + dT; // back
        mwpcEvt[1].t += T0;      // front
        mwpcEvt[2].t += T0 + dT3 ; // small back

        /*
        // The same definition to 2det conf.
        mwpcEvt[0].t += T0 ; // back
        mwpcEvt[1].t += T0 + dT;      // front
        //mwpcEvt[2].t += (T0-3) + (dT3+5); // small back
        mwpcEvt[2].t += T03 + dT3; // small back
         */

        //printf("Checking events... dt3: %f\n",dT3);
        //printf("beamtype: %i\n",rxn->beamType);

    }// only modify calibrated times if this is relevant (AC beam, separate CF time vs RF for both detectors)

  // check for unrealistic tdiff
 //cout<<"rxn->tdiff="<< rxn->tdiff <<endl;
  if (rxn->tdiff < -300){  
      rxn->tdiff=-300;}
  if (rxn->tdiff > 300){  
      rxn->tdiff=300;}
 //cout<<"rxn->tdiff="<< rxn->tdiff <<endl;

}//end calcTime

/*------------------------------------------------------------------------------------
  calcQuadrantCenter
  : Calculate quadrant for each event, so quadrant offsets can be applied
  Define the small back detector size. Default centre is (11.7,0)
  Redefine the central position depending on XR=130 or XL=130.
  - Yun Jeung, May 2019 update
  -------------------------------------------------------------------------------------*/
void calcQuadrantCenter(MWPCgeo* mwpcGeo, MWPCcorr* mwpcCorr){

    float quadSizeX = 142.0; //XL=130.0+12.0 = 142 mm (Quad1) size
    float quadSizeX_det3 = 131.5;  //src_v5.0
    float quadSizeY = 178.5;

    /*Back Dedector*/
    if ((cos(mwpcGeo->detPhi) < 0) && (mwpcGeo->detID) == 0 ){ //Back
        // works when 2 detector configraion without blocking by the 3rd detector
        if(fabs(mwpcGeo->XRmm)==130.0){
            mwpcCorr->qx0 = - quadSizeX + fabs( mwpcGeo->XRmm ) + mwpcCorr->detxOffset; // -142 + 130 = -12.0
        }else if(fabs(mwpcGeo->XRmm)==149.0){
            mwpcCorr->qx0 = -quadSizeX + fabs( mwpcGeo->XRmm) + 5 + mwpcCorr->detxOffset; // -142+149+5=12.0
        //}else if(fabs(mwpcGeo->XRmm)==191.4){ // OPTIMALLY for fis220th dataset, 
        // but something is wrong with the fabs comparison!!!
        }else{ // For fis220th dataset
            mwpcCorr->qx0 = quadSizeX - fabs( mwpcGeo->XLmm) + mwpcCorr->detxOffset;
            //mwpcCorr->qx0 = -quadSizeX + fabs( mwpcGeo->XRmm) + mwpcCorr->detxOffset; // -142+XRmm+offset  Berriman 11Dec2019
        }


        /*Small Back Dedector*/
    }else if((cos(mwpcGeo->detPhi) < 0) && (mwpcGeo->detID == 2 )){

        mwpcCorr->qx0 = quadSizeX_det3 - fabs( mwpcGeo->XLmm )  + mwpcCorr->detxOffset;

        /*Front Dedector*/
    }else{

        mwpcCorr->qx0 = - quadSizeX + fabs( mwpcGeo->XRmm ) + mwpcCorr->detxOffset;//-142.0 + 130 = -12.0

    }

    //calculate y coordinate of center in detector coordinates
    if (mwpcGeo->YBmm < mwpcGeo->YTmm){
        mwpcCorr->qy0 = quadSizeY + mwpcGeo->YBmm + mwpcCorr->detyOffset;
    }
    else{
        mwpcCorr->qy0 = quadSizeY + mwpcGeo->YTmm + mwpcCorr->detyOffset;
    }
    //For printing out in the terminal
    cout << " \t" << mwpcCorr->qx0<<"  | "<<mwpcCorr->qy0<<" \t  "<< mwpcGeo->XLmm  <<" | "<<  mwpcGeo->XRmm << "\t     "<< mwpcCorr->detxOffset <<" | "<< mwpcCorr->detyOffset<< endl;

} //end calcQuadrantCenter

/*------------------------------------------------------------------------------------
  calcQuadrant
  : Figures out what quadrant an event is in; calculated for each (x,y)
  pair in each detector [det coordinates]
  -------------------------------------------------------------------------------------*/
int calcQuadrant(MWPCcorr* mwpcCorr, MWPCEvt* mwpcEvt){

    float quadX=mwpcCorr->qx0;
    float quadY=mwpcCorr->qy0;

    float x=mwpcEvt->x_orig;
    float y=mwpcEvt->y_orig;

    if ( x >= quadX && y >= quadY){
        return 1;
    }
    else if (x < quadX && y >= quadY){
        return 2;
    }
    else if (x < quadX && y < quadY){
        return 3;
    }
    else if (x >= quadX && y < quadY){
        return 4;
    }
    else{
        return 0;
        cout << "calcQuadrant: error: Invalid quadrant calculated! Check position calibration." << endl;
    }

} //end calcQuadrant


/*------------------------------------------------------------------------------------
  calcPosCartesian
  : Transforms detector coordinates into cartesian coordinates (x,y,z)
  -------------------------------------------------------------------------------------*/
void calcPosCartesian( MWPCEvt* mwpcEvt, MWPCgeo* mwpcGeo){

    //calculate cartesian coords
    mwpcEvt->x =  mwpcGeo->x0 + mwpcEvt->x_mm * cos(mwpcGeo->detTheta);
    mwpcEvt->y =  mwpcGeo->y0 + mwpcEvt->y_mm;
    mwpcEvt->z =  mwpcGeo->z0 + ( ( mwpcEvt->x_mm * sin(mwpcGeo->detTheta) ) * mwpcGeo->signPhi );

}//end calcPosCartesian


/*------------------------------------------------------------------------------------
  calcPosSpherical
  : Transforms cartesian coordinates into spherical coordinates (r,theta,phi)
  -------------------------------------------------------------------------------------*/
void calcPosSpherical(MWPCEvt* mwpcEvt){

    float x=0,y=0,z=0,r=0,theta=0,phi=0;

    x=mwpcEvt->x;
    y=mwpcEvt->y;
    z=mwpcEvt->z;

    //calculate spherical coordinates
    r = x*x + y*y + z*z;
    r = sqrt(r);

    theta = acos ( z / r );
    theta = theta * r2d;

    phi = atan ( y / x );

    if (x<0){
        phi=phi+pi;
    } //selects correct quadrant of phi

    if (phi<(-pi/2.)){
        phi = 2.*pi-fabs(phi);
    }//done to be consistent with dagui; go from -180->180 to -90->270 deg

    phi = phi * r2d;

    mwpcEvt->r = r;
    mwpcEvt->theta = theta;
    mwpcEvt->phi = phi;

} //end calcPosSpherical


/*------------------------------------------------------------------------------------
  mwpcPosCalib
  : Calculates position calibration for transformation from channels -> detector coordinates
  based on information from the detfile
  -------------------------------------------------------------------------------------*/
void mwpcPosCalib(MWPCgeo* mwpcGeo){

    int detXwidth_ch, detYwidth_ch;
    float detXwidth_mm, detYwidth_mm;
    float ax,ay,bx,by;

    detXwidth_ch = mwpcGeo->XLch - mwpcGeo->XRch;
    detYwidth_ch = mwpcGeo->YBch - mwpcGeo->YTch;

    detXwidth_mm = mwpcGeo->XLmm - mwpcGeo->XRmm;
    detYwidth_mm = mwpcGeo->YBmm - mwpcGeo->YTmm;

    ax = detXwidth_mm / (float)detXwidth_ch;
    ay = detYwidth_mm / (float)detYwidth_ch;

    bx=mwpcGeo->XLmm - ( ax * (float)mwpcGeo->XLch );
    by=mwpcGeo->YBmm - ( ay * (float)mwpcGeo->YBch );

    mwpcGeo->ax_mm = ax;
    mwpcGeo->bx_mm = bx;

    mwpcGeo->ay_mm = ay;
    mwpcGeo->by_mm = by;

}//end mwpcPosCalib

/*------------------------------------------------------------------------------------
  calcMwpcCenter
  : Calculates MWPC detector center position in cartesian coordinate frame
  next calculate detector centre in cartesian coordinates (uses Rickard's coord. sys. convention)
  -------------------------------------------------------------------------------------*/
void calcMwpcCenter(MWPCgeo* mwpcGeo){

    float term=cos (mwpcGeo->detPhi);

    mwpcGeo->signPhi = -1.0 * term / fabs ( term );

    mwpcGeo->x0 = fabs(mwpcGeo->detDist)*sin(mwpcGeo->detTheta)*cos(mwpcGeo->detPhi);
    mwpcGeo->y0 = fabs(mwpcGeo->detDist)*sin(mwpcGeo->detTheta)*sin(mwpcGeo->detPhi);
    mwpcGeo->z0 = fabs(mwpcGeo->detDist)*cos(mwpcGeo->detTheta);

}//end calcMwpcCenter

/*------------------------------------------------------------------------------------
  mwpcDetPosCalib
  : Calculates event coordinates in detector coordinate system
  -------------------------------------------------------------------------------------*/
void mwpcDetPosCalib(MWPCEvt* mwpcEvt, MWPCgeo* mwpcGeo, int i){

    mwpcEvt->x_orig = mwpcGeo->ax_mm * mwpcEvt->x_raw + mwpcGeo->bx_mm;
    mwpcEvt->y_orig = mwpcGeo->ay_mm * mwpcEvt->y_raw + mwpcGeo->by_mm;

    mwpcEvt->x_mm = mwpcEvt->x_orig;
    mwpcEvt->y_mm = mwpcEvt->y_orig; //seems reduntant but is used in the event that shifts to the raw position data are required.

    //if it is back detector, x_mm --> x1det y_mm --> y1det
    //specific code for datasets with position anomalies due to time mismatch in CF - only part that uses the detector number

#define USER_DET
#include "my_code.h"
#undef USER_DET

}//end mwpcDetPosCalib

/*------------------------------------------------------------------------------------
  mwpcTimeCalib
  : for each event, applies quadrant corrections
  -------------------------------------------------------------------------------------*/
float mwpcTimeCalib(MWPCcorr* mwpcCorr, MWPCEvt* mwpcEvt){

    float qc = 0;
    float t;
    int quadNo;

    //* Calculate time information: *//
    quadNo=calcQuadrant(mwpcCorr,mwpcEvt);
    mwpcEvt->quadID=quadNo;

    if (mwpcCorr->qcunits == 0){     //apply quadrant corrections in channels
        mwpcEvt->t_raw += (int)mwpcCorr->qc[quadNo-1];
        qc = mwpcCorr->qc[quadNo-1] * mwpcCorr->timeSlope; //save quadrant correction for event (to be used below)
    }

    t = - mwpcCorr->timeSlope * (float)mwpcEvt->t_raw;

    if (mwpcCorr->qcunits == 1){     //apply quadrant corrections in ns
        t += mwpcCorr->qc[quadNo-1];
        qc = mwpcCorr->qc[quadNo-1]; // save quadrant correction for event (to be used below)
    }


    mwpcEvt->t = t; // save it to event structure

    return qc; //return quadrant correction value (to be used in TOF calculation)

}//end mwpcTimeCalib

/*------------------------------------------------------------------------------------
  checkTime
  : for each event, check time
    check time data is calculted correctly
    corrects error in convTree and accounts for time difference method
    Modifed by Annette 8Feb2021
  -------------------------------------------------------------------------------------*/
void checkTime(MWPCEvt* mwpcEvt,reactPar* rxn, float rf ){

     // determine raw time for detector 3 
     //from top (T3A_raw) or bottom (T3B_raw) of detector
     // this corrects error in convTree.cxx
     mwpcEvt[2].T_raw=0;
     if(mwpcEvt[2].T3A_raw>0 && mwpcEvt[2].T3B_raw==0){ 
     mwpcEvt[2].T_raw=mwpcEvt[2].T3A_raw;
      }
     if(mwpcEvt[2].T3A_raw==0 && mwpcEvt[2].T3B_raw>0){
     mwpcEvt[2].T_raw=mwpcEvt[2].T3B_raw;
      }
     if(mwpcEvt[2].T3A_raw>0 && mwpcEvt[2].T3B_raw>0){
     mwpcEvt[2].T_raw=mwpcEvt[2].T3A_raw;
      }

     // determine converted times for all detectors
     // time difference method no longer required RF>0
     for (int i=0;i<3;i++){
	mwpcEvt[i].t_raw=0;

       if(rxn->beamType==1){	//AC beam analysis
    	 if(rf>0 && mwpcEvt[i].T_raw>0){
    	   mwpcEvt[i].t_raw = rf-mwpcEvt[i].T_raw;
   	  }else{
  	     mwpcEvt[i].t_raw = 10000;
  	  }
 	}else			//time difference analysis
	if(mwpcEvt[i].T_raw>0){
    	   //mwpcEvt[i].t_raw = rf-mwpcEvt[i].T_raw;
    	   mwpcEvt[i].t_raw = -mwpcEvt[i].T_raw;	//test Annette 11Feb2021
   	  }else{
  	     mwpcEvt[i].t_raw = 10000;
  	  }
     }

}//end checkTime


