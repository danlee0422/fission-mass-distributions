#ifndef GUARD_CubeDet
#define GUARD_CubeDet

#include "CubeStruct.h"
#include "physConst.h"


void derivTcal(reactPar*, MWPCEvt*, MWPCfis* );
//void calcTOF(reactPar*, MWPCEvt*, float, MWPCfis*);
void calcTOF(reactPar*, MWPCEvt*, float, MWPCfis*,int);//src_v5.0

void calcQuadrantCenter(MWPCgeo*, MWPCcorr*);
int calcQuadrant(MWPCcorr*, MWPCEvt*);
void calcPosCartesian(MWPCEvt*,MWPCgeo*);
void calcPosSpherical(MWPCEvt*);
void mwpcPosCalib(MWPCgeo*);
void calcMwpcCenter(MWPCgeo*);
void mwpcDetPosCalib(MWPCEvt*, MWPCgeo*,int);
float mwpcTimeCalib(MWPCcorr*, MWPCEvt*);
void checkTime(MWPCEvt*,reactPar*, float);

#endif
