#ifndef CubeFis_h
#define CubeFis_h

#include "CubeStruct.h"
#include "physConst.h"
#include "eloss.h"
#include "MassTable.h"
#include "CubeLoss.h"


void calcFold(MWPCEvt*,MWPCfis*,int);
//void calcTimeSharp(reactPar*, MWPCEvt*, MWPCfis*);
void calcTimeSharp(reactPar*, MWPCEvt*, MWPCfis*, int);
void calcVelAC(MWPCEvt*);
//void calcVelAC(MWPCEvt*,MWPCEvt*);
void calcVelTdiff(MWPCEvt*, MWPCfis*, reactPar*,int);
void calcVcm(reactPar*, MWPCEvt*, MWPCfis*, int,int);
void calcMasses(reactPar*, MWPCfis*);
//void calcThetaCM(reactPar*, MWPCEvt*,int);
void calcThetaCM(reactPar*, MWPCfis*, MWPCEvt*,int); //test
void calcVparVperp(MWPCEvt*, MWPCfis*,int);

float calcVfromEM(float, float);
float calcEfromMV(float, float);
void calcTKE(MWPCfis*);
void calcTKEViola(MWPCfis*, reactPar*);
void calcAngleFFTarg(targetPar*, MWPCEvt*,int);
//void elossVMRcorr(reactPar*, targetPar*, MWPCEvt*, MWPCfis*,int);
void elossVMRcorr(reactPar*, targetPar*, MWPCEvt*, MWPCfis*, MWPCgeo*, int); //*Annette*14Jan2021*add_mwpcGeo
//void calcMrVelThetaCM(reactPar*, targetPar*, MWPCEvt*, MWPCfis*,int);
void calcMrVelThetaCM(reactPar*, targetPar*, MWPCEvt*, MWPCfis*, MWPCgeo*,int); //*Annette*14Jan2021*add_mwpcGeo

//void Tdiffcalc(MWPCEvt*, MWPCfis*, reactPar*, targetPar*,int);//src_v5.0
void Tdiffcalc(MWPCEvt*, MWPCfis*, MWPCgeo*,reactPar*, targetPar*,int); //*Annette*14Jan2021*add_mwpcGeo

void calcTKEfromFoldMR(MWPCfis*,MWPCEvt*,reactPar*,int);
void calcExFis(reactPar*,MWPCfis*, MassTable*);
int* chooseBackdet(MWPCEvt*,MWPCgeo*,int* event);

#endif
