/* CubeFis.cxx

   For src_3det_v5 - Yun Jeung, May 2019 update

   Does all velocity, mass, etc, calculations based on the assumption that coincident events are two-body fission
   Dependencies: CubeFis.h, eloss.h, eloss.cxx

 */

/***********************************/
/*         HEADER FILES            */
/***********************************/

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

//specialized header file for cubeFis
#include "CubeFis.h"
#include "CubeLoss.h"

using namespace std;
/*
 --- Experimental velocity sum data ---
 velocity sum: K=v1+v2 (cm/ns), as function of fragment mass
 Used in function calcVelTdiff where VsumType=1 (from input file)

 first row: Acn (or 1), projectile lab energies in MeV
 subsequent rows: lighter fragment mass (or mass ratio), K (from selected mass and lab energy)

 note 1: Calculations in calcVelTdiff convert masses to mass ratios
 note 2: Acn in list does not need to be equal to Acn from input file
 note 3: Assumes symmetric mass distribution, so only list lighter fragment masses up to Acn/2
 note 4: List lightest masses first
 note 5: Masses do not need to be integer or evenly spaced
 note 6: Can include mass-ratio (instead of masses). If so, set Acn(in first row) to 1.
 note 7: Array size can be adjusted to correspond to number of data points and lab energies.
 note 8: Automatically finds size of array (KExpRows and KExpColumns)
*/

 #define VSUM_TAB
 #include "vsum_table.h"
 #undef VSUM_TAB

 static int KExpRows =sizeof(KExp)/sizeof(KExp[0]);
 static int KExpColumns =sizeof(KExp[0])/sizeof(KExp[0][0]);

/*------------------------------------------------------------------------------------
                            chooseBackdet
 : Check if det3 has events, if so then feed this into the two-body kinematics
 Suggesting 5% increasment in judging events by the boundary of each detector as we still
 see events in tails.
 - Taiki Tanaka, May 2019
 - Yun Jeung, May 2019 update
-------------------------------------------------------------------------------------*/
int* chooseBackdet(MWPCEvt* mwpcEvt, MWPCgeo* mwpcGeo, int* event){

    int choice = 0;
    (void)choice; // To remove warning sign.

    //int event[N_MWPC];
    for(int i=0;i<N_MWPC;++i){event[i]=0;}
    // int multiplicity = 0;

    // ...Depening on how we define YSmallBack in convTree.cxx script
    int detYPolarity[3] = {1,1,-1}; // Detector 3 appears to be upside down in detfile YB > 0 && YT < 0

    // Check for events within detector limits - for all detectors
    for(int i=0;i<N_MWPC;++i)
      {
        if((mwpcEvt[i].x_raw >  mwpcGeo[i].XLch*1.05) &&
           (mwpcEvt[i].x_raw <  mwpcGeo[i].XRch*1.05) &&
           (mwpcEvt[i].y_raw > detYPolarity[i]*mwpcGeo[i].YBch*1.05) &&
           (mwpcEvt[i].y_raw < detYPolarity[i]*mwpcGeo[i].YTch*1.05))
	  {
            event[i] = 1; //found event!
	  }
    }
    // Make choice - at the moment just on detector 3
    if(event[2]) choice = 2;
    else choice = 0;

    //return choice;
    return &event[0];
}
/*------------------------------------------------------------------------------------
                            calcFold
 : Calculates folding angle (theta12 or theta32) and phi1-phi2
 ------------------------------------------------------------------------------------*/
void calcFold(MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, int choice){

  //folding angle and phi difference calculations - for each pair of fission fragments
  mwpcFis->phi12 = -(mwpcEvt[1].phi - mwpcEvt[choice].phi);
  mwpcFis->theta12= mwpcEvt[choice].theta+mwpcEvt[1].theta;

}//end calcFold

/*------------------------------------------------------------------------------------
                            calcVelAC
 : Calculates velocities from position and time for AC beam selection
------------------------------------------------------------------------------------*/
void calcVelAC(MWPCEvt* mwpcEvt){

  for (int i=0; i<N_MWPC; i++){
    mwpcEvt[i].v = mwpcEvt[i].r / mwpcEvt[i].t;
  }

}//end calcVelAC

/*---------------------------------------------------------------------------------
                            calcTimeSharp
 : time sharpening calculations
 Used for linac data in which the time resolution is poor (pre 2013 data)
 Only the following variables are affected by time sharpening: v, vcm, mass, mr, vcmsum
 TKE will also be affected, as it is calculated after the velocities and masses are known
 - Kaushik Banerjee, Jul 2018 update
---------------------------------------------------------------------------------*/
void calcTimeSharp(reactPar* rxn, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis,int choice){

  float r[N_MWPC]={};
  float theta[N_MWPC]={};
  float t[N_MWPC]={};
  float v[N_MWPC]={};
  float vcm[N_MWPC]={};
  float m[N_MWPC]={};
  float dT, denom, mr,dT3;
  float vcn = rxn->Vcn;

  int i;

  for (i=0;i<N_MWPC;i++){
    r[i]=mwpcEvt[i].r;
    theta[i]=mwpcEvt[i].theta*d2r;
    t[i]=mwpcEvt[i].t;
  }

  //Detector 1 & 2 Configuration
  if(choice == 0){
    denom = (r[choice]*sin(theta[0])+r[1]*sin(theta[1]))*vcn;
    dT =  (r[choice]*r[1]*(sin(theta[choice])*cos(theta[1])+sin(theta[1])*cos(theta[choice]))
	   - (r[choice]*sin(theta[choice])*t[1]+r[1]*sin(theta[1])*t[choice])*vcn);
    dT = dT / denom;
   //Recalculate velocities
    for (i=0;i<2;i++){
      v[i] = r[i]/(t[i]+dT);
      vcm[i] = sqrt( v[i]*v[i] + vcn*vcn - 2*v[i]*vcn*cos(theta[i]) );
    }

    mr = vcm[1] / (vcm[choice] + vcm[1]);
    m[choice] =  mr * ((float)rxn->Acn);
    m[1] =  (1.0 - mr) * ((float)rxn->Acn);

    for (i=0; i<N_MWPC; i++){
      mwpcEvt[i].v = v[i]; // mm/ns
    }

    for (i=0; i<2; i++){/* vcm and m always have 2 variables */
      mwpcFis->Vcm[i] = vcm[i]; // mm/ns
      mwpcFis->mass[i] = m[i];
    }

  //save altered values to appropriate structures
    mwpcFis->mr = mr;
    mwpcFis->VcmSum = vcm[0]+vcm[1];
    mwpcFis->TSharp = dT;

    //Detector 2 & 3 Configuration
  }else if(choice == 2){

    denom = (r[choice]*sin(theta[choice])+r[1]*sin(theta[1]))*vcn;
    dT3 =  (r[choice]*r[1]*(sin(theta[choice])*cos(theta[1])+sin(theta[1])*cos(theta[choice]))
	    - (r[choice]*sin(theta[choice])*t[1]+r[1]*sin(theta[1])*t[choice])*vcn);
    dT3 = dT3 / denom;

   //Recalculate velocities
    for (i=1;i<N_MWPC;i++){
      v[i] = r[i]/(t[i]+dT3);
      vcm[i] = sqrt( v[i]*v[i] + vcn*vcn - 2*v[i]*vcn*cos(theta[i]) );
    }

    mr = vcm[1] / (vcm[choice] + vcm[1]);
    m[choice] =  mr * ((float)rxn->Acn);
    m[1] =  (1.0 - mr) * ((float)rxn->Acn);

    //save altered values to appropriate structures
    mwpcEvt[1].v = v[1];
    mwpcEvt[0].v = v[2];
    mwpcFis->Vcm[1] = vcm[1];
    mwpcFis->Vcm[0] = vcm[2];
    mwpcFis->mass[1] = m[1];
    mwpcFis->mass[0] = m[2];
    mwpcFis->mr = mr;
    mwpcFis->VcmSum = vcm[1]+vcm[2];
    mwpcFis->TSharp = dT3;

  }
} //end calcTimeSharp

/*------------------------------------------------------------------------------------
                            calcVelTdiff
 : Calculates velocities, masses, and mass ratios for ALL TIME DIFFERENCE METHODS
 Also derives times from position and velocity calculations
 This uses the same procedure outlined in dagui, which originated in RGTC code. Assumes vpar = vcn, and assumes two-body fission
 Note: Discussion with David: In the electronics setup for time difference, the time for each detector is no longer CF time versus RF (or if it is, this is legacy).
 The back detector TAC is changed such that the CF time for one detector is the start, while the CF time (delayed) for the second detector is the stop.
 This is why the front detector time is ignored completely in the logic below.
 Thanks to Annette for a great contribution in time difference method calculations (src_2det).
 - Annette Berriman, Mar 2019
 - Yun Jeung, May 2019 updated for src_3det
 Now it is okay to use rxn->tdiff which already takes a choice of 0 or 1 in CubeDet.cxx into account.
 I won't delete rxn->tdiff12 and rxn->tdiff32 since they are useful parameters to check dT or dT3 ranges separately.
------------------------------------------------------------------------------------*/
void calcVelTdiff(MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, reactPar* rxn, int choice){
 // initialize variables --> set to all 0.
  float m[N_MWPC]={};
  float v[N_MWPC]={};
  float vcm[N_MWPC]={};
  float r[N_MWPC]={};
  float theta[N_MWPC]={};

  float mr=0.0;
  float cotx, p1, p0;
  float vcn=rxn->Vcn;

  for (int i=0;i<N_MWPC;i++){
    r[i]=mwpcEvt[i].r;
    theta[i]=mwpcEvt[i].theta*d2r;
   }

  if ((float)rxn->Ap >=3){// Ap>=3 start

    cotx = 1.0/tan( theta[choice]);
    p1 = ((float)rxn->Acn * vcn) / ( cos( theta[1] ) + sin( theta [1] )*cotx);
    p0 = p1 * sin( theta[1]) / sin(theta[choice] );

    // calculate masses and mass ratios, save to mwpcFis structure
    m[1] =  ( rxn->tdiff + (float)rxn->Acn * ( r[choice] / p0 ) ) / ( ( r[1]/p1 ) +( r[choice] / p0 ) );
    m[0] =  (float)rxn->Acn - m[1];
    mr = m[0]/(m[1]+m[0]);

    mwpcFis->mr=mr;
    mwpcFis->mass[0]=m[0];
    mwpcFis->mass[1]=m[1];

    //calculate lab velocities
    v[0] = p0/m[0];
    v[1] = p1/m[1];

    mwpcEvt[choice].v=v[0];
    mwpcEvt[1].v=v[1];

    //derive time from position and calculated velocities
    mwpcEvt[choice].t= r[choice]/mwpcEvt[choice].v;
    mwpcEvt[1].t= r[1]/mwpcEvt[1].v;

    //calculate CM velocities
    for (int i=0;i<2;i++){
      calcVcm(rxn,mwpcEvt,mwpcFis,i,choice);
    }

  }//end Ap>=3
  else{//Ap<3 start

     mr=0.5;
    /*
      For light projectiles with time diff method
      Trying the new approximate formula for mass ratio derived by Nanda and David
      float aa,bb,cc temporary variables used for mass ratio calculation
    */
    float K = 0;
    float aa,bb,cc;
    int iterations;

    // calculation for K, where K is the velocity sum K=v1+v2
    int VsumType= rxn->VsumType;	// Set Velocity Sum Calculation type (0=Viola, 1=Mulgin, 2=Logan)
/*
    cout <<"------------------------"<< endl;
    cout <<"You choose VsumType = "<< VsumType << endl;
    cout <<"------------------------"<< endl;
  */
    // set number of iterations of mr calculation
    if (VsumType==0){iterations=2;}  // Viola
    if (VsumType==1){iterations=3;}  // Experimental (table at start of this file)
    if (VsumType==2){iterations=2;}  // Logan

    //Do error checking...
    if (VsumType>=3){ // check for invalid Velocity Sum type
      cout << "You have chosen invalid velocity sum type. Runing with type = 0" << endl;
      VsumType=0;
    }

    for (int jj=0; jj<iterations; jj++){ //start iterations

//--- Viola velocity sum ------
//This type was used in dacube_v4.0 .
      if (VsumType==0){

	K = 10 * 0.982 * sqrt(1.51 * pow((float)rxn->Zcn,2.) /
			      (pow((float)rxn->Acn,4./3.) * (pow(mr,1./3.) + pow(1-mr,1./3.)) )
			      + 14.6 /  ( (float)rxn->Acn * mr * (1-mr) ) );
      }//end Viola

//Introduce two more types in dacube_v5.0
//--- Experimental velocity sum (modified Berriman August 2020) -----
      if (VsumType==1){
	// ** Berriman **
	// new K calculation - using experimental velocity sum
	// based on array KExp

    //testing-start
    float mr1, El, row1_fraction, row2_fraction, col1_fraction, col2_fraction;
    int row1, row2, column1, column2, count;

    El=rxn->Elab;

    if (mr>0.5){		//mass ratios must be <=0.5, mirrored if above 0.5
      mr1=1-mr;
    }else{
      mr1=mr;
    }


    // determine array row (mass or mr)
    row1=0;
    row1_fraction=0;
    row2=0;
    row2_fraction=0;
    if (mr1<=KExp[1][0]/KExp[0][0]){   // mr1 value outside range from experimental data
    	row1=1;
    	row1_fraction=1;
    	row2=1;
    	row2_fraction=0;
    }else{
      if (mr1>KExp[KExpRows-1][0]/KExp[0][0]){   // mr1 around 0.5 (within one mass unit)
    	row1=KExpRows-1;
    	row1_fraction=1;
    	row2=KExpRows-1;
    	row2_fraction=0;
    }else{
     count=1;
     do {
       if ((mr1>KExp[count][0]/KExp[0][0]) and (mr1<=KExp[count+1][0]/KExp[0][0])){
        row1=count;
        row1_fraction=(KExp[count+1][0]/KExp[0][0]-mr1)/((KExp[count+1][0]-KExp[count][0])/KExp[0][0]);
    	row2=count+1;
    	row2_fraction=(mr1-KExp[count][0]/KExp[0][0])/((KExp[count+1][0]-KExp[count][0])/KExp[0][0]);
		  }
    	count=count+1;
    	} while ((row1==0) && (count<KExpRows));
      }
    }

    // determine array column (projectile energy)
    column1=0;
    col1_fraction=0;
    column2=0;
    col2_fraction=0;
    if (El<=KExp[0][1]){   // Elab below range from experimental data
    	column1=1;
    	col1_fraction=1;
    	column2=1;
    	col2_fraction=0;
    }else{
      if (El>KExp[0][KExpColumns-1]){   // Elab above range from experimental data
    	column1=KExpColumns-1;
    	col1_fraction=1;
    	column2=KExpColumns-1;
    	col2_fraction=0;
    }else{
    	count=1;
    	do {
    	  if (El>KExp[0][count] and El<=KExp[0][count+1]){
    	    column1=count;
    	    col1_fraction=(KExp[0][count+1]-El)/(KExp[0][count+1]-KExp[0][count]);
    	    column2=count+1;
    	    col2_fraction=(El-KExp[0][count])/(KExp[0][count+1]-KExp[0][count]);
    	  }
    	  count=count+1;
    	} while ((column1==0) && (count<KExpColumns));
      }
    }

	// determine velocity sum (look up from array)

    K=(KExp[row1][column1]*row1_fraction+KExp[row2][column1]*row2_fraction)*col1_fraction+(KExp[row1][column2]*row1_fraction+KExp[row2][column2]*row2_fraction)*col2_fraction;
    //set scale
	K=10*K;

      }//end Experimental Velocity Sum

//--- Logan velocity sum  -----
      if (VsumType==2){
	K = 10 * sqrt( (1.51 * pow((float)rxn->Zcn,2.))
		       / ( pow((float)rxn->Acn,4./3.) )
		       + 92.704 / rxn->Acn ) / sqrt( pow(mr,1./3.) + pow(1-mr,1./3.));
      }//end Logan

//--- End choice of VsumType --------
      // calculate mr (Hinde April 2019)
      aa = -K*(rxn->tdiff);
      bb = -aa + r[choice] + r[1] + (rxn->tdiff)*vcn*(cos(theta[choice])-cos(theta[1]));
      cc = -r[1] + vcn*( (rxn->tdiff)*cos(theta[1]) - (r[1]*cos(theta[0]) - r[choice]*cos(theta[1]))/K
			 + (rxn->tdiff)*vcn*cos(theta[choice])*cos(theta[1])/K);

      mr=(-bb+sqrt(bb*bb-4.0*aa*cc))/2.0/aa;//

    }//end iterations //end 'for' loop

//---- Save parameters --------
    //save mass ratio
    mwpcFis->mr = mr;  //still dependent on choice!!

    //calculate CM velocities
    vcm[1] = K*mr;//mm/ns
    vcm[choice] = K-vcm[1];

    mwpcFis->Vcm[1] = vcm[1];
    mwpcFis->Vcm[0] = vcm[choice];// only two parameters Vcm[0] and Vcm[1]

    // calculate masses
    m[choice] = ((float)rxn->Acn)*vcm[1]/K;
    m[1] = (float)rxn->Acn - m[choice];

    mwpcFis->mass[0] = m[choice];//!!!!! // only two parameters  m[0] and m[1]
    mwpcFis->mass[1] = m[1];

    //calculate lab velocities
    //v[choice] = vcn*cos(theta[choice] )+sqrt(vcm[choice]*vcm[choice]-vcn*vcn*(1-cos( theta[choice] )));
    // v[1] = vcn*cos( theta[1])+sqrt(vcm[1]*vcm[1]-vcn*vcn*(1-cos( theta[1] )));

    v[choice] = vcm[choice]*sin(pi-theta[choice]-(asin(vcn*sin(theta[choice])/vcm[choice])))/sin(theta[choice]);
    v[1] = vcm[1]*sin(pi-theta[1]-(asin(vcn*sin(theta[1])/vcm[1])))/sin(theta[1]);

    mwpcEvt[choice].v=v[choice];
    mwpcEvt[1].v=v[1];


   //derive time from position and calculated velocities
    mwpcEvt[choice].t=r[choice]/mwpcEvt[choice].v;
    mwpcEvt[1].t=mwpcEvt[1].r/mwpcEvt[1].v;

     // calculate final time difference
    if (rxn->beamType == 2 || rxn->beamType == 3){
      rxn->tdiff=mwpcEvt[1].t-mwpcEvt[choice].t; //T2-(T1 or T3)
      if(choice==0){rxn->tdiff12 =mwpcEvt[1].t-mwpcEvt[0].t;}//T2-T1
      else{rxn->tdiff32 =mwpcEvt[1].t-mwpcEvt[2].t;}//T3-T2
    }
    if(rxn->beamType == 4){
      rxn->tdiff=mwpcEvt[choice].t-mwpcEvt[1].t; //(T1 or T3)-T2
      if(choice==0){rxn->tdiff12 =mwpcEvt[0].t-mwpcEvt[1].t;}//T2-T1
      else{rxn->tdiff32 =mwpcEvt[2].t-mwpcEvt[1].t;}//T3-T2
    }

  }//end Ap<3

  // calculate lab energies
    mwpcEvt[0].Elab=calcEfromMV(mwpcFis->mass[choice], mwpcEvt[choice].v);
    mwpcEvt[1].Elab=calcEfromMV(mwpcFis->mass[1], mwpcEvt[1].v);

}//end calcVelTdiff

/*------------------------------------------------------------------------------------
                            calcVcm
 : Calculates centre of mass velocities
------------------------------------------------------------------------------------*/
void calcVcm(reactPar* rxn, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, int i, int choice){

  float vcn=rxn->Vcn;
  float theta,v;

  int tmp = 0;

  if(i==0) tmp = choice;//det1 or det3
  if(i==1) tmp = 1;

  theta = mwpcEvt[tmp].theta * d2r;
  v=mwpcEvt[tmp].v;

  mwpcFis->Vcm[i] = sqrt( v*v + vcn*vcn - 2*v*vcn*cos(theta) );

}//end calcVcm

/*------------------------------------------------------------------------------------
                            calcMasses
 : Calculates centre of mass velocities
------------------------------------------------------------------------------------*/
void calcMasses(reactPar* rxn, MWPCfis* mwpcFis){

  int i;
  float vcm[2]={};
  float m[2]={};
  float mr;

  for (i=0;i<2;i++){
    vcm[i] = mwpcFis->Vcm[i];
  }

  //calculate mass ratios, masses
  mr = vcm[1] / (vcm[0] + vcm[1]);
  m[0] =  mr * ((float)rxn->Acn);
  m[1] =  (1.0 - mr) * ((float)rxn->Acn);

  //save calculations to structures
  for (i=0;i<2;i++){
    mwpcFis->mass[i] = m[i];
  }

  mwpcFis->mr = mr;

}//end calcMasses

/*------------------------------------------------------------------------------------
                            calcThetaCM
 : Calculates scattering angle for each fission fragment
 We need to define vdsine = (vpar-Vcn)sinThetaCM here since we need corrected ThetaCM values
 See calcMrVelThetaCM
 - Yun Jeung, May 2019 update
------------------------------------------------------------------------------------*/
//void calcThetaCM(reactPar* rxn, MWPCEvt* mwpcEvt,int choice){
void calcThetaCM(reactPar* rxn,MWPCfis* mwpcFis,MWPCEvt* mwpcEvt,int choice){

  float theta[2]={};
  float u[2];
  float w[2];
  float term;
  float vcn=rxn->Vcn;
  float ThetaCM[2]={};

  //initialize variables
  u[0]=1.;
  u[1]=1.;
  w[0]=1.;
  w[1]=1.;

  mwpcEvt[0].thetaCM = 0;//det1
  mwpcEvt[2].thetaCM = 0;//det3

  //for (int i=0;i<2;i++){

    theta[0]=mwpcEvt[choice].theta * d2r;//det1 or 3 --> det1 (stored at theta[0] leaf)
    theta[1]=mwpcEvt[1].theta * d2r;//det2

    // calculate parallel and perpendicular velocity components
    u[0]=mwpcEvt[choice].v*sin( theta[0] ); //VPERP in dagui
    u[1]=mwpcEvt[1].v*sin( theta[1] ); //VPERP in dagui

    w[0]=mwpcEvt[choice].v*cos( theta[0] ); //VPAR in dagui
    w[1]=mwpcEvt[1].v*cos( theta[1] ); //VPAR in dagui

    term = w[0] - vcn;
    ThetaCM[0] = atan( u[0] / term);

    if ((w[0] - vcn)<0){
      ThetaCM[0] += pi;
    }
    ThetaCM[0] = ThetaCM[0]*r2d;

    mwpcEvt[choice].thetaCM = ThetaCM[0]; //det1 or det3


    term = w[1] - vcn;
    ThetaCM[1] = atan( u[1] / term);

    if ((w[1] - vcn)<0){
      ThetaCM[1] += pi;
    }
    ThetaCM[1] = ThetaCM[1]*r2d;

    mwpcEvt[1].thetaCM = ThetaCM[1]; //det2

   //}

    //Calculate a 'vdsine' parameter
    mwpcFis->vdsine = ((mwpcFis->vpar)-(rxn->Vcn))*sin ((mwpcEvt[choice].thetaCM)*d2r);

}//end calcThetaCM

/*------------------------------------------------------------------------------------
                            calcVparVperp
 : Calculates vpar and vperp
 Add vdsin calculation --> Move to calcThetaCM function
 - Yun Jeung, May 2019 update
------------------------------------------------------------------------------------*/
void calcVparVperp(MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, int choice){

  float u[N_MWPC]={};
  float w[N_MWPC]={};

  float v[N_MWPC]={};
  float theta[N_MWPC]={};

  float vpar=0, vperp=0;

  int i;

  // initialize variables
  for (i=0; i<N_MWPC; i++ ){
    u[i]=1.0;
    w[i]=1.0;
    v[i]=0.0;
    theta[i]=0.0;
  }
  vperp=0;
  vpar=0;

  for (i=0;i<N_MWPC;i++){
    //recall variables from event structure; do any unit conversions required
    theta[i] = mwpcEvt[i].theta * d2r;
    v[i] = mwpcEvt[i].v;

    // calculate parallel and perpendicular velocity components
    u[i] = v[i] * sin( theta[i] ); //VPERP in dagui
    w[i] = v[i] * cos( theta[i] ); //VPAR in dagui
  }

  // Use velocity components u, w, to calculate total parallel and perpendicular velocities for the binary fragments.
  // For the DC beam case, vpar should = vcn by assumption
  // Only Det12 conf.
  //vpar = ( (u[0] * w[1]) + (u[1] * w[0]) )/ ( u[0] + u[1] );
  //vperp = u[0]*u[1] * sin( mwpcFis->phi12*d2r ) / sqrt( u[0]*u[0] + u[1]*u[1] - 2*u[0]*u[1]*cos(mwpcFis->phi12*d2r));

  vpar = ( (u[choice] * w[1]) + (u[1] * w[choice]) )/ ( u[choice] + u[1] );
  vperp = u[choice]*u[1] * sin( mwpcFis->phi12*d2r ) / sqrt( u[choice]*u[choice] + u[1]*u[1] - 2*u[choice]*u[1]*cos(mwpcFis->phi12*d2r));

  mwpcFis->vpar=vpar;
  mwpcFis->vperp=vperp;

  //if(mwpcFis->EvtNo < 60000)
  //printf("\nEvent number: %llu, vpar: %f, vperp: %f\n",mwpcFis->EvtNo,vpar,vperp);

}// end calcVparVperp

/*------------------------------------------------------------------------------------
                          calcVfromEM
 : Calculate velocity from energy, mass
------------------------------------------------------------------------------------*/
float calcVfromEM(float E, float m){

  float v = 0;
  float Econst = m_u/(2.0*speed_of_light*speed_of_light); // = 1/2 * unit conversions needed for kinetic energy calculations yielding units of MeV

   v = sqrt(E/(Econst*m));
   return v;
}//end calcVfromEM

/*------------------------------------------------------------------------------------
                          calcEfromMV
 : Calculate energy from velocity, mass
------------------------------------------------------------------------------------*/
float calcEfromMV(float m, float v){

  float E;
  float Econst = m_u/(2.0*speed_of_light*speed_of_light); // = 1/2 * unit conversions needed for kinetic energy calculations yielding units of MeV

  E = Econst * m * v * v; // Units: MeV
  return E;
}//end calcEfromMV

/*------------------------------------------------------------------------------------
                           calcTKE
 : Calculate total kinetic energy of fission fragments
------------------------------------------------------------------------------------*/
void calcTKE(MWPCfis* mwpcFis){

  float TKE=0;

  for (int i=0;i<2;i++){

    TKE = TKE + calcEfromMV(mwpcFis->mass[i],mwpcFis->Vcm[i]);

  }

  mwpcFis->TKE = TKE;

}//end calcTKE

/*------------------------------------------------------------------------------------
                           calcTKE
 : Calculate TKEViola from V. E. Viola et al, PRC 31, 1550 (1985) -- empirical eq
 rxn->TKEViola = 0.1189 * (float)rxn->Zcn * (float)rxn->Zcn / pow( (float)rxn->Acn, 1./3.) + 7.3; //[MeV]
 Eqn above assumes symmetric mass split. We want to go back from this and not assume symm mass split;
 Work backwards to get the equation below (including the funny pow(2,8/3) term). Ignore the 7.3 when you
 do so; you're simply starting from TKE is proportional to Z1*Z2/(A1^1/3 + A2^1/3)
------------------------------------------------------------------------------------*/
void calcTKEViola(MWPCfis* mwpcFis, reactPar* rxn){

  float Z[2]={};

  for (int i=0;i<2;i++){
    //calculate Z (roughly) for each fission fragment
    Z[i] = mwpcFis->mass[i] * (float)rxn->Zcn / (float)rxn->Acn;
  }

  rxn->TKEViola = 0.1189 * Z[0] * Z[1] * pow(2.,8./3.) /(pow(mwpcFis->mass[0],1./3.)+pow(mwpcFis->mass[1],1./3.)) + 7.3;

}//end calcTKEViola

/*------------------------------------------------------------------------------------
                         calcAngleFFTarg
 : Calculate angle between target normal and fission fragment for each event
------------------------------------------------------------------------------------*/
void calcAngleFFTarg(targetPar* targ, MWPCEvt* mwpcEvt, int choice){

    float targV0[2] = {pi - targ->targTheta*d2r,pi}; //theta, phi
    float targV1[2] = {targ->targTheta*d2r,0};

    float evtV0[2] = {mwpcEvt[choice].theta*d2r,mwpcEvt[choice].phi*d2r};
    float evtV1[2] = {mwpcEvt[1].theta*d2r,mwpcEvt[1].phi*d2r};

    //Basically calculating the terms of the dot product (/r1r2)
    float term0 = cos( evtV0[1] ) * sin ( evtV0[0] ) * cos( targV0[1] ) * sin( targV0[0]);
    term0 = term0 + sin( evtV0[1] ) * sin( evtV0[0] ) * sin ( targV0[1] ) * sin( targV0[0] );
    term0 = term0 + cos( evtV0[0] ) * cos ( targV0[0] );

    float term1 = cos( evtV1[1] ) * sin ( evtV1[0] ) * cos( targV1[1] ) * sin( targV1[0]);
    term1 = term1 + sin( evtV1[1] ) * sin( evtV1[0] ) * sin ( targV1[1] ) * sin( targV1[0] );
    term1 = term1 + cos( evtV1[0] ) * cos ( targV1[0] );

    targ->angle[0] = acos(term0);
    targ->angle[1] = acos(term1);

}//end calcAngleFFTarg

/*------------------------------------------------------------------------------------
                          elossVMRcorr
 : Calculate velocities, mass ratios taking into account energy loss corrections

 number of iterations (j) set to 15 and removal of
 energy convergence check to exit loop. These changes were made to address a problem with uneven evaluation of energy
 loss and resulting irregular derived fragment masses (due to some events undertaking more iterations that others).
 This was a problem even with a low convergence check energy of 1 keV (i.e. difference in successive calculated Elab of
 less that 1keV). Simulations with a thick backing material (300 mcg/cm^2 of carbon), suggest that to ensure
 convergence of energy loss calculations, a conservative value of 15 iterations should cover most target and backing
 thicknesses encountered when using the CUBE. Note that this may increase processing time a little.

 Modified - Berriman (2det), Yun Jeung (3det), Jan 2018.
 Modified - Berriman (Jan 2021)

 ------------------------------------------------------------------------------------*/
//void elossVMRcorr(reactPar* rxn, targetPar* targ, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis,int choice){
void elossVMRcorr(reactPar* rxn, targetPar* targ, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, MWPCgeo* mwpcGeo, int choice){	//*Annette*14Jan2021*add_mwpcGeo

  float Elab[2]={};// fission fragment energies
  // float Zproj[2]={};

  // int backZ[2] = {}; Not using
  float targThick[2] = {};
  float supportThick[2] = {};
  float dE[2]={};
  float E;
  float Einit[2]={};
  float term=0;

  bool stopECorr[2]={};
  // float ElabTemp[2]={};//** Berriman ** parameter not used - removed 9 October 2018
  int j=0;
  int i;

  int tmp=0;

  //----------------------------------------------------------
  //declare additional parameters

  float Zav[2]={};  //Charge
  float Zu[2]={};   //rounding UP
  float Zd[2]={};   //rounding DOWN

  float Aav[2]={}; //Charge
  float Au[2]={};  //rounding UP
  float Ad[2]={};  //rouding DOWN
  //Z-A combinations
  float dEuu[2]={}; // Zu-Au
  float dEud[2]={}; // Zu-Ad
  float dEdu[2]={}; // Zd-Au
  float dEdd[2]={}; // Zd-Ad

  //obtain target variables from targ structure
  /*backZ[0]=targ->backZ;
  backZ[1]=targ->frontZ;*/

  targThick[0]=targ->targThick;
  targThick[1]=targ->targThick;

  supportThick[0]=targ->backThick;
  supportThick[1]=targ->frontThick;

  //Calculate angle between target normal and fission fragment for each event
  calcAngleFFTarg(targ,mwpcEvt,choice);

  //Calculate target and backing thicknesses traversed by each fission fragment, assuming interaction occurs at center of target and beam spot is centered.
  for (i=0;i<2;i++){

    if(i==0) tmp = choice;
    if(i==1) tmp = 1;

    term = fabs(cos(targ->angle[i]));
    targThick[i] = 0.5 * targThick[i] / term; // assumes interaction occurs at center of target
    supportThick[i] = supportThick[i] / term; // full backing thickness is traversed by each fission fragment, so no factor of 1/2

  }
  stopECorr[0]=0;
  stopECorr[1]=0;


  float v[2];
  float v_temp=0;
  float dEnErGy=0;
  float dv=0;
  float r_prime;
  float dr;
  float mylar_eff_thiccness;

  // grab the initial velocities
  v[0] = mwpcEvt[choice].v;
  v[1] = mwpcEvt[1].v;

  // now we need to encode the detector distances here
  // so that we can use them to determine the thickness of mylar

  /*    float det_distance;
      if (i == 1) {
        det_distance = 180; // mm
      } else {
        if (choice == 0) {
          det_distance = 195;
        } else {
          det_distance = 180;
        }
      }
  */
      float det_distance;  //*Annette*14Jan2021*add_mwpcGeo
      if (i == 1) {
        det_distance = mwpcGeo[1].detDist; // mm
      } else {
        if (choice == 0) {
          det_distance = mwpcGeo[0].detDist;
        } else {
          det_distance = mwpcGeo[2].detDist;
        }
      }

  // we can now calculate the distance to the mylar and the fraction of distance to the wire
      r_prime = (det_distance - mylar_CF_sep) * mwpcEvt[tmp].r / det_distance;
      dr = mwpcEvt[tmp].r - r_prime;

      mylar_eff_thiccness = mylar_thiccness * mwpcEvt[tmp].r / det_distance;


  while ( stopECorr[0] == 0 || stopECorr[1] == 0 ){

    for (i=0; i<2; i++){

      if(i==0) tmp = choice;
      if(i==1) tmp = 1;


      // Calculate initial energies for each fragment.
      Einit[i] = calcEfromMV(mwpcFis->mass[i], v[i]);
      E = 0;
      dE[i] = 0;
      v_temp=0;
      dEnErGy=0;
      Elab[i]=0;

	//cout<<"-1- j="<<j<<", i="<<i<<", Einit="<<Einit[i]<<", E="<<E<<", dE[i]="<<dE[i]<<endl;

      if(i==0) tmp = choice;
      if(i==1) tmp = 1;

      // initialise new variables
      dEuu[2]=0.;
      dEud[2]=0.;
      dEdu[2]=0.;
      dEdd[2]=0.;

      //----- Define rounding up/down mass and charge -----------------
      Aav[i] = mwpcFis->mass[i];
      Au[i] = ceil (mwpcFis->mass[i]);
      Ad[i] = floor (mwpcFis ->mass[i]);

      Zav[i] = Aav[i]*(float)rxn->Zcn / (float)rxn->Acn;
      Zu[i] = ceil(mwpcFis->mass[i]*(float)rxn->Zcn / (float)rxn->Acn);
      Zd[i] = floor(mwpcFis->mass[i]*(float)rxn->Zcn / (float)rxn->Acn);

      // we need to add the mylar energy loss here before any of the 'backings'
      // or target energy losses are included...
      dEuu[i] = mylar_eff_thiccness * getEloss(Zu[i], Au[i], Einit[i], mylar_table);
      dEud[i] = mylar_eff_thiccness * getEloss(Zu[i], Ad[i], Einit[i], mylar_table);
      dEdu[i] = mylar_eff_thiccness * getEloss(Zd[i], Au[i], Einit[i], mylar_table);
      dEdd[i] = mylar_eff_thiccness * getEloss(Zd[i], Ad[i], Einit[i], mylar_table);


      // now calculate the interpolated energy loss
      //float dEnErGy;
      dEnErGy = dEuu[i]*(Zav[i]-Zd[i])*(Aav[i]-Ad[i]) +
            dEud[i]*(Zav[i]-Zd[i])*(Au[i]-Aav[i]) +
            dEdu[i]*(Zu[i]-Zav[i])*(Aav[i]-Ad[i]) +
            dEdd[i]*(Zu[i]-Zav[i])*(Au[i]-Aav[i]);

      // make sure the energy loss through the mylar is not larger than the actual energy
      // of the fragment, these events are rubbish so we can just ignore them
      if (Einit[i] < dEnErGy) {
        continue;
      }

      // calculate the velocity difference
      //float dv = v[i] - calcVfromEM(Einit[i] - dEnErGy, mwpcFis->mass[i]);
      dv = v[i] - calcVfromEM(Einit[i] - dEnErGy, mwpcFis->mass[i]);

      // and now the real velocity (Documentation on this equation will be provided in the future)
      //float v_temp = (mwpcEvt[tmp].r * (dv + v[i]) + sqrt(mwpcEvt[tmp].r * (r_prime * pow(dv - v[i], 2) + dr * pow(dv + v[i], 2)))) / (2 * mwpcEvt[tmp].r);    //**3Dec2020**removed**

	v_temp = v[i] + (dr*dv)/mwpcEvt[tmp].r;	//**3Dec2020**added**


      // and update the initial guess

      //Einit[i] = calcEfromMV(mwpcFis->mass[i], v_temp); //**1Dec2020**removed**
      //E = Einit[i]; 					  //**1Dec2020**removed**
      //dE[i] = 0.0; 					  //**1Dec2020**removed**


      E = calcEfromMV(mwpcFis->mass[i], v_temp); //**1Dec2020**added**
      dE[i] = E-Einit[i]; 			 //**1Dec2020**added**

	//cout<<"-2- j="<<j<<", i="<<i<<", Einit="<<Einit[i]<<", E="<<E<<", dE[i]="<<dE[i]<<endl;


      //----- Define rounding up/down mass and charge -----------------

      //--- Backing correction --------
      if (supportThick[i]>0){
        //dE[i]=eloss(Zproj[i],round(mwpcFis->mass[i]),backZ[i],E,supportThick[i]);
        if (i == 0) {
          dEuu[i]=supportThick[i]*getEloss(Zu[i],Au[i],E,back_table);
          dEud[i]=supportThick[i]*getEloss(Zu[i],Ad[i],E,back_table);
          dEdu[i]=supportThick[i]*getEloss(Zd[i],Au[i],E,back_table);
          dEdd[i]=supportThick[i]*getEloss(Zd[i],Ad[i],E,back_table);
        } else {
          dEuu[i]=supportThick[i]*getEloss(Zu[i],Au[i],E,front_table);
          dEud[i]=supportThick[i]*getEloss(Zu[i],Ad[i],E,front_table);
          dEdu[i]=supportThick[i]*getEloss(Zd[i],Au[i],E,front_table);
          dEdd[i]=supportThick[i]*getEloss(Zd[i],Ad[i],E,front_table);
        }

        // weighted dE
        dE[i] += dEuu[i]*(Zav[i]-Zd[i])*(Aav[i]-Ad[i]) +
          dEud[i]*(Zav[i]-Zd[i])*(Au[i]-Aav[i]) +
          dEdu[i]*(Zu[i]-Zav[i])*(Aav[i]-Ad[i]) +
          dEdd[i]*(Zu[i]-Zav[i])*(Au[i]-Aav[i]);

        //E = E + dE[i]; // calcuate corrected energy
        E = Einit[i] + dE[i]; // calcuate corrected energy Annette 9 Feb2021


	/*cout<<"-3-eloss in support="<<dEuu[i]*(Zav[i]-Zd[i])*(Aav[i]-Ad[i]) +
          dEud[i]*(Zav[i]-Zd[i])*(Au[i]-Aav[i]) +
          dEdu[i]*(Zu[i]-Zav[i])*(Aav[i]-Ad[i]) +
          dEdd[i]*(Zu[i]-Zav[i])*(Au[i]-Aav[i])<<endl;
	cout<<"-3- j="<<j<<", i="<<i<<", Einit="<<Einit[i]<<", E="<<E<<", dE[i]="<<dE[i]<<endl;*/

      }

      //--- Target correction ---------
      //dE[i]+=eloss(Zproj[i],round(mwpcFis->mass[i]),targZ,E,targThick[i]);
      dEuu[i]=targThick[i]*getEloss(Zu[i],Au[i],E,targ_table);
      dEud[i]=targThick[i]*getEloss(Zu[i],Ad[i],E,targ_table);
      dEdu[i]=targThick[i]*getEloss(Zd[i],Au[i],E,targ_table);
      dEdd[i]=targThick[i]*getEloss(Zd[i],Ad[i],E,targ_table);

      // weighted dE
      dE[i] += dEuu[i]*(Zav[i]-Zd[i])*(Aav[i]-Ad[i]) +
        dEud[i]*(Zav[i]-Zd[i])*(Au[i]-Aav[i]) +
        dEdu[i]*(Zu[i]-Zav[i])*(Aav[i]-Ad[i]) +
        dEdd[i]*(Zu[i]-Zav[i])*(Au[i]-Aav[i]);

      //E = E + dE[i];
      Elab[i] = Einit[i]+ dE[i];// corrected fission fragment energies.


	/*cout<<"-4-eloss in target="<<dEuu[i]*(Zav[i]-Zd[i])*(Aav[i]-Ad[i]) +
        dEud[i]*(Zav[i]-Zd[i])*(Au[i]-Aav[i]) +
        dEdu[i]*(Zu[i]-Zav[i])*(Aav[i]-Ad[i]) +
        dEdd[i]*(Zu[i]-Zav[i])*(Au[i]-Aav[i])<<endl;
	cout<<"-4- j="<<j<<", i="<<i<<", Einit="<<Einit[i]<<", E="<<E<<", dE[i]="<<dE[i]<<endl;
	cout<<"-5- Elab="<<Elab[i]<<endl; */

      //E 4: Recalculate lab and center of mass velocities of fragments, taking into account energy loss corrections
      mwpcEvt[tmp].v=calcVfromEM(Elab[i], mwpcFis->mass[i]);
      calcVcm(rxn, mwpcEvt, mwpcFis,i,choice); //Vcm saved in this function
      /* //No longer using this.
      //E 5: Check if energy loss calculation is complete (yes if change in Elab < 10 keV)
      if (fabs(Elab[i]-ElabTemp[i])<0.010){
      stopECorr[i]=1;

      //E 6: Store Elab for next round
      ElabTemp[i]=Elab[i];
      }*/

    }// end loop over fragments

    //recalculate fission fragment masses, mr
    calcMasses(rxn, mwpcFis);


    //---------Here depending on Tsharpeing ON and OFF--------------

    // Recalculate energy loss corrected parallel/perpendicular velocities and scattering angles for NO time sharpening
    // (for time sharpening case, calculation is done in elossVMRcorr)
    if (rxn->tsharpBool==0 && rxn->beamType==1){
      // calcVparVperp(mwpcEvt, mwpcFis,choice);
    }

    //Do time sharpening
    if (rxn->tsharpBool==1){
      if (j==0){
         calcVparVperp(mwpcEvt, mwpcFis,choice);
        //by calculating vpar, vperp here, we will save the
        //original eloss-corrected vpar, vperp from first iteration. vpar, vperp won't be recalculated
        //from here on out. This allows us to look at the original vpar, vperp histogram if we like; if
        //we try to do this AFTER time sharpening is applied, the spectrum is a band at vpar=vcn
        //(which is what we assume to do time sharpening in the first place).
      }
      //do time sharpening calculation with energy loss corrections
      calcTimeSharp(rxn,mwpcEvt,mwpcFis,choice);
    }//end time sharpening loop

    // Keep track of iterations; limit number of iterations to 10-->15
    // if (j++ == 14){ //9 --> 14** Berriman ** number of iterations set to 15 - 9 October 2018

    if (++j == rxn->Eloop ){ //src_v5.0
      stopECorr[0] = 1;
      stopECorr[1] = 1;
    }

  }//end for 'while' loop


  // Save eloss-related variables to the relevant structure
  // 3det configuration
  mwpcEvt[choice].Elab = Elab[0];
  mwpcEvt[choice].Eloss = dE[0];
  mwpcEvt[choice].Emratio = Elab[0]/mwpcFis->mass[0];

  mwpcEvt[1].Elab = Elab[1];
  mwpcEvt[1].Eloss = dE[1];
  mwpcEvt[1].Emratio = Elab[1]/mwpcFis->mass[1];

  // calculate what travel times would be if there was no loss of energy
  mwpcEvt[choice].t=mwpcEvt[choice].r/mwpcEvt[choice].v;
  mwpcEvt[1].t=mwpcEvt[1].r/mwpcEvt[1].v;

}//end elossVMRcorr

/*------------------------------------------------------------------------------------
                           calcMrVelThetaCM
 : Calculate vcm and MR
 Velocity reconstruction follows PRC 53 (1996) 1290 - Appendix on Kinematic Coincidence Method
 Assumes binary fission
  Term change rxn->elossBool --> rxn->dEcalcType
 - Yun Jeung, May 2019 update
------------------------------------------------------------------------------------*/

//void calcMrVelThetaCM(reactPar* rxn, targetPar* targ, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, int choice){

void calcMrVelThetaCM(reactPar* rxn, targetPar* targ, MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, MWPCgeo* mwpcGeo, int choice){  //*Annette*14Jan2021*with_mwpcGeo

  int i=0;

  //carry out and do conversions for folding angle and difference calcs
  calcFold(mwpcEvt, mwpcFis,choice);

  //Calculate velocities in the lab frame for AC beam case
  calcVelAC(mwpcEvt);

  // Calculate center of mass velocities for ALL BEAM TYPES
  for (i=0;i<2;i++){
    calcVcm(rxn,mwpcEvt,mwpcFis,i,choice);
  }

  // For AC beam ONLY, use center of mass velocities to calculate mass ratios
  if (rxn->beamType==1){
    calcMasses(rxn,mwpcFis);
  }

  // If time sharpening is enabled and energy loss is NOT enabled, recalculate velocities and masses with time sharpening
  // if (rxn->tsharpBool==1 && rxn->elossBool==0){
  if (rxn->tsharpBool==1 && rxn->dEcalcType==0){
    calcTimeSharp(rxn,mwpcEvt,mwpcFis,choice);
  }

  // ENERGY LOSS CALCULATIONS
  // if (rxn->elossBool==1){
  if (rxn->dEcalcType==1 || rxn->dEcalcType==2 || rxn->dEcalcType==3 ){
    //elossVMRcorr(rxn,targ,mwpcEvt,mwpcFis,choice); // Inside eloss VMRcorr, vpar and vper will be recalculated
    elossVMRcorr(rxn,targ,mwpcEvt,mwpcFis,mwpcGeo, choice); //*Annette*14Jan2021*add_mwpcGeo
  }


  // Calculate parallel and perpendicular velocity vectors
  calcVparVperp(mwpcEvt, mwpcFis,choice);
  //-----------------------------

  // Calculate scattering angles
  calcThetaCM(rxn,mwpcFis,mwpcEvt,choice);//Test

  //calculate TKE and TKEViola for fission fragment pair
  calcTKE(mwpcFis);
  calcTKEViola(mwpcFis,rxn);

  //calculate a few simple derived parameters
  mwpcFis->VcmSum = mwpcFis->Vcm[0]+mwpcFis->Vcm[1];
  mwpcFis->dESum = mwpcEvt[choice].e_raw + mwpcEvt[1].e_raw;  //sum of CF energy loss for both events

}//end calcMrVelThetaCM


/*-----------------------------------------------------------------------------------
                            Tdiffcalc
 : A new function introduced in src_v5.0
 : Calculates velocities, masses, mass ratios and energies for ALL TIME DIFFERENCE METHODS
 Includes energy loss in target and supporting layers
 uses calculated velocity sum, K=v1+v2
 - Annet Berriman, Mar 2019
-----------------------------------------------------------------------------------*/
//void Tdiffcalc(MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, reactPar* rxn, targetPar* targ, int choice){
void Tdiffcalc(MWPCEvt* mwpcEvt, MWPCfis* mwpcFis, MWPCgeo* mwpcGeo, reactPar* rxn, targetPar* targ, int choice){ //*Annette*14Jan2021*add_mwpcGeo

  //carry out and do conversions for folding angle and difference calcs

  calcFold(mwpcEvt, mwpcFis,choice);

  calcVelTdiff(mwpcEvt,mwpcFis,rxn,choice);

  //if (rxn->elossBool==1){
  if (rxn->dEcalcType==1 || rxn->dEcalcType==2 || rxn->dEcalcType==3 ){
     // calculate energy loss
     //elossVMRcorr(rxn,targ,mwpcEvt,mwpcFis,choice);
     elossVMRcorr(rxn,targ,mwpcEvt,mwpcFis,mwpcGeo, choice); //*Annette*14Jan2021*add_mwpcGeo

     // recalculate times after energy loss
     //mwpcEvt[0].t=mwpcEvt[0].r/mwpcEvt[0].v;  // now in eloss function
     //mwpcEvt[1].t=mwpcEvt[1].r/mwpcEvt[1].v;
     //mwpcEvt[2].t=mwpcEvt[2].r/mwpcEvt[2].v;

     // rxn->tdiff =mwpcEvt[choice].t-mwpcEvt[1].t;
     //rxn->tdiff12 =mwpcEvt[1].t-mwpcEvt[0].t; //T2-T1
     //rxn->tdiff32 =mwpcEvt[2].t-mwpcEvt[1].t; //T2-T3
    // calculate final time difference
    if (rxn->beamType == 2 || rxn->beamType == 3){
      rxn->tdiff=mwpcEvt[1].t-mwpcEvt[choice].t; //T2-T1
      if(choice==0){rxn->tdiff12 =mwpcEvt[1].t-mwpcEvt[0].t;}//T2-T1
      else{rxn->tdiff32 =mwpcEvt[1].t-mwpcEvt[2].t;}//T3-T2
    }
    if(rxn->beamType == 4){
      rxn->tdiff=mwpcEvt[choice].t-mwpcEvt[1].t; //T1-T2
      if(choice==0){rxn->tdiff12 =mwpcEvt[0].t-mwpcEvt[1].t;}//T2-T1
      else{rxn->tdiff32 =mwpcEvt[2].t-mwpcEvt[1].t;}//T3-T2
    }
     //re-calculate velocities, masses, mass ratios and energies from new tdiff
     calcVelTdiff(mwpcEvt,mwpcFis,rxn,choice);
   }

  calcVparVperp(mwpcEvt, mwpcFis,choice);//calculate parallel/perpendicular velocities

  calcThetaCM(rxn,mwpcFis,mwpcEvt,choice); // Calculate scattering angles

  //calculate TKE and TKEViola for fission fragment pair
  calcTKE(mwpcFis);
  calcTKEViola(mwpcFis,rxn);

  //calculate a few simple derived parameters
  mwpcFis->VcmSum = mwpcFis->Vcm[0]+mwpcFis->Vcm[1];
  mwpcFis->dESum = mwpcEvt[0].e_raw + mwpcEvt[1].e_raw;  //sum of CF energy loss for both events

}//end Tdiffcalc


/*------------------------------------------------------------------------------------
                           calcTKEfromFoldMR
 : Function is for Ni+Ni data; calculating TKE from folding angle and MR
------------------------------------------------------------------------------------*/
void calcTKEfromFoldMR(MWPCfis *mwpcFis, MWPCEvt* mwpcEvt, reactPar* rxn, int choice){

  float vcn=rxn->Vcn;
  float Acn=rxn->Acn;
  float theta12 = mwpcFis->theta12 * d2r;
  float theta1=mwpcEvt[choice].theta * d2r;
  float theta2=mwpcEvt[1].theta * d2r;
  float m1=mwpcFis->mass[0];
  float m2=mwpcFis->mass[1];
  float E1=0;
  float E2=0;
  float v1=0;
  float v2=0;
  float v1cm=0;
  float v2cm=0;

  mwpcFis->TKE_fold=0;
  mwpcFis->vCMfold[0]=0;
  mwpcFis->vCMfold[1]=0;

  v1= (Acn*vcn/m1) * (sin(theta2)/sin(theta12));
  v2= (Acn*vcn/m2) * (sin(theta1)/sin(theta12));

  v1cm=sqrt( v1*v1 + vcn*vcn - 2*v1*vcn*cos(theta1) );
  v2cm=sqrt( v2*v2 + vcn*vcn - 2*v2*vcn*cos(theta2) );

  mwpcFis->vCMfold[0]=v1cm;
  mwpcFis->vCMfold[1]=v2cm;

  E1=calcEfromMV(m1, v1cm);
  E2=calcEfromMV(m2, v2cm);

  mwpcFis->TKE_fold=E1+E2;

}//end calcTKEfromFoldMR

/*------------------------------------------------------------------------------------
                           calcExFis
 : Written for Ni+Ni data. Calculates excitation energy of fission fragments.
------------------------------------------------------------------------------------*/
void calcExFis(reactPar *rxn,MWPCfis *mwpcFis, MassTable* m){

  float ExCN=rxn->ExCN;
  float TKEfis=mwpcFis->TKE_fold;
  int A1,Z1,A2,Z2;
  float dA1, dZ1, dA2, dZ2;
  float Zcn=(float)rxn->Zcn;
  float Acn=(float)rxn->Acn;
  float Qfis=0.0;

  A1=A2=Z1=Z2=0;
  dA1=dA2=dZ1=dZ2=0.;

  mwpcFis->ExFis = 0;
  mwpcFis->Qfis = 0;

  float Zfactor = Zcn/Acn;

  dA1=mwpcFis->mass[0];
  dZ1=mwpcFis->mass[0]*Zfactor;
  dA2=mwpcFis->mass[1];
  dZ2=mwpcFis->mass[1]*Zfactor;

  //purposely cast to integer to get floor of value
  A1=(int)dA1;
  Z1=(int)dZ1;
  A2=(int)dA2;
  Z2=(int)dZ2;

  int tot=0;

  for (int i=A1; i<(A1+2);i++){
    for (int j=Z1; j<(Z1+2); j++){
      for (int ii=A2;ii<(A2+2);ii++){
	for (int jj=Z2; jj<(Z2+2); jj++){

	  Qfis +=  m->calcQval(0,0,rxn->Acn,rxn->Zcn,i,j,ii,jj);
	  tot++;

	}
      }
    }
  }

  Qfis=Qfis/tot;
  /*just do an average over all neighboring possibilities...
    This seems to be good enough for Ni+Ni; no longer have blocky structure in fission fragment excitation energy calculations.*/

  mwpcFis->Qfis = Qfis;
  mwpcFis->ExFis = Qfis + ExCN - TKEfis;

}//end calcExFis
