/* CubeGate.cxx: Handles gating for daCube sort program. User should define all gates in

   Users should not have to modify CubeGate.cxx itself.

Dependencies: my_code.h

Liz Williams Jan 2014
 */


#include <iostream>
#include <fstream>

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TEventList.h"
#include "TCutG.h"
#include "TStyle.h"
#include "TH1I.h"

#include "CubeSort.h"
#include "CubeGate.h"
#include "CubeStruct.h"

using namespace std;

//Insert user-defined gate code here
#define USER_GATE
#include "my_code.h"
#undef USER_GATE

void fillGates(){

#define USER_GATEFUNC
#include "my_code.h"
#undef USER_GATEFUNC

}
/*------------------------------------------------------------------------------------
  ApplyGate
  : Apply gates to each event
  - Yun Jeung, July 2018 update
  -------------------------------------------------------------------------------------*/
int ApplyGate(char* inputfile){

    TFile *f1;
    TTree *tree;
    TTree *rtree;//Y

    ifstream input;
    //TString gates;
    char *procfile;
    char *gatefile;
    string dum;
    string junk;

    //define and initialize input structures (see CubeStruct.h)
    sortInfo sortStruct={"","","",0,0,0,0,0,0,0,0,"","",""};
    //not used but assumed in readInput, so I've defined structures again here

    reactPar rxn={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//src_v5.0
    targetPar targ={0,0,0,0,0,0,0,{0,0}};

    // read in input file information and store in appropriate place
    int errCheck=readInput(inputfile,&sortStruct,&rxn,&targ);

    if ( errCheck == -1 ){
        cout << "Input file read-in error. Sort failed." << endl;
        return -1;
    }

    string proc;
    proc=sortStruct.procfile;
    procfile=&proc[0];
    string gatf;
    gatf=sortStruct.gatefile;
    gatefile=&gatf[0];
    dum=sortStruct.gates;
    TString gates(&dum[0]);

    fillGates();//defined in GateDef.C


    //get sorted tree
    f1 = new TFile(procfile,"read");

    if (!f1->IsOpen()){
        cout << "Error: CubeGate: Sort file " << procfile << " not found."<< endl;
        return -1;
    }

    tree = (TTree*)f1->Get("CubeTreeNew");
    rtree = (TTree*)f1->Get("RawTreeNew");//Y

    cout << "Number of entries in original tree: " << tree->GetEntries() << endl;
    cout << "Gates to be applied: " << gates << endl;

    TFile *ef=new TFile(gatefile,"recreate");

    TTree *smalltree=tree->CloneTree(0);
    smalltree->SetName("GatedTree");

    TTree *smallrawtree=rtree->CloneTree(0);//Y
    smallrawtree->SetName("GatedRawTree");//Y


    TEventList *evlist=new TEventList("gatelist");

    tree->Draw(">>gatelist",gates);
    //rtree->Draw(">>gatelist",gates);//Y

    for (int i=0;i<evlist->GetN();i++){

        tree->GetEntry(evlist->GetEntry(i));
        rtree->GetEntry(evlist->GetEntry(i));

        smalltree->Fill();
        smallrawtree->Fill();//Y
        //print statement to keep people occupied while gating continues
        if ( i%1000 == 0 ){
            cout << "Total events: " << evlist->GetN() << "  Sorted events: " << i+1 << '\r';
        }
        if (i==(evlist->GetN()-1)){
            cout << "Total events: " << evlist->GetN() << "  Sorted events: " << i+1 << endl;
        }
    }

    //For raw signal parameters

    smalltree->Write("",TObject::kOverwrite); //->GatedTree
    smallrawtree->Write("",TObject::kOverwrite); //GatedRawTree
    evlist->Write("",TObject::kOverwrite);
    cout << "Number of entries in returned tree: " << smalltree->GetEntries() << endl;


    delete tree;
    delete rtree;
    delete smalltree;
    delete smallrawtree;
    //delete final;
    delete evlist;

    delete f1;
    delete ef;


    return 0;
}
