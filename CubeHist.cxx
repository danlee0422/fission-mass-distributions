/*

   CubeHist.cxx : Histogram generator for daCube. Dependencies: root libraries, CubeHist.h, UserHist.h

   Users should add code defining any additional histograms to UserHist.h
 */


#include "CubeHist.h"


using namespace std;

extern int UngatedCubeHist(char* filename,char* outfile);

/*------------------------------------------------------------------------------------
  CreateHist
  : Creates histogrames. Run options are -h, -hg
  Here we plots various histograms. To reduce your srotig time, you can only build interested
  histograms by commenting out uninterested histograms in this script or modify CubeHistEss.cxx.
  - Yun Jeung, Apr 2019 update
  -------------------------------------------------------------------------------------*/
int CreateHist(char* inputfile, bool gatebool){

    TFile *sfile;
    TTree *tree;
    TTree *rtree;

    char *procfile;
    char *gatefile;
    string junk, sortf, finf;

    //define and initialize input structures (see CubeStruct.h)
    sortInfo sortStruct={"","","",0,0,0,0,0,0,0,0,"","",""};
    //not used but assumed in readInput, so I've defined structures again here
    reactPar rxn={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//src_3det_v5 +2
    targetPar targ={0,0,0,0,0,0,0,{0,0}};

    // read in input file information and store in appropriate place
    int errCheck=readInput(inputfile,&sortStruct,&rxn,&targ);

    float slope = 2*(90-rxn.intercept)*1.0;
    int acn = rxn.Ap + rxn.At;
    //int zcn = rxn.Zp + rxn.Zt;
    //ratio = rxn.TestPar;
    int angbin = 90; // 60 (3deg) 90 (2deg.)

    cout<< ""<<endl;
    cout << "-------------------MAD Mirror Fuction--------------------------------" << endl;
    cout << " Intercept = " << rxn.intercept << " deg.\t\t" << "slope = " << slope  << endl;
    cout << "------------------------------------------------------------------------" << endl;

    if ( errCheck == -1 ){
        cout << "Input file read-in error. Sort failed." << endl;
        return -1;
    }

    // if (rxn.elossBool==true){
    if (rxn.dEcalcType==1 || rxn.dEcalcType==2 || rxn.dEcalcType==3 ){
        calcElossBeam(&targ,&rxn);
    }

    string proc;
    proc=sortStruct.procfile;
    procfile=&proc[0];
    string gatf;
    gatf=sortStruct.gatefile;
    gatefile=&gatf[0];

    if (gatebool){
        UngatedCubeHist(procfile,gatefile); // make and save ungated monitor histograms to final gated tree

        //sfile=new TFile(procfile,"UPDATE");
        sfile=TFile::Open(gatefile,"UPDATE");
        if (!sfile->IsOpen()){
            cout << "CubeHist: Root file " << gatefile << " not found!" << endl;
            return -1;
        }
        if (sfile->GetListOfKeys()->Contains("GatedTree")==true){
            tree = (TTree*) sfile->Get("GatedTree");
            rtree = (TTree*) sfile->Get("GatedRawTree");
        }
        else{
            cout << "Make sure your root file name is correct. GatedTree not found in"<< endl;
            cout << gatefile << endl;
            return -1;
        }

    }
    else
    {
        //sfile=new TFile(procfile,"UPDATE");
        sfile=TFile::Open(procfile,"UPDATE");
        if (!sfile->IsOpen()){
            cout << "CubeHist: Root file " << procfile << " not found!" << endl;
            return -1;
        }
        if (sfile->GetListOfKeys()->Contains("CubeTreeNew")==true){
            tree = (TTree*) sfile->Get("CubeTreeNew");
            rtree = (TTree*) sfile->Get("RawTreeNew");
        }
        else{
            cout << "Make sure your processed, ungated root file name is correct. CubeTreeNew not found in"<< endl;
            cout << procfile << endl;
            return -1;
        }
    }

    //--------------------------------------------------

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("UHist")!=true){
        sfile->mkdir("UHist");
    }
    sfile->cd("UHist");

#define USER_HIST
#include "my_code.h"
#undef USER_HIST

    /*
    // ##### CUBE Position Histograms #####

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("CUBE_pos")!=true){
    sfile->mkdir("CUBE_pos");
    }
    sfile->cd("CUBE_pos");

    {

    TH2I XBACK_det("XBACK_det","XBACK_det",4000,0,4000,8000,-4000,4000);
    XBACK_det.SetOption("COLZ");
    XBACK_det.SetMinimum(1);
    rtree->Draw("X1B-rs_T1:X1F-rs_T1 >> XBACK_det", "","");
    XBACK_det.SetTitle("X1B-rs_T1 vs X1F-rs_T1 [Counts]; X1B-rs_T1 [ch];X1F-rs_T1 [ch]");
    XBACK_det.Write("",TObject::kOverwrite);

    TH2I YBACK_det("YBACK_det","YBACK_det",4000,-4000,5000,8000,-4000,6000);
    YBACK_det.SetOption("COLZ");
    YBACK_det.SetMinimum(1);
    rtree->Draw("Y1B-rs_T1:Y1T-rs_T1 >> YBACK_det","","");
    YBACK_det.SetTitle("Y1B-rs_T1 vs Y1T-rs_T1 [Counts]; Y1B-rs_T1 [ch];Y1T-rs_T1 [ch]");
    YBACK_det.Write("",TObject::kOverwrite);

    TH2I XFRONT_det("XFRONT_det","XFRONT_det",4000,-4000,4000,8000,-4000,4000);
    XFRONT_det.SetOption("COLZ");
    XFRONT_det.SetMinimum(1);
    rtree->Draw("X2B-rs_T2:X2F-rs_T2 >> XFRONT_det","","");
    XFRONT_det.SetTitle("X2B-rs_T2 vs X2F-rs_T2 [Counts]; X2B-rs_T2 [ch];X2R-_rs_T2 [ch]");
    XFRONT_det.Write("",TObject::kOverwrite);

    TH2I YFRONT_det("YFRONT_det","YFRONT_det",4000,-4000,5000,8000,-4000,6000);
    YFRONT_det.SetOption("COLZ");
    YFRONT_det.SetMinimum(1);
    rtree->Draw("Y2B-rs_T2:Y2T-rs_T2 >> YFRONT_det","","");
    YFRONT_det.SetTitle("Y2B-rs_T2 vs Y2T-rs_T2 [Counts]; Y2B-rs_T2 [ch];Y2T-rs_T2 [ch]");
    YFRONT_det.Write("",TObject::kOverwrite);

    }

    std::cout << "Produced histograms of raw CUBE postion data..." << std::endl;
     */

    // ##### raw histograms #####

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("Raw data")!=true){
        sfile->mkdir("Raw data");
    }
    sfile->cd("Raw data");

    //1D raw
    {
        TH1I x1_raw("x1_raw", "x1_raw", 4000, -4000, 4000);
        tree->Draw("raw_x1>>x1_raw", "", "");
        x1_raw.SetTitle("raw_x1;x1[ch];Counts");
        x1_raw.Write("", TObject::kOverwrite);

        TH1I y1_raw("y1_raw", "y1_raw", 5000, -5000, 5000);
        tree->Draw("raw_y1>>y1_raw","","");
        y1_raw.SetTitle("raw_y1;y1[ch];Counts");
        y1_raw.Write("",TObject::kOverwrite);

        TH1I x2_raw("x2_raw", "x2_raw", 3000,-3000, 3000);
        tree->Draw("raw_x2>>x2_raw", "", "");
        x2_raw.SetTitle("raw_x2;x2[ch];Counts");
        x2_raw.Write("", TObject::kOverwrite);

        TH1I y2_raw("y2_raw","y2_raw",4000,-4000,4000);
        tree->Draw("raw_y2>>y2_raw","","");
        y2_raw.SetTitle("raw_y2;y2[ch];Counts");
        y2_raw.Write("",TObject::kOverwrite);

        TH1I x3_raw("x3_raw", "x2_raw", 3000,-3000, 1000);
        tree->Draw("raw_x3>>x3_raw", "", "");
        x3_raw.SetTitle("raw_x3;x3[ch];Counts");
        x3_raw.Write("", TObject::kOverwrite);

        TH1I y3_raw("y3_raw","y3_raw",4000,-4000,4000);
        tree->Draw("raw_y3>>y3_raw","","");
        y3_raw.SetTitle("raw_y3;y3[ch];Counts");
        y3_raw.Write("",TObject::kOverwrite);

        TH1I T1_raw("T1_raw","T1_raw",1024,0,2048);
        tree->Draw("raw_T1>>T1_raw","","");
        T1_raw.SetTitle("raw_T1;T1[ch];Counts");
        T1_raw.Write("",TObject::kOverwrite);

        TH1I T2_raw("T2_raw","T2_raw",1024,0,2048);
        tree->Draw("raw_T2>>T2_raw","","");
        T2_raw.SetTitle("raw_T2;T2[ch];Counts");
        T2_raw.Write("",TObject::kOverwrite);

        TH1I E1_raw("E1_raw","E1_raw",1024,0,2048);
        tree->Draw("raw_E1>>E1_raw","","");
        E1_raw.SetTitle("raw_E1;E1[ch];Counts");
        E1_raw.Write("",TObject::kOverwrite);

        TH1I E2_raw("E2_raw","E2_raw",1024,0,2048);
        tree->Draw("raw_E2>>E2_raw","","");
        E2_raw.SetTitle("raw_E2;E2[ch];Counts");
        E2_raw.Write("",TObject::kOverwrite);

        //monitor 1 energy
        TH1I mon1E("mon1E","mon1E",1024,0,1024);
        tree->Draw("Mon1E>>mon1E","","");
        mon1E.SetTitle("Monitor 1 Energy;Channels;Counts");
        mon1E.Write("",TObject::kOverwrite);

        //monitor 2 energy
        TH1I mon2E("mon2E","mon2E",1024,0,1024);
        tree->Draw("Mon2E>>mon2E","","");
        mon2E.SetTitle("Monitor 2 Energy;Channels;Counts");
        mon2E.Write("",TObject::kOverwrite);
        //2016Linac
        TH1I monTAC("monTAC","monTAC",1024,0,1024);
        rtree->Draw("MonTAC>>monTAC","","");
        monTAC.SetTitle("Monitor Tac;Channels;Counts");
        monTAC.Write("",TObject::kOverwrite);

        TH1I t1t2TAC("t1t2TAC","t1t2TAC",1024,0,1024);
        rtree->Draw("T1T2TAC>>t1t2TAC","","");
        t1t2TAC.SetTitle("T1T2 Tac;Channels;Counts");
        t1t2TAC.Write("",TObject::kOverwrite);

    }//1D raw

    //2D raw
    {

        TH2I x1_y1raw("x1_y1raw","x1_y1raw",2500,-5000,5000,2500,-5000,5000);
        x1_y1raw.SetOption("COLZ");
        x1_y1raw.SetMinimum(1);
        tree->Draw("raw_y1:raw_x1>>x1_y1raw",""," ");
        x1_y1raw.SetTitle("raw_x1 vs raw_y1 [Counts]; raw_x1 [ch];raw_y1 [ch]");
        x1_y1raw.Write("",TObject::kOverwrite);

        //TH2I x2_y2raw("x2_y2raw","x2_y2raw",1024,0,2048,1024,0,2048);
        TH2I x2_y2raw("x2_y2raw","x2_y2raw",2500,-5000,5000,2500,-5000,5000);
        x2_y2raw.SetOption("COLZ");
        x2_y2raw.SetMinimum(1);
        tree->Draw("raw_y2:raw_x2>>x2_y2raw",""," ");
        x2_y2raw.SetTitle("raw_x2 vs raw_y2 [Counts]; raw_x2 [ch];raw_y2 [ch]");
        x2_y2raw.Write("",TObject::kOverwrite);

        TH2I x3_y3raw("x3_y3raw","x3_y3raw",2500,-5000,5000,2500,-5000,5000);
        x3_y3raw.SetOption("COLZ");
        x3_y3raw.SetMinimum(1);
        tree->Draw("raw_y3:raw_x3>>x3_y3raw",""," ");
        x3_y3raw.SetTitle("raw_x3 vs raw_y3 [Counts]; raw_x3 [ch];raw_y3 [ch]");
        x3_y3raw.Write("",TObject::kOverwrite);


        TH2I T1_T2raw("T1_T2raw","T1_T2raw",2000,0,4000,1600,-500,1500);
        T1_T2raw.SetOption("COLZ");
        T1_T2raw.SetMinimum(1);
        tree->Draw("raw_T2:raw_T1>>T1_T2raw",""," ");
        T1_T2raw.SetTitle("raw_T1 vs raw_T2 [Counts];raw_T1 [ch];raw_T2 [ch]");
        T1_T2raw.Write("",TObject::kOverwrite);

        TH2I E1_E2raw("E1_E2raw","E1_E2raw",500,0,500,500,0,600);
        E1_E2raw.SetOption("COLZ");
        E1_E2raw.SetMinimum(1);
        tree->Draw("raw_E2:raw_E1>>E1_E2raw",""," ");
        E1_E2raw.SetTitle("raw_E1 vs raw_E2 [Counts];raw_E1 [ch];raw_E2 [ch]");
        E1_E2raw.Write("",TObject::kOverwrite);

        //SMALL BACK DETECTOR& FRONT
        TH2I T3_T2raw("T3_T2raw","T3_T2raw",1000,-4000,2500,1000,-500,600);
        T3_T2raw.SetOption("COLZ");
        T3_T2raw.SetMinimum(1);
        tree->Draw("raw_T2:raw_T3>>T3_T2raw",""," ");
        T3_T2raw.SetTitle("raw_T3 vs raw_T2 [Counts];raw_T3 [ch];raw_T2 [ch]");
        T3_T2raw.Write("",TObject::kOverwrite);

        TH2I T1_E1raw("T1_E1raw","T1_E1raw",2800,0,2800,250,0,250);
        T1_E1raw.SetOption("COLZ");
        T1_E1raw.SetMinimum(1);
        tree->Draw("raw_E1:raw_T1>>T1_E1raw",""," ");
        T1_E1raw.SetTitle("raw_T1 vs raw_E1 [Counts];raw_T1 [ch];raw_E1 [ch]");
        T1_E1raw.Write("",TObject::kOverwrite);

        TH2I T2_E2raw("T2_E2raw","T2_E2raw",1000,0,1000,500,0,500);
        T2_E2raw.SetOption("COLZ");
        T2_E2raw.SetMinimum(1);
        tree->Draw("raw_E2:raw_T2>>T2_E2raw",""," ");
        T2_E2raw.SetTitle("raw_T2 vs raw_E2 [Counts];raw_T2 [ch];raw_E2 [ch]");
        T2_E2raw.Write("",TObject::kOverwrite);

        TH2I T3_E3raw("T3_E3raw","T3_E3raw",1000,-1000,2000,500,0,300);
        T3_E3raw.SetOption("COLZ");
        T3_E3raw.SetMinimum(1);
        tree->Draw("raw_E3:raw_T3>>T3_E3raw",""," ");
        T3_E3raw.SetTitle("raw_T3 vs raw_E3 [Counts];raw_T3 [ch];raw_E3 [ch]");
        T3_E3raw.Write("",TObject::kOverwrite);

        TH2I tback_T2raw("tback_T2raw","tback_T2raw",2000,0,4000,1600,-500,1500);
        tback_T2raw.SetOption("COLZ");
        tback_T2raw.SetMinimum(1);
        tree->Draw("raw_T2:raw_tback>>tback_T2raw",""," ");
        tback_T2raw.SetTitle("raw_tback vs raw_T2 [Counts];raw_tback [ch];raw_T2 [ch]");
        tback_T2raw.Write("",TObject::kOverwrite);

    }//2D raw

    std::cout << "Produced histograms of raw data..." << std::endl;

    // ##### Derived or calibrated histograms ######

    // 1D Histograms

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("Hist1D")!=true){
        sfile->mkdir("Hist1D");
    }
    sfile->cd("Hist1D");

    {
        TH1F x1det("x1det","x1det",400,-200,200);
        tree->Draw("x1det>>x1det","","");
        x1det.SetTitle("x1det;x1det[mm];Counts");
        x1det.Write("",TObject::kOverwrite);

        TH1F y1det("y1det","y1det",400,-200,200);
        tree->Draw("y1det>>y1det","","");
        y1det.SetTitle("y1det;y1det[mm];Counts");
        y1det.Write("",TObject::kOverwrite);

        TH1F x2det("x2det","x2det",400,-200,200);
        tree->Draw("x2det>>x2det","","");
        x2det.SetTitle("x2det;x2det[mm];Counts");
        x2det.Write("",TObject::kOverwrite);

        TH1F y2det("y2det","y2det",400,-200,200);
        tree->Draw("y2det>>y2det","","");
        y2det.SetTitle("y2det;y2det[mm];Counts");
        y2det.Write("",TObject::kOverwrite);

        TH1F t1("t1","t1",1000,-500,500);
        tree->Draw("t1>>t1","","");
        t1.SetTitle("t1;t1[ns];Counts");
        t1.Write("",TObject::kOverwrite);

        TH1F t2("t2","t2",1000,-500,500);
        tree->Draw("t2>>t2","","");
        t2.SetTitle("t2;t2[ns];Counts");
        t2.Write("",TObject::kOverwrite);

        TH1F t3("t3","t3",1000,-500,500);
        tree->Draw("t3>>t3","","");
        t3.SetTitle("t3;t3[ns];Counts");
        t3.Write("",TObject::kOverwrite);

        TH1F theta1("theta1","theta1",180,0,180);
        tree->Draw("theta1>>theta1","","");
        theta1.SetTitle("theta1;theta1[deg];Counts");
        theta1.Write("",TObject::kOverwrite);

        TH1F theta2("theta2","theta2",180,0,180);
        tree->Draw("theta2>>theta2","","");
        theta2.SetTitle("theta2;theta2[deg];Counts");
        theta2.Write("",TObject::kOverwrite);

        TH1F theta3("theta3","theta3",360,0,360);
        tree->Draw("theta3>>theta3","","");
        theta3.SetTitle("theta3;theta3[deg];Counts");
        theta3.Write("",TObject::kOverwrite);

        TH1F phi1("phi1","phi1",180,90,270);
        tree->Draw("phi1>>phi1","","");
        phi1.SetTitle("phi1;phi1[deg];Counts");
        phi1.Write("",TObject::kOverwrite);

        TH1F phi2("phi2","phi2",180,-90,90);
        tree->Draw("phi2>>phi2","","");
        phi2.SetTitle("phi2;phi2[deg];Counts");
        phi2.Write("",TObject::kOverwrite);

        TH1F phi3("phi3","phi3",360,0,360);
        tree->Draw("phi3>>phi3","","");
        phi3.SetTitle("phi3;phi3[deg];Counts");
        phi3.Write("",TObject::kOverwrite);

        TH1F phi12("phi12","phi12",3600,0,360);
        tree->Draw("phi12>>phi12","","");
        phi12.SetTitle("phi12;phi12[deg];Counts");
        phi12.Write("",TObject::kOverwrite);

        TH1F thcm1("thcm1","thcm1",1800,0,180);
        tree->Draw("thetaCM1>>thcm1","","");
        thcm1.SetTitle("Scattering angle 1 (CM frame);thetaCM1 [deg];Counts");
        thcm1.Write("",TObject::kOverwrite);

        TH1F thcm2("thcm2","thcm2",1800,0,180);
        tree->Draw("thetaCM2>>thcm2","","");
        thcm2.SetTitle("Scattering angle 2 (CM frame);thetaCM2 [deg];Counts");
        thcm2.Write("",TObject::kOverwrite);

        TH1F thcm3("thcm3","thcm3",3600,0,360);
        tree->Draw("thetaCM3>>thcm3","","");
        thcm3.SetTitle("Scattering angle 3 (CM frame);thetaCM3 [deg];Counts");
        thcm3.Write("",TObject::kOverwrite);

        TH1F v1("v1","v1",500,0,50);
        tree->Draw("v1>>v1","","");
        v1.SetTitle("v1;v1[mm/ns];Counts");
        v1.Write("",TObject::kOverwrite);

        TH1F v2("v2","v2",500,0,50);
        tree->Draw("v2>>v2","","");
        v2.SetTitle("v2;v2[mm/ns];Counts");
        v2.Write("",TObject::kOverwrite);

        TH1F v3("v3","v3",1000,-50,50);
        tree->Draw("v3>>v3","","");
        v3.SetTitle("v3;v3[mm/ns];Counts");
        v3.Write("",TObject::kOverwrite);

        TH1F vcm1("vcm1","vcm1",2000,0,100);
        tree->Draw("Vcm1>>Vcm1","","");
        vcm1.SetTitle("Vcm1;Vcm1[mm/ns];Counts");
        vcm1.Write("",TObject::kOverwrite);

        TH1F vcm2("vcm2","vcm2",2000,0,100);
        tree->Draw("Vcm2>>Vcm2","","");
        vcm2.SetTitle("Vcm2;Vcm2[mm/ns];Counts");
        vcm2.Write("",TObject::kOverwrite);
        /*//Vcm3 is not defined in CubeTree
          TH1F vcm3("vcm3","vcm3",2000,0,100);
          tree->Draw("Vcm3>>Vcm3","","");
          vcm3.SetTitle("Vcm3;Vcm3[mm/ns];Counts");
          vcm3.Write("",TObject::kOverwrite);
         */
        TH1F vpar("vpar","vpar",500,-10,40);
        tree->Draw("vpar>>vpar","","");
        vpar.SetTitle("vpar;vpar[mm/ns];Counts");
        vpar.Write("",TObject::kOverwrite);

        TH1F vperp("vperp","vperp",500,-5,5);
        tree->Draw("vperp>>vperp","","");
        vperp.SetTitle("vperp;vperp[mm/ns];Counts");
        vperp.Write("",TObject::kOverwrite);
        /*
           TH1F tdifference("tdifference","tdifference",2000,-500,500);
           tree->Draw("tdiff>>tdifference","","");
           tdifference.SetTitle("tdiff;tdiff[ns];Counts");
           tdifference.Write("",TObject::kOverwrite);
         */
        TH1F tdiff12("tdiff12","tdiff12",2000,-500,500);
        tree->Draw("tdiff12>>tdiff12","","");
        tdiff12.SetTitle("tdiff12;tdiff12[ns];Counts");
        tdiff12.Write("",TObject::kOverwrite);

        TH1F tdiff32("tdiff32","tdiff32",2000,-500,500);
        tree->Draw("tdiff32>>tdiff32","","");
        tdiff32.SetTitle("tdiff32;tdiff32[ns];Counts");
        tdiff32.Write("",TObject::kOverwrite);


        TH1F T0deriv("T0deriv","T0deriv",2200,0,110);
        tree->Draw("T0der>>T0deriv","","");
        T0deriv.SetTitle("Derived T0 values; T0der[ns]; Counts");
        T0deriv.Write("",TObject::kOverwrite);

        TH1F dTderiv("dTderiv","dTderiv",800,-20,20);
        tree->Draw("dTder>>dTderiv","","");
        dTderiv.SetTitle("Derived dT values; dTder[ns]; Counts");
        dTderiv.Write("",TObject::kOverwrite);

        TH1F dT3deriv("dT3deriv","dT3deriv",800,-20,20);
        tree->Draw("dT3der>>dT3deriv","","");
        dT3deriv.SetTitle("Derived d3T values; dT3der[ns]; Counts");
        dT3deriv.Write("",TObject::kOverwrite);

        TH1F DetectorChoice("DetectorChoice","DetectorChoice",10,0,10);
        tree->Draw("DetChoice>>DetectorChoice","","");
        DetectorChoice.SetTitle("DetectorChoice;Choice;Counts");
        DetectorChoice.Write("",TObject::kOverwrite);

    }//1D

    cout << "Produced 1D histograms..." << endl;

    // 2D Histograms

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("Hist2D")!=true){
        sfile->mkdir("Hist2D");
    }
    sfile->cd("Hist2D");

    {
        TH2F x1_y1("x1_y1","x1_y1",400,-200,200,400,-200,200);
        x1_y1.SetOption("COLZ");
        x1_y1.SetMinimum(1);
        tree->Draw("y1det:x1det>>x1_y1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        x1_y1.SetTitle("x1 vs y1 in detector reference frame [Counts]; x1det [mm];y1det [mm]");
        x1_y1.Write("",TObject::kOverwrite);

        TH2F x2_y2("x2_y2","x2_y2",400,-200,200,400,-200,200);
        x2_y2.SetOption("COLZ");
        x2_y2.SetMinimum(1);
        tree->Draw("y2det:x2det>>x2_y2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        x2_y2.SetTitle("x2 vs y2 in detector reference frame [Counts]; x2det [mm];y2det [mm]");
        x2_y2.Write("",TObject::kOverwrite);

        TH2F x3_y3("x3_y3","x3_y3",400,-200,200,400,-200,200);
        x3_y3.SetOption("COLZ");
        x3_y3.SetMinimum(1);
        tree->Draw("y3det:x3det>>x3_y3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        x3_y3.SetTitle("x3 vs y3 in detector reference frame [Counts]; x3det [mm];y3det [mm]");
        x3_y3.Write("",TObject::kOverwrite);

        TH2F t1_t2("t1_t2","t1_t2",1000,-500,500,1000,-500,500);
        t1_t2.SetOption("COLZ");
        t1_t2.SetMinimum(1);
        tree->Draw("t2:t1>>t1_t2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t1_t2.SetTitle("t1 vs t2 [Counts];t1 [ns];t2 [ns]");
        t1_t2.Write("",TObject::kOverwrite);

        TH2F t3_t2("t3_t2","t3_t2",1000,-500,500,1000,-500,500);
        t3_t2.SetOption("COLZ");
        t3_t2.SetMinimum(1);
        tree->Draw("t2:t3>>t3_t2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t3_t2.SetTitle("t3 vs t2 [Counts];t3 [ns];t2 [ns]");
        t3_t2.Write("",TObject::kOverwrite);

        TH2F t1_x1("t1_x1","t1_x1",1000,-500,500,2000,-50,150);
        t1_x1.SetOption("COLZ");
        t1_x1.SetMinimum(1);
        tree->Draw("t1:x1det>>t1_x1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t1_x1.SetTitle("x1 in detector reference frame vs t1 [Counts]; x1det [mm]; t1 [ns]");
        t1_x1.Write("",TObject::kOverwrite);

        TH2F t2_x2("t2_x2","t2_x2",1000,-500,500,2000,-50,150);
        t2_x2.SetOption("COLZ");
        t2_x2.SetMinimum(1);
        tree->Draw("t2:x2det>>t2_x2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t2_x2.SetTitle("x2 in detector reference frame vs t2 [Counts]; x2det [mm]; t2 [ns]");
        t2_x2.Write("",TObject::kOverwrite);

        TH2F t3_x3("t3_x3","t3_x3",1000,-500,500,2000,-50,150);
        t3_x3.SetOption("COLZ");
        t3_x3.SetMinimum(1);
        tree->Draw("t3:x3det>>t3_x3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t3_x3.SetTitle("x3 in detector reference frame vs t3 [Counts]; x3det [mm]; t3 [ns]");
        t3_x3.Write("",TObject::kOverwrite);

        TH2F t1_y1("t1_y1","t1_y1",2000,-50,150,1000,-500,500);
        t1_y1.SetOption("COLZ");
        t1_y1.SetMinimum(1);
        tree->Draw("y1det:t1>>t1_y1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t1_y1.SetTitle("t1 vs y1 in detector reference frame [Counts]; t1 [ns];y1det [mm]");
        t1_y1.Write("",TObject::kOverwrite);

        TH2F t2_y2("t2_y2","t2_y2",2000,-50,150,1000,-500,500);
        t2_y2.SetOption("COLZ");
        t2_y2.SetMinimum(1);
        tree->Draw("y2det:t2>>t2_y2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t2_y2.SetTitle("t2 vs y2 in detector reference frame [Counts]; t2 [ns];y2det [mm]");
        t2_y2.Write("",TObject::kOverwrite);

        TH2F t3_y3("t3_y3","t3_y3",2000,-50,150,1000,-500,500);
        t3_y3.SetOption("COLZ");
        t3_y3.SetMinimum(1);
        tree->Draw("y3det:t3>>t3_y3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t3_y3.SetTitle("t3 vs y3 in detector reference frame [Counts]; t3 [ns];y3det [mm]");
        t3_y3.Write("",TObject::kOverwrite);

        TH2F th1_phi1("th1_phi1","th1_phi1",1400,20,160,2000,100,300);
        th1_phi1.SetOption("COLZ");
        th1_phi1.SetMinimum(1);
        tree->Draw("phi1:theta1>>th1_phi1",""," ");
        th1_phi1.SetTitle("theta1 vs phi1 [Counts];theta1 [deg];phi1 [deg]");
        th1_phi1.Write("",TObject::kOverwrite);

        TH2F th2_phi2("th2_phi2","th2_phi2",1800,0,180,3600,-180,180);
        th2_phi2.SetOption("COLZ");
        th2_phi2.SetMinimum(1);
        tree->Draw("phi2:theta2>>th2_phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        th2_phi2.SetTitle("theta2 vs phi2 [Counts];theta2 [deg];phi2 [deg]");
        th2_phi2.Write("",TObject::kOverwrite);

        TH2F th3_phi3("th3_phi3","th3_phi3",1800,0,180,2000,100,300);
        th3_phi3.SetOption("COLZ");
        th3_phi3.SetMinimum(1);
        tree->Draw("phi3:theta3>>th3_phi3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        th3_phi3.SetTitle("theta3 vs phi3 [Counts];theta3 [deg];phi3 [deg]");
        th3_phi3.Write("",TObject::kOverwrite);
        /*
           TH2F th1_th2("th1_th2","th1_th2",1800,0,180,1800,0,180);
           th1_th2.SetOption("COLZ");
           th1_th2.SetMinimum(1);
           tree->Draw("theta2:theta1>>th1_th2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           th1_th2.SetTitle("theta1 vs theta2 [Counts];theta1 [deg];theta2 [deg]");
           th1_th2.Write("",TObject::kOverwrite);
         */
        TH2F phi1_phi2("phi1_phi2","phi1_phi2",1800,0,360,3600,-180,180);
        phi1_phi2.SetOption("COLZ");
        phi1_phi2.SetMinimum(1);
        tree->Draw("phi2:phi1>>phi1_phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        phi1_phi2.SetTitle("phi1 vs phi2 [Counts];phi1 [deg];phi2 [deg]");
        phi1_phi2.Write("",TObject::kOverwrite);

        TH2F th1_thcm1("th1_thcm1","theta1:thetaCM1",1800,0,180,1800,0,180);
        th1_thcm1.SetOption("COLZ");
        th1_thcm1.SetMinimum(1);
        tree->Draw("thetaCM1:theta1>>th1_thcm1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        th1_thcm1.SetTitle("det1: thetalab vs thetaCM [Counts];thetaLAB_1 [deg];thetaCM_1 [deg]");
        th1_thcm1.Write("",TObject::kOverwrite);

        TH2F th2_thcm2("th2_thcm2","theta2:thetaCM2",1800,0,180,1800,0,180);
        th2_thcm2.SetOption("COLZ");
        th2_thcm2.SetMinimum(1);
        tree->Draw("thetaCM2:theta2>>th2_thcm2",""," ");
        th2_thcm2.SetTitle("det2: thetalab vs thetaCM [Counts];thetaLAB_2 [deg];thetaCM_2 [deg]");
        th2_thcm2.Write("",TObject::kOverwrite);

        TH2F th3_thcm3("th3_thcm3","theta3:thetaCM3",3600,0,360,3600,0,360);
        th3_thcm3.SetOption("COLZ");
        th3_thcm3.SetMinimum(1);
        tree->Draw("thetaCM3:theta3>>th3_thcm3",""," ");
        th3_thcm3.SetTitle("det3: thetalab vs thetaCM [Counts];thetaLAB_3 [deg];thetaCM_3 [deg]");
        th3_thcm3.Write("",TObject::kOverwrite);


        TH2F phi1_phi12("phi1_phi12","phi1_phi12",2000,100,300,3600,0,360);
        phi1_phi12.SetOption("COLZ");
        phi1_phi12.SetMinimum(1);
        tree->Draw("phi12:phi1>>phi1_phi12",""," ");
        phi1_phi12.SetTitle("phi1 vs phi12 [Counts];phi1 [deg];phi12 [deg]");
        phi1_phi12.Write("",TObject::kOverwrite);

        TH2F phi2_phi12("phi2_phi12","phi2_phi12",2000,-100,100,3600,0,360);
        phi2_phi12.SetOption("COLZ");
        phi2_phi12.SetMinimum(1);
        tree->Draw("phi12:phi2>>phi2_phi12",""," ");
        phi2_phi12.SetTitle("phi2 vs phi12 [Counts];phi2 [deg];phi2-phi1 [deg]");
        phi2_phi12.Write("",TObject::kOverwrite);

        TH2F phi3_phi12("phi3_phi12","phi3_phi12",2000,100,300,3600,0,360);
        phi3_phi12.SetOption("COLZ");
        phi3_phi12.SetMinimum(1);
        tree->Draw("phi12:phi3>>phi3_phi12",""," ");
        phi3_phi12.SetTitle("phi3 vs phi12 [Counts];phi3 [deg];phi2-phi1 [deg]");
        phi3_phi12.Write("",TObject::kOverwrite);

        /*
           TH2F folding("folding","theta1:theta12",1800,0,180,2700,0,270);
           folding.SetOption("COLZ");
           folding.SetMinimum(1);
           tree->Draw("theta12:theta1>>folding",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           folding.SetTitle("thetalab1 vs theta1+theta2 (folding angle) [Counts];thetaLAB_1 [deg];theta12 [deg]");
           folding.Write("",TObject::kOverwrite);

           TH2F folding3("folding3","theta3:theta12",1800,0,180,2700,0,270);
           folding3.SetOption("COLZ");
           folding3.SetMinimum(1);
           tree->Draw("theta12:theta3>>folding3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           folding3.SetTitle("thetalab3 vs theta1+theta2 (folding angle) [Counts];thetaLAB_3 [deg];theta12 [deg]");
           folding3.Write("",TObject::kOverwrite);
         */

        TH2F folding("folding","thetaback:thetaback2",1800,0,180,2700,0,270);
        folding.SetOption("COLZ");
        folding.SetMinimum(1);
        tree->Draw("thetaback+theta2:thetaback>>folding",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        folding.SetTitle("thetaback vs thetaback+theta2 (folding angle) [Counts];thetaback [deg];thetaback2 [deg]");
        folding.Write("",TObject::kOverwrite);


        TH2F foldDe("foldDe","theta12:dESum",1800,0,360,1000,0,1000);
        foldDe.SetOption("COLZ");
        foldDe.SetMinimum(1);
        tree->Draw("dESum:theta12>>foldDe",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        foldDe.SetTitle("folding angle vs E1+E2 [Counts];theta12 [deg];dESum");
        foldDe.Write("",TObject::kOverwrite);

        TH2F mrvr("mrvr","MR:Vpar/Vcn",1000,0,1,400,-1,3);
        mrvr.SetOption("COLZ");
        mrvr.SetMinimum(1);
        tree->Draw("vpar/Vcn:MR>>mrvr",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrvr.SetTitle("MR vs vpar/Vcn [Counts];Mass Ratio; vpar/Vcn [mm/ns]");
        mrvr.Write("",TObject::kOverwrite);

        TH2F mrth1("mrth1","MR:Theta1",1000,0,1,1400,20,160);
        mrth1.SetOption("COLZ");
        mrth1.SetMinimum(1);
        tree->Draw("theta1:MR>>mrth1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrth1.SetTitle("MR vs Theta [Counts];Mass Ratio; Theta1 [deg]");
        mrth1.Write("",TObject::kOverwrite);

        TH2F mrth2("mrth2","MR:Theta2",1000,0,1,1200,0,120);
        mrth2.SetOption("COLZ");
        mrth2.SetMinimum(1);
        tree->Draw("theta2:1.-MR>>mrth2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrth2.SetTitle("MR vs theta2 [Counts];Mass Ratio; Theta2 [deg]");
        mrth2.Write("",TObject::kOverwrite);

        TH2F mrthcm1("mrthcm1","MR:thetaCM1",1000,0,1,3600,0,360);
        mrthcm1.SetOption("COLZ");
        mrthcm1.SetMinimum(1);
        tree->Draw("thetaCM1:MR>>mrthcm1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrthcm1.SetTitle("MR vs thetaCM [Counts];Mass Ratio; thetaCM1 [deg]");
        mrthcm1.Write("",TObject::kOverwrite);

        TH2F mrthcm2("mrthcm2","MR:thetaCM2",1000,0,1,2000,0,180);
        mrthcm2.SetOption("COLZ");
        mrthcm2.SetMinimum(1);
        tree->Draw("thetaCM2:1.-MR>>mrthcm2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrthcm2.SetTitle("MR vs thetaCM [Counts];Mass Ratio; thetaCM2 [deg]");
        mrthcm2.Write("",TObject::kOverwrite);

        TH2F mrthcm3("mrthcm3","MR:thetaCM3",1000,0,1,3600,0,360);
        mrthcm3.SetOption("COLZ");
        mrthcm3.SetMinimum(1);
        tree->Draw("thetaCM3:MR>>mrthcm3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrthcm3.SetTitle("MR vs thetaCM [Counts];Mass Ratio; thetaCM3 [deg]");
        mrthcm3.Write("",TObject::kOverwrite);

        TH2F tempA("tempA","tempA",1000,0,1,1800,0,180);

        tree->Draw("thetaCM1:(MR)>>tempA",""," ");

        TH2F tempB("tempB","tempB",1000,0,1,1800,0,180);

        tree->Draw("thetaCM3:(MR)>>tempB",""," ");

        TH2F mrthcm13("mrthcm13","mrthcm13",1000,0,1,1800,0,180);
        mrthcm13.SetOption("COLZ");
        mrthcm13.SetMinimum(1);
        mrthcm13.Add(&mrthcm13, &tempA);
        mrthcm13.Add(&mrthcm13, &tempB);
        mrthcm13.SetTitle("MR vs thetaCM1 and thetaCM3 [Counts]; Mass Ratio; thetaCM1 and thetaCM3 [deg]");
        mrthcm13.Write("",TObject::kOverwrite);


        /*------------- NEW -------------------*/
       //mass vs thetaCM
        TH2D massthcm1("massthcm1","MR:thetaCM1",acn+0.5,0,acn+0.5,180,0,360);
        //TH2D massthcm1("massthcm1","MR:thetaCM1",248,0.5,248+0.5,180,0,360);
        massthcm1.SetOption("COLZ");
        massthcm1.SetMinimum(1);
        tree->Draw("thetaCM1:mass1>>massthcm1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        massthcm1.SetTitle("mass1 [amu] vs thetaCM [Counts];mass [amu]; thetaCM1 [deg]");
        massthcm1.Write("",TObject::kOverwrite);
        TH2D massthcm3("massthcm3","MR:thetaCM3",acn,0.5,acn+0.5,180,0,360);
        //TH2D massthcm3("massthcm3","MR:thetaCM3",248,0.5,248+0.5,180,0,360);
        massthcm3.SetOption("COLZ");
        massthcm3.SetMinimum(1);
        tree->Draw("thetaCM3:mass1>>massthcm3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        massthcm3.SetTitle("mass1 [amu] vs thetaCM [Counts];mass1 [amu]; thetaCM3 [deg]");
        massthcm3.Write("",TObject::kOverwrite);

        TH2F tfront_tback("tfront_tback","t2:tback",200,-20,100,200,0,55);
        //TH2F tfront_tback("tfront_tback","t2:tback",1000,-20,200,1000,-20,100);
        tfront_tback.SetOption("COLZ");
        tfront_tback.SetMinimum(1);
        tree->Draw("t2:tback>>tfront_tback",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
        tfront_tback.SetTitle("t2 vs tback [Counts];tback [ns];t2 [ns]");
        tfront_tback.Write("",TObject::kOverwrite);

        TH2F thetafront_thetaback("thetafront_thetaback","theta2:thetaback",1000,0,180,1000,0,360);
        thetafront_thetaback.SetOption("COLZ");
        thetafront_thetaback.SetMinimum(1);
        tree->Draw("theta2:thetaback>>thetafront_thetaback",""," "); //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetafront_thetaback.SetTitle("theta2 vs thetaback [Counts];thetaback [deg];theta2 [deg]");
        thetafront_thetaback.Write("",TObject::kOverwrite);

        TH2F thetaback_phiback("thetaback_phiback","phiback:thetaback",1000,0,180,1000,100,300);
        thetaback_phiback.SetOption("COLZ");
        thetaback_phiback.SetMinimum(1);
        tree->Draw("phiback:thetaback>>thetaback_phiback",""," "); //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaback_phiback.SetTitle("phiback vs thetaback [Counts];thetaback [deg];phiback [deg]");
        thetaback_phiback.Write("",TObject::kOverwrite);

        TH2F thetaCMback_phiback("thetaCMback_phiback","phiback:thetaCMback",1000,0,180,1000,80,300);
        thetaCMback_phiback.SetOption("COLZ");
        thetaCMback_phiback.SetMinimum(1);
        tree->Draw("phiback:thetaCMback>>thetaCMback_phiback",""," ");
        //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaCMback_phiback.SetTitle("phiback vs thetaCMback [Counts];thetaCMback [deg];phiback [deg]");
        thetaCMback_phiback.Write("",TObject::kOverwrite);

    }

    //mirrored histograms

    {
        TH2F temp("temp","temp",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp","thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM1):(1.-MR)>>temp",TString::Format("thetaCM1 > 90. + %g * (MR-0.5)",slope),"");

        TH2F temp2("temp2","temp2",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM1:MR>>temp2","thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("thetaCM1:MR>>temp2","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM1:MR>>temp2",TString::Format("thetaCM1 > 90. + %g*(MR-0.5)",slope)," ");

        TH2F mrthcm1_mir("mrthcm1_mir","mrthcm1_mir",1000,0,1,1800,0,180);
        mrthcm1_mir.SetOption("COLZ");
        mrthcm1_mir.SetMinimum(1);
        mrthcm1_mir.Add(&mrthcm1_mir, &temp);
        mrthcm1_mir.Add(&mrthcm1_mir, &temp2);
        mrthcm1_mir.SetTitle("MR vs thetaCM1 (mirrored) [Counts]; Mass Ratio; thetaCM1 [deg]");
        mrthcm1_mir.Write("",TObject::kOverwrite);

        TH2F temp3("temp3","temp3",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM2):MR>>temp3","thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM2):MR>>temp3","thetaCM2 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM2):MR>>temp3",TString::Format("thetaCM2 > 90. + %g * (MR-0.5)",slope)," ");

        TH2F temp4("temp4","temp4",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM2:1.-MR>>temp4","thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("thetaCM2:1.-MR>>temp4","thetaCM2 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM2:1.-MR>>temp4",TString::Format("thetaCM2 > 90. + %g *(MR-0.5)",slope)," ");

        TH2F mrthcm2_mir("mrthcm2_mir","mrthcm2_mir",1000,0,1,1800,0,180);
        mrthcm2_mir.SetOption("COLZ");
        mrthcm2_mir.SetMinimum(1);
        mrthcm2_mir.Add(&mrthcm2_mir, &temp3);
        mrthcm2_mir.Add(&mrthcm2_mir, &temp4);
        mrthcm2_mir.SetTitle("MR vs thetaCM2 (mirrored) [Counts]; Mass Ratio; thetaCM2 [deg]");
        mrthcm2_mir.Write("",TObject::kOverwrite);

        TH2F tempx("tempx","tempx",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>tempx","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM1):(1.-MR)>>tempx",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

        TH2F tempy("tempy","tempy",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM1:MR>>tempy","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM1:MR>>tempy",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

        TH2F tempz("tempz","tempz",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM3):(1.-MR)>>tempz","thetaCM3 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM3):(1.-MR)>>tempz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

        TH2F tempzz("tempzz","tempzz",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM3:MR>>tempzz","thetaCM3 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM3:MR>>tempzz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

        TH2F mrthcm13_mir("mrthcm13_mir","mrthcm13_mir",1000,0,1,1800,0,180);
        mrthcm13_mir.SetOption("COLZ");
        mrthcm13_mir.SetMinimum(1);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempx);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempy);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempz);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempzz);
        mrthcm13_mir.SetTitle("MR vs thetaCM13 (mirrored) [Counts]; Mass Ratio; thetaCM13 [deg]");
        mrthcm13_mir.Write("",TObject::kOverwrite);

        /*------------- NEW -------------------*/
        //mass vs thetaCM
                TH2D tmpx("tmpx","tmpx",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("(180.-thetaCM1):mass2>>tmpx",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

                TH2D tmpy("tmpy","tmpy",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("thetaCM1:mass1>>tmpy",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

                TH2D tmpz("tmpz","tmpz",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("(180.-thetaCM3):mass2>>tmpz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

                TH2D tmpzz("tmpzz","tmpzz",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("thetaCM3:mass1>>tmpzz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

                TH2D massthcm13_mir("massthcm13_mir","massthcm13_mir",acn,0.5,acn+0.5,angbin,0,180);
                massthcm13_mir.SetOption("COLZ");
                massthcm13_mir.SetMinimum(1);
                massthcm13_mir.Add(&massthcm13_mir, &tmpx);
                massthcm13_mir.Add(&massthcm13_mir, &tmpy);
                massthcm13_mir.Add(&massthcm13_mir, &tmpz);
                massthcm13_mir.Add(&massthcm13_mir, &tmpzz);
                massthcm13_mir.SetTitle("Mass vs thetaCM13 (mirrored) [Counts]; Mass [amu]; thetaCM13 [deg]");
                massthcm13_mir.Write("",TObject::kOverwrite);


        TH2F temp7("temp7","temp7",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp7","TKE/TKEViola*(thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp7","TKE/TKEViola*(thetaCM1 > 90. + 74.*(MR-0.5))"," "); //gayatri
        tree->Draw("(180.-thetaCM1):(1.-MR)>>temp7",TString::Format("TKE/TKEViola*(thetaCM1 > 90. + %g * (MR-0.5))",slope)," ");

        TH2F temp8("temp8","temp8",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM1:MR>>temp8","TKE/TKEViola*(thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("thetaCM1:MR>>temp8","TKE/TKEViola*(thetaCM1 > 90. + 74.*(MR-0.5))"," ");
        tree->Draw("thetaCM1:MR>>temp8",TString::Format("TKE/TKEViola*(thetaCM1 > 90. + %g * (MR-0.5))",slope)," ");

        TH2F mrthcm1_TKE("mrthcm1_TKE","mrthcm1_TKE",1000,0,1,1800,0,180);
        mrthcm1_TKE.SetOption("COLZ");
        mrthcm1_TKE.SetMinimum(1);
        mrthcm1_TKE.Add(&mrthcm1_TKE, &temp7);
        mrthcm1_TKE.Add(&mrthcm1_TKE, &temp8);
        mrthcm1_TKE.SetTitle("MR vs thetaCM1 (mirrored) [TKE/TKEViola]; Mass Ratio; thetaCM1 [deg]");
        mrthcm1_TKE.Divide(&mrthcm1_TKE, &mrthcm1_mir);
        mrthcm1_TKE.SetMaximum(1.825);
        mrthcm1_TKE.SetMinimum(0.825);
        mrthcm1_TKE.Write("",TObject::kOverwrite);

        TH2F temp5("temp5","temp5",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM2):MR>>temp5","TKE/TKEViola*(thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM2):MR>>temp5","TKE/TKEViola*(thetaCM2 > 90. + 74.*(MR-0.5))"," ");
        tree->Draw("(180.-thetaCM2):MR>>temp5",TString::Format("TKE/TKEViola*(thetaCM2 > 90. + %g * (MR-0.5))",slope)," ");

        TH2F temp6("temp6","temp6",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM2:1.-MR>>temp6","TKE/TKEViola*(thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("thetaCM2:1.-MR>>temp6","TKE/TKEViola*(thetaCM2 > 90. + 74.*(MR-0.5))"," ");
        tree->Draw("thetaCM2:1.-MR>>temp6",TString::Format("TKE/TKEViola*(thetaCM2 > 90. + %g * (MR-0.5))",slope)," ");

        TH2F mrthcm2_TKE("mrthcm2_TKE","mrthcm2_TKE",1000,0,1,1800,0,180);
        mrthcm2_TKE.SetOption("COLZ");
        mrthcm2_TKE.SetMinimum(1);
        mrthcm2_TKE.Add(&mrthcm2_TKE, &temp5);
        mrthcm2_TKE.Add(&mrthcm2_TKE, &temp6);
        mrthcm2_TKE.SetTitle("MR vs thetaCM2 (mirrored) [TKE/TKEViola]; Mass Ratio; thetaCM2 [deg]");
        mrthcm2_TKE.Divide(&mrthcm2_TKE, &mrthcm2_mir);
        mrthcm2_TKE.SetMaximum(1.825);
        mrthcm2_TKE.SetMinimum(0.825);
        mrthcm2_TKE.Write("",TObject::kOverwrite);
        //end mirrored histograms

        //make plot showing mirror line
        //TF1 mirrorfunc("mirrorfunc","43.125 + 197.917*x - 312.5*x*x + 208.33*x*x*x",0,1);
        //TF1 mirrorfunc("mirrorfunc","90.0 + 74.0*(x-0.5)",0,1);
        TF1 mirrorfunc("active_mirrorfunc",TString::Format("90.0 + %g *(x-0.5)",slope),0,1);

        mirrorfunc.Draw();
        mirrorfunc.Write("",TObject::kOverwrite);

        //make plot showing mirror line
        TF1 mirrorfunc2("mirrorfunc2","43.125 + 197.917*x - 312.5*x*x + 208.33*x*x*x",0,1);
        mirrorfunc2.Draw();
        mirrorfunc2.Write("",TObject::kOverwrite);

        TH2F mrTKEViola("mrTKEViola","MR:TKE/TKEViola",1000,0,1,500,0,5);
        mrTKEViola.SetOption("COLZ");
        mrTKEViola.SetMinimum(1);
        tree->Draw("TKE/TKEViola:MR>>mrTKEViola",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrTKEViola.SetTitle("MR vs TKE/TKEViola [Counts];Mass Ratio;TKE/TKEViola");
        mrTKEViola.Write("",TObject::kOverwrite);
    }

    {
        TH2F mrTKE("mrTKE","MR:TKE",1000,0,1,900,0,400);
        mrTKE.SetOption("COLZ");
        mrTKE.SetMinimum(1);
        tree->Draw("TKE:MR>>mrTKE",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrTKE.SetTitle("MR vs TKE [Counts];Mass Ratio;TKE [MeV]");
        mrTKE.Write("",TObject::kOverwrite);

        TH2F mrEraw1("mrEraw1","MR:raw_E1",1000,0,1,1024,0,1024);
        mrEraw1.SetOption("COLZ");
        mrEraw1.SetMinimum(1);
        tree->Draw("raw_E1:MR>>mrEraw1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrEraw1.SetTitle("MR vs raw_E1 [Counts];Mass Ratio;raw_E1 [ch]");
        mrEraw1.Write("",TObject::kOverwrite);

        TH2F mrEraw2("mrEraw2","MR:Eraw2",1000,0,1,1024,0,1024);
        mrEraw2.SetOption("COLZ");
        mrEraw2.SetMinimum(1);
        tree->Draw("raw_E2:MR>>mrEraw2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrEraw2.SetTitle("MR vs raw_E2 [Counts];Mass Ratio;raw_E2 [ch]");
        mrEraw2.Write("",TObject::kOverwrite);

        TH2F mrElab1("mrElab1","MR:Elab1",1000,0,1,2000,0,400);
        mrElab1.SetOption("COLZ");
        mrElab1.SetMinimum(1);
        tree->Draw("Elab1:MR>>mrElab1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrElab1.SetTitle("MR vs Elab1 [Counts];Mass Ratio;Elab1 [MeV]");
        mrElab1.Write("",TObject::kOverwrite);

        TH2F mrElab2("mrElab2","MR:Elab2",1000,0,1,2000,0,400);
        mrElab2.SetOption("COLZ");
        mrElab2.SetMinimum(1);
        tree->Draw("Elab2:1.-MR>>mrElab2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrElab2.SetTitle("MR vs Elab2 [Counts];Mass Ratio;Elab2 [MeV]");
        mrElab2.Write("",TObject::kOverwrite);

        TH2F mrphi1("mrphi1","MR:phi1",1000,0,1,2000,100,300);
        mrphi1.SetOption("COLZ");
        mrphi1.SetMinimum(1);
        tree->Draw("phi1:MR>>mrphi1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrphi1.SetTitle("MR vs phi1 [Counts];Mass Ratio;phi1 [deg]");
        mrphi1.Write("",TObject::kOverwrite);

        TH2F mrphi2("mrphi2","MR:phi2",1000,0,1,2000,-100,100);
        mrphi2.SetOption("COLZ");
        mrphi2.SetMinimum(1);
        tree->Draw("phi2:1.-MR>>mrphi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrphi2.SetTitle("MR vs phi2 [Counts];Mass Ratio;phi2 [deg]");
        mrphi2.Write("",TObject::kOverwrite);

        TH2F m1_phi1("m1_phi1","m1_phi1",100,-100,350,1800,0,360);
        m1_phi1.SetOption("COLZ");
        m1_phi1.SetMinimum(1);
        tree->Draw("phi1:mass1>>m1_phi1",""," ");
        m1_phi1.SetTitle("m1 vs phi1 [Counts];mass1 [amu];phi1 [deg]");
        m1_phi1.Write("",TObject::kOverwrite);

        TH2F m2_phi2("m2_phi2","m2_phi2",100,-100,350,1800,-180,180);
        m2_phi2.SetOption("COLZ");
        m2_phi2.SetMinimum(1);
        tree->Draw("phi2:mass2>>m2_phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        m2_phi2.SetTitle("m2 vs phi2 [Counts];mass2 [amu];phi2 [deg]");
        m2_phi2.Write("",TObject::kOverwrite);

        TH2F thetaCM1_TKE("thetaCM1_TKE","thetaCM1:TKE",1800,0,180,900,0,300);
        thetaCM1_TKE.SetOption("COLZ");
        thetaCM1_TKE.SetMinimum(1);
        tree->Draw("TKE:thetaCM1>>thetaCM1_TKE",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaCM1_TKE.SetTitle("thetaCM1 vs TKE [Counts];thetaCM1;TKE [MeV]");
        thetaCM1_TKE.Write("",TObject::kOverwrite);

        TH2F thetaCM2_TKE("thetaCM2_TKE","thetaCM2:TKE",1800,0,180,900,0,300);
        thetaCM2_TKE.SetOption("COLZ");
        thetaCM2_TKE.SetMinimum(1);
        tree->Draw("TKE:thetaCM2>>thetaCM2_TKE",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaCM2_TKE.SetTitle("thetaCM2 vs TKE [Counts];thetaCM2;TKE [MeV]");
        thetaCM2_TKE.Write("",TObject::kOverwrite);
    }

    {
        TH2F thetaCM1_Em1("thetaCM1_Em1","thetaCM1:Emratio1",360,0,360,200,0,10);
        thetaCM1_Em1.SetOption("COLZ");
        thetaCM1_Em1.SetMinimum(1);
        tree->Draw("Emratio1:thetaCM1>>thetaCM1_Em1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaCM1_Em1.SetTitle("thetaCM1 vs Emratio1 [Counts];thetaCM1 [deg]; Elab1/m1[MeV/amu]");
        thetaCM1_Em1.Write("",TObject::kOverwrite);

        TH2F thetaCM2_Em2("thetaCM2_Em2","thetaCM2:Emratio2",360,0,360,100,0,10);
        thetaCM2_Em2.SetOption("COLZ");
        thetaCM2_Em2.SetMinimum(1);
        tree->Draw("Emratio2:thetaCM2>>thetaCM2_Em2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaCM2_Em2.SetTitle("thetaCM2 vs Emratio2 [Counts];thetaCM2 [deg]; Elab2/m2[MeV/amu]");
        thetaCM2_Em2.Write("",TObject::kOverwrite);

        TH2F EM1_eloss1("EM1_eloss1","EMratio1:Eloss1",100,0,10,100,0,10);
        EM1_eloss1.SetOption("COLZ");
        EM1_eloss1.SetMinimum(1);
        tree->Draw("Eloss1:Emratio1>>EM1_eloss1","","");
        EM1_eloss1.SetTitle("Emratio1 versus Eloss1 [Counts];Emratio1[MeV]; Eloss1[MeV]");
        EM1_eloss1.Write("",TObject::kOverwrite);

        TH2F EM2_eloss2("EM2_eloss2","EMratio2:Eloss2",100,0,10,100,0,10);
        EM2_eloss2.SetOption("COLZ");
        EM2_eloss2.SetMinimum(1);
        tree->Draw("Eloss2:Emratio2>>EM2_eloss2","","");
        EM2_eloss2.SetTitle("Emratio2 versus Eloss2 [Counts];Emratio2[MeV]; Eloss2[MeV]");
        EM2_eloss2.Write("",TObject::kOverwrite);


        TH2F t1phi1("t1phi1","t1:phi1",2000,-50,150,1800,0,360);
        t1phi1.SetOption("COLZ");
        t1phi1.SetMinimum(1);
        tree->Draw("phi1:t1>>t1phi1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t1phi1.SetTitle("t1 ts phi1 [Counts]; t1 [mm/ns]; phi1 [deg]");
        t1phi1.Write("",TObject::kOverwrite);

        TH2F t2phi2("t2phi2","t2:phi2",2000,-50,150,1800,-180,180);
        t2phi2.SetOption("COLZ");
        t2phi2.SetMinimum(1);
        tree->Draw("phi2:t2>>t2phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t2phi2.SetTitle("t2 ts phi2 [Counts]; t2 [mm/ns]; phi2 [deg]");
        t2phi2.Write("",TObject::kOverwrite);

        TH2F tdif_fold("tdif_fold","theta12:t1-t2",2000,-500,500,1800,0,360);
        tdif_fold.SetOption("COLZ");
        tdif_fold.SetMinimum(1);
        tree->Draw("theta12:t1-t2>>tdif_fold",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        tdif_fold.SetTitle(" t1-t2 vs folding angle [Counts];t1-t2 [ns]; theta12 [deg]");
        tdif_fold.Write("",TObject::kOverwrite);

        TH2F tdifphi1("tdifphi1","phi1:t1-t2",2000,-200,200,1800,100,300);
        tdifphi1.SetOption("COLZ");
        tdifphi1.SetMinimum(1);
        tree->Draw("phi1:t1-t2>>tdifphi1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        tdifphi1.SetTitle("t1-t2 vs phi1 [Counts];t1-t2 [ns]; phi1 [deg]");
        tdifphi1.Write("",TObject::kOverwrite);

        TH2F tdifphi2("tdifphi2","phi2:t1-t2",2000,-200,200,1800,-200,200);
        tdifphi2.SetOption("COLZ");
        tdifphi2.SetMinimum(1);
        tree->Draw("phi2:t1-t2>>tdifphi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        tdifphi2.SetTitle("t1-t2 vs phi2 [Counts];t1-t2 [ns]; phi2 [deg]");
        tdifphi2.Write("",TObject::kOverwrite);

        TH2F dtdESum("dtdESum","t1-t2:dESum",900,-150,150,1024,0,2048);
        dtdESum.SetOption("COLZ");
        dtdESum.SetMinimum(1);
        tree->Draw("dESum:t1-t2>>dtdESum",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        dtdESum.SetTitle("t1-t2 vs dESum [Counts];t1-t2 [ns];dESum [ch]");
        dtdESum.Write("",TObject::kOverwrite);


        TH2F v1_v2("v1_v2","v1_v2",1000,0,100,1000,0,100);
        v1_v2.SetOption("COLZ");
        v1_v2.SetMinimum(1);
        tree->Draw("v2:v1>>v1_v2",""," ");
        v1_v2.SetTitle("v1 vs v2 [Counts];v1 [mm/ns];v2 [mm/ns]");
        v1_v2.Write("",TObject::kOverwrite);

        TH2F v1phi1("v1phi1","v1:phi1",1000,0,100,1800,0,360);
        v1phi1.SetOption("COLZ");
        v1phi1.SetMinimum(1);
        tree->Draw("phi1:v1>>v1phi1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        v1phi1.SetTitle("v1 vs phi1 [Counts]; v1 [mm/ns]; phi1 [deg]");
        v1phi1.Write("",TObject::kOverwrite);

        TH2F v2phi2("v2phi2","v2:phi2",1000,0,100,1800,-180,180);
        v2phi2.SetOption("COLZ");
        v2phi2.SetMinimum(1);
        tree->Draw("phi2:v2>>v2phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        v2phi2.SetTitle("v2 vs phi2 [Counts]; v2 [mm/ns]; phi2 [deg]");
        v2phi2.Write("",TObject::kOverwrite);

        TH2F vcm1_vcm2("vcm1_vcm2","vcm1:vcm2",2000,0,100,2000,0,100);
        vcm1_vcm2.SetOption("COLZ");
        vcm1_vcm2.SetMinimum(1);
        tree->Draw("Vcm2:Vcm1>>vcm1_vcm2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vcm1_vcm2.SetTitle("Vcm1 vs Vcm2 [Counts];Vcm1 [mm/ns];Vcm2 [mm/ns]");
        vcm1_vcm2.Write("",TObject::kOverwrite);

        TH2F VcmdEsum("VcmdEsum","Vcm:dESum",1000,0,1000,1000,0,2000);
        VcmdEsum.SetOption("COLZ");
        VcmdEsum.SetMinimum(1);
        tree->Draw("dESum:VcmSum>>VcmdEsum",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        VcmdEsum.SetTitle("Vcm1+Vcm2 vs E1+E2 [Counts];VcmSum [mm/ns];dESum [ch]");
        VcmdEsum.Write("",TObject::kOverwrite);

        TH2F vdvp("vdvp","Vpar-Vcn:vperp",2000,-20,20,1600,-10,10);
        vdvp.SetOption("COLZ");
        vdvp.SetMinimum(1);
        tree->Draw("vperp:vpar-Vcn>>vdvp",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdvp.SetTitle("vpar-Vcn vs vperp [Counts];vpar-vcn [mm/ns]; vperp[mm/ns]");
        vdvp.Write("",TObject::kOverwrite);
        //******************
        TH2F vdsinevp("vdsinevp","(Vpar-Vcn)sinThetaCM:vperp",2000,-20,20,1600,-10,10);
        vdsinevp.SetOption("COLZ");
        vdsinevp.SetMinimum(1);
        tree->Draw("vperp:vdsine>>vdsinevp",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdsinevp.SetTitle("(vpar-Vcn)sinThetaCM vs vperp [Counts];(vpar-vcn)sinThetaCM [mm/ns]; vperp[mm/ns]");
        vdsinevp.Write("",TObject::kOverwrite);


        TH2F vrvp("vrvp","Vpar/Vcn:vperp",1000,-5,5,2000,-10,10);
        vrvp.SetOption("COLZ");
        vrvp.SetMinimum(1);
        tree->Draw("vperp:vpar/Vcn>>vrvp",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vrvp.SetTitle("vpar/Vcn vs vperp [Counts];vpar/Vcn ; vperp[mm/ns]");
        vrvp.Write("",TObject::kOverwrite);
        /*//Lab Frame
          TH2F vdthetaback("vdthetaback","thetaback:vpar-Vcn",1000,20,180,1000,-3,3);
          vdthetaback.SetOption("COLZ");
          vdthetaback.SetMinimum(1);
          tree->Draw("vpar-Vcn:thetaback>>vdthetaback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
          vdthetaback.SetTitle("thetaback vs vpar-Vcn [Counts];thetaback [deg];vpar-Vcn");
          vdthetaback.Write("",TObject::kOverwrite);
         */
        TH2F vdthetaCMback("vdthetaCMback","thetaCMback:vpar-Vcn",1000,20,180,1000,-3,3);
        vdthetaCMback.SetOption("COLZ");
        vdthetaCMback.SetMinimum(1);
        tree->Draw("vpar-Vcn:thetaCMback>>vdthetaCMback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdthetaCMback.SetTitle("thetaCMback vs vpar-Vcn [Counts];thetaCMback [deg];vpar-Vcn");
        vdthetaCMback.Write("",TObject::kOverwrite);


        //******************
        /* // Lab Frame
           TH2F vdsinethetaback("vdsinethetaback","thetaback:(vpar-Vcn)sinThetaCM",1000,20,180,1000,-3,3);
           vdsinethetaback.SetOption("COLZ");
           vdsinethetaback.SetMinimum(1);
           tree->Draw("vdsine:thetaback>>vdsinethetaback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           vdsinethetaback.SetTitle("thetaback vs (vpar-Vcn)sinThetaCM [Counts];thetaback [deg];vpar-Vcn");
           vdsinethetaback.Write("",TObject::kOverwrite);
         */
        TH2F vdsinethetaCMback("vdsinethetaCMback","thetaCMback:(vpar-Vcn)sinThetaCM",1000,20,180,1000,-3,3);
        vdsinethetaCMback.SetOption("COLZ");
        vdsinethetaCMback.SetMinimum(1);
        tree->Draw("vdsine:thetaCMback>>vdsinethetaCMback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdsinethetaCMback.SetTitle("thetaCMback vs (vpar-Vcn)sinThetaCM [Counts];thetaCMback [deg];(vpar-Vcn)ThetaCM");
        vdsinethetaCMback.Write("",TObject::kOverwrite);

        TH2F T0dTder("T0dTder","T0der:dTder",1100,0,110,2000,-100,100);
        T0dTder.SetOption("COLZ");
        T0dTder.SetMinimum(1);
        tree->Draw("dTder:T0der>>T0dTder",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        T0dTder.SetTitle("T0der vs dTder [Counts];T0der [ns]; dTder[ns]");
        T0dTder.Write("",TObject::kOverwrite);
    }//2D

    cout << "Produced 2D histograms..." << endl;

    // ----------------

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("HistQC")!=true){
        sfile->mkdir("HistQC");
    }
    sfile->cd("HistQC");
    ///*
    //quadrant corrections - det 1
    {
        TH2F det1_qc12("det1_qc12","x1det:t1",1000,-200,200,1000,-70,170);
        det1_qc12.SetOption("COLZ");
        det1_qc12.SetMinimum(1);
        tree->Draw("t1:x1det>>det1_qc12","quadID_1==1||quadID_1==2"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det1_qc12.SetTitle("det1 quad corr 1+2: x1det vs t1 [Counts]; x1det [mm]; t1 [ns] ");
        det1_qc12.Write("",TObject::kOverwrite);

        TH2F det1_qc23("det1_qc23","t1:y1det",1000,-70,170,1000,-200,200);
        det1_qc23.SetOption("COLZ");
        det1_qc23.SetMinimum(1);
        tree->Draw("y1det:t1>>det1_qc23","quadID_1==2||quadID_1==3"," ");
        det1_qc23.SetTitle("det1 quad corr 2+3: t vs ydet [Counts]; t1 [ns]; y1det [mm] ");
        det1_qc23.Write("",TObject::kOverwrite);

        TH2F det1_qc34("det1_qc34","x1det:t1",1000,-200,200,1000,-70,170);
        det1_qc34.SetOption("COLZ");
        det1_qc34.SetMinimum(1);
        tree->Draw("t1:x1det>>det1_qc34","quadID_1==3||quadID_1==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det1_qc34.SetTitle("det1 quad corr 3+4: x1det vs t1 [Counts]; x1det [mm]; t1 [ns] ");
        det1_qc34.Write("",TObject::kOverwrite);

        TH2F det1_qc41("det1_qc41","t1:y1det",1000,-70,170,1000,-200,200);
        det1_qc41.SetOption("COLZ");
        det1_qc41.SetMinimum(1);
        tree->Draw("y1det:t1>>det1_qc41","quadID_1==1||quadID_1==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det1_qc41.SetTitle("det1 quad corr 1+4: t1 vs y1det [Counts]; t1 [ns]; y1det [mm] ");
        det1_qc41.Write("",TObject::kOverwrite);
    }

    {
        //quadrant corrections - det 2
        TH2F det2_qc12("det2_qc12","x2:t2",1000,-200,200,1000,-70,170);
        det2_qc12.SetOption("COLZ");
        det2_qc12.SetMinimum(1);
        tree->Draw("t2:x2det>>det2_qc12","quadID_2==1||quadID_2==2"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det2_qc12.SetTitle("det2 quad corr 1+2: x2det vs t2 [Counts]; x2det [mm]; t2 [ns] ");
        det2_qc12.Write("",TObject::kOverwrite);

        TH2F det2_qc23("det2_qc23","t2:y2det",1000,-70,170,1000,-200,200);
        det2_qc23.SetOption("COLZ");
        det2_qc23.SetMinimum(1);
        tree->Draw("y2det:t2>>det2_qc23","quadID_2==2||quadID_2==3"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det2_qc23.SetTitle("det2 quad corr 2+3: t vs ydet [Counts]; t2 [ns]; y2det [mm] ");
        det2_qc23.Write("",TObject::kOverwrite);

        TH2F det2_qc34("det2_qc34","x2det:t2",1000,-200,200,1000,-70,170);
        det2_qc34.SetOption("COLZ");
        det2_qc34.SetMinimum(1);
        tree->Draw("t2:x2det>>det2_qc34","quadID_2==3||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det2_qc34.SetTitle("det2 quad corr 3+4: x2det vs t2 [Counts]; x2det [mm]; t2 [ns] ");
        det2_qc34.Write("",TObject::kOverwrite);

        TH2F det2_qc41("det2_qc41","t2:y2det",1000,-70,170,1000,-200,200);
        det2_qc41.SetOption("COLZ");
        det2_qc41.SetMinimum(1);
        tree->Draw("y2det:t2>>det2_qc41","quadID_2==1||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname","",""
        det2_qc41.SetTitle("det2 quad corr 1+4: t2 vs y2det [Counts]; t2 [ns]; y2det [mm] ");
        det2_qc41.Write("",TObject::kOverwrite);
    }

    {
        //quadrant corrections - det 3
        TH2F det3_qc12("det3_qc12","x3:t3",1000,-200,200,1000,-70,170);
        det3_qc12.SetOption("COLZ");
        det3_qc12.SetMinimum(1);
        tree->Draw("t3:x3det>>det3_qc12","quadID_2==1||quadID_2==2"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det3_qc12.SetTitle("det3 quad corr 1+2: x3det vs t3 [Counts]; x3det [mm]; t3 [ns] ");
        det3_qc12.Write("",TObject::kOverwrite);

        TH2F det3_qc23("det3_qc23","t3:y3det",1000,-70,170,1000,-200,200);
        det3_qc23.SetOption("COLZ");
        det3_qc23.SetMinimum(1);
        tree->Draw("y3det:t3>>det3_qc23","quadID_2==2||quadID_2==3"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det3_qc23.SetTitle("det3 quad corr 2+3: t vs ydet [Counts]; t3 [ns]; y3det [mm] ");
        det3_qc23.Write("",TObject::kOverwrite);

        TH2F det3_qc34("det3_qc34","x3det:t3",1000,-200,200,1000,-70,170);
        det3_qc34.SetOption("COLZ");
        det3_qc34.SetMinimum(1);
        tree->Draw("t3:x3det>>det3_qc34","quadID_2==3||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        det3_qc34.SetTitle("det3 quad corr 3+4: x3det vs t3 [Counts]; x3det [mm]; t3 [ns] ");
        det3_qc34.Write("",TObject::kOverwrite);

        TH2F det3_qc41("det3_qc41","t3:y3det",1000,-70,170,1000,-200,200);
        det3_qc41.SetOption("COLZ");
        det3_qc41.SetMinimum(1);
        tree->Draw("y3det:t3>>det3_qc41","quadID_2==1||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname","",""
        det3_qc41.SetTitle("det3 quad corr 1+4: t3 vs y3det [Counts]; t3 [ns]; y3det [mm] ");
        det3_qc41.Write("",TObject::kOverwrite);
    }

    //
    cout << "Produced quadrant correction histograms..." << endl;


    //Close tree file
    delete sfile;

    if (gatebool){
        plot_kin(gatefile,&rxn);
    }
    else{
        plot_kin(procfile,&rxn);
    }

    cout << "Histogram creation complete!" << endl;

    return (0);


}//end CreateHist

/*------------------------------------------------------------------------------------
  UngatedCubeHist
  : Creates ungated histograms. USER SHOULD NOT MODIFY THIS
  -------------------------------------------------------------------------------------*/
int UngatedCubeHist(char* filename,char* outfile){ //function produces ungated monitor histograms

    TTree* CubeTreeNew;
    Long64_t nentries=0;
    int nevent;

    //open root file
    TFile sfile(filename,"UPDATE");
    if (!sfile.IsOpen()){
        std::cout << "CubeHist: Root file " << filename << " not found!" << std::endl;
        return -1;
    }

    if (sfile.GetListOfKeys()->Contains("CubeTreeNew")==true){

        CubeTreeNew = (TTree*)sfile.Get("CubeTreeNew");
        nentries=CubeTreeNew->GetEntries();

        nevent = nentries;

        if (nentries > 11000000){
            cout << "Raw tree too large to avoid memory issues; use unprocessed file to view ungated monitor histograms." << endl;
            return -1;
        }

    }
    else{
        cout << "Make sure your processed, ungated root file name is correct. CubeTreeNew not found in"<< endl;
        cout << filename << endl;
        return -1;
    }

    // gStyle.SetCanvasColor(0);
    // gStyle.SetStatBorderSize(1);

    if (sfile.GetListOfKeys()->Contains("Raw data")!=true){
        sfile.mkdir("Raw data");
    }

    // ##### 1D histograms #####
    sfile.cd("/");
    sfile.cd("Raw data");

    //monitor 1 energy
    TH1I mon1E_noGate("mon1E_noGate","mon1E_noGate",1048,0,1048);
    CubeTreeNew->Draw("Mon1E>>mon1E_noGate","","");
    mon1E_noGate.SetTitle("Monitor 1 Energy;Channels;Counts");
    mon1E_noGate.Write();

    //monitor 2 energy
    TH1I mon2E_noGate("mon2E_noGate","mon2E_noGate",1048,0,1048);
    CubeTreeNew->Draw("Mon2E>>mon2E_noGate","","");
    mon2E_noGate.SetTitle("Monitor 2 Energy;Channels;Counts");
    mon2E_noGate.Write();


    // ##### 2D histograms #####
    // Add src_v4.3
    TH2F TbackEvt("TbackEvt","raw_T1:EvtNo",8192,0,nevent,8192,1,8192);
    TbackEvt.SetOption("COLZ");
    TbackEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T1:EvtNo>>TbackEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TbackEvt.SetTitle("raw_T1 vs EvtNo [Counts];Evt No;raw_T1 [ch]");// title;x-axis;y-axis

    TH2F TfrontEvt("TfrontEvt","raw_T2:EvtNo",8192,0,nevent,8192,1,-8192);
    TfrontEvt.SetOption("COLZ");
    TfrontEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T2:EvtNo>>TfrontEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TfrontEvt.SetTitle("raw_T2 vs EvtNo [Counts];Evt No;raw_T2 [ch]");// title;x-axis;y-axis

    TH2F TsmallbackEvt("TsmallbackEvt","raw_T3:EvtNo",8192,0,nevent,8192,1,8192);
    TsmallbackEvt.SetOption("COLZ");
    TbackEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T3:EvtNo>>TsmallbackEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TsmallbackEvt.SetTitle("raw_T3 vs EvtNo [Counts];Evt No;raw_T3 [ch]");// title;x-axis;y-axis


    //open gated root file
    TFile ofile(outfile,"UPDATE");
    if (!ofile.IsOpen()){
        std::cout << "CubeHist: Gated Root file "<< outfile << " not found!" << std::endl;
        return -1;
    }

    if (ofile.GetListOfKeys()->Contains("Raw data")!=true){
        ofile.mkdir("Raw data");
    }

    ofile.cd("/");
    ofile.cd("Raw data");

    mon1E_noGate.Write("",TObject::kOverwrite);
    mon2E_noGate.Write("",TObject::kOverwrite);
    TbackEvt.Write("",TObject::kOverwrite);
    TfrontEvt.Write("",TObject::kOverwrite);
    TsmallbackEvt.Write("",TObject::kOverwrite);

    cout << "Ungated monitor histograms created." << endl;

    ofile.Close();

    return 0;

}//end UngatedCubeHist
