#ifndef HIST_H

#include <iostream>
#include <fstream>

#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
//#include "TStyle.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"

#include "CubeStruct.h"
#include "CubeInput.h"
#include "CubeReact.h"
#include "plot_kin.h"

int UngatedCubeHist(char*,char*);
int CreateHist(char*, bool);

#endif
