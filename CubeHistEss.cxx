/*
   CubeHistEss.C : Histogram generator for daCube. Dependencies: root libraries, CubeHistEss.h, UserHist.h

   Users should add code defining any additional histograms to UserHist.h
 */


#include "CubeHistEss.h"

using namespace std;

extern int UngatedCubeHistEss(char* filename,char* outfile);

/*------------------------------------------------------------------------------------
  CreateHistEss
  : Creates only interested histogrames. Run options are -he, -hge
  - Yun Jeung, May 2019 update
  -------------------------------------------------------------------------------------*/
int CreateHistEss(char* inputfile, bool gatebool){

    TFile *sfile;
    TTree *tree;
    TTree *rtree;

    char *procfile;
    char *gatefile;
    string junk, sortf, finf;

    //define and initialize input structures (see CubeStruct.h)
    sortInfo sortStruct={"","","",0,0,0,0,0,0,0,0,"","",""};
    //not used but assumed in readInput, so I've defined structures again here
    reactPar rxn={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//src_3det_v5 +2
    targetPar targ={0,0,0,0,0,0,0,{0,0}};

    // read in input file information and store in appropriate place
    int errCheck=readInput(inputfile,&sortStruct,&rxn,&targ);

    float slope = 2*(90-rxn.intercept)*1.0;
    int acn = rxn.Ap + rxn.At;
    //int zcn = rxn.Zp + rxn.Zt;
    //ratio = rxn.TestPar;
    int angbin = 90; // 60 (3deg) 90 (2deg.)

    cout<< ""<<endl;
    cout << "-------------------MAD Mirror Fuction--------------------------------" << endl;
    cout << " Intercept = " << rxn.intercept << " deg.\t\t" << "slope = " << slope  << endl;
    cout << "------------------------------------------------------------------------" << endl;

    if ( errCheck == -1 ){
        cout << "Input file read-in error. Sort failed." << endl;
        return -1;
    }

    // if (rxn.elossBool==true){
    if (rxn.dEcalcType==1 || rxn.dEcalcType==2 || rxn.dEcalcType==3 ){
        calcElossBeam(&targ,&rxn);
    }

    string proc;
    proc=sortStruct.procfile;
    procfile=&proc[0];
    string gatf;
    gatf=sortStruct.gatefile;
    gatefile=&gatf[0];

    if (gatebool){
        UngatedCubeHistEss(procfile,gatefile); // make and save ungated monitor histograms to final gated tree

        //sfile=new TFile(procfile,"UPDATE");
        sfile=TFile::Open(gatefile,"UPDATE");
        if (!sfile->IsOpen()){
            cout << "CubeHist: Root file " << gatefile << " not found!" << endl;
            return -1;
        }
        if (sfile->GetListOfKeys()->Contains("GatedTree")==true){
            tree = (TTree*) sfile->Get("GatedTree");
            rtree = (TTree*) sfile->Get("GatedRawTree");
        }
        else{
            cout << "Make sure your root file name is correct. GatedTree not found in"<< endl;
            cout << gatefile << endl;
            return -1;
        }

    }
    else
    {
        //sfile=new TFile(procfile,"UPDATE");
        sfile=TFile::Open(procfile,"UPDATE");
        if (!sfile->IsOpen()){
            cout << "CubeHist: Root file " << procfile << " not found!" << endl;
            return -1;
        }
        if (sfile->GetListOfKeys()->Contains("CubeTreeNew")==true){
            tree = (TTree*) sfile->Get("CubeTreeNew");
            rtree = (TTree*) sfile->Get("RawTreeNew");
        }
        else{
            cout << "Make sure your processed, ungated root file name is correct. CubeTreeNew not found in"<< endl;
            cout << procfile << endl;
            return -1;
        }
    }


    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("UHist")!=true){
        sfile->mkdir("UHist");
    }
    sfile->cd("UHist");

#define USER_HIST
#include "my_code.h"
#undef USER_HIST


    // ##### raw histograms #####

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("Raw data")!=true){
        sfile->mkdir("Raw data");
    }
    sfile->cd("Raw data");

    //1D raw
    {

        //monitor 1 energy
        TH1I mon1E("mon1E","mon1E",1024,0,1024);
        tree->Draw("Mon1E>>mon1E","","");
        mon1E.SetTitle("Monitor 1 Energy;Channels;Counts");
        mon1E.Write("",TObject::kOverwrite);

        //monitor 2 energy
        TH1I mon2E("mon2E","mon2E",1024,0,1024);
        tree->Draw("Mon2E>>mon2E","","");
        mon2E.SetTitle("Monitor 2 Energy;Channels;Counts");
        mon2E.Write("",TObject::kOverwrite);
        //2016Linac
        TH1I monTAC("monTAC","monTAC",1024,0,1024);
        rtree->Draw("MonTAC>>monTAC","","");
        monTAC.SetTitle("Monitor Tac;Channels;Counts");
        monTAC.Write("",TObject::kOverwrite);

    }//1D raw

    //2D raw
    {

        TH2I x1_y1raw("x1_y1raw","x1_y1raw",2500,-5000,5000,2500,-5000,5000);
        x1_y1raw.SetOption("COLZ");
        x1_y1raw.SetMinimum(1);
        tree->Draw("raw_y1:raw_x1>>x1_y1raw",""," ");
        x1_y1raw.SetTitle("raw_x1 vs raw_y1 [Counts]; raw_x1 [ch];raw_y1 [ch]");
        x1_y1raw.Write("",TObject::kOverwrite);

        //TH2I x2_y2raw("x2_y2raw","x2_y2raw",1024,0,2048,1024,0,2048);
        TH2I x2_y2raw("x2_y2raw","x2_y2raw",2500,-5000,5000,2500,-5000,5000);
        x2_y2raw.SetOption("COLZ");
        x2_y2raw.SetMinimum(1);
        tree->Draw("raw_y2:raw_x2>>x2_y2raw",""," ");
        x2_y2raw.SetTitle("raw_x2 vs raw_y2 [Counts]; raw_x2 [ch];raw_y2 [ch]");
        x2_y2raw.Write("",TObject::kOverwrite);

        TH2I x3_y3raw("x3_y3raw","x3_y3raw",2500,-5000,5000,2500,-5000,5000);
        x3_y3raw.SetOption("COLZ");
        x3_y3raw.SetMinimum(1);
        tree->Draw("raw_y3:raw_x3>>x3_y3raw",""," ");
        x3_y3raw.SetTitle("raw_x3 vs raw_y3 [Counts; raw_x3 [ch];raw_y3 [ch]");
        x3_y3raw.Write("",TObject::kOverwrite);

        TH2I T1_T2raw("T1_T2raw","T1_T2raw",2000,0,4000,1600,-500,1500);
        T1_T2raw.SetOption("COLZ");
        T1_T2raw.SetMinimum(1);
        tree->Draw("raw_T2:raw_T1>>T1_T2raw",""," ");
        T1_T2raw.SetTitle("raw_T1 vs raw_T2 [Counts];raw_T1 [ch];raw_T2 [ch]");
        T1_T2raw.Write("",TObject::kOverwrite);

        //SMALL BACK DETECTOR& FRONT
        TH2I T3_T2raw("T3_T2raw","T3_T2raw",1000,-4000,2500,1000,-500,600);
        T3_T2raw.SetOption("COLZ");
        T3_T2raw.SetMinimum(1);
        tree->Draw("raw_T2:raw_T3>>T3_T2raw",""," ");
        T3_T2raw.SetTitle("raw_T3 vs raw_T2 [Counts];raw_T3 [ch];raw_T2 [ch]");
        T3_T2raw.Write("",TObject::kOverwrite);

        TH2I T1_E1raw("T1_E1raw","T1_E1raw",2800,0,2800,250,0,250);
        T1_E1raw.SetOption("COLZ");
        T1_E1raw.SetMinimum(1);
        tree->Draw("raw_E1:raw_T1>>T1_E1raw",""," ");
        T1_E1raw.SetTitle("raw_T1 vs raw_E1 [Counts];raw_T1 [ch];raw_E1 [ch]");
        T1_E1raw.Write("",TObject::kOverwrite);

        TH2I T2_E2raw("T2_E2raw","T2_E2raw",1000,0,1000,500,0,500);
        T2_E2raw.SetOption("COLZ");
        T2_E2raw.SetMinimum(1);
        tree->Draw("raw_E2:raw_T2>>T2_E2raw",""," ");
        T2_E2raw.SetTitle("raw_T2 vs raw_E2 [Counts];raw_T2 [ch];raw_E2 [ch]");
        T2_E2raw.Write("",TObject::kOverwrite);

        TH2I T3_E3raw("T3_E3raw","T3_E3raw",1000,-1000,2000,500,0,300);
        T3_E3raw.SetOption("COLZ");
        T3_E3raw.SetMinimum(1);
        tree->Draw("raw_E3:raw_T3>>T3_E3raw",""," ");
        T3_E3raw.SetTitle("raw_T3 vs raw_E3 [Counts];raw_T3 [ch];raw_E3 [ch]");
        T3_E3raw.Write("",TObject::kOverwrite);

        TH2I tback_T2raw("tback_T2raw","tback_T2raw",2000,0,4000,1600,-500,1500);
        tback_T2raw.SetOption("COLZ");
        tback_T2raw.SetMinimum(1);
        tree->Draw("raw_T2:raw_tback>>tback_T2raw",""," ");
        tback_T2raw.SetTitle("raw_tback vs raw_T2 [Counts];raw_tback [ch];raw_T2 [ch]");
        tback_T2raw.Write("",TObject::kOverwrite);


    }//2D raw

    std::cout << "Produced histograms of raw data..." << std::endl;

    // ##### Derived or calibrated histograms #####

    //1D

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("Hist1D")!=true){
        sfile->mkdir("Hist1D");
    }
    sfile->cd("Hist1D");

    {

        TH1D vpar("vpar","vpar",500,-10,40);
        tree->Draw("vpar>>vpar","","");
        vpar.SetTitle("vpar;vpar[mm/ns];Counts");
        vpar.Write("",TObject::kOverwrite);

        TH1D vperp("vperp","vperp",500,-5,5);
        tree->Draw("vperp>>vperp","","");
        vperp.SetTitle("vperp;vperp[mm/ns];Counts");
        vperp.Write("",TObject::kOverwrite);

        TH1D Mr("Mr","Mr",500,0,1);
        tree->Draw("MR>>MR","","");
        Mr.SetTitle("MR;MR;Counts");
        Mr.Write("",TObject::kOverwrite);

        TH1D DetectorChoice("DetectorChoice","DetectorChoice",10,0,10);
        tree->Draw("DetChoice>>DetectorChoice","","");
        DetectorChoice.SetTitle("DetectorChoice;Choice;Counts");
        DetectorChoice.Write("",TObject::kOverwrite);

        TH1D tdifference("tdifference","tdifference",2000,-500,500);
        tree->Draw("tdiff>>tdifference","","");
        tdifference.SetTitle("tdiff;tdiff[ns];Counts");
        tdifference.Write("",TObject::kOverwrite);

        TH1D tdiff12("tdiff12","tdiff12",2000,-500,500);
        tree->Draw("tdiff12>>tdiff12","","");
        tdiff12.SetTitle("tdiff12;tdiff12[ns];Counts");
        tdiff12.Write("",TObject::kOverwrite);

        TH1D tdiff32("tdiff32","tdiff32",2000,-500,500);
        tree->Draw("tdiff32>>tdiff32","","");
        tdiff32.SetTitle("tdiff32;tdiff32[ns];Counts");
        tdiff32.Write("",TObject::kOverwrite);

    }

    cout << "Produced 1D histograms..." << endl;

    // 2D Histograms

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("Hist2D")!=true){
        sfile->mkdir("Hist2D");
    }
    sfile->cd("Hist2D");

    {
        TH2D x1_y1("x1_y1","x1_y1",400,-200,200,400,-200,200);
        x1_y1.SetOption("COLZ");
        x1_y1.SetMinimum(1);
        tree->Draw("y1det:x1det>>x1_y1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        x1_y1.SetTitle("x1 vs y1 in detector reference frame [Counts]; x1det [mm];y1det [mm]");
        x1_y1.Write("",TObject::kOverwrite);

        TH2D x2_y2("x2_y2","x2_y2",400,-200,200,400,-200,200);
        x2_y2.SetOption("COLZ");
        x2_y2.SetMinimum(1);
        tree->Draw("y2det:x2det>>x2_y2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        x2_y2.SetTitle("x2 vs y2 in detector reference frame [Counts]; x2det [mm];y2det [mm]");
        x2_y2.Write("",TObject::kOverwrite);

        TH2D x3_y3("x3_y3","x3_y3",400,-200,200,400,-200,200);
        x3_y3.SetOption("COLZ");
        x3_y3.SetMinimum(1);
        tree->Draw("y3det:x3det>>x3_y3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        x3_y3.SetTitle("x3 vs y3 in detector reference frame [Counts]; x3det [mm];y3det [mm]");
        x3_y3.Write("",TObject::kOverwrite);

        // TH2D t1_t2("t1_t2","t1_t2",1000,-500,500,1000,-500,500);
        TH2D t1_t2("t1_t2","t1_t2",200,-20,100,200,0,55);
        t1_t2.SetOption("COLZ");
        t1_t2.SetMinimum(1);
        tree->Draw("t2:t1>>t1_t2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t1_t2.SetTitle("t1 vs t2 [Counts];t1 [ns];t2 [ns]");
        t1_t2.Write("",TObject::kOverwrite);

        //TH2D t3_t2("t3_t2","t3_t2",1000,-500,500,1000,-500,500);
        TH2D t3_t2("t3_t2","t3_t2",200,-20,100,200,0,55);
        t3_t2.SetOption("COLZ");
        t3_t2.SetMinimum(1);
        tree->Draw("t2:t3>>t3_t2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        t3_t2.SetTitle("t3 vs t2 [Counts];t3 [ns];t2 [ns]");
        t3_t2.Write("",TObject::kOverwrite);

        TH2D th1_phi1("th1_phi1","th1_phi1",1400,20,160,2000,100,300);
        th1_phi1.SetOption("COLZ");
        th1_phi1.SetMinimum(1);
        tree->Draw("phi1:theta1>>th1_phi1",""," ");
        th1_phi1.SetTitle("theta1 vs phi1 [Counts];theta1 [deg];phi1 [deg]");
        th1_phi1.Write("",TObject::kOverwrite);

        TH2D th2_phi2("th2_phi2","th2_phi2",1800,0,180,3600,-180,180);
        th2_phi2.SetOption("COLZ");
        th2_phi2.SetMinimum(1);
        tree->Draw("phi2:theta2>>th2_phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        th2_phi2.SetTitle("theta2 vs phi2 [Counts];theta2 [deg];phi2 [deg]");
        th2_phi2.Write("",TObject::kOverwrite);

        TH2D th3_phi3("th3_phi3","th3_phi3",1800,0,180,2000,100,300);
        th3_phi3.SetOption("COLZ");
        th3_phi3.SetMinimum(1);
        tree->Draw("phi3:theta3>>th3_phi3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        th3_phi3.SetTitle("theta3 vs phi3 [Counts];theta3 [deg];phi3 [deg]");
        th3_phi3.Write("",TObject::kOverwrite);
        /*
           TH2D th1_th2("th1_th2","th1_th2",1800,0,180,1800,0,180);
           th1_th2.SetOption("COLZ");
           th1_th2.SetMinimum(1);
           tree->Draw("theta2:theta1>>th1_th2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           th1_th2.SetTitle("theta1 vs theta2 [Counts];theta1 [deg];theta2 [deg]");
           th1_th2.Write("",TObject::kOverwrite);
         */
        TH2D phi1_phi2("phi1_phi2","phi1_phi2",1800,0,360,3600,-180,180);
        phi1_phi2.SetOption("COLZ");
        phi1_phi2.SetMinimum(1);
        tree->Draw("phi2:phi1>>phi1_phi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        phi1_phi2.SetTitle("phi1 vs phi2 [Counts];phi1 [deg];phi2 [deg]");
        phi1_phi2.Write("",TObject::kOverwrite);

        TH2D phi1_phi12("phi1_phi12","phi1_phi12",2000,100,300,3600,0,360);
        phi1_phi12.SetOption("COLZ");
        phi1_phi12.SetMinimum(1);
        tree->Draw("phi12:phi1>>phi1_phi12",""," ");
        phi1_phi12.SetTitle("phi1 vs phi12 [Counts];phi1 [deg];phi12 [deg]");
        phi1_phi12.Write("",TObject::kOverwrite);

        TH2D phi2_phi12("phi2_phi12","phi2_phi12",2000,-100,100,3600,0,360);
        phi2_phi12.SetOption("COLZ");
        phi2_phi12.SetMinimum(1);
        tree->Draw("phi12:phi2>>phi2_phi12",""," ");
        phi2_phi12.SetTitle("phi2 vs phi12 [Counts];phi2 [deg];phi2-phi1 [deg]");
        phi2_phi12.Write("",TObject::kOverwrite);

        TH2D phi3_phi12("phi3_phi12","phi3_phi12",2000,100,300,3600,0,360);
        phi3_phi12.SetOption("COLZ");
        phi3_phi12.SetMinimum(1);
        tree->Draw("phi12:phi3>>phi3_phi12",""," ");
        phi3_phi12.SetTitle("phi3 vs phi12 [Counts];phi3 [deg];phi2-phi1 [deg]");
        phi3_phi12.Write("",TObject::kOverwrite);

        /*
           TH2D folding("folding","theta1:theta12",1800,0,180,2700,0,270);
           folding.SetOption("COLZ");
           folding.SetMinimum(1);
           tree->Draw("theta12:theta1>>folding",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           folding.SetTitle("thetalab1 vs theta1+theta2 (folding angle) [Counts];thetaLAB_1 [deg];theta12 [deg]");
           folding.Write("",TObject::kOverwrite);

           TH2D folding3("folding3","theta3:theta12",1800,0,180,2700,0,270);
           folding3.SetOption("COLZ");
           folding3.SetMinimum(1);
           tree->Draw("theta12:theta3>>folding3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           folding3.SetTitle("thetalab3 vs theta1+theta2 (folding angle) [Counts];thetaLAB_3 [deg];theta12 [deg]");
           folding3.Write("",TObject::kOverwrite);
         */

        TH2D folding("folding","thetaback:thetaback2",1800,0,180,2700,0,270);
        folding.SetOption("COLZ");
        folding.SetMinimum(1);
        tree->Draw("thetaback+theta2:thetaback>>folding",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        folding.SetTitle("thetaback vs thetaback+theta2 (folding angle) [Counts];thetaback [deg];thetaback2 [deg]");
        folding.Write("",TObject::kOverwrite);

        TH2D mrvr("mrvr","MR:Vpar/Vcn",1000,0,1,400,-1,3);
        mrvr.SetOption("COLZ");
        mrvr.SetMinimum(1);
        tree->Draw("vpar/Vcn:MR>>mrvr",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrvr.SetTitle("MR vs vpar/Vcn [Counts];Mass Ratio; vpar/Vcn [mm/ns]");
        mrvr.Write("",TObject::kOverwrite);

        TH2D mrth1("mrth1","MR:Theta1",1000,0,1,1400,20,160);
        mrth1.SetOption("COLZ");
        mrth1.SetMinimum(1);
        tree->Draw("theta1:MR>>mrth1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrth1.SetTitle("MR vs Theta [Counts];Mass Ratio; Theta1 [deg]");
        mrth1.Write("",TObject::kOverwrite);

        TH2D mrth2("mrth2","MR:Theta2",1000,0,1,1200,0,120);
        mrth2.SetOption("COLZ");
        mrth2.SetMinimum(1);
        tree->Draw("theta2:1.-MR>>mrth2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrth2.SetTitle("MR vs theta2 [Counts];Mass Ratio; Theta2 [deg]");
        mrth2.Write("",TObject::kOverwrite);

        TH2D mrthcm1("mrthcm1","MR:thetaCM1",1000,0,1,3600,0,360);
        mrthcm1.SetOption("COLZ");
        mrthcm1.SetMinimum(1);
        tree->Draw("thetaCM1:MR>>mrthcm1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrthcm1.SetTitle("MR vs thetaCM [Counts];Mass Ratio; thetaCM1 [deg]");
        mrthcm1.Write("",TObject::kOverwrite);

        TH2D mrthcm2("mrthcm2","MR:thetaCM2",1000,0,1,2000,0,180);
        mrthcm2.SetOption("COLZ");
        mrthcm2.SetMinimum(1);
        tree->Draw("thetaCM2:1.-MR>>mrthcm2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrthcm2.SetTitle("MR vs thetaCM [Counts];Mass Ratio; thetaCM2 [deg]");
        mrthcm2.Write("",TObject::kOverwrite);

        TH2D mrthcm3("mrthcm3","MR:thetaCM3",1000,0,1,3600,0,360);
        mrthcm3.SetOption("COLZ");
        mrthcm3.SetMinimum(1);
        tree->Draw("thetaCM3:MR>>mrthcm3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrthcm3.SetTitle("MR vs thetaCM [Counts];Mass Ratio; thetaCM3 [deg]");
        mrthcm3.Write("",TObject::kOverwrite);

        TH2D tempA("tempA","tempA",1000,0,1,1800,0,180);

        tree->Draw("thetaCM1:(MR)>>tempA",""," ");

        TH2D tempB("tempB","tempB",1000,0,1,1800,0,180);

        tree->Draw("thetaCM3:(MR)>>tempB",""," ");

        TH2D mrthcm13("mrthcm13","mrthcm13",1000,0,1,1800,0,180);
        mrthcm13.SetOption("COLZ");
        mrthcm13.SetMinimum(1);
        mrthcm13.Add(&mrthcm13, &tempA);
        mrthcm13.Add(&mrthcm13, &tempB);
        mrthcm13.SetTitle("MR vs thetaCM1 and thetaCM3 [Counts]; Mass Ratio; thetaCM1 and thetaCM3 [deg]");
        mrthcm13.Write("",TObject::kOverwrite);
        /*------------- NEW -------------------*/
        //mass vs thetaCM
        TH2D massthcm1("massthcm1","MR:thetaCM1",acn+0.5,0,acn+0.5,180,0,360);
        //TH2D massthcm1("massthcm1","MR:thetaCM1",248,0.5,248+0.5,180,0,360);
        massthcm1.SetOption("COLZ");
        massthcm1.SetMinimum(1);
        tree->Draw("thetaCM1:mass1>>massthcm1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        massthcm1.SetTitle("mass1 [amu] vs thetaCM [Counts];mass [amu]; thetaCM1 [deg]");
        massthcm1.Write("",TObject::kOverwrite);
        TH2D massthcm3("massthcm3","MR:thetaCM3",acn,0.5,acn+0.5,180,0,360);
        //TH2D massthcm3("massthcm3","MR:thetaCM3",248,0.5,248+0.5,180,0,360);
        massthcm3.SetOption("COLZ");
        massthcm3.SetMinimum(1);
        tree->Draw("thetaCM3:mass1>>massthcm3",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        massthcm3.SetTitle("mass1 [amu] vs thetaCM [Counts];mass1 [amu]; thetaCM3 [deg]");
        massthcm3.Write("",TObject::kOverwrite);

        // Histograms with combined back parameters. JW Jun16
        TH2D tfront_tback("tfront_tback","t2:tback",200,-20,100,200,0,55);
        //TH2D tfront_tback("tfront_tback","t2:tback",1000,-20,200,1000,-20,100);
        tfront_tback.SetOption("COLZ");
        tfront_tback.SetMinimum(1);
        tree->Draw("t2:tback>>tfront_tback",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
        tfront_tback.SetTitle("t2 vs tback [Counts];tback [ns];t2 [ns]");
        tfront_tback.Write("",TObject::kOverwrite);

        TH2D thetafront_thetaback("thetafront_thetaback","theta2:thetaback",1000,0,180,1000,0,360);
        thetafront_thetaback.SetOption("COLZ");
        thetafront_thetaback.SetMinimum(1);
        tree->Draw("theta2:thetaback>>thetafront_thetaback",""," "); //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetafront_thetaback.SetTitle("theta2 vs thetaback [Counts];thetaback [deg];theta2 [deg]");
        thetafront_thetaback.Write("",TObject::kOverwrite);

        TH2D thetaback_phiback("thetaback_phiback","phiback:thetaback",1000,0,180,1000,100,300);
        thetaback_phiback.SetOption("COLZ");
        thetaback_phiback.SetMinimum(1);
        tree->Draw("phiback:thetaback>>thetaback_phiback",""," "); //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaback_phiback.SetTitle("phiback vs thetaback [Counts];thetaback [deg];phiback [deg]");
        thetaback_phiback.Write("",TObject::kOverwrite);

        TH2D thetaCMback_phiback("thetaCMback_phiback","phiback:thetaCMback",1000,0,180,1000,80,300);
        thetaCMback_phiback.SetOption("COLZ");
        thetaCMback_phiback.SetMinimum(1);
        tree->Draw("phiback:thetaCMback>>thetaCMback_phiback",""," ");
        //Format of draw argument: "yaxis:xaxis>>histname",""," "
        thetaCMback_phiback.SetTitle("phiback vs thetaCMback [Counts];thetaCMback [deg];phiback [deg]");
        thetaCMback_phiback.Write("",TObject::kOverwrite);

    }

    //mirrored histograms

    {
        TH2D temp("temp","temp",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp","thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM1):(1.-MR)>>temp",TString::Format("thetaCM1 > 90. + %g * (MR-0.5)",slope),"");

        TH2D temp2("temp2","temp2",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM1:MR>>temp2","thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("thetaCM1:MR>>temp2","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM1:MR>>temp2",TString::Format("thetaCM1 > 90. + %g*(MR-0.5)",slope)," ");

        TH2D mrthcm1_mir("mrthcm1_mir","mrthcm1_mir",1000,0,1,1800,0,180);
        mrthcm1_mir.SetOption("COLZ");
        mrthcm1_mir.SetMinimum(1);
        mrthcm1_mir.Add(&mrthcm1_mir, &temp);
        mrthcm1_mir.Add(&mrthcm1_mir, &temp2);
        mrthcm1_mir.SetTitle("MR vs thetaCM1 (mirrored) [Counts]; Mass Ratio; thetaCM1 [deg]");
        mrthcm1_mir.Write("",TObject::kOverwrite);

        TH2D temp3("temp3","temp3",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM2):MR>>temp3","thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM2):MR>>temp3","thetaCM2 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM2):MR>>temp3",TString::Format("thetaCM2 > 90. + %g * (MR-0.5)",slope)," ");

        TH2D temp4("temp4","temp4",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM2:1.-MR>>temp4","thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR"," "); //condition from Renju's code
        //tree->Draw("thetaCM2:1.-MR>>temp4","thetaCM2 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM2:1.-MR>>temp4",TString::Format("thetaCM2 > 90. + %g *(MR-0.5)",slope)," ");

        TH2D mrthcm2_mir("mrthcm2_mir","mrthcm2_mir",1000,0,1,1800,0,180);
        mrthcm2_mir.SetOption("COLZ");
        mrthcm2_mir.SetMinimum(1);
        mrthcm2_mir.Add(&mrthcm2_mir, &temp3);
        mrthcm2_mir.Add(&mrthcm2_mir, &temp4);
        mrthcm2_mir.SetTitle("MR vs thetaCM2 (mirrored) [Counts]; Mass Ratio; thetaCM2 [deg]");
        mrthcm2_mir.Write("",TObject::kOverwrite);

        TH2D tempx("tempx","tempx",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>tempx","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM1):(1.-MR)>>tempx",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

        TH2D tempy("tempy","tempy",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM1:MR>>tempy","thetaCM1 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM1:MR>>tempy",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

        TH2D tempz("tempz","tempz",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM3):(1.-MR)>>tempz","thetaCM3 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("(180.-thetaCM3):(1.-MR)>>tempz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

        TH2D tempzz("tempzz","tempzz",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM3:MR>>tempzz","thetaCM3 > 90. + 74.*(MR-0.5)"," ");
        tree->Draw("thetaCM3:MR>>tempzz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

        TH2D mrthcm13_mir("mrthcm13_mir","mrthcm13_mir",1000,0,1,1800,0,180);
        mrthcm13_mir.SetOption("COLZ");
        mrthcm13_mir.SetMinimum(1);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempx);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempy);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempz);
        mrthcm13_mir.Add(&mrthcm13_mir, &tempzz);
        mrthcm13_mir.SetTitle("MR vs thetaCM13 (mirrored) [Counts]; Mass Ratio; thetaCM13 [deg]");
        mrthcm13_mir.Write("",TObject::kOverwrite);

        /*------------- NEW -------------------*/
        //mass vs thetaCM
                TH2D tmpx("tmpx","tmpx",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("(180.-thetaCM1):mass2>>tmpx",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

                TH2D tmpy("tmpy","tmpy",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("thetaCM1:mass1>>tmpy",TString::Format("thetaCM1 > 90. + %g *(MR-0.5)",slope),"");

                TH2D tmpz("tmpz","tmpz",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("(180.-thetaCM3):mass2>>tmpz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

                TH2D tmpzz("tmpzz","tmpzz",acn,0.5,acn+0.5,angbin,0,180);
                tree->Draw("thetaCM3:mass1>>tmpzz",TString::Format("thetaCM3 > 90. + %g *(MR-0.5)",slope),"");

                TH2D massthcm13_mir("massthcm13_mir","massthcm13_mir",acn,0.5,acn+0.5,angbin,0,180);
                massthcm13_mir.SetOption("COLZ");
                massthcm13_mir.SetMinimum(1);
                massthcm13_mir.Add(&massthcm13_mir, &tmpx);
                massthcm13_mir.Add(&massthcm13_mir, &tmpy);
                massthcm13_mir.Add(&massthcm13_mir, &tmpz);
                massthcm13_mir.Add(&massthcm13_mir, &tmpzz);
                massthcm13_mir.SetTitle("Mass vs thetaCM13 (mirrored) [Counts]; Mass [amu]; thetaCM13 [deg]");
                massthcm13_mir.Write("",TObject::kOverwrite);



        TH2D temp7("temp7","temp7",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp7","TKE/TKEViola*(thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM1):(1.-MR)>>temp7","TKE/TKEViola*(thetaCM1 > 90. + 74.*(MR-0.5))"," "); //gayatri
        tree->Draw("(180.-thetaCM1):(1.-MR)>>temp7",TString::Format("TKE/TKEViola*(thetaCM1 > 90. + %g * (MR-0.5))",slope)," ");

        TH2D temp8("temp8","temp8",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM1:MR>>temp8","TKE/TKEViola*(thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("thetaCM1:MR>>temp8","TKE/TKEViola*(thetaCM1 > 90. + 74.*(MR-0.5))"," ");
        tree->Draw("thetaCM1:MR>>temp8",TString::Format("TKE/TKEViola*(thetaCM1 > 90. + %g * (MR-0.5))",slope)," ");

        TH2D mrthcm1_TKE("mrthcm1_TKE","mrthcm1_TKE",1000,0,1,1800,0,180);
        mrthcm1_TKE.SetOption("COLZ");
        mrthcm1_TKE.SetMinimum(1);
        mrthcm1_TKE.Add(&mrthcm1_TKE, &temp7);
        mrthcm1_TKE.Add(&mrthcm1_TKE, &temp8);
        mrthcm1_TKE.SetTitle("MR vs thetaCM1 (mirrored) [TKE/TKEViola]; Mass Ratio; thetaCM1 [deg]");
        mrthcm1_TKE.Divide(&mrthcm1_TKE, &mrthcm1_mir);
        mrthcm1_TKE.SetMaximum(1.825);
        mrthcm1_TKE.SetMinimum(0.825);
        mrthcm1_TKE.Write("",TObject::kOverwrite);

        TH2D temp5("temp5","temp5",1000,0,1,1800,0,180);
        //tree->Draw("(180.-thetaCM2):MR>>temp5","TKE/TKEViola*(thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("(180.-thetaCM2):MR>>temp5","TKE/TKEViola*(thetaCM2 > 90. + 74.*(MR-0.5))"," ");
        tree->Draw("(180.-thetaCM2):MR>>temp5",TString::Format("TKE/TKEViola*(thetaCM2 > 90. + %g * (MR-0.5))",slope)," ");

        TH2D temp6("temp6","temp6",1000,0,1,1800,0,180);
        //tree->Draw("thetaCM2:1.-MR>>temp6","TKE/TKEViola*(thetaCM2 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR)"," "); //condition from Renju's code
        //tree->Draw("thetaCM2:1.-MR>>temp6","TKE/TKEViola*(thetaCM2 > 90. + 74.*(MR-0.5))"," ");
        tree->Draw("thetaCM2:1.-MR>>temp6",TString::Format("TKE/TKEViola*(thetaCM2 > 90. + %g * (MR-0.5))",slope)," ");

        TH2D mrthcm2_TKE("mrthcm2_TKE","mrthcm2_TKE",1000,0,1,1800,0,180);
        mrthcm2_TKE.SetOption("COLZ");
        mrthcm2_TKE.SetMinimum(1);
        mrthcm2_TKE.Add(&mrthcm2_TKE, &temp5);
        mrthcm2_TKE.Add(&mrthcm2_TKE, &temp6);
        mrthcm2_TKE.SetTitle("MR vs thetaCM2 (mirrored) [TKE/TKEViola]; Mass Ratio; thetaCM2 [deg]");
        mrthcm2_TKE.Divide(&mrthcm2_TKE, &mrthcm2_mir);
        mrthcm2_TKE.SetMaximum(1.825);
        mrthcm2_TKE.SetMinimum(0.825);
        mrthcm2_TKE.Write("",TObject::kOverwrite);
        //end mirrored histograms

        //make plot showing mirror line
        //TF1 mirrorfunc("mirrorfunc","43.125 + 197.917*x - 312.5*x*x + 208.33*x*x*x",0,1);
        //TF1 mirrorfunc("mirrorfunc","90.0 + 74.0*(x-0.5)",0,1);
        TF1 mirrorfunc("active_mirrorfunc",TString::Format("90.0 + %g *(x-0.5)",slope),0,1);

        mirrorfunc.Draw();
        mirrorfunc.Write("",TObject::kOverwrite);

        TH2D mrTKEViola("mrTKEViola","MR:TKE/TKEViola",1000,0,1,500,0,5);
        mrTKEViola.SetOption("COLZ");
        mrTKEViola.SetMinimum(1);
        tree->Draw("TKE/TKEViola:MR>>mrTKEViola",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrTKEViola.SetTitle("MR vs TKE/TKEViola [Counts];Mass Ratio;TKE/TKEViola");
        mrTKEViola.Write("",TObject::kOverwrite);
    }

    {
        TH2D mrTKE("mrTKE","MR:TKE",1000,0,1,900,0,400);
        mrTKE.SetOption("COLZ");
        mrTKE.SetMinimum(1);
        tree->Draw("TKE:MR>>mrTKE",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrTKE.SetTitle("MR vs TKE [Counts];Mass Ratio;TKE [MeV]");
        mrTKE.Write("",TObject::kOverwrite);

        TH2D mrphi1("mrphi1","MR:phi1",1000,0,1,2000,100,300);
        mrphi1.SetOption("COLZ");
        mrphi1.SetMinimum(1);
        tree->Draw("phi1:MR>>mrphi1",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrphi1.SetTitle("MR vs phi1 [Counts];Mass Ratio;phi1 [deg]");
        mrphi1.Write("",TObject::kOverwrite);

        TH2D mrphi2("mrphi2","MR:phi2",1000,0,1,2000,-100,100);
        mrphi2.SetOption("COLZ");
        mrphi2.SetMinimum(1);
        tree->Draw("phi2:1.-MR>>mrphi2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        mrphi2.SetTitle("MR vs phi2 [Counts];Mass Ratio;phi2 [deg]");
        mrphi2.Write("",TObject::kOverwrite);


    }

    {

        TH2D tdif_fold("tdif_fold","theta12:t1-t2",2000,-500,500,1800,0,360);
        tdif_fold.SetOption("COLZ");
        tdif_fold.SetMinimum(1);
        tree->Draw("theta12:t1-t2>>tdif_fold",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        tdif_fold.SetTitle(" t1-t2 vs folding angle [Counts];t1-t2 [ns]; theta12 [deg]");
        tdif_fold.Write("",TObject::kOverwrite);

        TH2D v1_v2("v1_v2","v1_v2",1000,0,100,1000,0,100);
        v1_v2.SetOption("COLZ");
        v1_v2.SetMinimum(1);
        tree->Draw("v2:v1>>v1_v2",""," ");
        v1_v2.SetTitle("v1 vs v2 [Counts];v1 [mm/ns];v2 [mm/ns]");
        v1_v2.Write("",TObject::kOverwrite);

        TH2D vcm1_vcm2("vcm1_vcm2","vcm1:vcm2",2000,0,100,2000,0,100);
        vcm1_vcm2.SetOption("COLZ");
        vcm1_vcm2.SetMinimum(1);
        tree->Draw("Vcm2:Vcm1>>vcm1_vcm2",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vcm1_vcm2.SetTitle("Vcm1 vs Vcm2 [Counts];Vcm1 [mm/ns];Vcm2 [mm/ns]");
        vcm1_vcm2.Write("",TObject::kOverwrite);


        TH2D vdvp("vdvp","Vpar-Vcn:vperp",2000,-20,20,1600,-10,10);
        vdvp.SetOption("COLZ");
        vdvp.SetMinimum(1);
        tree->Draw("vperp:vpar-Vcn>>vdvp",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdvp.SetTitle("vpar-Vcn vs vperp [Counts];vpar-vcn [mm/ns]; vperp[mm/ns]");
        vdvp.Write("",TObject::kOverwrite);
        //******************
        TH2D vdsinevp("vdsinevp","(Vpar-Vcn)sinThetaCM:vperp",2000,-20,20,1600,-10,10);
        vdsinevp.SetOption("COLZ");
        vdsinevp.SetMinimum(1);
        tree->Draw("vperp:vdsine>>vdsinevp",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdsinevp.SetTitle("(vpar-Vcn)sinThetaCM vs vperp [Counts];(vpar-vcn)sinThetaCM [mm/ns]; vperp[mm/ns]");
        vdsinevp.Write("",TObject::kOverwrite);

        TH2D vel("vel","Vpar-Vcn:vperp",500,-5,5,500,-5,5);
        vel.SetOption("COLZ");
        vel.SetMinimum(1);
        tree->Draw("vperp*0.1:(vpar-Vcn)*0.1>>vel",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vel.SetTitle("vpar-Vcn vs vperp [Counts];vpar-vcn [cm/ns]; vperp[cm/ns]");
        vel.Write("",TObject::kOverwrite);

        TH2D vrvp("vrvp","Vpar/Vcn:vperp",1000,-5,5,2000,-10,10);
        vrvp.SetOption("COLZ");
        vrvp.SetMinimum(1);
        tree->Draw("vperp:vpar/Vcn>>vrvp",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vrvp.SetTitle("vpar/Vcn vs vperp [Counts];vpar/Vcn ; vperp[mm/ns]");
        vrvp.Write("",TObject::kOverwrite);
        /* // Lab frame
           TH2D vdthetaback("vdthetaback","thetaback:vpar-Vcn",1000,20,180,1000,-3,3);
           vdthetaback.SetOption("COLZ");
           vdthetaback.SetMinimum(1);
           tree->Draw("vpar-Vcn:thetaback>>vdthetaback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           vdthetaback.SetTitle("thetaback vs vpar-Vcn [Counts];thetaback [deg];vpar-Vcn");
           vdthetaback.Write("",TObject::kOverwrite);
         */
        TH2D vdthetaCMback("vdthetaCMback","thetaCMback:vpar-Vcn",1000,20,180,1000,-3,3);
        vdthetaCMback.SetOption("COLZ");
        vdthetaCMback.SetMinimum(1);
        tree->Draw("vpar-Vcn:thetaCMback>>vdthetaCMback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdthetaCMback.SetTitle("thetaCMback vs vpar-Vcn [Counts];thetaCMback [deg];vpar-Vcn");
        vdthetaCMback.Write("",TObject::kOverwrite);

        //******************
        /* //Lab Frame
           TH2D vdsinethetaback("vdsinethetaback","thetaback:(vpar-Vcn)sinThetaCM",1000,20,180,1000,-3,3);
           vdsinethetaback.SetOption("COLZ");
           vdsinethetaback.SetMinimum(1);
           tree->Draw("vdsine:thetaback>>vdsinethetaback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
           vdsinethetaback.SetTitle("thetaback vs (vpar-Vcn)sinThetaCM [Counts];thetaback [deg];(vpar-Vcn)sinThetaCM");
           vdsinethetaback.Write("",TObject::kOverwrite);
         */
        TH2D vdsinethetaCMback("vdsinethetaCMback","thetaCMback:(vpar-Vcn)sinThetaCM",1000,20,180,1000,-3,3);
        vdsinethetaCMback.SetOption("COLZ");
        vdsinethetaCMback.SetMinimum(1);
        tree->Draw("vdsine:thetaCMback>>vdsinethetaCMback",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        vdsinethetaCMback.SetTitle("thetaCMback vs (vpar-Vcn)sinThetaCM [Counts];thetaCMback [deg];(vpar-Vcn)ThetaCM");
        vdsinethetaCMback.Write("",TObject::kOverwrite);

        //TH2D T0dTder("T0dTder","T0der:dTder",1100,0,110,2000,-100,100);
        TH2D T0dTder("T0dTder","T0der:dTder",1100,-50,50,2000,100,150);
        T0dTder.SetOption("COLZ");
        T0dTder.SetMinimum(1);
        tree->Draw("dTder:T0der>>T0dTder",""," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
        T0dTder.SetTitle("T0der vs dTder [Counts];T0der [ns]; dTder[ns]");
        T0dTder.Write("",TObject::kOverwrite);
        //2D

    }

    cout << "Produced 2D histograms..." << endl;

    sfile->cd("/");
    if (sfile->GetListOfKeys()->Contains("HistQC")!=true){
        sfile->mkdir("HistQC");
    }
    sfile->cd("HistQC");
    /*
    //quadrant corrections - det 1
    {

    TH2D det1_qc12("det1_qc12","x1det:t1",1000,-200,200,1000,-70,170);
    det1_qc12.SetOption("COLZ");
    det1_qc12.SetMinimum(1);
    tree->Draw("t1:x1det>>det1_qc12","quadID_1==1||quadID_1==2"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det1_qc12.SetTitle("det1 quad corr 1+2: x1det vs t1 [Counts]; x1det [mm]; t1 [ns] ");
    det1_qc12.Write("",TObject::kOverwrite);

    TH2D det1_qc23("det1_qc23","t1:y1det",1000,-70,170,1000,-200,200);
    det1_qc23.SetOption("COLZ");
    det1_qc23.SetMinimum(1);
    tree->Draw("y1det:t1>>det1_qc23","quadID_1==2||quadID_1==3"," ");
    det1_qc23.SetTitle("det1 quad corr 2+3: t vs ydet [Counts]; t1 [ns]; y1det [mm] ");
    det1_qc23.Write("",TObject::kOverwrite);

    TH2D det1_qc34("det1_qc34","x1det:t1",1000,-200,200,1000,-70,170);
    det1_qc34.SetOption("COLZ");
    det1_qc34.SetMinimum(1);
    tree->Draw("t1:x1det>>det1_qc34","quadID_1==3||quadID_1==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det1_qc34.SetTitle("det1 quad corr 3+4: x1det vs t1 [Counts]; x1det [mm]; t1 [ns] ");
    det1_qc34.Write("",TObject::kOverwrite);

    TH2D det1_qc41("det1_qc41","t1:y1det",1000,-70,170,1000,-200,200);
    det1_qc41.SetOption("COLZ");
    det1_qc41.SetMinimum(1);
    tree->Draw("y1det:t1>>det1_qc41","quadID_1==1||quadID_1==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det1_qc41.SetTitle("det1 quad corr 1+4: t1 vs y1det [Counts]; t1 [ns]; y1det [mm] ");
    det1_qc41.Write("",TObject::kOverwrite);
    }

    {
    //quadrant corrections - det 2
    TH2D det2_qc12("det2_qc12","x2:t2",1000,-200,200,1000,-70,170);
    det2_qc12.SetOption("COLZ");
    det2_qc12.SetMinimum(1);
    tree->Draw("t2:x2det>>det2_qc12","quadID_2==1||quadID_2==2"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det2_qc12.SetTitle("det2 quad corr 1+2: x2det vs t2 [Counts]; x2det [mm]; t2 [ns] ");
    det2_qc12.Write("",TObject::kOverwrite);

    TH2D det2_qc23("det2_qc23","t2:y2det",1000,-70,170,1000,-200,200);
    det2_qc23.SetOption("COLZ");
    det2_qc23.SetMinimum(1);
    tree->Draw("y2det:t2>>det2_qc23","quadID_2==2||quadID_2==3"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det2_qc23.SetTitle("det2 quad corr 2+3: t vs ydet [Counts]; t2 [ns]; y2det [mm] ");
    det2_qc23.Write("",TObject::kOverwrite);

    TH2D det2_qc34("det2_qc34","x2det:t2",1000,-200,200,1000,-70,170);
    det2_qc34.SetOption("COLZ");
    det2_qc34.SetMinimum(1);
    tree->Draw("t2:x2det>>det2_qc34","quadID_2==3||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det2_qc34.SetTitle("det2 quad corr 3+4: x2det vs t2 [Counts]; x2det [mm]; t2 [ns] ");
    det2_qc34.Write("",TObject::kOverwrite);

    TH2D det2_qc41("det2_qc41","t2:y2det",1000,-70,170,1000,-200,200);
    det2_qc41.SetOption("COLZ");
    det2_qc41.SetMinimum(1);
    tree->Draw("y2det:t2>>det2_qc41","quadID_2==1||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname","",""
    det2_qc41.SetTitle("det2 quad corr 1+4: t2 vs y2det [Counts]; t2 [ns]; y2det [mm] ");
    det2_qc41.Write("",TObject::kOverwrite);
    }

    {
    //quadrant corrections - det 3
    TH2D det3_qc12("det3_qc12","x3:t3",1000,-200,200,1000,-70,170);
    det3_qc12.SetOption("COLZ");
    det3_qc12.SetMinimum(1);
    tree->Draw("t3:x3det>>det3_qc12","quadID_2==1||quadID_2==2"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det3_qc12.SetTitle("det3 quad corr 1+2: x3det vs t3 [Counts]; x3det [mm]; t3 [ns] ");
    det3_qc12.Write("",TObject::kOverwrite);

    TH2D det3_qc23("det3_qc23","t3:y3det",1000,-70,170,1000,-200,200);
    det3_qc23.SetOption("COLZ");
    det3_qc23.SetMinimum(1);
    tree->Draw("y3det:t3>>det3_qc23","quadID_2==2||quadID_2==3"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det3_qc23.SetTitle("det3 quad corr 2+3: t vs ydet [Counts]; t3 [ns]; y3det [mm] ");
    det3_qc23.Write("",TObject::kOverwrite);

    TH2D det3_qc34("det3_qc34","x3det:t3",1000,-200,200,1000,-70,170);
    det3_qc34.SetOption("COLZ");
    det3_qc34.SetMinimum(1);
    tree->Draw("t3:x3det>>det3_qc34","quadID_2==3||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname",""," "
    det3_qc34.SetTitle("det3 quad corr 3+4: x3det vs t3 [Counts]; x3det [mm]; t3 [ns] ");
    det3_qc34.Write("",TObject::kOverwrite);

    TH2D det3_qc41("det3_qc41","t3:y3det",1000,-70,170,1000,-200,200);
    det3_qc41.SetOption("COLZ");
    det3_qc41.SetMinimum(1);
    tree->Draw("y3det:t3>>det3_qc41","quadID_2==1||quadID_2==4"," ");                  //Format of draw argument: "yaxis:xaxis>>histname","",""
    det3_qc41.SetTitle("det3 quad corr 1+4: t3 vs y3det [Counts]; t3 [ns]; y3det [mm] ");
    det3_qc41.Write("",TObject::kOverwrite);
}

*/
cout << "Produced quadrant correction histograms..." << endl;


//Close tree file
delete sfile;

if (gatebool){
    plot_kin(gatefile,&rxn);
}
else{
    plot_kin(procfile,&rxn);
}

cout << "Histogram creation complete!" << endl;

return (0);


}//end CreateHistEss

/*------------------------------------------------------------------------------------
  UngatedCubeHistEss
  : Creates ungated histograms. USER SHOULD NOT MODIFY THIS
  -------------------------------------------------------------------------------------*/
int UngatedCubeHistEss(char* filename,char* outfile){ //function produces ungated monitor histograms


    TTree* CubeTreeNew;
    Long64_t nentries=0;
    int nevent;

    //open root file
    TFile sfile(filename,"UPDATE");
    if (!sfile.IsOpen()){
        std::cout << "CubeHist: Root file " << filename << " not found!" << std::endl;
        return -1;
    }

    if (sfile.GetListOfKeys()->Contains("CubeTreeNew")==true){

        CubeTreeNew = (TTree*)sfile.Get("CubeTreeNew");
        nentries=CubeTreeNew->GetEntries();

        nevent = nentries;
        nevent=nevent;
        if (nentries > 11000000){
            cout << "Raw tree too large to avoid memory issues; use unprocessed file to view ungated monitor histograms." << endl;
            return -1;
        }

    }
    else{
        cout << "Make sure your processed, ungated root file name is correct. CubeTreeNew not found in"<< endl;
        cout << filename << endl;
        return -1;
    }

    // gStyle.SetCanvasColor(0);
    // gStyle.SetStatBorderSize(1);

    if (sfile.GetListOfKeys()->Contains("Raw data")!=true){
        sfile.mkdir("Raw data");
    }

    // ##### 1D histograms #####
    sfile.cd("/");
    sfile.cd("Raw data");

    //monitor 1 energy
    TH1I mon1E_noGate("mon1E_noGate","mon1E_noGate",1048,0,1048);
    CubeTreeNew->Draw("Mon1E>>mon1E_noGate","","");
    mon1E_noGate.SetTitle("Monitor 1 Energy;Channels;Counts");
    mon1E_noGate.Write();


    //monitor 2 energy
    TH1I mon2E_noGate("mon2E_noGate","mon2E_noGate",1048,0,1048);
    CubeTreeNew->Draw("Mon2E>>mon2E_noGate","","");
    mon2E_noGate.SetTitle("Monitor 2 Energy;Channels;Counts");
    mon2E_noGate.Write();

    TH2D TfrontEvt("TfrontEvt","raw_T2:EvtNo",8192,0,nevent,8192,1,-8192);
    TfrontEvt.SetOption("COLZ");
    TfrontEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T2:EvtNo>>TfrontEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TfrontEvt.SetTitle("raw_T2 vs EvtNo [Counts];Evt No;raw_T2 [ch]");// title;x-axis;y-axis
    /*
    // Add src_v4.3
    TH2D TbackEvt("TbackEvt","raw_T1:EvtNo",8192,0,nevent,8192,1,8192);
    TbackEvt.SetOption("COLZ");
    TbackEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T1:EvtNo>>TbackEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TbackEvt.SetTitle("raw_T1 vs EvtNo [Counts];Evt No;raw_T1 [ch]");// title;x-axis;y-axis

    TH2D TfrontEvt("TfrontEvt","raw_T2:EvtNo",8192,0,nevent,8192,1,-8192);
    TfrontEvt.SetOption("COLZ");
    TfrontEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T2:EvtNo>>TfrontEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TfrontEvt.SetTitle("raw_T2 vs EvtNo [Counts];Evt No;raw_T2 [ch]");// title;x-axis;y-axis

    TH2D TsmallbackEvt("TsmallbackEvt","raw_T3:EvtNo",8192,0,nevent,8192,1,8192);
    TsmallbackEvt.SetOption("COLZ");
    TbackEvt.SetMinimum(1);
    CubeTreeNew->Draw("raw_T3:EvtNo>>TsmallbackEvt",""," ");     //Format of draw argument: "yaxis:xaxis>>histname",""," "
    TsmallbackEvt.SetTitle("raw_T3 vs EvtNo [Counts];Evt No;raw_T3 [ch]");// title;x-axis;y-axis
     */
    //open gated root file
    TFile ofile(outfile,"UPDATE");
    if (!ofile.IsOpen()){
        std::cout << "CubeHist: Gated Root file "<< outfile << " not found!" << std::endl;
        return -1;
    }

    if (ofile.GetListOfKeys()->Contains("Raw data")!=true){
        ofile.mkdir("Raw data");
    }

    // ##### 1D histograms #####
    ofile.cd("/");
    ofile.cd("Raw data");

    mon1E_noGate.Write("",TObject::kOverwrite);
    mon2E_noGate.Write("",TObject::kOverwrite);

    cout << "Ungated monitor histograms created." << endl;

    ofile.Close();

    return 0;

}//end UngatedCubeHistEss
