#ifndef HIST_H

#include <iostream>
#include <fstream>

#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
//#include "TStyle.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"

#include "TMath.h"
#include "TGraph.h"
#include "TAxis.h"

#include "CubeStruct.h"
#include "CubeInput.h"
#include "CubeReact.h"
#include "plot_kin.h"

#include "Nucleus.h"
#include "Reaction.h"

int UngatedCubeHistEss(char*,char*);
int CreateHistEss(char*, bool);

#endif
