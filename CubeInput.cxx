//CubeInput.cxx - contains code that controls input file i/o for dacube

// For src_v5.0 - Yun Jeung, May 2019 update

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

#include "CubeInput.h"

using namespace std;

//**************** AutoFileExtension ***************//
// figure out what to call the processed root file produced during sorting
string AutoFileExtension(sortInfo* sort, reactPar* rxn){

    string procdata,procext;
    string rawname=sort->rawdata;

    //figure out file name for processed file
    size_t length=rawname.length();
    int i=0,k=0;

    size_t namelen=0;
    while (i<(int)length){
        k=(int)length-i;
        if (rawname[k]=='/'){
            namelen=(size_t)(k+1);
            i=(int)length;
            cout << endl;
        }
        i++;
    }

    //trying to automatically generate name for processed file here - to be saved in working directory
    procdata = rawname.substr(namelen,length-namelen-5);
    //cout << procdata << endl;
    //if (rxn->elossBool==0){
    if (rxn->dEcalcType==0){
        procext=".P.root";
    }
    else{
        procext=".PE.root";
    }
    procdata = procdata + procext;

    return procdata;

}// end AutoFileExtension

//**************** AutoFileExtension ***************//
// figure out what to call the processed root file produced during sorting
string AutoPath(string rawname){

    string procdata,procext;

    //figure out file name for processed file
    size_t length=rawname.length();
    int i=0,k=0;

    size_t namelen=0;
    while (i<(int)length){
        k=(int)length-i;
        if (rawname[k]=='/'){
            namelen=(size_t)(k+1);
            i=(int)length;
            cout << endl;
        }
        i++;
    }

    //trying to automatically generate name for processed file here - to be saved in working directory
    procdata = rawname.substr(0,namelen);

    return procdata;

}// end AutoPath



//**************** AutoRootExt ***************//
// Delete extension on root files and replace with ext
string AutoRootExt(string rawname,string ext){

    string procdata;

    //figure out file name for processed file
    size_t length=rawname.length();

    procdata = rawname.substr(0,length-5);

    procdata = procdata + ext;

    return procdata;

}// end AutoRootExt


//************** readInput **************//
// Reads input file and saves relevant information to structures
int readInput(char* sortfile, sortInfo* sort, reactPar* rxn, targetPar* targ){

    ifstream input;
    string comment;
    string dum, gatestring;
    int temp;
    bool gatetest=0;
    gatestring = "";
    string test;

    input.open(sortfile);

    if (!input.is_open()){
        cout << "CubeInput:: Error: Sort parameter file " << sortfile << " not found!" << endl;
        return -1;
    }
    else{

        //\\ read in user-provided parameters from sortfile \\//
        // cout << "Importing '" << sortfile << "' input file. Reading input parameters..." << endl;
        cout << "Reading input parameters..." << endl;
        input >> comment >> comment;

        temp = strcmp(&comment[0],"dacube");
        if (temp == 0){
            cout << "Do you have the wrong input file (y/n)? File provided: " << sortfile << "\n";
            cin >> test;
            if (strcmp(&test[0],"y")==0){
                return -1;
            }
        }

        input >> comment >> comment; //ignore comment line
        if (!(input >> sort->rawdata)) {
            std::cerr << "Error reading the input file..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> sort->detfilepath)) {
            std::cerr << "Error reading the detector file..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> sort->calibpath)) {
            std::cerr << "Error reading the tcalib filepath..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> sort->minEntry >> sort->maxEntry)) {
            std::cerr << "Error reading the min and max entry..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> rxn->Zp >> rxn->Ap >> rxn->Zt >> rxn->At )) {
            std::cerr << "Error reading Zp, Ap, Zt, and At..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        //save target Z to target parameters as well.
        targ->targZ = rxn->Zt;
        if (!(input >> rxn->Elab)) {
            std::cerr << "Error reading Elab..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> rxn->T0 >> rxn->dT >> rxn->dT3)) {
            std::cerr << "Error reading T0, dT, and dT3..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> sort->T0init >> sort->T0fin >> sort->T0step >> sort->dTinit >> sort->dTfin >> sort->dTstep)) {
            std::cerr << "Error reading tCalib parameters..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        //error checking -- avoids accidentally reversing initial and final T0,dT ranges
        if ((sort->T0init > sort->T0fin)&&(sort->T0step>0)){
            sort->T0step = -(sort->T0step);
        }
        if ((sort->dTinit > sort->dTfin)&&(sort->dTstep>0)){
            sort->dTstep = -(sort->dTstep);
        }
        if (!(input >> rxn->beamType >> rxn->VsumType)) {
            std::cerr << "Error reading beamType and VsumType..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> targ->targThick >> targ->targTheta)) {
            std::cerr << "Error reading target thickness and target theta..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> targ->backThick >> targ->backZ)) {
            std::cerr << "Error reading upstream layer thickness and Z..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> targ->frontThick >> targ->frontZ)) {
            std::cerr << "Error reading downstream layer thickness and Z..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> rxn->dEcalcType >> rxn->Eloop)) {
            std::cerr << "Error reading dEcalcType and Eloop number..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> rxn->tsharpBool)) {
            std::cerr << "Error reading time sharpening flag..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> rxn->intercept)) {
            std::cerr << "Error reading MAD intercept angle..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        input.ignore(1000, '\n');
        if (!(input >> sort->procfile)) {
            std::cerr << "Error reading processed filename..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        if (!(input >> sort->gatefile)) {
            std::cerr << "Error reading gatefile name..." << std::endl;
            std::exit(1);
        }
        input.ignore(1000, '\n');
        //read gates
        int i=0;
        while (gatetest == 0){
            input >> dum;
            temp = strcmp(&dum[0],"#Gates");
            if (temp == 0){
                gatetest = 1;
            }
            else{
                if (i==0){
                    gatestring = dum;
                }
                else{
                    dum = "&&"+dum;
                    gatestring += dum;
                }

            }
            i++;
        }

        sort->gates = gatestring;
        // cout << "Gate command:" << sort->gates << endl;


        input.close();


        if ((rxn->beamType!=1) && (rxn->beamType!=2) && (rxn->beamType!=3) && (rxn->beamType!=4) && (rxn->beamType!=5)){
            cout << "CubeSort:: Error: Beam type requested by user is invalid. AC beam = 1, AC beam (tdiff method) = 2, DC beam (dT - det1 time)=3, DC beam (dT + det1 time)=4." << endl;
            return -1;
        }


        if (targ->targTheta == 90){
            cout << "Check target parameters. Target theta value is unusual!" << endl;
            return -1;
        }
        if ( targ->targThick <= 0 ){
            cout << "Check target parameters. Target thickness is unusual." << endl;
            return -1;
        }
        // cout << "Finished reading " << sortfile << " ..." << endl;
        cout << "Finished reading the input file ..." << endl;
        return 0;

    }

} //end readInput



//******************* readDetInfo **********************//
// reads in detector information from detfile
int readDetInfo(sortInfo* sort, MWPCgeo* mwpcGeo, MWPCcorr* mwpcCorr){

    ifstream detinput;
    string comment;

    //open detector file
    detinput.open(&sort->detfilepath[0]);

    if (detinput.is_open()){

        for (unsigned int i=0;i<N_MWPC;i++){

            if (!detinput.eof()){
                detinput >> comment; //ignore detector label
                mwpcGeo[i].detID = i;

                detinput >> mwpcGeo[i].XLch >> mwpcGeo[i].XRch >> mwpcGeo[i].YBch >> mwpcGeo[i].YTch;
                detinput >> mwpcGeo[i].XLmm >> mwpcGeo[i].XRmm >> mwpcGeo[i].YBmm >> mwpcGeo[i].YTmm;
                detinput >> mwpcGeo[i].detTheta >> mwpcGeo[i].detPhi >> mwpcGeo[i].detDist;
                detinput >> mwpcCorr[i].timeSlope;
                detinput >> mwpcCorr[i].detxOffset >> mwpcCorr[i].detyOffset;
                detinput >> mwpcCorr[i].qcunits;
                if ( mwpcCorr[i].qcunits > 1){
                    cout << "CubeSort: Error: Quadrant correction units should be specified as either 0 (=ch) or 1 (=ns)." << endl;
                    return -1;
                }
                detinput >> mwpcCorr[i].qc[0] >> mwpcCorr[i].qc[1] >>  mwpcCorr[i].qc[2] >> mwpcCorr[i].qc[3];

                //cout << "Read in detector " << i+1 << " information..." << endl;

                //change theta and phi into radians
                mwpcGeo[i].detTheta = mwpcGeo[i].detTheta * d2r;
                mwpcGeo[i].detPhi = mwpcGeo[i].detPhi * d2r;
                /*
                   cout << endl << "Mwpc " << i+1 << " has: " << endl;
                   cout << "Theta: " << mwpcGeo[i].detTheta *r2d <<"  [deg]"<< endl;
                   cout << "Phi: "  << mwpcGeo[i].detPhi *r2d <<"  [deg]"<< endl;
                   cout << "distance: " << mwpcGeo[i].detDist <<"  [mm]" << endl<<endl;

                 */

            }
            else{
                cout << "CubeSort:: Error - check detector file format. " << endl; // attempt at error checking.
            }
        }


        cout<<"----------------- MWPC positions --------------------" << endl;
        cout << "Det.\t Theta [deg]\tPhi [deg]\t Distance [mm]" << endl;
        for (int i=0;i<N_MWPC;i++){
            cout << i << " \t  " << mwpcGeo[i].detTheta *r2d <<"\t\t" << mwpcGeo[i].detPhi *r2d << "\t\t" << mwpcGeo[i].detDist << endl;
        }

        cout<<"----------------- MWPC edges [ch no.] ----------------------" << endl;
        cout << "Det.  \t\t" << "   XL | XR \t\t   YB | YT  " << endl;

        for (int i=0;i<N_MWPC;i++){
            cout << i  <<"\t\t" << mwpcGeo[i].XLch <<" | " << mwpcGeo[i].XRch << "\t\t" << mwpcGeo[i].YBch << " | " << mwpcGeo[i].YTch << endl;
        }


        cout << "---------- Quandrant centre & size [mm] ----------------" << endl;
        cout << "Det. \t centre  \t  QuadSize \t    Offset " << endl;
        cout << "\tqx0 | qy0  \t     XL | XR  \t     X | Y" << endl;
        /*
           for (int i=0;i<N_MWPC;i++){
           cout << i  <<"  \t " <<mwpcCorr[i].qx0<<"  |  "<<mwpcCorr[i].qy0<<"   \t "<< mwpcGeo[i].XLmm  <<"\t|"<<  mwpcGeo[i].XRmm << "\t     "<< mwpcCorr[i].detxOffset <<" |"<< mwpcCorr[i].detyOffset<< endl;
           }
         */

        detinput.close();
        return 0;
    }
    else{
        cout << "CubeSort:: Error: Detector parameter file " << &sort->detfilepath[0] << " not found!" << endl;
        return -1;
    }
}// end readDetInfo



//************** readXsecInput **************//
// Reads cross section input file and saves relevant information to structures
int readXsecInput(char* xsecFile, xsecSpecs* xsecSpec, list<xsecInfo>* fisInputList, xsecInfo* calibInput1, xsecInfo* calibInput3, list<sortInfo>* fisSortList, sortInfo* calibSort1, sortInfo* calibSort3, list<reactPar>* fisRxnList, reactPar* calibRxn1, reactPar* calibRxn3, list<targetPar>* fisTargList, targetPar* calibTarg1, targetPar* calibTarg3){

    ifstream input;
    string comment;
    int errCheck;
    int temp;
    string test;
    int fisTest=0;
    string dum;
    float fisMonAngle;

    input.open(xsecFile);

    if (!input.is_open()){
        cout << "CubeInput:: Error: Xsec parameter file " << xsecFile << " not found!" << endl;
        return -1;
    }
    else{

        //\\ read in user-provided parameters from sortfile \\//
        cout <<  xsecFile << " opened. Reading cross section input parameters..." << endl;



        // now read in cross section info
        input >> comment >> comment >> comment;

        temp = strcmp(&comment[0],"cross");
        if (temp != 0){
            cout << "Do you have the wrong input file (y/n)? File provided: " << xsecFile << "\n";
            cin >> test;
            if (strcmp(&test[0],"y")==0){
                return -1;
            }
        }
        input >> comment >> comment >> comment;

        //read in results path
        input >> comment >> comment >> comment >> comment >> comment;
        input >> xsecSpec->resultsPath;

        //read in calibration run detector parameters

        cout << "\n\nStarting to read calibration file information...\n\n";

        input >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> calibInput1->monAngleLab;

        if (N_MWPC==3){
            input >> comment >> comment >> calibInput3->monAngleLab;
        }

        // read in fission run detector parameters (common)
        input >> comment >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> fisMonAngle; //same for all runs assumed, as mon angles are not v. accurate -- should not be different!

        // read in histogram settings (bin size in degrees)
        input >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >>  comment >> comment >> comment >> comment;
        input >> comment >> comment >> comment >> xsecSpec->binSize;

        if (xsecSpec->binSize <= 0){
            cout << "Error: CubeInput::readXsecInput. User-specified bin size is invalid. Bin size: " << xsecSpec->binSize << "\n";
        }

        input >> comment >> comment >> comment >> comment;

        // read in run information for calibration file


        input >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> calibInput1->sortFile;
        input >> calibInput1->dataFile;
        input >> calibInput1->preScal;
        input >> calibInput1->monScal[0];
        input >> calibInput1->monScal[1];
        input >> calibInput1->cubeScal;
        input >> calibInput1->monDAQ[0];
        input >> calibInput1->monDAQ[1];
        input >> calibInput1->cubeDAQ;
        input >> calibInput1->monSumPeak;

        errCheck = readInput(&calibInput1->sortFile[0], calibSort1, calibRxn1, calibTarg1);
        if (errCheck!=0){
            cout << "Failed to read in all relevant parameters for calibration data. Check sortfile name." << endl;
            cout << "Sortfile: " << calibInput1->sortFile << "\n";
            return -1;
        }

        if (N_MWPC==3){
            input >> calibInput3->sortFile;
            input >> calibInput3->dataFile;
            input >> calibInput3->preScal;
            input >> calibInput3->monScal[0];
            input >> calibInput3->monScal[1];
            input >> calibInput3->cubeScal;
            input >> calibInput3->monDAQ[0];
            input >> calibInput3->monDAQ[1];
            input >> calibInput3->cubeDAQ;
            input >> calibInput3->monSumPeak;

            errCheck = readInput(&calibInput3->sortFile[0], calibSort3, calibRxn3, calibTarg3);

            if (errCheck!=0){
                cout << "Failed to read in all relevant parameters for calibration data. Check sortfile name." << endl;
                cout << "Sortfile: " << calibInput3->sortFile << "\n";
                return -1;
            }
        }


        //ignore fission file info headers
        input >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> comment >> comment >> comment >> comment;
        input >> comment >> comment >> comment;



        cout << "\n\nStarting to read fission file information...\n\n";

        while ((fisTest == 0) && (!input.eof())){

            //initialize structures
            xsecInfo fisInput = {"","",0,0,0,{0,0},0,{0,0},0,0,0,0,0,0};
            sortInfo fisSort = {"","","",0,0,0,0,0,0,0,0,"","",""};
            //reactPar fisRxn = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            reactPar fisRxn = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//src_v5 24+1+1
            targetPar fisTarg = {0,0,0,0,0,0,0,{0,0}};

            // read in fission run information sequentially



            input >> fisInput.sortFile;
            temp = strcmp(&fisInput.sortFile[0],"#endruns");

            if (temp == 0){
                fisTest = 1;
            }
            else{


                input >> fisInput.dataFile;
                input >> fisInput.preScal;
                fisInput.monAngleLab = fisMonAngle;
                input >> fisInput.monScal[0];
                input >> fisInput.monScal[1];
                input >> fisInput.cubeScal;
                input >> fisInput.monDAQ[0];
                input >> fisInput.monDAQ[1];
                input >> fisInput.cubeDAQ;
                input >> fisInput.monSumPeak;
                input >> dum;
                temp = strcmp(&dum[0],"y");
                if (temp==0){
                    fisInput.fifrangOut = 1;
                }
                else{
                    fisInput.fifrangOut = 0;
                }

                //Now read in the sort parameters
                errCheck = 0;
                errCheck = readInput(&fisInput.sortFile[0], &fisSort, &fisRxn, &fisTarg);

                if (errCheck != 0){
                    cout << "Failed to read in all relevant parameters for " << fisInput.sortFile << ". Check sortfile name." << endl;
                    return -1;
                }

                //add structures to list
                fisInputList->push_back(fisInput);
                fisSortList->push_back(fisSort);
                fisRxnList->push_back(fisRxn);
                fisTargList->push_back(fisTarg);


            }
        }//fission portion






        input.close();

    }//end loop for valid input file



    return 0;

} //end readXsecInput
