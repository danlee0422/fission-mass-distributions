#ifndef GUARD_CubeInput
#define GUARD_CubeInput

#include <list>
#include "CubeStruct.h"
#include "physConst.h"


std::string AutoRootExt(std::string rawname,std::string ext);
std::string AutoPath(std::string rawname);
std::string AutoFileExtension(sortInfo*, reactPar*);
int readInput(char*, sortInfo*, reactPar*, targetPar*);
int readDetInfo(sortInfo*, MWPCgeo*, MWPCcorr*);
int readXsecInput(char* xsecFile, xsecSpecs* xsecSpec, 
        std::list<xsecInfo>* fisInputList, xsecInfo* calibInput1, 
        xsecInfo* calibInput3, std::list<sortInfo>* fisSortList, 
        sortInfo* calibSort1, sortInfo* calibSort3, 
        std::list<reactPar>* fisRxnList, 
        reactPar* calibRxn1, reactPar* calibRxn3,
        std::list<targetPar>* fisTargList, 
        targetPar* calibTarg1, targetPar* calibTarg3);
#endif
