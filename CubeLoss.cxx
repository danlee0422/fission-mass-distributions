#include <math.h>
#include <vector>
#include <fstream>

#include "CubeLoss.h"


// we also want to find the maximum Z
const int Z_top = 119;
const int A_top = 301;

// the energy values live here
const float E_max = 500.1;  // MeV
const float E_min = 0.1;  // MeV
const int limit = 20; // MeV
const int spacing = 5; // MeV
const int E_nsteps = limit + int(E_max - limit) / spacing + 1;
const float dE = (E_max - E_min) / E_nsteps;

// these tables will store the normalised energy losses
float** targ_table;
float** back_table;
float** front_table;
float** mylar_table;
float* energy_table;
// this will hold the indices of the corresponding value in the energy loss tables
unsigned short int AZ[301][150]; // makes this as big as possible, could probably shrink 

/*! \brief This function calculates the energy losses for projectiles in the target layers
 *
 *  \param rxn A pointer to the reaction parameter struct
 *  \param targ A pointer to the target parameter struct
 *
 * The calculated energy loss rates are then stored in look up tables.
 */
void calcElossTable(reactPar *rxn, targetPar *targ) {

  int num_isotopes = 5 * rxn->Acn;

  targ_table = (float **)malloc(num_isotopes * sizeof(float *));  
  back_table = (float **)malloc(num_isotopes * sizeof(float *));  
  front_table = (float **)malloc(num_isotopes * sizeof(float *));  
  mylar_table = (float **)malloc(num_isotopes * sizeof(float *));  
  energy_table = (float *)malloc(E_nsteps * sizeof(float *));  
  for (int i=0; i < num_isotopes; i++){
    targ_table[i] = (float *)malloc(E_nsteps * sizeof(float *));
    back_table[i] = (float *)malloc(E_nsteps * sizeof(float *));
    front_table[i] = (float *)malloc(E_nsteps * sizeof(float *));
    mylar_table[i] = (float *)malloc(E_nsteps * sizeof(float *));
  }

  // get the Z and thicknesses for the target
  int Zt = targ->targZ;
  int Zfront = targ->frontZ;
  int Zback = targ->backZ;

  int Acn = rxn->Acn;
  int Zcn = rxn->Zcn;

  unsigned short int i = 0; // this will be the main index for the energy loss table
  // calculate the tables here, they're all global anyway
  for (int a = 1; a <= Acn; a++) {
    // calculate the mass fraction
    int z = Zcn * float(a) / float(Acn); // this should be around the integer charge here
    for (int iz = std::max(1, z - 2); iz <= std::min(119, z + 2); iz++) { 
      AZ[a][iz] = i; // track the variable

      // now make the energy calculations
      for (int e = 0; e < E_nsteps; e++) {
        float energy = E_min + (e * (1 * (e < limit) + spacing * (e >= limit)) - ((limit - limit/spacing) * spacing) * (e >= limit));
        energy_table[e] = energy;
        if (rxn->dEcalcType == 1) {
          targ_table[i][e] = eloss(iz, a, Zt, (int)rxn->At, energy)/1000.0;
          front_table[i][e] = eloss(iz, a, Zfront, 0, energy)/1000.0;
          back_table[i][e] = eloss(iz, a, Zback, 0, energy)/1000.0;
          mylar_table[i][e] = 0.042*eloss(iz, a, 1, 0, energy) + 0.625*eloss(iz,a,6,0, energy) + 0.333*eloss(iz,a,8,0, energy);
          mylar_table[i][e] *= 0.13970; // convert this to MeV / um

        } else if (rxn->dEcalcType == 2) {
          targ_table[i][e] = elossBT(iz, a, Zt, E_min + e * dE, 1.0);
          front_table[i][e] = elossBT(iz, a, Zfront, E_min + e * dE, 1.0);
          back_table[i][e] = elossBT(iz, a, Zback, E_min + e * dE, 1.0);
          mylar_table[i][e] = 0.042*elossBT(iz,a,1,E_min + e * dE, 1.0) + 0.625*elossBT(iz,a,6,E_min + e * dE, 1.0) + 0.333*elossBT(iz,a,8,E_min + e * dE, 1.0);
          mylar_table[i][e] *= 0.13970 * 1000; // convert this to MeV / um
        } else if (rxn->dEcalcType == 3) {
          targ_table[i][e] = elossT(iz, a, Zt, E_min + e * dE, 1.0);
          front_table[i][e] = elossT(iz, a, Zfront, E_min + e * dE, 1.0);
          back_table[i][e] = elossT(iz, a, Zback, E_min + e * dE, 1.0);
          mylar_table[i][e] = 0.042*elossT(iz,a,1,E_min + e * dE, 1.0) + 0.625*elossT(iz,a,6,E_min + e * dE, 1.0) + 0.333*elossT(iz,a,8,E_min + e * dE, 1.0);
          mylar_table[i][e] *= 0.13970 * 1000; // convert this to MeV / um
        } else {
          std::cerr << "You have specified an invalid dEcalcType" << std::endl;
          std::cerr << "The allowed values are 1, 2, or 3." << std::endl;
          std::exit(1);
        }
      }
      i += 1;
    }
  }
}

/*! \brief Returns the energy loss per unit thickness of the target according to the supplied
 * table.
 *
 * \param Zp The integer charge of the projectile
 * \param Ap The mass number of the projectile
 * \param E The energy of the projectile
 * \param table The energy loss table to be used
 */
float getEloss(int Zp, int Ap, float E, float** table) {
  // std::cout << Zp << " " << Ap << " " << E << std::endl;
  unsigned short int i = AZ[Ap][Zp];
  // check for over values
  if (E > E_max) {
    // std::cerr << "MAXIMUM ENERGY RANGE OF " << E_max << " EXCEEDED at " << E << std::endl;
    return table[i][E_nsteps - 1];
  } else if (E < E_min) {
    return table[i][0];
  }
  //
  // the first thing to do is to determine where our energy loss values are
  int e_index = int((E - E_min - limit)) / (1 + (spacing - 1) * (E > limit)) + limit - (E < (limit + E_min));

  float interpolated = table[i][e_index] + (E - energy_table[e_index]) * (table[i][e_index + 1] - table[i][e_index]) / (energy_table[e_index + 1] - energy_table[e_index]);

  return interpolated;
}
