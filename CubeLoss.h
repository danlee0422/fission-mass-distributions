#ifndef __CUBELOSS_H__
#define __CUBELOSS_H__

#include <iostream>
#include "CubeStruct.h"
#include "eloss.h"

// we also want to find the maximum Z
extern const int Z_top;
extern const int A_top;

// the energy values live here
extern const float E_max;  // MeV
extern const float E_min;
extern const int E_nsteps;
extern const float dE;

extern const int limit; // MeV
extern const int spacing; // MeV

// these tables will hold the energy loss calculations
extern float** targ_table;
extern float** back_table;
extern float** front_table;
extern float** mylar_table;
extern float* energy_table;
// this will hold the indices corresponding to the 
extern unsigned short int AZ[301][150]; // makes this as big as possible, could probably shrink

//! Mylar thickness in um
const float mylar_thiccness = 0.9; 

//! Distance of Mylar to Centre foil in mm
const float mylar_CF_sep = 18.2;

void calcElossTable(reactPar *rxn, targetPar *targ);

float getEloss(int Zp, int Ap, float E, float** table);
#endif
