/* CubeReact.cxx
   For src_3det_v5  - Yun Jeung, May 2019 update

   Any code used for calculations based on reaction parameters can be found here.

   Liz Williams May 2014 update
 */

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

/* specialized header files*/
#include "CubeReact.h"
#include "MassTable.h"

using namespace std;


/*------------------------------------------------------------------------------------
  calcElossBeam
  : Calculates energy loss corrected beam

  (Beam in)---->[Upstream target layer | Target | Downstream target lyaer] -----> (Beam out)
  Ecorr = Eini - dEup - dEtar(up to a half point)

  For 248Cm spontaeous fission, where is no beam energy correction.
  ------------------------------------------------------------------------------------*/
int calcElossBeam(targetPar* targ, reactPar* rxn){

    float E,dE;
    float dx;
    float thick[2];
    float theta;
    int Z[2];

    thick[0] = targ->backThick; // Upstream target layer(-z) thickness in ug/cm^2
    Z[0] = targ->backZ;

    thick[1] = targ->targThick; // Target thickness in ug/cm^2;
    Z[1] =  targ->targZ;

    theta = targ->targTheta; //  target normal angle relative to beam axis in deg
    theta = theta*d2r;
    E = rxn->Elab; //beam energy in MeV
    dE = 0;

//[1] -> target, X[0]--> Upstream backing

    if ( Z[1]<=0 || Z[0]<0 ){
        cout << "Error: calcElossBeam: Check your target Z values; Z<=0 or Zbacking<0!" << endl;
        return -1;

    }else if (thick[1]==0){
        cout << "Error: calcElossBeam: check target thickness; center foil thickness is set to 0. " << endl;
        return -1;

    }else{
        // Energy lost by the backing material
        dx = thick[0]/(fabs(cos(theta))); // full back foil thickness traversed by beam
        //cout << "Back foil thickness traversed by beam: " << dx << endl;

        if (dx>0){
            if(rxn->dEcalcType ==1){
                dE = eloss(rxn->Zp, rxn->Ap, Z[0], 0, E, dx); // returns energy loss in MeV
            }else if(rxn->dEcalcType ==2){
                dE = elossBT(rxn->Zp,rxn->Ap,Z[0],E,dx);
            }else if(rxn->dEcalcType ==3){
                dE = elossT(rxn->Zp,rxn->Ap,Z[0],E,dx);
            }
            E=E-dE;
        }

        // Energy lost by the target
        //cout << "" << endl;
        //cout << "---------- Check energy loss -------------" << endl;
        //cout << "Energy loss by the front layer in MeV:\t " << dE << endl;

        dx = thick[1]/(2.0*fabs(cos(theta))); // thickness traversed by beam assuming interaction takes place at target layer center
        //dE = eloss(rxn->Zp,rxn->Ap,Z[1],E,dx); // returns energy loss in MeV

        if(rxn->dEcalcType ==1){
            dE = eloss(rxn->Zp,rxn->Ap,Z[1], rxn->At, E,dx); // returns energy loss in MeV
        }else if(rxn->dEcalcType==2){
            dE = elossBT(rxn->Zp,rxn->Ap,Z[1],E,dx);
        }else if(rxn->dEcalcType==3){
            dE = elossT(rxn->Zp,rxn->Ap,Z[1],E,dx);
        }

        //cout << "Energy loss through target in MeV: " << dE << endl;

        //cout << "---------- Check target ------------------" << endl;
        //cout << "Target thickness:" << thick[1] << " Targ. angle theta: " << theta*r2d << endl;
        //cout << "Thickness traversed by beam assuming interaction takes place at target centre: " << dx << endl;
        //cout << "------------------------------------------" << endl;

        E=E-dE;

    }

  // check if E is zero or negative (for spontaneous fission)
  //  Berriman 1 Sept 2020
  if (rxn->Elab<=0) {		
	E=0;
  };

    rxn->Elab=E;

    //cout <<"HERE !!!" << rxn->Elab << endl;

    if (isnan(E)==true){
        cout << "Energy loss correction yielded invalid result. Check input parameters." << endl;
        cout << "**********************************************************************" << endl;
        cout << "Supplied values (for debugging): " << endl;
        cout << "Target Z, Backing Z: " << Z[1] << ", " << Z[0] << endl;
        cout << "Target theta (radians): " << theta << endl;
        cout << "Target thickness traversed by beam: " << dx << endl;
        cout << "Energy loss in 0.5*target thickness: " << dE << endl;
        cout << "Final Elab = " << E << endl;
        cout << "**********************************************************************" << endl;
        return -1;
    }

    return 0;

}//////end calcElossBeam


/*------------------------------------------------------------------------------------
  calcExCN
  : Calculates excitation energy
  Written for Ni+Ni. Calculates CN excitation energy for reaction
  ------------------------------------------------------------------------------------*/
void calcExCN(reactPar *rxn){


    MassTable m;

    m.readMassTable();

    rxn->QvalCN = m.calcQval(rxn->Ap,rxn->Zp,rxn->At,rxn->Zt,rxn->Acn,rxn->Zcn,0,0);
    //cout << "Qval_gs (CN): " << rxn->QvalCN << "\n";

    rxn->ExCN = rxn->QvalCN + rxn->Ecm;
    //cout << "ExCN: " << rxn->ExCN << "\n";//Ecm+QvalCN

}//end CalcExCN


/*------------------------------------------------------------------------------------
  calcRxn
  : Calculates reaction parameters
  - Yun Jeung, May 2019 update
  ------------------------------------------------------------------------------------*/
void calcRxn(reactPar* rxn){

    float tMrat,pMrat;

    //CN mass & charge
    rxn->Acn = float (rxn->Ap) + float (rxn->At);
    rxn->Zcn = float (rxn->Zp) + float (rxn->Zt);

    Nucleus *Nucl_proj = new Nucleus(rxn->Ap,rxn->Zp);
    Nucleus *Nucl_targ = new Nucleus(rxn->At,rxn->Zt);
    Nucleus *Nucl_CN = new Nucleus(rxn->Acn, rxn->Zcn);

    tMrat = rxn->At / float (rxn->Acn);
    pMrat = rxn->Ap / float (rxn->Acn);

    //calculate centre of mass energy
    rxn->Ecm = tMrat * rxn->Elab; // units of MeV

    float csq = speed_of_light*speed_of_light;

    rxn->Vcn = pMrat * sqrt ( rxn->Elab / rxn->Ap ) * sqrt ( (2. * csq) / m_u ); // units of mm/ns

    calcExCN(rxn);
    //fissility
    float x = pow(rxn->Zcn,2.0)/(48*rxn->Acn);
    float z = (rxn->Zp)*(rxn->Zt)/(pow(rxn->Ap,1./3.)+pow(rxn->At,1./3.));

    //------ Capture barriers --------
    //Swiatecki et al., PRC71, 014602 (2005)
    float Vb = 0.85247 * z + 0.001361 * pow(z,2.0) - 0.00000223 * pow(z,3.0);
    //Emprical barrier (D.Y. Jeung et al..(2021))
    float empVb = (0.0585*log((rxn->Zp)*(rxn->Zt))+0.5785)*z;


    cout << "\n------------------ Reaction ---------------------" << endl;
    cout << "        \t| Proj. | " << "Targ. |" << " CN" << endl;
    cout << " A      \t| " << Nucl_proj->GetNucleus().c_str() << "   | " << Nucl_targ->GetNucleus().c_str() << " | " << Nucl_CN->GetNucleus().c_str() << endl;
    cout << " Z      \t| " << rxn->Zp << "    | " << rxn->Zt << "    | " << rxn->Zcn << endl;
    cout << " Qval_gs\t|                 " << rxn->QvalCN << " MeV" << endl;
    cout << " ExCN   \t|                 " << rxn->ExCN << " MeV" << endl;
    cout << " Vcn    \t|                 " << rxn->Vcn << " mm/ns" << endl;
    cout << " x(fissility) \t|                 " << x << "" << endl;
    cout << "" << endl;
    cout << "------ Corrected Beam Energy* / Fusion Barrier ---------- \n *Thickness traversed by beam assuming interaction takes place at target centre" << endl;
    cout << " Elab = " << rxn->Elab << " MeV" << endl;
    cout << " Ecm = " << rxn->Ecm << " MeV" << endl;
    cout << " Vb = " << Vb << " MeV (Swiatecki et al.)" << endl;
    cout << " Vb = " << empVb << " MeV (Empirical VB),   E/VB = " << float(rxn->Ecm/empVb) << endl;

    cout <<""<< endl;

}//end calcRxn
