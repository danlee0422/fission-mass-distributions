#ifndef GUARD_CubeReact
#define GUARD_CubeReact

#include "CubeStruct.h"
#include "eloss.h"
#include "physConst.h"

#include "Nucleus.h"
#include "Reaction.h"


int calcElossBeam(targetPar*, reactPar*);
void calcExCN(reactPar*);
void calcRxn(reactPar*);


#endif
