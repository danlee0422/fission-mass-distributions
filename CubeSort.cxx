#define ZClass_cxx

// For src_3det_v5  - Yun Jeung, May 2019 update
/* CubeSort.cxx

   Dependencies: ZClass.h, eloss.C, eloss.h, Root (CERN) libraries,
   GNU math library
   This is the heart of the daCube code, which sorts CUBE data. The root tree
   structure, data processing, etc, are all done in here, with the exception
   of energy loss calculations, which depend on eloss.C.

  -  Liz Williams, May 2014 update
*/

/***********************************/
/*         HEADER FILES            */
/***********************************/

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <vector>

/* Specialized header files for Cube Sort*/
#include "CubeSort.h"

using namespace std;

int Numchoices = 0;

/*------------------------------------------------------------------------------------
                            completeEvent
 : complete calculations of derived parameters for each event and store
   in mwpcEvt and mwpcFis structures
------------------------------------------------------------------------------------*/
void completeEvent(MWPCEvt* mwpcEvt, MWPCgeo* mwpcGeo, MWPCcorr* mwpcCorr, MWPCfis* mwpcFis, reactPar* rxn, targetPar* targ, MassTable *m){

  float quadcorr[N_MWPC]={};

  //--------CubeDet.cxx----------

  //* calculate position and calibrated time information *//
  for (int i=0;i<N_MWPC;i++){

    mwpcDetPosCalib(&mwpcEvt[i],&mwpcGeo[i],i);//calculate event coordinates in detector reference frame
    calcPosCartesian(&mwpcEvt[i],&mwpcGeo[i]);//calculate event coordinates in cartesian reference frame
    calcPosSpherical(&mwpcEvt[i]);//calculate event coordinates in spherical reference frame
    quadcorr[i] = mwpcTimeCalib(&mwpcCorr[i], &mwpcEvt[i]);//calculate quadrant-corrected and calibrated times
  }//end loop over detectors

  // Array events records if there is an event in each detector (1/0)
  int events[N_MWPC];
  int Choice;

  //--------CubeFis.cxx----------
  chooseBackdet(mwpcEvt,mwpcGeo,events); // CubeFis.cxx

  if(events[2]) Choice = 2;
  else Choice = 0;

  mwpcFis->choice = Choice;

  // Count the number of good events
  int multiplicity = 0;

  for(int i=0;i<N_MWPC;++i) multiplicity += events[i];

  //--------CubeDet.cxx----------

  //calculate derived time parameters (For rough estimate of appropriate values)
  derivTcal(rxn,mwpcEvt,mwpcFis);

      //cout<<"post derivTcal:rxn->Elab="<<rxn->Elab<<endl;

  //Calculate time of flight or time difference, depending on beam type
  calcTOF(rxn,mwpcEvt,quadcorr[1],mwpcFis,Choice);

  //--------CubeFis.cxx----------

  //calculate derived parameters assuming two-body fission
    if  (rxn->beamType==1){ // only AC beam type 1
    //calcMrVelThetaCM(rxn,targ,mwpcEvt,mwpcFis,Choice);
    calcMrVelThetaCM(rxn,targ,mwpcEvt,mwpcFis,mwpcGeo,Choice); //*Annette*14Jan2021*add_mwpcGeo
    }else{ // beam type 2 and DC beam types 3 and 4

      //cout<<"pre Tdiffcalc:rxn->Elab="<<rxn->Elab<<endl;

      //Tdiffcalc(mwpcEvt, mwpcFis, rxn, targ,Choice);
      Tdiffcalc(mwpcEvt, mwpcFis, mwpcGeo, rxn, targ,Choice); //*Annette*14Jan2021*add_mwpcGeo
    }

  if(Choice == 0 && mwpcEvt[2].thetaCM != 0)
    printf("Choice: 0, theta3cm: %f\n",mwpcEvt[2].thetaCM);

  //calculates TKE from mr, theta12 and known reaction (pretty similar to TKE calculated in the usual way)
  calcTKEfromFoldMR(mwpcFis, mwpcEvt, rxn,Choice);

  //calculate excitation energy of the fission fragments. Right now too discrete due to rounding of fission fragment masses.
  calcExFis(rxn,mwpcFis,m);

  /*if(mwpcFis->EvtNo < N_MWPC){
    cout << "Check Detector["<<mwpcFis->EvtNo <<"] Edges in ch." << "\t XLch: " << mwpcGeo[mwpcFis->EvtNo].XLch
    << "\tXR: " <<mwpcGeo[mwpcFis->EvtNo].XRch << " \tYB:" << mwpcGeo[mwpcFis->EvtNo].YBch << " \tYT: " << mwpcGeo[mwpcFis->EvtNo].YTch << endl;
    }
    */
    /*
  Once you activate following statement, ... it shows sharp cutting edges.
  Following statements will ask to repopulate some events. It may not good idea to apply a gate here.
  - Yun Jeung, May 2019
  */
  /*
  if(multiplicity < 3) {
    //cout <<  mwpcEvt[int(fabs(Choice-2))].x_raw << endl;
    // cout <<multiplicity <<"\t" << Choice << endl; // multiplicity =0,1,2 --> Choice = 0
    mwpcEvt[int(fabs(Choice-2))].x_raw = -1E4;
    mwpcEvt[int(fabs(Choice-2))].y_raw = -1E4;
    mwpcEvt[int(fabs(Choice-2))].x = -1E4;
    mwpcEvt[int(fabs(Choice-2))].y = -1E4;
    mwpcEvt[int(fabs(Choice-2))].z = -1E4;
    mwpcEvt[int(fabs(Choice-2))].phi = -1E4;
    mwpcEvt[int(fabs(Choice-2))].r = -1E4;
    mwpcEvt[int(fabs(Choice-2))].thetaCM = -1E4;
    mwpcEvt[int(fabs(Choice-2))].x_mm = -1E4;
    mwpcEvt[int(fabs(Choice-2))].y_mm = -1E4;
    mwpcEvt[int(fabs(Choice-2))].x_orig = -1E4;
    mwpcEvt[int(fabs(Choice-2))].y_orig = -1E4;
    mwpcEvt[int(fabs(Choice-2))].t = -1E4;
    //cout << mwpcEvt[2].x_raw << endl;

  }
  */

  // Populate combined back parameters
  mwpcFis->tback = mwpcEvt[Choice].t;
  mwpcFis->thetaback = mwpcEvt[Choice].theta;
  mwpcFis->phiback = mwpcEvt[Choice].phi;
  mwpcFis->thetaCMback = mwpcEvt[Choice].thetaCM;
  mwpcFis->raw_tback = mwpcEvt[Choice].t_raw;//src_v4.3

}//end completeEvent

/*------------------------------------------------------------------------------------
                            SORT THE DATA: INITIAL
 : Data collected after 2015, we used ZClass instread of CubeClass
 Definitons of some substracted parameters (e.g. raw_x = XL-XR, raw_T = RF - T) are given in convTree.cxx
Two testing parameters are added
mwpcEvt.TestVar[det] -> event parameter in each detectors
mwpcFis.TestPar -> single parameter.
- Yun Jeung. May 2019 update
------------------------------------------------------------------------------------*/
void ZClass::Loop(int minEntry,int maxEntry,MWPCgeo* mwpcGeo,MWPCcorr* mwpcCorr,reactPar* rxn, targetPar* targ, char* datafile)
{
  //initialize variables
  int initEvent=0;
  int finEvent=0;
  int det; // here testing
  //MWPCEvt mwpcEvt[N_MWPC]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}; //for storing event information (27)
  MWPCEvt mwpcEvt[N_MWPC]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}; //for storing event information (29), Modifed by Annette 8Feb2021
  MWPCfis mwpcFis={0,0,0,0,{0,0},0,0,{0,0},0,0,0,0,0,0,0,0,{0,0},0,0,0,0,0,0,0,0,0,0,0}; //src_v5.0 (31)

  int monE[N_mon];
  int monTac;
  int t1t2Tac;
  int pulsTAC;
  int rf;
  MassTable m;
  int backT[4];

  Long64_t nentries = fChain->GetEntries();

  //create new sorted tree
  TFile* sfile=new TFile(datafile,"RECREATE");
  TTree* CubeTreeNew=new TTree("CubeTreeNew","Cube Tree sorted");
  TTree* RawTreeNew=new TTree("RawTreeNew","Raw Tree sorted");

  //FETECTOR 1 BRANCHES
  det=0;

  // calculate the energy loss tables

  if (rxn->dEcalcType != 0) {
    std::cout << "Calculating Energy Loss Table..." << std::flush;
    calcElossTable(rxn, targ);
    std::cout << " DONE. (╯°□°)╯︵ ┻━┻ " << std::flush;
  } else {
    std::cout << "Running without energy loss calculations, why?" << std::endl;
  }


  // raw parameters
  RawTreeNew->Branch("X1B",&mwpcEvt[det].XB_raw,"X1B_ch/s");
  RawTreeNew->Branch("X1F",&mwpcEvt[det].XF_raw,"X1F_ch/s");
  RawTreeNew->Branch("Y1B",&mwpcEvt[det].YB_raw,"Y1B_ch/s");
  RawTreeNew->Branch("Y1T",&mwpcEvt[det].YT_raw,"Y1T_ch/s");
  RawTreeNew->Branch("rs_T1",&mwpcEvt[det].T_raw,"rs_T1_ch/s");

  // CubeTreeNew->Branch("det1", &mwpcEvt[0], "raw_x/I:raw_y:raw_T:raw_E:QuadID:x_det/F:y_det/F:x:y:z:r:theta:phi:thetaCM:t:v:Elab:Eloss:Emratio:TestVar");
  // CubeTreeNew->Branch("det2", &mwpcEvt[1], "raw_x/I:raw_y:raw_T:raw_E:QuadID:x_det/F:y_det/F:x:y:z:r:theta:phi:thetaCM:t:v:Elab:Eloss:Emratio:TestVar");
  // CubeTreeNew->Branch("det3", &mwpcEvt[2], "raw_x/I:raw_y:raw_T:raw_E:QuadID:x_det/F:y_det/F:x:y:z:r:theta:phi:thetaCM:t:v:Elab:Eloss:Emratio:TestVar");
  CubeTreeNew->Branch("raw_x1",&mwpcEvt[det].x_raw,"x1_ch/S"); //Subtraction raw_x1 = X1B - X1F
  CubeTreeNew->Branch("raw_y1",&mwpcEvt[det].y_raw,"y1_ch/S"); //Subtraction raw_y1 = Y1B - Y1T

  CubeTreeNew->Branch("raw_E1",&mwpcEvt[det].e_raw,"E1_ch/s");
  CubeTreeNew->Branch("raw_T1",&mwpcEvt[det].t_raw,"T1_ch/S"); //Subtraction raw_T1 = RF - org_t1
  CubeTreeNew->Branch("QuadID_1",&mwpcEvt[det].quadID,"quadID_1/s");

  // calibrated parameters
  CubeTreeNew->Branch("x1det",&mwpcEvt[det].x_mm,"x1det_mm/F");
  CubeTreeNew->Branch("y1det",&mwpcEvt[det].y_mm,"y1det_mm/F");

  CubeTreeNew->Branch("x1",&mwpcEvt[det].x,"x1_mm/F");
  CubeTreeNew->Branch("y1",&mwpcEvt[det].y,"y1_mm/F");
  CubeTreeNew->Branch("z1",&mwpcEvt[det].z,"z1_mm/F");

  CubeTreeNew->Branch("r1",&mwpcEvt[det].r,"r1_mm/F");
  CubeTreeNew->Branch("theta1",&mwpcEvt[det].theta,"theta1_deg/F");
  CubeTreeNew->Branch("phi1",&mwpcEvt[det].phi,"phi1_deg/F");

  CubeTreeNew->Branch("thetaCM1",&mwpcEvt[det].thetaCM,"ThetaCM1_deg/F");

  CubeTreeNew->Branch("t1",&mwpcEvt[det].t,"t1_ns/F");
  CubeTreeNew->Branch("v1",&mwpcEvt[det].v,"v1_mm_ns/F");

  CubeTreeNew->Branch("Elab1",&mwpcEvt[det].Elab,"Elab1_MeV/F");
  CubeTreeNew->Branch("Eloss1",&mwpcEvt[det].Eloss,"Eloss1_MeV/F");
  CubeTreeNew->Branch("Emratio1",&mwpcEvt[det].Emratio,"Emratio1_MeV_amu/F");

  CubeTreeNew->Branch("TestVar1",&mwpcEvt[det].TestVar,"TestVar1/F");//values from each detector
  //FETECTOR 2 BRANCHES
  det=1;

  // raw parameters
  RawTreeNew->Branch("X2B",&mwpcEvt[det].XB_raw,"X2B_ch/s");
  RawTreeNew->Branch("X2F",&mwpcEvt[det].XF_raw,"X2F_ch/s");
  RawTreeNew->Branch("Y2B",&mwpcEvt[det].YB_raw,"Y2B_ch/s");
  RawTreeNew->Branch("Y2T",&mwpcEvt[det].YT_raw,"Y2F_ch/s");
  RawTreeNew->Branch("rs_T2",&mwpcEvt[det].T_raw,"rsT2_ch/s");

  CubeTreeNew->Branch("raw_x2",&mwpcEvt[det].x_raw,"x2_ch/S");//Subtraction raw_x2 = x2_left - x2_right
  CubeTreeNew->Branch("raw_y2",&mwpcEvt[det].y_raw,"y2_ch/S");//Subtraction raw_y2 = y2-bot - y2_top

  CubeTreeNew->Branch("raw_E2",&mwpcEvt[det].e_raw,"E2_ch/s");
  CubeTreeNew->Branch("raw_T2",&mwpcEvt[det].t_raw,"T2_ch/S");//Subtraction raw_T2 = RF - org_t2
  CubeTreeNew->Branch("QuadID_2",&mwpcEvt[det].quadID,"quadID_2/s");

  // calibrated parameters
  CubeTreeNew->Branch("x2det",&mwpcEvt[det].x_mm,"x2det_mm/F");
  CubeTreeNew->Branch("y2det",&mwpcEvt[det].y_mm,"y2det_mm/F");

  CubeTreeNew->Branch("x2",&mwpcEvt[det].x,"x2_mm/F");
  CubeTreeNew->Branch("y2",&mwpcEvt[det].y,"y2_mm/F");
  CubeTreeNew->Branch("z2",&mwpcEvt[det].z,"z2_mm/F");

  CubeTreeNew->Branch("r2",&mwpcEvt[det].r,"r2_mm/F");
  CubeTreeNew->Branch("theta2",&mwpcEvt[det].theta,"theta2_deg/F");
  CubeTreeNew->Branch("phi2",&mwpcEvt[det].phi,"phi2_deg/F");
  CubeTreeNew->Branch("thetaCM2",&mwpcEvt[det].thetaCM,"ThetaCM2_deg/F");

  CubeTreeNew->Branch("t2",&mwpcEvt[det].t,"t2_ns/F");
  CubeTreeNew->Branch("v2",&mwpcEvt[det].v,"v2_mm_ns/F");

  CubeTreeNew->Branch("Elab2",&mwpcEvt[det].Elab,"Elab2_MeV/F");
  CubeTreeNew->Branch("Eloss2",&mwpcEvt[det].Eloss,"Eloss2_MeV/F");
  CubeTreeNew->Branch("Emratio2",&mwpcEvt[det].Emratio,"Emratio2_MeV_amu/F");

  CubeTreeNew->Branch("TestVar2",&mwpcEvt[det].TestVar,"TestVar2/F");//values from each detector
  //********************************************************************************************************

  //3rd Detector
  det=2;

  RawTreeNew->Branch("X3B",&mwpcEvt[det].XB_raw,"X3B_ch/s");
  RawTreeNew->Branch("X3F",&mwpcEvt[det].XF_raw,"X3F_ch/s");
  RawTreeNew->Branch("Y3B",&mwpcEvt[det].YB_raw,"Y3B_ch/s");
  RawTreeNew->Branch("Y3T",&mwpcEvt[det].YT_raw,"Y3T_ch/s");
  //RawTreeNew->Branch("rs_T3A",&mwpcEvt[det].T_raw,"rsT3A_ch/s");
  //RawTreeNew->Branch("rs_T3B",&mwpcEvt[det].T_raw,"rsT3B_ch/s");
  RawTreeNew->Branch("rs_T3A",&mwpcEvt[det].T3A_raw,"rsT3A_ch/s");//Modifed by Annette 8Feb2021
  RawTreeNew->Branch("rs_T3B",&mwpcEvt[det].T3B_raw,"rsT3B_ch/s");//Modifed by Annette 8Feb2021

  CubeTreeNew->Branch("raw_x3",&mwpcEvt[det].x_raw,"x3_ch/S"); //Subtraction raw_x3 = x3_left - x3_right
  CubeTreeNew->Branch("raw_y3",&mwpcEvt[det].y_raw,"y3_ch/S"); //Subtraction raw_y3 = y3-bot - y3_top

  CubeTreeNew->Branch("raw_E3",&mwpcEvt[det].e_raw,"E3_ch/s");
  CubeTreeNew->Branch("raw_T3",&mwpcEvt[det].t_raw,"T3_ch/S"); //Subtraction raw_T3 = RF - org_t3a or raw_T3 = RF - org_t3b
  CubeTreeNew->Branch("QuadID_3",&mwpcEvt[det].quadID,"quadID_3/s");

  // calibrated parameters
  CubeTreeNew->Branch("x3det",&mwpcEvt[det].x_mm,"x3det_mm/F");
  CubeTreeNew->Branch("y3det",&mwpcEvt[det].y_mm,"y3det_mm/F");

  CubeTreeNew->Branch("x3",&mwpcEvt[det].x,"x3_mm/F");
  CubeTreeNew->Branch("y3",&mwpcEvt[det].y,"y3_mm/F");
  CubeTreeNew->Branch("z3",&mwpcEvt[det].z,"z3_mm/F");

  CubeTreeNew->Branch("r3",&mwpcEvt[det].r,"r3_mm/F");
  CubeTreeNew->Branch("theta3",&mwpcEvt[det].theta,"theta3_deg/F");
  CubeTreeNew->Branch("phi3",&mwpcEvt[det].phi,"phi3_deg/F");

  CubeTreeNew->Branch("thetaCM3",&mwpcEvt[det].thetaCM,"ThetaCM3_deg/F");

  CubeTreeNew->Branch("t3",&mwpcEvt[det].t,"t3_ns/F");
  CubeTreeNew->Branch("v3",&mwpcEvt[det].v,"v3_mm_ns/F");

  CubeTreeNew->Branch("Elab3",&mwpcEvt[det].Elab,"Elab3_MeV/F");
  CubeTreeNew->Branch("Eloss3",&mwpcEvt[det].Eloss,"Eloss3_MeV/F");
  CubeTreeNew->Branch("Emratio3",&mwpcEvt[det].Emratio,"Emratio3_MeV_amu/F");

  CubeTreeNew->Branch("TestVar3",&mwpcEvt[det].TestVar,"TestVar3/F");//values from each detector
  //**************************************************************************************************

   // COINCIDENT EVENTS - DERIVED PARAMETERS - ASSUMES 2-BODY FISSION
  CubeTreeNew->Branch("tdiff",&rxn->tdiff,"tdiff_ns/F");
  CubeTreeNew->Branch("tdiff12",&rxn->tdiff12,"tdiff12_ns/F");
  CubeTreeNew->Branch("tdiff32",&rxn->tdiff32,"tdiff32_ns/F");

  CubeTreeNew->Branch("Vcn",&rxn->Vcn,"Vcn_mm_ns/F");

  // CubeTreeNew->Branch("fis", &mwpcFis, "vpar/F:vperp:vdsine:MR:mass1:mass2:phi12:theta12:Vcm1:Vcm2:VcmSum:dESum:TKE:EvtNo/i:TSharp/F:T0der:dTder:dT3der:Vcmfold1:Vcmfold2:Qfis:TKE_fold:ExFis:DetChoice/I:TestPar/F:tback:thetaback:phiback:thetaCMback:raw_tback");
  CubeTreeNew->Branch("vpar",&mwpcFis.vpar,"vpar_mm_ns/F");
  CubeTreeNew->Branch("vperp",&mwpcFis.vperp,"vperp_mm_ns/F");
  CubeTreeNew->Branch("vdsine",&mwpcFis.vdsine,"vdsine_mm_ns/F");

  CubeTreeNew->Branch("MR",&mwpcFis.mr,"MR/F");
  CubeTreeNew->Branch("mass1",&mwpcFis.mass[0],"Mass1/F");
  CubeTreeNew->Branch("mass2",&mwpcFis.mass[1],"Mass2/F");
  CubeTreeNew->Branch("phi12",&mwpcFis.phi12,"Phi12/F");
  CubeTreeNew->Branch("theta12",&mwpcFis.theta12,"Theta12/F");

  CubeTreeNew->Branch("Vcm1",&mwpcFis.Vcm[0],"Vcm1/F");
  CubeTreeNew->Branch("Vcm2",&mwpcFis.Vcm[1],"Vcm2/F");
  CubeTreeNew->Branch("VcmSum",&mwpcFis.VcmSum,"VcmSum/F");
  CubeTreeNew->Branch("dESum",&mwpcFis.dESum,"dESum/F");

  CubeTreeNew->Branch("TKE",&mwpcFis.TKE,"TKE/F");
  CubeTreeNew->Branch("TKEViola",&rxn->TKEViola,"TKEViola/F");
  CubeTreeNew->Branch("TSharp",&mwpcFis.TSharp,"TSharp/F");
  CubeTreeNew->Branch("EvtNo",&mwpcFis.EvtNo,"EvtNo/l");

  CubeTreeNew->Branch("T0der",&mwpcFis.T0der,"T0der/F");
  CubeTreeNew->Branch("dTder",&mwpcFis.dTder,"dTder/F");
  CubeTreeNew->Branch("dT3der",&mwpcFis.dT3der,"dT3der/F"); //src_v5.0

  CubeTreeNew->Branch("vCMfold1",&mwpcFis.vCMfold[0],"vCMfold_1_mm_ns/F");
  CubeTreeNew->Branch("vCMfold2",&mwpcFis.vCMfold[1],"vCMfold_2_mm_ns/F");

  CubeTreeNew->Branch("Qfis",&mwpcFis.Qfis,"Qfis_MeV/F");
  CubeTreeNew->Branch("TKE_fold",&mwpcFis.TKE_fold,"TKE_fold_MeV/F");

  CubeTreeNew->Branch("ExFis",&mwpcFis.ExFis,"ExFis/F");

  CubeTreeNew->Branch("EbeamCM",&rxn->Ecm,"EbeamCM/F");
  ////HMD***
  CubeTreeNew->Branch("DetChoice",&mwpcFis.choice,"DetChoice/I");

  // Back detectors combined parameters. JW
  CubeTreeNew->Branch("tback",&mwpcFis.tback,"tback_ns/F");
  CubeTreeNew->Branch("thetaback",&mwpcFis.thetaback,"thetaback_deg/F");
  CubeTreeNew->Branch("phiback",&mwpcFis.phiback,"phiback_deg/F");
  CubeTreeNew->Branch("thetaCMback",&mwpcFis.thetaCMback,"thetaCMback_deg/F");

  CubeTreeNew->Branch("raw_tback",&mwpcFis.raw_tback,"raw_tback_ns/F");//src_v4.3
  CubeTreeNew->Branch("TestPar",&mwpcFis.TestPar,"TestPar/F"); //src_v5.0

  //MONITOR BRANCHES
  CubeTreeNew->Branch("Mon1E",&monE[0],"Mon1E_ch/s");
  CubeTreeNew->Branch("Mon2E",&monE[1],"Mon2E_ch/s");

  RawTreeNew->Branch("PulsTAC",&pulsTAC,"PulsTAC_ch/s");//FissionPulsers
  RawTreeNew->Branch("MonTAC",&monTac,"MonTAC_ch/s");
  RawTreeNew->Branch("T1T2TAC",&t1t2Tac,"T1T2TAC_ch/s");
  RawTreeNew->Branch("RF",&rf,"RF_ch/s");

  //2016Linac
  RawTreeNew->Branch("raw_T1A",&backT[0],"raw_T1A_ch/S"); //3rd det timing signal
  RawTreeNew->Branch("raw_T1B",&backT[1],"raw_T1B_ch/S");
  RawTreeNew->Branch("raw_T1C",&backT[2],"raw_T1C_ch/S");
  RawTreeNew->Branch("raw_T1D",&backT[3],"raw_T1D_ch/S");


  //error checking on event bounds
  if (maxEntry==0||maxEntry>(int)nentries){
    finEvent=(int)nentries;
    }
  else{
    finEvent=maxEntry;
  }
  if (minEntry>0 && minEntry<=finEvent){
    initEvent=minEntry;
  }
  else{
    if (minEntry>(int)nentries){
      cout << "Error: Loop: You have asked the sort to begin after the last event in the raw datafile!"<< endl;
      cout << "       Setting the start event to 0 by default. " << endl;
      initEvent=0;
    }
    else{
      initEvent=0;
    }
  }//end bound checking

  //for testing:
  cout << endl<< "Sorting events " << initEvent<< "-" << finEvent << "..." << endl;

  //Actually begin loop through events:
  for (int jentry=initEvent; jentry<=finEvent; jentry+=1) { //loop over events

    GetEntry(jentry);  //Get each event

    // save raw parameters

    //BACK DETECTOR
    det = 0;
    mwpcEvt[det].x_raw=(int)Cube_XBack;
    mwpcEvt[det].y_raw=(int)Cube_YBack;
    mwpcEvt[det].t_raw=(int)Cube_TBack;
    mwpcEvt[det].e_raw=(int)Cube_EBack;

    mwpcEvt[det].XB_raw=(int)Raw_rs_x1b;
    mwpcEvt[det].XF_raw=(int)Raw_rs_x1f;
    mwpcEvt[det].YB_raw=(int)Raw_rs_y1b;
    mwpcEvt[det].YT_raw=(int)Raw_rs_y1t;
    mwpcEvt[det].T_raw=(int)Raw_rs_t1;

    //front detector
    det = 1;
    mwpcEvt[det].x_raw=(int)Cube_XFront;
    mwpcEvt[det].y_raw=(int)Cube_YFront;
    mwpcEvt[det].t_raw=(int)Cube_TFront;
    mwpcEvt[det].e_raw=(int)Cube_EFront;

    mwpcEvt[det].XB_raw=(int)Raw_rs_x2b;
    mwpcEvt[det].XF_raw=(int)Raw_rs_x2f;
    mwpcEvt[det].YB_raw=(int)Raw_rs_y2b;
    mwpcEvt[det].YT_raw=(int)Raw_rs_y2t;
    mwpcEvt[det].T_raw=(int)Raw_rs_t2;

    //3rd detector
    det = 2;
    mwpcEvt[det].x_raw=(int)Cube_XBackSmall;
    mwpcEvt[det].y_raw=(int)Cube_YBackSmall;
    mwpcEvt[det].t_raw=(int)Cube_TBackSmall;
    mwpcEvt[det].e_raw=(int)Cube_EBackSmall;

    mwpcEvt[det].XB_raw=(int)Raw_rs_x3b;
    mwpcEvt[det].XF_raw=(int)Raw_rs_x3f;
    mwpcEvt[det].YT_raw=(int)Raw_rs_y3b;
    mwpcEvt[det].YB_raw=(int)Raw_rs_y3t;
    //mwpcEvt[det].YT_raw=(int)Raw_rs_t3;
    mwpcEvt[det].T3A_raw=(int)Raw_rs_t3a; //Modifed by Annette 8Feb2021
    mwpcEvt[det].T3B_raw=(int)Raw_rs_t3b; //Modifed by Annette 8Feb2021

    //monitors
    monE[0]=(int)Monitors_Monitor1;
    monE[1]=(int)Monitors_Monitor2;
    pulsTAC=(int)Monitors_FissionPulser;
    monTac=(int)Monitors_Mon_Tac;
    t1t2Tac=(int)Monitors_T1T2_Tac;
    rf=(int)Raw_RF;

    //2016Linac
    backT[0]=(int)Cube_TBack_A;
    backT[1]=(int)Cube_TBack_B;
    backT[2]=(int)Cube_TBack_C;
    backT[3]=(int)Cube_TBack_D;

    mwpcFis.EvtNo=jentry;

    // check time data is correct
    // corrects error in convTree and accounts for time difference method
    // Modifed by Annette 8Feb2021
    checkTime(mwpcEvt,rxn,rf);

    //fill the rest of the event with calibrated parameters
    completeEvent(mwpcEvt, mwpcGeo, mwpcCorr, &mwpcFis, rxn, targ, &m);

    //fill the tree
    CubeTreeNew->Fill();
    RawTreeNew->Fill();

    //print statement to keep people occupied while sort continues
    if ( jentry%1000 == 0 ){
      cout << "  Current Event: "; //**
     cout << jentry << "\r" << flush;

    }
    if (jentry==finEvent){
      cout << "\n" << endl;
      cout << "Total events: " << finEvent-initEvent+1 << "  Last Event: " ;
      cout << finEvent << endl;
    }

  }// end loop over events


  CubeTreeNew->Write("",TObject::kOverwrite);
  RawTreeNew->Write("",TObject::kOverwrite);

  cout << "Sort complete! " << endl;

  delete CubeTreeNew;
  delete RawTreeNew;
  delete sfile;


}//end CubeClass::Loop

/*------------------------------------------------------------------------------------
                            CubeSort
 : Sorting data event-by-event
------------------------------------------------------------------------------------*/
int CubeSort(char* argv, int nini){

  //define variables for user input
  ifstream input, detinput;

  string detinfo, procdata;
  string procext;
  int errCheck;

  char *sortfile;

  char *datafile;
  char *procfile;

  //define+initialize detector structures; see CubeStruct.h
  MWPCgeo mwpcGeo[N_MWPC]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
  MWPCcorr mwpcCorr[N_MWPC]={{0,0,0,0,{0,0,0,0},0,0}};

  //define+initialize structures to store reaction+target params; see CubeStruct.h
  reactPar rxn={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //src_v5.0 +2
  targetPar targ={0,0,0,0,0,0,0,{0,0}};

  rxn.niTag=nini;//controls whether symmetric system assumptions are applied in data

  //define and initialize input structures (see CubeStruct.h)
  sortInfo sortStruct={"","","",0,0,0,0,0,0,0,0,"","",""};

  // open user-provided input file defining sort
  sortfile=argv;

  // read in input file information and store in appropriate place
  errCheck=readInput(sortfile,&sortStruct,&rxn,&targ);

  if ( errCheck == -1 ){
    cout << "Input file read-in error. Sort failed." << endl;
    return -1;
  }

  datafile = &sortStruct.rawdata[0];//create char array with file name to be sorted
  cout << "Sorting " << datafile << " ... " << endl;

  procdata = AutoFileExtension(&sortStruct,&rxn); //figure out name of processed file
  procfile = &procdata[0];
  cout << "Processed file " << procfile << " will be produced ... " << endl;

  ////\\\\ end sort file input \\\\////

  //calculate energy loss correction first - this assumes interactions halfway through EACH target layer
  //if (rxn.elossBool==true){
  if (rxn.dEcalcType==1 || rxn.dEcalcType==2 || rxn.dEcalcType==3 ){
    calcElossBeam(&targ,&rxn); // in CubeReact.cxx
  }

  //immediately calculate the remaining undefined parameters in rxn struct
  calcRxn(&rxn);

  //next: read in the detector file contents
  errCheck = readDetInfo(&sortStruct,mwpcGeo,mwpcCorr);

  if (errCheck==-1){
    return -1;
  }

  //Now do the calibrations:

  //Position (using detector edges):
  for (int i=0;i<N_MWPC;i++){
    mwpcPosCalib(&mwpcGeo[i]);
    calcMwpcCenter(&mwpcGeo[i]);
    calcQuadrantCenter(&mwpcGeo[i],&mwpcCorr[i]);
  }

  //actually begin the sort

  TFile *f1;
  f1=new TFile(datafile);  // File to be sorted
  if (!f1->IsOpen()){
    cout << "CubeSort: Error: could not find sort file" << endl;
    return -1;
  }

  // Assumes CubeTree structure - define TTree name, get object
  TTree* CubeTree;
  f1->GetObject("CubeTree",CubeTree);

  //create new CubeClass type
  ZClass sort(CubeTree);

  //initialize sort for CubeTree
  //sort.Init(CubeTree);

  //complete sort as defined in Loop
  sort.Loop(sortStruct.minEntry,sortStruct.maxEntry,mwpcGeo,mwpcCorr,&rxn,&targ,procfile);

  //Felete CubeTree object after sort is complete
  delete CubeTree;
  delete f1;
  return 0;

}// ***** end CubeSort ******//
