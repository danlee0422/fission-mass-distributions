#ifndef CubeSort_h
#define CubeSort_h

/* ROOT header files */
#include "TTree.h"
#include "TH2D.h"
#include "TH2I.h"
#include "TCutG.h"
#include "TStyle.h"

/*Specialized header files*/
#include "CubeStruct.h"
#include "CubeInput.h"
#include "CubeDet.h"
#include "CubeFis.h"
#include "CubeReact.h"
//#include "CubeClass.h"
#include "ZClass.h"
#include "physConst.h"
#include "eloss.h"
#include "MassTable.h"
#include "CubeLoss.h"

void completeEvent(MWPCEvt*, MWPCgeo*, MWPCcorr*, MWPCfis*, reactPar*, targetPar*, MassTable*);
int CubeSort(char*,int);

#endif
