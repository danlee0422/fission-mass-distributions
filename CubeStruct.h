#ifndef GUARD_CubeStruct
#define GUARD_CubeStruct

#include <string>

// define number of detectors of each type here
#define N_MWPC 3
#define N_mon 2
//#define N_Ge 3
#define xsecTest 0
#define xsecTest2 0
//includes structure definitions for all structures used in dacube

//************** input file structure ************************//
struct sortInfo {
    //total = 14

    std::string rawdata; // path+filename for unprocessed root file location
    std::string detfilepath; //path+filename for detector information file
    std::string calibpath; //path to where calibration sorts should be stored
    int minEntry, maxEntry;
    float T0init, T0fin, T0step;
    float dTinit, dTfin, dTstep;
    std::string procfile;
    std::string gatefile;
    std::string gates;

};

//*************** Event / Fission structures ********************//
struct MWPCEvt {
    //total = 27

    int x_raw,y_raw;
    int t_raw,e_raw;
    int quadID; //stores quadrant identification for each event


    float x_mm,y_mm; //pos coordinates in detector coord sys (mm,mm)
    float x_orig, y_orig; //used to store original detector coordinates for quadrant corrections if shift in raw position data is required

    float x,y,z; // position coordinates (cartesian, mm)
    float r,theta,phi; //position coordinates (spherical, mm, deg, deg)
    float thetaCM;//since VCN is used, I assume fusion occurred
    //float t;
    float t;
    float v;

    //energy loss variables
    float Elab, Eloss, Emratio;
    //Test variable *** Berriman****
    float TestVar;

    //int XB_raw, XF_raw,YB_raw,YT_raw,T_raw;//Modifed by Yun
    int XB_raw, XF_raw,YB_raw,YT_raw,T_raw,T3A_raw,T3B_raw;//Modifed by Annette 8Feb2021

};

//*************** Derived parameter structures ********************//

struct MWPCfis {
    //total = 31

    //  float vpar,vperp,mr;
    float vpar,vperp,vdsine,mr; //vdsine = (vpar-Vcn)sinThetaCM
    float mass[2];
    float phi12,theta12; // phi12=phi1-phi2 (deg); theta12=folding angle=theta1+theta2 (deg)
    float Vcm[2];
    float VcmSum,dESum;
    float TKE;
    unsigned int EvtNo;
    float TSharp;
    float T0der, dTder, dT3der; //src_v5.0
    float vCMfold[2]; //velocities calculated from folding angles and masses
    float Qfis, TKE_fold;//Qvalue is Qval for CN-> FF1+FF2 rxn; TKE_fold is TKE calculated using folding angle and mass ratio
    float ExFis; //Ex is excitation energy of fission fragments
    int choice;
    //Test variable *** Yun****
    float TestPar;


    float tback,thetaback,phiback,thetaCMback; // back parameters choose between detector 1 and detector 3 -JW
    float raw_tback; // src_v4.3
    int unphys;//error checking variable. Set to 1 if energy loss calculations were not performed because the event was unphysical (one fragment had Z=0, total energy very high)
}; // stores variables calculated using kinematic coincidence method


// store reaction properties
struct reactPar {
    //total = 26

    int Ap,Zp,At,Zt;
    int Acn,Zcn;
    float Elab,Ecm;
    int beamType;
    int VsumType; //Set Velocity Sum Calculation type (0=Viola, 1=Experimental (e.g. Mulgin), 2=Logan)
    float Vcn;
    float T0,dT,tdiff,tdiff12,tdiff32;
    // bool elossBool;
    int dEcalcType; //Fission fragment E loss calculation type (1=Ziegler,2=Bohr,3=Knyazheva)
    int Eloop; // src_v5.0
    bool tsharpBool;
    float intercept; // edited Yun 16/06/17
    float TKEViola;
    int niTag;
    float QvalCN, ExCN;
    float T03,dT3;
};

//store target parameters
struct targetPar {
    //total = 9

    float targThick,targTheta, backThick, frontThick;//ug/cm^2, deg,ug/cm^2,ug/cm^2
    int targZ,backZ,frontZ;
    float angle[2];
};



//***************** DETECTOR STRUCTURES ************************//
struct MWPCgeo {
    //total = 19
    unsigned int detID;
    //MWPC geometry variables
    int XLch,XRch,YBch,YTch; //detector edges in channels
    float XLmm,XRmm,YBmm,YTmm; //detector edges in mm

    float detTheta,detPhi; //detector position theta,phi (input file deg, converted to rad)
    float detDist; // detector position relative to centre of target in mm

    float ax_mm,bx_mm,ay_mm,by_mm;
    //to store position calibrations; x(mm)=ax*x(ch)+bx form assumed
    //to store position calibrations; y(mm)=ay*x(ch)+by form assumed
    float x0,y0,z0; //detector center in cartesian coordinates
    int signPhi; //used in pos reconstruction of events (z addition depends on phi of dets)

};

//define correction parameter structure for mwpc
struct MWPCcorr {
    //total = 10

    float detxOffset,detyOffset; // x,y offset of physical detector centre (-12 mm offset of centre foil to be built into the quad reconstruction)
    float qx0,qy0;//centre of quadrants in detector coord system [mm]
    float qc[4]; //quadrant corrections
    int qcunits; //0=ch,1=ns
    float timeSlope; //slope of time calibration for conversion from ch->ns

};

//************* CROSS SECTION STRUCTURE *******************//
struct xsecInfo {
    //total = 16
    std::string sortFile;
    std::string dataFile;
    unsigned int preScal;
    float monAngleLab; //deg
    float monAngleCM; //deg
    unsigned int monScal[2];
    unsigned int cubeScal;
    unsigned int monDAQ[2];
    unsigned int cubeDAQ;
    unsigned int monSumPeak;
    bool fifrangOut;
    float ruthMon;
    float monEffCal; //averaged for both dets
    float cubeEffCal;

};

struct xsecSpecs {
    //total =2
    std::string resultsPath;
    float binSize; // units: degrees
};


#endif
