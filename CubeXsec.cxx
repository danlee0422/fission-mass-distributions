// Code for calculating cross sections based on user-provided fission and calibration root trees,
// sort parameters, and additional information for deadtime / solid angle corrections.
//
// Does NOT assume a mass split of 0.5!
//
// Liz Williams, Nov 2014

/* Testing phase: checks performed
   Feb 2015 
   - fixed an int/int->float calculation error and confirmed that the 
   differential xsection for a test CRANU run follows the right trend, at least.
   - Found a lab->CM angle order error in function input
   - Added in list structure to deal with multiple fission files
   Mar 2015
   - Decided to restructure the code for ease of future use.
   April 2015
   - Testing confirms the code works reasonably well; best for bin sizes of 5 degrees or larger, but angular distributions (on average) 
   are consistent with Rutherford scattering calcs even for 1 degree bins. I'm still not sure why the SA correction isn't perfect.

////////////////////////////////////////////READ////////////////////////////////////////////////////////////
////////////////////////////////////////////READ!!!/////////////////////////////////////////////////////////
////////////////////////////////////////////R!E!A!D!////////////////////////////////////////////////////////
May 2017 edited by Yun. 

Standard version of CubeXsec.cxx (I have my own CubeXec.cxx for my analysis)

This code has been tested by me (Yun) with SICO run (30Si+208Pb) and compared with old code calculations.
All tested files are available in nucserver1 or 7. Several misconceptions were found and fixed them. 

This code sorts data event-by-event. Therefore we don't need to use Lab to CM conversion factor! (deleted dXLab2dXCM term)

SA normalisation was done in dsig/dtheta not directrly done in dsig/domega. Therefore, there is sin term in NormConst.
finRuth=finRuth*2*pi*sin(ThetaCalib*d2r) (Rutherford cross section in Lab frame for calibration run).

Redefine SA normalisation coeffient.

normVal = normConst

dsigdth=normVal; // [mb/sr]
dsigdomega = normVal/(2*pi*sin(fisThetaCM*d2r));  //[mb/rad]

Figures for papers, you need to use figures in /XsecDisplay directory.
Figures in this directory, histograms are filled with dsig/dtheta.  you can change to dsigdomega.

Because MR spectrum is also binned into N bins (normally 100) per unit MR. This information also need to be taking into account.
If it's not 100 bin, you need to change MR bin in this script.
norm plot only works when you sort bin size is 3!!!!! or give the same bin size to experimental MAD (Exp_MAD)!!

Ascii file for binned thetaCM sorting (using calcAngDist function) is 'fisAngDist_#.dat'.
Ascii file for event-by-event sorting is xsecAndDist_#.dat //dsig/domega (you can change to dsig/dtheta)

This code only works for 2 detector configuration. 
For 3detector conf. you need to change

theta1->thetaback
thetaCM1->thetaCMback
And also need mondifications for reading calibration file.

 */


#include "CubeXsec.h"
#include "CubeHist.h"
#include <TGraph.h>
using namespace std;


int xsecCalc(char* xsecInputFile){

    TTree *fisTree;
    ofstream logfile;
    ofstream normfile;
    string test;

    //initialize universal parameters
    int monNum = 2;
    int errCheck = 0;
    xsecSpecs xsecSpec = {"",0};

    xsecInfo calibInfo = {"","",0,0,0,{0,0},0,{0,0},0,0,0,0,0,0};
    sortInfo calibSort = {"","","",0,0,0,0,0,0,0,0,"","",""};
    reactPar calibRxn = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //for 3det conf.  src_3det_v5.0 +2
    targetPar calibTarg = {0,0,0,0,0,0,0,{0,0}};

    //create lists for storing fission parameters for all runs. 
    list<xsecInfo> fisInfoList;
    list<sortInfo> fisSortList;
    list<reactPar> fisRxnList;
    list<targetPar> fisTargList;

    //Read in all relevant file names, trees, and parameters - populates required structures 
    errCheck = readXsecInput(xsecInputFile, &xsecSpec, &fisInfoList, &calibInfo, 
            &fisSortList, &calibSort, &fisRxnList, &calibRxn, 
            &fisTargList, &calibTarg);
    if (errCheck == -1){
        return -1;
    }

    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );

    stringstream logtime;
    logtime << (now->tm_year + 1900) << '-' 
        << (now->tm_mon + 1) << '-'
        <<  now->tm_mday << "_" << now->tm_hour << "-" << now->tm_min;
    string logPre = "xsec_";
    string logExt = ".log";

    string logName;
    string logFileName = logPre + logtime.str() + logExt;
    logName = xsecSpec.resultsPath + logFileName;

    //LOGFILE: open and print header
    logfile.open(&logName[0]);
    if (!logfile.is_open()){
        cout << "Error: couldn't open log file. Soldiering on...\n";
    }



    logfile << "Cross section calculation log, Date: " <<  (now->tm_year + 1900) << '-' 
        << (now->tm_mon + 1) << '-'
        <<  now->tm_mday
        << "\n\n";

    cout << "\n ************ CALIBRATION RUN **************** \n";

    //** Now carry out all calculations relating only to the calibration run **//
    errCheck = preCalcs(&calibInfo, &calibRxn, &calibTarg, monNum);

    if (errCheck == -1){
        cout << "Calibration run: Problem with efficiency calibrations. Check input file!\n";
        cout << calibInfo.monEffCal << "  " << calibInfo.cubeEffCal << "\n";
        logfile << "Error - calibration run efficiency calibrations  Check input file."; //this will be a return -1 tag from preCalcs function
    }

    //monitor normalization factor part 1, including cube efficiency correction. Calib only portion.
    float monNormConst_cal =  (float)calibInfo.monSumPeak / (calibInfo.ruthMon);


    //Calculate bin numbers, etc, for xsec calcs
    int numBins = (int)round( 180. / xsecSpec.binSize );
    if (numBins <= 0){
        cout << "Error: CubeXsec::xsecCalc: Calculated bin size is zero.\n";
        logfile << "Error - bin size. Calculation aborted.\n";
        return -1;
    }

    cout << "Calculating normalization constants for a bin size of " 
        << xsecSpec.binSize << ", corresponding to " 
        << numBins << " bins...\n\n";

    //now that we know bin numbers, create structures for storing xsec info
    float* th_cm=new float[numBins];
    float* xf=new float[numBins];//dsigdomega
    float* xferr=new float[numBins];
    //edited by Yun
    float* Xf=new float[numBins];//dsigdtheta
    float* Xferr=new float[numBins];

    //Create array for storing normalization data on stack
    float *normConst = new float[numBins];
    float *normConstErr = new float[numBins];

    int *yield_cal = new int[numBins];

    for (int i=0;i<numBins;i++){
        normConst[i]=0;
        normConstErr[i]=0;
    }

    //Access calibration root tree ********************************************
    //Calibration data - open file, get tree

    TFile calibFile(&calibInfo.dataFile[0],"update");
    TTree *calibTree;

    if (!calibFile.IsOpen()){
        cout << "Error: CubeXsec:: xsecCalc: Calibration data file " 
            << &calibInfo.dataFile[0] << " not found."<< endl;
        logfile << "Error: couldn't find calibration data file. ";
        logfile << "Calculation aborted.\n";
        return -1;
    }
    if (calibFile.GetListOfKeys()->Contains("GatedTree")==true){
        calibTree = (TTree*) calibFile.Get("GatedTree");
    }
    else if (calibFile.GetListOfKeys()->Contains("CubeTreeNew")==true){
        calibTree = (TTree*) calibFile.Get("CubeTreeNew");
    }
    else{
        cout << "Calibration file: Tree named GatedTree or CubeTreeNew "
            << "not found. Calculation aborted."<< endl;
        return -1;
    }


    //Create calibration file histograms
    TH1D calibTheta1("calibTheta1","calibTheta1",numBins,0,180);
    calibTree->Draw("theta1>>calibTheta1","","");
    //	calibTheta1.SetTitle("theta1;theta1[deg];Counts");
    calibTheta1.SetTitle("calibTheta1;theta1[deg];Counts");

    TH2D calibThLthCM("calibThLthCM","calibThLthCM",numBins,0,180,numBins,0,180);
    calibTree->Draw("thetaCM1:theta1>>calibThLthCM","","");
    //	calibThLthCM.SetTitle("theta1;thetaCM1[deg];Counts");
    calibThLthCM.SetTitle("CalibThLthCM (Counts);theta1 [deg];thetaCM1");

    calibFile.cd("/");

    float cThetaCMcalc=0.;
    float cThetaCM=0.;
    float cTheta=0.;
    float cRuth=0.;
    float cRuthLab=0.;


    TBranch *thetaCM = calibTree->GetBranch("thetaCM1");
    TBranch *theta = calibTree->GetBranch("theta1");

    TTree xsecCalibTree("xsecCalibTree","xsecCalibTree");

    TBranch *thetaCMcalc = xsecCalibTree.Branch("calibThetaCMcalc"
            ,&cThetaCMcalc,"thetaCM1calc/D");
    TBranch *newThetaCM = xsecCalibTree.Branch("calibthetaCM1"
            , &cThetaCM,"thetaCM1/D");
    TBranch *RuthObsThCM = xsecCalibTree.Branch("RuthObsThCM"
            , &cRuth,"RuthObsThCM/D");
    TBranch *RuthLab = xsecCalibTree.Branch("RuthLab"
            , &cRuthLab,"RuthLab/D");

    TH1D calibRuthHist("calibRuthHist","calibRuthHist",numBins,0,180);
    TH1D calibcalcThetaCM1("calibcalcThetaCM1","calibcalcThetaCM1",numBins,0,180);

    //calibRuthHist is used for normalization calculation later but
    //is not saved (it doesn't yet take into account the number of events per
    //bin)

    //ofstream testout;
    //testout.open("testout.dat");

    //get total events in fission tree
    Long64_t nentries = calibTree->GetEntries();
    //cout << "Calibration reaction parameters:\n";
    //cout << calibRxn.Zp << " " << calibRxn.Zt << " " << calibRxn.Ecm << endl;

    for (Long64_t i=0;i<nentries;i++){

        cThetaCMcalc=0.;
        cThetaCM=0.;
        cTheta=0.;
        cRuth=0.;
        cRuthLab=0.;

        thetaCM->SetAddress(&cThetaCM);
        theta->SetAddress(&cTheta);
        thetaCMcalc->SetAddress(&cThetaCMcalc);
        newThetaCM->SetAddress(&cThetaCM);
        RuthObsThCM->SetAddress(&cRuth);
        RuthLab->SetAddress(&cRuthLab);

        thetaCM->GetEvent(i);
        theta->GetEvent(i);

        cThetaCMcalc = thLab2ThCM_elastics(calibRxn.Ap, calibRxn.At, cTheta);
        cRuth=Rutherford(calibRxn.Zp, calibRxn.Zt, calibRxn.Ecm, cThetaCMcalc);
        cRuthLab=cRuth*dXCM2dXLab(cTheta, cThetaCMcalc);
        //testout << " " << cTheta << " " << cThetaCM << " " << cThetaCMcalc << endl;
        //cin.ignore();

        if(cThetaCMcalc > 0) {
            calibRuthHist.Fill(cTheta,cRuthLab);
            calibcalcThetaCM1.Fill(cThetaCMcalc);
            // testout << " " << cTheta << " " << cThetaCM << " " << cRuth << " " << cRuthLab << " " << cThetaCMcalc << endl;
        }

        xsecCalibTree.Fill();

    }

    //	testout.close();

    xsecCalibTree.Write("",TObject::kOverwrite);


    //normalize calibRuthHist to number of counts per bin (so Ruth is the 
    //Rutherford xsec, not Rutherford*counts)
    for (int i=1;i<=numBins;i++){

        float ruthBinContent = calibRuthHist.GetBinContent(i);
        float theta1BinContent = calibTheta1.GetBinContent(i);

        float ThetaCalib = calibTheta1.GetBinCenter(i);//Y


        float finRuth=0.;
        //	cout << i << " " << ruthBinContent << " " << theta1BinContent << endl;
        if (theta1BinContent>0){
            finRuth = ruthBinContent / (float)theta1BinContent;

            finRuth = finRuth*2*pi*sin(ThetaCalib*d2r); //11.05.17 Fixed!!!!!

            calibRuthHist.SetBinContent(i,finRuth); 
        }
        else{
            calibRuthHist.SetBinContent(i,0.);
        }

    }
    //Y
    calibRuthHist.SetTitle("theta1;Rutherford[lab, mb/sr]");
    cout << "Calibration tree recorded (for check on lab->CM)..." << endl;


    calibRuthHist.SetTitle("calibRuthHist;theta1;Rutherford[lab, mb/sr]");
    calibRuthHist.SetLineColor(kBlack);
    calibRuthHist.SetMarkerStyle(20);

    calibcalcThetaCM1.SetTitle("calcThetaCM;calcThetaCM;counts ");

    calibFile.cd("/");
    if (calibFile.GetListOfKeys()->Contains("RuthHist")!=true){
        calibFile.mkdir("RuthHist");
    }
    calibFile.cd("RuthHist");


    calibRuthHist.Write("",TObject::kOverwrite);
    calibTheta1.Write("",TObject::kOverwrite);
    calibcalcThetaCM1.Write("",TObject::kOverwrite);
    // calib histograms will be used / written to file later



    /////////////////// SET UP FISSION DATA PROCESSING ///////////////////

    //create iterator pointing to first element in fission list
    list<xsecInfo>::iterator fisInfoIndex = fisInfoList.begin();
    list<sortInfo>::iterator fisSortIndex = fisSortList.begin();
    list<reactPar>::iterator fisRxnIndex = fisRxnList.begin();
    list<targetPar>::iterator fisTargIndex = fisTargList.begin();

    string normstring, normstring2;


    //   ///////// LOOP OVER FISSION RUNS //////////  //
    int fisRunNum=0;

    while (fisInfoIndex != fisInfoList.end()){

        cout << "\n********** Fission Run " << fisRunNum << " ***********" << endl;

        if (fisRxnIndex == fisRxnList.end() || fisTargIndex == fisTargList.end() 
                || fisSortIndex ==fisSortList.end()){
            cout << "Error: mismatch in fission data structure indices! "
                << "Aborting." << endl;
            logfile << "Error: mismatch in fission data structure indices! "
                << "Aborting." << endl;
            return -1;
        }



        //define structures for all the relevant stuff... 
        xsecInfo& fisInfo = *fisInfoIndex;
        reactPar& fisRxn = *fisRxnIndex;
        targetPar& fisTarg = *fisTargIndex;

        //Do all initial calcs for fission run
        errCheck = preCalcs(&fisInfo, &fisRxn, &fisTarg, monNum);

        if ( errCheck == -1 ){
            cout << "Problem with fission efficiency calibrations. ";
            cout << "Check input file!\n";
            cout << fisInfo.monEffCal << "  " << fisInfo.cubeEffCal << "\n";
            logfile << "Error: problem with fission efficiency calibrations."
                << " Calculation aborted.\n";
            return -1;
        }

        if (fisInfo.monAngleLab!=calibInfo.monAngleLab){
            cout << "Beware: You should use calibration and fission runs from "
                << "the same experiment; monitor positions aren't precise!\n";
            logfile << "Beware: You should use calibration and fission runs "
                <<"from the same experiment; monitor positions aren't precise!\n";
        }

        float monErrTerm = 1./(float)fisInfo.monSumPeak 
            + 1./(float)calibInfo.monSumPeak;

        //monitor normalization factor, including cube efficiency correction
        float monNormConst = monNormConst_cal 
            * (calibInfo.cubeEffCal / fisInfo.cubeEffCal) 
            * (fisInfo.ruthMon / (float)fisInfo.monSumPeak);


        /////////////////// OPEN FISSION FILE FOR CURRENT RUN ///////////////////////

        TFile fisFile(&fisInfo.dataFile[0],"update");

        if (!fisFile.IsOpen()){
            cout << "Error: CubeXsec:: xsecCalc. Fission data file " 
                << fisInfo.dataFile << " not found."<< endl;
            return -1;
        }
        if (fisFile.GetListOfKeys()->Contains("GatedTree")==true){
            fisTree = (TTree*) fisFile.Get("GatedTree");
        }
        else if (fisFile.GetListOfKeys()->Contains("CubeTreeNew")==true){
            fisTree = (TTree*) fisFile.Get("CubeTreeNew");
        }
        else{
            cout << "VALID TREE NOT FOUND for " << fisInfo.dataFile << endl;
            return -1;
        }

        //Create the appropriately binned histogram
        fisFile.cd("/");
        if (fisFile.GetListOfKeys()->Contains("XsecInput")!=true){
            fisFile.mkdir("XsecInput");
        }
        fisFile.cd("XsecInput");

        calibTheta1.Write("",TObject::kOverwrite);
        calibThLthCM.Write("",TObject::kOverwrite);


        /////////// COMPUTE AND SAVE SOLID ANGLE NORMALIZATIONS ////////////

        //zero arrays
        for (int i=0;i<numBins;i++){
            normConst[i]=0;
            normConstErr[i]=0;

        }


        calcSAnorms(&xsecSpec,  
                normConst, normConstErr, 
                yield_cal,
                numBins, fisRunNum, 
                &calibTheta1, &calibRuthHist,
                monNormConst, monErrTerm,0);

        float slope = 2*(90-fisRxn.intercept)*1.0;

        cout<< ""<<endl;
        cout << "-------------------SA MAD Mirror Fuction--------------------------------" << endl;
        cout << " Intercept = " << fisRxn.intercept << " deg.\t\t" << "slope = " << slope  << endl;
        cout << "------------------------------------------------------------------------" << endl;


        //////////////////// XSEC TREE+HIST CREATION //////////////////////////

        //Ok, now I need to create a tree to store cross section normalizations, 
        //relevant data for cross section plots
        float normVal=0.0; //place to store point by point norm coefficient
        float normValBinned=0.;
        float fisThetaLab=0.0;
        float fisThetaCM=0.0;
        float binnedThetaCM=0.0;

        float fisMR=0.0;
        float fisTKE=0.0;
        float fisTKEViola=0.0;

        //From Liz's original code, I added more histograms and historames are divided into two groups, dsig/dtheta and dsigdomega

        //DEFINE HISTOGRAMS HERE

        TH2D Exp_MAD("Exp_MAD","Exp_MAD",100,0,1,60,0,180);// Normal experiments MAD plot [counts]


        //1D hist
        TH1D MR("MR","MR",100,0,1);

        TH1D xsecCM_O("xsecCM_O","xsecCM_O",numBins, 0,180);
        TH1D xsecCM_O_mir("xsecCM_O_mir","xsecCM_O_mir",numBins, 0,180);// same as Y projection of MrThCMxsec_O_mir

        TH1D xsecCM_T("xsecCM_T","xsecCM_T",numBins, 0,180);
        TH1D xsecCM_T_mir("xsecCM_T_mir","xsecCM_T_mir",numBins, 0,180);// same as Y projection of MrThCMxsec_T_mir

        //2D hist
        TH2D thLthCM("thLthCM","thLthCM",numBins,0,180,numBins,0,180);
        TH2D MrTKExsec("MrTKExsec","MrTKExsec",100,0,1,800,0,400);
        TH2D MrTKEVxsec("MrTKEVxsec","MrTKEVxsec",100,0,1,500,0,5);

        TH2D MrThCMxsec_T("MrThCMxsec_T","MrThCMxsec_T",100,0,1,numBins,0,180); //no mirroring
        TH2D MrThCMxsec_T_mir("MrThCMxsec_T_mir","MrThCMxsec_T_mir",100,0,1,numBins,0,180);

        TH2D MrThCMxsec_O("MrThCMxsec_O","MrThCMxsec_O",100,0,1,numBins,0,180); //no mirroring
        TH2D MrThCMxsec_O_mir("MrThCMxsec_O_mir","MrThCMxsec_O_mir",100,0,1, numBins,0,180);

        // For display plots. dsig/domega based plots
        TH2D MrThCMxsec("MrThCMxsec","MrThCMxsec",100,0,1,numBins,0,180); //no mirroring
        TH2D MrThCMxsec_mir("MrThCMxsec_mir","MrThCMxsec_mir",100,0,1,numBins,0,180);
        TH1D xsecCM("xsecCM","xsecCM",numBins, 0,180);	

        int MRbin; 
        MRbin = MR.GetXaxis()->GetNbins();

        fisFile.cd("/");

        //Branches from fission tree that I need
        TBranch *fthetaLab = fisTree->GetBranch("theta1");
        TBranch *fthetaCM = fisTree->GetBranch("thetaCM1");
        TBranch *fMR = fisTree->GetBranch("MR");
        TBranch *fTKE = fisTree->GetBranch("TKE");
        TBranch *fTKEViola = fisTree->GetBranch("TKEViola");


        //New tree and branch needed
        TTree newTree("xsecTree","xsecTree");

        TBranch *normBranch = newTree.Branch("xsecNormCM",
                &normVal,"xsecNormCM/D");
        TBranch *thetaCM = newTree.Branch("thetaCM1",
                &fisThetaCM,"thetaCM1/D");

        //get total events in fission tree
        Long64_t nentries = fisTree->GetEntries();

        for (Long64_t i=0;i<nentries;i++){

            //set addresses - read somewhere that I had to do this for 
            //each event for some reason?
            fthetaLab->SetAddress(&fisThetaLab);
            fthetaCM->SetAddress(&fisThetaCM);
            fMR->SetAddress(&fisMR);
            fTKE->SetAddress(&fisTKE);
            fTKEViola->SetAddress(&fisTKEViola);


            //set addresses for new tree
            thetaCM->SetAddress(&binnedThetaCM);
            normBranch->SetAddress(&normValBinned);

            //get values we will use from fis tree
            fthetaLab->GetEvent(i);
            fthetaCM->GetEvent(i);
            fMR->GetEvent(i);
            fTKE->GetEvent(i);
            fTKEViola->GetEvent(i);

            int binNum=0;
            if (fisThetaLab >= 0){

                //figure out which bin corresponds to the theta value 
                //for this event
                binNum = (int) round(fisThetaLab / xsecSpec.binSize);
                int binNumCM = (int) round(fisThetaCM / xsecSpec.binSize);

                //used to record value at bin center (effectively used in histogram method)
                float binnedThetaL = binNum * xsecSpec.binSize - 0.5*xsecSpec.binSize;
                binnedThetaCM = binNumCM * xsecSpec.binSize - 0.5*xsecSpec.binSize;

                // Trick is used because you need to take into account binning
                // used to derive SA normalization. All angle quantities in 
                // xsec tree are binned quantities


                //use that info to recall the appropriate lab norm coeff, 
                //then change to CM

                //normVal = normConst[binNum-1]
                //	* dXLab2dXCM(fisThetaLab,fisThetaCM); //Lab->CM  Don't need conversion factor!!!

                normVal = normConst[binNum-1]; //11.05.17 Fixed!!!!!

                //Use a value that reflects effective binning of data
                //We don't use following term, if you need to use you need to drop dXLab2dXCM conversion term.
                //To reduce unwanted warning sig. I'm just leaving like this. -Yun
                float normValBinned = normConst[binNum-1]
                    * dXLab2dXCM(binnedThetaL,binnedThetaCM); //Lab->CM 

                normValBinned=normValBinned;//Just to turn off unused error at 
                //compile time. (this variable was 
                //for a legacy test that is worth keeping in code)


                //****************************************************************************************
                float dsigdth = normVal;
                float dsigdomega = normVal/(2*pi*sin(fisThetaCM*d2r));

                //****************************************************************************************

                //fill histograms
                //EXP. MAD
                if (fisThetaCM > 90. + slope*(fisMR-0.5)){

                    Exp_MAD.Fill(fisMR,fisThetaCM);
                    Exp_MAD.Fill(1.-fisMR,180.-fisThetaCM);

                }
                //1D hist
                MR.Fill(fisMR);
                xsecCM_O.Fill(fisThetaCM,dsigdomega);
                xsecCM_T.Fill(fisThetaCM,dsigdth);

                if (fisThetaCM > 90. + slope*(fisMR-0.5)){

                    //1D hist
                    xsecCM_O_mir.Fill(fisThetaCM,dsigdomega);
                    xsecCM_O_mir.Fill(180.-fisThetaCM,dsigdomega);				  

                    xsecCM_T_mir.Fill(fisThetaCM,dsigdth);
                    xsecCM_T_mir.Fill(180.-fisThetaCM,dsigdth);

                }


                //2D hist		
                thLthCM.Fill(fisThetaLab,fisThetaCM);//used to compute error on ang dist later

                MrTKExsec.Fill(fisMR,fisTKE,dsigdth);
                MrTKEVxsec.Fill(fisMR,fisTKE/fisTKEViola,dsigdth);

                //non-mirroring
                MrThCMxsec_T.Fill(fisMR,fisThetaCM,dsigdth);
                MrThCMxsec_O.Fill(fisMR,fisThetaCM,dsigdomega);	

                //mir plots
                if (fisThetaCM > 90. + slope*(fisMR-0.5)){

                    //old mirroring condition
                    //43.125 + 197.917*fisMR - 312.5*fisMR*fisMR 
                    // + 208.33*fisMR*fisMR*fisMR){ //mirroring condition
                    MrThCMxsec_T_mir.Fill(fisMR,fisThetaCM,dsigdth);
                    MrThCMxsec_T_mir.Fill(1.-fisMR,180.-fisThetaCM,dsigdth);

                    MrThCMxsec_O_mir.Fill(fisMR,fisThetaCM,dsigdomega);
                    MrThCMxsec_O_mir.Fill(1.-fisMR,180.-fisThetaCM,dsigdomega);

                    MrThCMxsec_mir.Fill(fisMR,fisThetaCM,dsigdth*MRbin);
                    MrThCMxsec_mir.Fill(1.-fisMR,180.-fisThetaCM,dsigdth*MRbin);	

                }

                xsecCM.Fill(fisThetaCM,dsigdth*MRbin);
                MrThCMxsec.Fill(fisMR,fisThetaCM,dsigdth*MRbin);//Check agian! * MRbin or * 1/MRbin 

                }
                else{
                    normVal = 0.0;
                }

                //calculate for CM frame
                newTree.Fill();


            }
            cout << "Writing xsec tree...";
            newTree.Write("",TObject::kOverwrite);
            cout << " Tree written ... \n";


            //we need errors on the angular distribution, so the distribution is calc'd
            //here in the usual way

            for (int i=0;i<numBins;i++){
                th_cm[i]=0;
                xf[i]=0;
                xferr[i]=0;

                Xf[i]=0;//Y
                Xferr[i]=0;//Y	
            }

            int validCnt=0;
            bool calTag=0; //just says we're not calculating the angular distribution
            //for the calibration file (xcheck)
            /*
               errCheck = calcAngDist(&thLthCM, &calibThLthCM, th_cm, 
               xf, xferr, numBins,&validCnt,
               calTag, &xsecSpec, yield_cal,
               normConst, monErrTerm,&fisRxn);
             */
            errCheck = calcAngDist(&thLthCM, &calibThLthCM, th_cm, 
                    xf, xferr, Xf, Xferr, numBins,&validCnt,
                    calTag, &xsecSpec, yield_cal,
                    normConst, monErrTerm,&fisRxn);

            if (errCheck!=0){
                return -1;
            }

            cout << endl;
            //------------------------------------------------------------------------------------------------------------------	
            ///xsecCM ascii files

            ofstream xsecCMfile;
            stringstream ss;
            string xsecstring= xsecSpec.resultsPath + ss.str(); 

            Int_t n=xsecCM_O.GetXaxis()->GetNbins();


            //Int_t n=xsecCM_O.GetXaxis()->GetNbins();
            ss << "xsecAngDist_" << fisRunNum << ".dat";

            xsecstring = xsecSpec.resultsPath + ss.str();
            xsecCMfile.open(&xsecstring[0]);
            //xsecCMfile << "#thetaCM[deg] \tdsig/dth[mb/rad] \terror[mb/rad] "<< endl;
            //xsecCMfile << "#thetaCM[deg] \tdsig/domega[mb/sr] \terror[mb/sr] "<< endl;


            for (Int_t i=0;i<=n;i++){

                if (xsecCM_O.GetBinContent(i)>0){
                    //dsigdomega outputs

                    int j;
                    //xsecCMfile << xsecCM_O.GetBinCenter(i) << "\t" << xsecCM_O.GetBinContent(i) <<"\t\t"<< xferr[j] << endl; 
                    xsecCMfile << setw(5) << xsecCM_O.GetBinCenter(i) << setw(5) <<"\t"
                        << setw(5) << xsecCM_O.GetBinContent(i) << "\t"
                        << setw(5) << "\t" << setw(5) << xferr[j+1] << setw(5) << "\t" << setw(5) << "-42" << endl; // for fifrang input format

                    j += 1;
                }

            }

            xsecCMfile.close();
            //--------------------------------------------------------------------------------------------------------------------


            // OUTPUT FIFRANG-FRIENDLY ASCII FILE 


            if (fisInfo.fifrangOut == true){

                stringstream ss;
                ss << "fisAngDist_" << fisRunNum << ".dat";
                string outName = xsecSpec.resultsPath + ss.str();
                fifrangOutput(th_cm, xf, xferr, (size_t)validCnt, 
                        outName);
            }


            //special plot of xsec data
            //TCanvas *c1 = new TCanvas("c1","c1",600,600);
            //c1->GetPad(0)->SetMargin(0.15,0.15,0.15,0.15);
            //desigdOmega
            TGraphErrors *gr1 = new TGraphErrors(validCnt,th_cm,xf,0,xferr); //# points,x,y,dx,dy
            gr1->SetMarkerColor(kBlue);
            gr1->SetLineColor(2);
            gr1->SetLineWidth(2);		
            gr1->SetMarkerStyle(21);
            gr1->SetMarkerSize(1.0);		
            gr1->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            gr1->GetXaxis()->CenterTitle(true);
            gr1->GetXaxis()->SetTitleOffset(1.0);
            gr1->GetXaxis()->SetLimits(20,180);
            gr1->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega}(#theta_{cm}) mb/sr");
            gr1->GetYaxis()->CenterTitle(true);
            gr1->GetYaxis()->SetTitleOffset(1.5);
            gr1->SetTitle("Angular Distribution");
            gr1->Draw("acp");

            //dsigdtheta edited by Yun
            TGraphErrors *gr4 = new TGraphErrors(validCnt,th_cm,Xf,0,Xferr); //# points,x,y,dx,dy
            gr4->SetMarkerColor(kBlue);
            gr4->SetLineColor(2);
            gr4->SetLineWidth(2);		
            gr4->SetMarkerStyle(21);
            gr4->SetMarkerSize(1.0);
            gr4->SetFillColor(38);
            gr4->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            gr4->GetXaxis()->CenterTitle(true);
            gr4->GetXaxis()->SetTitleOffset(1.0);
            gr4->GetXaxis()->SetLimits(20,180);
            gr4->GetYaxis()->SetTitle("#frac{d#sigma}{d#theta}(#theta_{cm}) mb/rad");
            gr4->GetYaxis()->CenterTitle(true);
            gr4->GetYaxis()->SetTitleOffset(1.5);
            gr4->SetTitle("Angular Distribution");
            gr4->Draw("acp");

            ///*********************************
            //Extra histograms


            TH2D *SAnorm_MAD =(TH2D*)MrThCMxsec_O_mir.Clone("SA_MAD");

            TH2D *exp =(TH2D*)Exp_MAD.Clone("exp");
            TH1D *exp_py=exp->ProjectionY("exp_py",0,-1,"");

            TH2D *norm =(TH2D*)MrThCMxsec_O_mir.Clone("norm");
            TH1D *sa_py=SAnorm_MAD->ProjectionY("sa_py",0,-1,"");

            norm->Divide(exp);

            TH2D *temp2 =(TH2D*)MrThCMxsec_mir.Clone("temp2");
            TH1D *MrThCMxsec_mir_py = temp2->ProjectionY("MrThCMxsec_mir_py",0,-1,"[light]");

            cout << "Now writing histograms of data ...";

            // - create relevant histograms, taking user-specified 
            //bin size into account

            //********************************************************************
            //Xsec Histogram Options
            //LABEL HISTOGRAMS HERE


            xsecCM_T.SetTitle("Angular distribution;theta1CM[deg];#frac{d#sigma}{d#theta}(#theta_{cm}) [mb/rad]");
            xsecCM_T.SetLineColor(8);
            xsecCM_T.SetLineWidth(3);

            MrThCMxsec_T.SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#theta dmr)] ;MR;ThCM [deg]");
            MrThCMxsec_T.SetOption("COLSCATZ");
            MrThCMxsec_T_mir.SetTitle("MR vs ThCM_mir [ d^{2}#sigma/(d#theta dmr) ];MR; theta1CM[deg]");
            MrThCMxsec_T_mir.SetOption("COLSCATZ");

            xsecCM_T_mir.SetTitle("Angular distribution;theta1CM[deg];#frac{d#sigma}{d#theta}(#theta_{cm}) [mb/rad]");
            xsecCM_T_mir.SetLineWidth(2);

            //---------------------------

            xsecCM_O.SetTitle("Angular distribution;theta1CM[deg];#frac{d#sigma}{d#Omega}(#theta_{cm}) [mb/sr]");
            xsecCM_O.SetLineColor(8);
            xsecCM_O.SetLineWidth(3);

            MrThCMxsec_O.SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#Omega dmr)] ;MR;ThCM [deg]");
            MrThCMxsec_O.SetOption("COLSCATZ");
            MrThCMxsec_O_mir.SetTitle("MR vs ThCM_mir [ d^{2}#sigma/(d#Omega dmr) ];MR; theta1CM[deg]");
            MrThCMxsec_O_mir.SetOption("COLSCATZ");

            xsecCM_O_mir.SetTitle("Angular distribution;theta1CM[deg];#frac{d#sigma}{d#Omega}(#theta_{cm}) [mb/sr]");
            xsecCM_O_mir.SetLineWidth(2);

            //----------------------------

            Exp_MAD.SetTitle("MR vs thetaCM1 (mirrored) [Counts]; M_{R}; #theta_{CM} [deg]");
            Exp_MAD.SetOption("COLSCATZ");
            MR.SetTitle("MR;MR;counts");

            //xsec hist input
            thLthCM.SetTitle("ThetaLab vs ThetaCM;theta1[deg];Theta1CM [deg]");
            //thLthCM.SetOption("COLSCATZ");

            //

            MrTKExsec.SetTitle("Mr vs TKE [SAnorm];MR;TKE [MeV]");
            MrTKExsec.SetOption("COLSCATZ");
            MrTKEVxsec.SetTitle("Mr vs TKE / TKEViola [SAnorm];MR;TKE /TKEViola");
            MrTKEVxsec.SetOption("COLSCATZ");

            SAnorm_MAD->SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#theta dmr)] ;MR;ThCM [deg]");
            SAnorm_MAD->SetOption("COLSCATZ");

            norm->SetTitle("Mr vs ThCM [ SA_MAD/EXP_MAD] ;MR;ThCM [deg]");
            norm->SetOption("COLZ");

            exp_py->SetTitle("ThCM [counts] ;ThCM;counts");
            sa_py->SetTitle("SA_NormConst ;ThCM;norm coeff");

            //--------------------------

            MrThCMxsec.SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#theta dmr)] ;MR;ThCM [deg]");
            MrThCMxsec.SetOption("COLSCATZ");
            MrThCMxsec_mir.SetTitle("MR vs ThCM_mir [ d^{2}#sigma/(d#theta dmr) ];MR; theta1CM[deg]");
            MrThCMxsec_mir.SetOption("COLSCATZ");

            xsecCM.SetTitle("Angular distribution;theta1CM[deg];#frac{d#sigma}{d#theta}(#theta_{cm}) [mb/rad]");
            xsecCM.SetLineWidth(2);

            MrThCMxsec_mir_py->SetTitle("MrThCMxsec_mir_py;#theta_{cm};#frac{d#sigma}{d#theta} [mb/rad]");
            MrThCMxsec_mir_py->SetLineWidth(2.0);

            //********************************************************************
            //Xsec Histogram Directory
            //SAVE HISTOGRAMS TO FISSION ROOT FILE


            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_dsigdTh")!=true){
                fisFile.mkdir("Xsec_dsigdTh");
            }
            fisFile.cd("Xsec_dsigdTh");

            gr4->Write("AngDist_disigdtheta",TObject::kOverwrite);
            xsecCM_T.Write("",TObject::kOverwrite);		
            xsecCM_T_mir.Write("",TObject::kOverwrite);
            MrThCMxsec_T.Write("",TObject::kOverwrite);
            MrThCMxsec_T_mir.Write("",TObject::kOverwrite);

            Exp_MAD.Write("",TObject::kOverwrite);
            exp_py->Write("",TObject::kOverwrite);

            cout <<"\n"<<endl;
            cout << "  1.  Finished creating Xsec_dsigdTh directory ... \n" << endl;

            //////////////////////////////////////////////////////

            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_dsigdOmega")!=true){
                fisFile.mkdir("Xsec_dsigdOmega");
            }
            fisFile.cd("Xsec_dsigdOmega");

            gr1->Write("AngDist_dsigdomega",TObject::kOverwrite);
            xsecCM_O.Write("",TObject::kOverwrite);
            xsecCM_O_mir.Write("",TObject::kOverwrite);
            MrThCMxsec_O.Write("",TObject::kOverwrite);
            MrThCMxsec_O_mir.Write("",TObject::kOverwrite);
            MR.Write("",TObject::kOverwrite);
            thLthCM.Write("",TObject::kOverwrite);

            SAnorm_MAD->Write("",TObject::kOverwrite);
            norm->Write("",TObject::kOverwrite);
            sa_py->Write("",TObject::kOverwrite);

            cout << "  2.  Finished creating Xsec_dsigdOmega directory ... \n" << endl;

            //////////////////////////////////////////////////////
            //If MR binning = 100
            // - create relevant histograms, taking user-specified 
            //bin size into account
            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_Display")!=true){
                fisFile.mkdir("Xsec_Display");
            }
            fisFile.cd("Xsec_Display");

            xsecCM.Write("",TObject::kOverwrite);//dsigdth
            MrThCMxsec.Write("",TObject::kOverwrite);
            MrThCMxsec_mir.Write("",TObject::kOverwrite);
            MrThCMxsec_mir_py->Write("",TObject::kOverwrite);
            //cout << "Finished creating histograms ...\n\n";

            cout << "  3.  Finished creating Xsec_Display directory ... \n" << endl;


            //********************************************************************************************		

            //END HISTOGRAM CREATION

            cout << "Finished creating histograms ...\n\n";

            //END HISTOGRAM CREATION


            //increment iteration through all fission run lists
            fisInfoIndex++;
            //fisSortIndex++;
            fisRxnIndex++;
            fisTargIndex++;

            fisRunNum++;

        }//END FISSION LOOP


        //BEGIN CALIBRATION TEST (makes graph of calc'd versus observed ang dist
        //for calibration file, using histogram method

        for (int i=0;i<numBins;i++){
            normConst[i]=0;
            normConstErr[i]=0;
        }

        //monitor normalization factor, including cube efficiency correction
        float monNormConst = monNormConst_cal 
            * (calibInfo.ruthMon / (float)calibInfo.monSumPeak); 

        float monErrTerm = 1./(float)calibInfo.monSumPeak
            + 1./(float)calibInfo.monSumPeak;
        //float monErrTerm = 1./sqrt((float)calibInfo.monSumPeak)
        //	+ 1./sqrt((float)calibInfo.ruthMon);

        bool calTag=1;//tells certain functions this is the calib test


        cout << "********** Solid Angle Check **********\n\n";

        for (int i=0;i<numBins;i++){
            th_cm[i]=0;
            xf[i]=0;
            xferr[i]=0;

            Xf[i]=0;
            Xferr[i]=0;


        }
        calcSAnorms(&xsecSpec, 
                normConst, normConstErr, 
                yield_cal,
                numBins, 9999, 
                &calibTheta1, &calibRuthHist,
                monNormConst, monErrTerm,calTag);// 9999 is legacy label; not used


        //compute angular distribution and plot in comparison to calc'd values



        int validCnt=0;
        /*
           errCheck = calcAngDist(&calibThLthCM, &calibThLthCM, th_cm, 
           xf, xferr, numBins,&validCnt,
           calTag, &xsecSpec, yield_cal,
           normConst, monErrTerm, &calibRxn);
         */

        errCheck = calcAngDist(&calibThLthCM, &calibThLthCM, th_cm, 
                xf, xferr,Xf,Xferr, numBins,&validCnt,
                calTag, &xsecSpec, yield_cal,
                normConst, monErrTerm, &calibRxn);



        if (errCheck!=0){
            return -1;//error messages printed in function
        }

        // make plots save to calib root file



        string checkName="calibCheck.root";
        string checkPathName=xsecSpec.resultsPath + checkName;

        TFile calCheck(&checkPathName[0],"update");

        cout << "Writing out angular distribution test plots to " 
            << checkPathName << "\n";

        //special plot of xsec data
        TGraphErrors *gr2 = new TGraphErrors(validCnt,th_cm,xf,0,xferr); //# points,x,y,dx,dy
        gr2->SetMarkerColor(1);
        gr2->SetMarkerStyle(7);
        gr2->GetXaxis()->SetTitle("#theta_{c.m.} deg");
        gr2->GetXaxis()->CenterTitle(true);
        gr2->GetXaxis()->SetTitleOffset(1.5);
        gr2->GetXaxis()->SetLimits(20,180);
        gr2->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega}(#theta) mb/sr");
        gr2->GetYaxis()->CenterTitle(true);
        gr2->GetYaxis()->SetTitleOffset(1.5);
        gr2->SetTitle("Angular Distribution (calibration)");
        gr2->Draw("ap");
        gr2->Write("AngDist_calib",TObject::kOverwrite);

        for (int i=0;i<numBins;i++){
            xf[i]=0;
            xferr[i]=0;
        }
        for (int i=0;i<validCnt;i++){
            xf[i] = Rutherford(calibRxn.Zp, calibRxn.Zt, calibRxn.Ecm, th_cm[i]);
        }

        //special plot of xsec data
        TGraphErrors *gr3 = new TGraphErrors(validCnt,th_cm,xf,0,0); //# points,x,y,dx,dy
        gr3->SetLineColor(2);
        gr3->SetMarkerColor(2);
        gr3->SetMarkerStyle(7);
        gr3->GetXaxis()->SetTitle("#theta_{c.m.} deg");
        gr3->GetXaxis()->CenterTitle(true);
        gr3->GetXaxis()->SetTitleOffset(1.5);
        gr3->GetXaxis()->SetLimits(20,180);
        gr3->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega}(#theta) mb/sr");
        gr3->GetYaxis()->CenterTitle(true);
        gr3->GetYaxis()->SetTitleOffset(1.5);
        gr3->SetTitle("Angular Distribution (Calculated, Rutherford)");
        gr3->Draw("ap");
        gr3->Write("AngDist_calib_calc",TObject::kOverwrite);


        //END CALIBRATION TEST

        //clean up stuff on stack

        delete[] normConst;
        delete[] normConstErr;
        delete[] yield_cal;
        delete[] th_cm;
        delete[] xf;
        delete[] xferr;
        delete[] Xf;//Y
        delete[] Xferr;//Y

        /////////////////////// SUMMARY LOGFILE //////////////////////////////////

        //CALIB LOG FILE OUTPUT //////////////////////////////////////////////

        logfile << "\n Calculations assume a bin size of " 
            << xsecSpec.binSize << " degrees (" << numBins << " bins).\n\n";
        cout << "Calculations assume a bin size of " 
            << xsecSpec.binSize << " degrees (" << numBins << " bins).\n\n";

        char logprint[500];

        //table header
        logfile << "SUMMARY TABLE:\n";
        cout << " *** SUMMARY TABLE ***\n";

        sprintf(logprint,"%-70s %-5s %-15s %-15s %-15s %-20s %-20s %-20s %-20s \n",
                "File","Type","Run Index","Mon Eff","Cube Eff","MonAngle[deg,lab]",
                "MonAngle[deg,CM]","MonCounts*","Rutherford [mb]^");
        logfile << logprint;
        cout << logprint;

        //table info

        //calib
        sprintf(logprint,"%-70s %-5s %-15s %-15f %-15f %-20f %-20f %-20i %-20f \n",
                &calibInfo.dataFile[0], "cal", "-", calibInfo.monEffCal, 
                calibInfo.cubeEffCal,calibInfo.monAngleLab, calibInfo.monAngleCM, 
                calibInfo.monSumPeak,calibInfo.ruthMon);
        logfile << logprint;
        cout << logprint;

        //fission loop

        fisRunNum=0;
        fisInfoIndex = fisInfoList.begin();


        while (fisInfoIndex != fisInfoList.end()){

            xsecInfo& fisInfo = *fisInfoIndex;

            sprintf(logprint,"%-70s %-5s %-15i %-15f %-15f %-20f %-20f %-20i %-20f \n",
                    &fisInfo.dataFile[0], "fis", fisRunNum, 
                    fisInfo.monEffCal, 
                    fisInfo.cubeEffCal,
                    fisInfo.monAngleLab, 
                    fisInfo.monAngleCM, 
                    fisInfo.monSumPeak,
                    fisInfo.ruthMon);
            logfile << logprint;
            cout << logprint;

            fisInfoIndex++;
            fisRunNum++;
        };



        //table footer
        logfile << "_________________________________________________\n";
        logfile << "* Corrected for pre-scale and detector efficiency.\n";
        logfile << "^ Calculated Rutherford scattering cross section at "
            <<"monitor angle in lab frame.\n";

        cout << "_________________________________________________\n";
        cout << "* Corrected for pre-scale and detector efficiency.\n";
        cout << "^ Calculated Rutherford scattering cross section at "
            <<"monitor angle in lab frame.\n";

        cout << "\nCross section calculations complete! Calculation summary saved"
            << " in " << logName << " \n";


        calibFile.Close();

        logfile.close(); //close log file for all runs

        return 0;
    }// end xsecCalc




    /////////////////////////////////////////////////////////////////////
    ///////////////////// BEGIN OTHER FUNCTIONS HERE ////////////////////
    /////////////////////////////////////////////////////////////////////




    ////////////////////// deadTimeCalc /////////////////////////

    // deadTimeCalc: calculates deadtimes for mons 1,2, and cube
    int deadTimeCalc(xsecInfo* runInput,int monNum){

        unsigned int totalScal=0;
        unsigned int totalDAQ=0;

        for (int i=0;i<monNum;i++){
            if (runInput->monScal[i]==0){
                cout << "Error in CubeXsec::deadTimeCalc. Monitor " << i+1 
                    << " scaler is zero. Check input file.\n";
                return -1;
            }
            else{
                totalScal = totalScal + runInput->monScal[i];
                totalDAQ = totalDAQ + runInput->monDAQ[i];
            }
        }
        runInput->monEffCal = (float) totalDAQ / (float) totalScal;


        if (runInput->cubeScal==0){
            cout << "Error in CubeXsec::deadTimeCalc. Cube scaler is zero."
                << "Check input file.\n";
            return -1;
        }
        else{

            runInput->cubeEffCal = (float)runInput->cubeDAQ 
                / (float)runInput->cubeScal;
        }

        return 0;
    }//end deadTimeCalc



    ///////////////////// Rutherford ////////////////////////////

    //calculate Rutherford scattering cross section - units of mb/sr
    float Rutherford(int Zp, int Zt, float Ecm, float thetaCM){

        float dsigdth = ((kp*kp)/(16.))*(Zp*Zt / Ecm)*(Zp*Zt / Ecm)
            *(1./pow(sin(0.5*thetaCM*d2r),4)) * fmsq2mb;
        return dsigdth;

    }//end Rutherford



    /////////////////// thLab2ThCM_elastics ///////////////////////

    //thLab2ThCM_elastics: Transforms theta from lab to CM frame - 
    //elastic scattering. Consistent with MMA results for Ni+Ni
    float thLab2ThCM_elastics(int Ap, int At, float thetaLab){

        float thetaCM = 0.0;
        float x = (float)Ap / (float)At;
        thetaLab = thetaLab * d2r;

        thetaCM = ( thetaLab + asin( x*sin( thetaLab ) ) );
        thetaCM = thetaCM * r2d;

        return thetaCM;
    }//end thLab2ThCM_elastics



    ////////////////// thCM2ThLab_elastics ///////////////////////////

    //thCM2ThLab_elastics: Transforms theta from CM to lab frame - elastic scattering
    float thCM2ThLab_elastics(int Ap, int At, float thetaCM){

        float thetaLab = 0.0;
        float x = (float)Ap / (float)At;
        thetaCM = thetaCM*d2r;

        thetaLab = sin(thetaCM) / ( x + cos(thetaCM));
        thetaLab = atan(thetaLab) * r2d;

        return thetaLab;
    }//end thCM2ThLab_elastics



    /////////////////////// dXCM2dXLab ///////////////////////////////

    //dXCM2dXLab - from Marion and Young. Same for reactions and scattering.
    float dXCM2dXLab(float thetaLab, float thetaCM){

        float factor=0.0;

        thetaCM = thetaCM * d2r;
        thetaLab = thetaLab * d2r;

        factor = ( sin(thetaCM) * sin(thetaCM) )/( sin(thetaLab) * sin(thetaLab));
        factor = factor / cos(thetaCM - thetaLab);

        //cout << "\n dXCM2dXLab ( "<< thetaLab / d2r <<","<<thetaCM /d2r << ") = " << factor << endl;

        return factor;

    } // end dXCM2dXLab



    ///////////////////////// dXLab2dXcm ////////////////////////////

    //dXLab2dXcm - from Marion and Young. Same for reactions and scattering.
    float dXLab2dXCM(float thetaLab, float thetaCM){

        float factor=0.0;

        thetaCM = thetaCM * d2r;
        thetaLab = thetaLab * d2r;

        factor = sin(thetaLab) * sin(thetaLab) / (sin(thetaCM) * sin(thetaCM));
        factor = factor * cos(thetaCM - thetaLab);

        return factor;

    } // end dXLab2dXCM




    ///////////////////////calibNormCalc ////////////////////////////////
    //method not used - original part of sa norm calc; uses calculated thetaCM
    // rather than "observed" CM. results with either method are pretty much the same

    float calibNormCalc_orig(reactPar* calibRxn, float binCenter, int binVal){

        //get bin info
        float normBin=0;

        float calThetaCM = thLab2ThCM_elastics(calibRxn->Ap, calibRxn->At, 
                binCenter);
        //calculate rutherford xsec for this point
        float RuthCube = Rutherford(calibRxn->Zp, calibRxn->Zt, calibRxn->Ecm, 
                calThetaCM);

        //getin histogram contents here
        if (binVal!=0){
            normBin = (1./binVal) * RuthCube * dXCM2dXLab(binCenter,calThetaCM); 
            //last term: change back to lab frame
        }
        else{
            normBin = 0.0;
        }
        return normBin;
    } //end calibNormCalc_orig




    /////////////////////////// fifrangOutput ///////////////////////////////////

    // Function outputs ascii file with angular distribution data, for use with fifrang
    int fifrangOutput(float *thCM, float *xfis, float *xfiserr, size_t numBins, 
            string outputFileName){

        ofstream fifrang;

        //open output file, now that we have a name
        fifrang.open(&outputFileName[0]);

        for (size_t i=0; i<numBins;i++){


            fifrang << setw(5) << thCM[i] << setw(5) << "\t" 
                << setw(5) <<  xfis[i];
            fifrang << setw(5) << "\t" << setw(5) <<  xfiserr[i] 
                << setw(5) << "\t" << setw(5) << "-42\n";

        }

        cout << "Finished writing fifrang-friendly data to " << outputFileName << "\n";

        //close output file
        fifrang.close();

        return 0;
    }//end fifrangOutput




    ////////////////////////////// preCalcs ////////////////////////////////

    //does all monitor normalization, dead time calc, etc.
    int preCalcs(xsecInfo* xsecInput, reactPar* rxn, targetPar* targ, int monNum){


        cout << "\nNow calculating reaction parameters for the calibration run ...\n";
        //if (rxn->elossBool==true){
        if (rxn->dEcalcType==1 || rxn->dEcalcType==2 || rxn->dEcalcType==3 ){
            calcElossBeam(targ,rxn);
        }
        //Immediately calculate the remaining undefined parameters in calib rxn struct. ***
        calcRxn(rxn);

        //Calculate deadtimes / efficiency corrections - calib run **************************
        cout << "\nCalculating detector efficiencies for the calibration run ...\n";
        int errCheck = deadTimeCalc(xsecInput, monNum);

        if ( errCheck != 0 ){
            cout << "Problem with efficiency calibrations. Check input file!\n";
            cout << xsecInput->monEffCal << "  " << xsecInput->cubeEffCal << "\n";
            return -1;
        }

        //Monitor Lab->CM calculation *******************************************
        cout << "Transforming monitor angles from lab to CM, calculating Rutherford cross sections...\n";

        xsecInput->monAngleCM = thLab2ThCM_elastics(rxn->Ap, 
                rxn->At, xsecInput->monAngleLab);

        xsecInput->ruthMon = Rutherford(rxn->Zp, rxn->Zt, 
                rxn->Ecm, xsecInput->monAngleCM); //cm frame
        xsecInput->ruthMon = xsecInput->ruthMon
            * dXCM2dXLab(xsecInput->monAngleLab, xsecInput->monAngleCM); //switch to lab frame

        //For monitor data, take into account prescale factors *******************
        xsecInput->monSumPeak = xsecInput->monSumPeak 
            * xsecInput->preScal / xsecInput->monEffCal;

        //cout << "xsecInput parameters: \n";
        //cout << xsecInput->monSumPeak << "\n";

        return 0;
    }//end preCalcs


    //////////////////////////// calcSAnorms ////////////////////////////

    //calculates solid angle normalization parameters for binned lab angles

    int calcSAnorms(xsecSpecs* xsecSpec, 
            float *normConst, float *normConstErr, int *yield_cal,
            int numBins, int fisRunNum, 
            TH1D *calibTheta1, TH1D* calibRuthHist,
            float monNormConst, float monErrTerm,bool calTest){

        time_t t = time(0);   // get time now
        struct tm * now = localtime( & t );

        //open file to save normalization data to, print header info

        string filepath = xsecSpec->resultsPath;
        string normstring;
        ofstream normfile;

        stringstream ss;

        if (calTest==1){
            ss << "calibNormTest.dat";
        }
        else{
            ss << "fisNorm_" << fisRunNum << ".dat";
        }
        normstring = filepath + ss.str();

        cout << "Normalization coefficients written to " << normstring << "\n";
        normfile.open(&normstring[0]);
        normfile << "Cross section normalization data, Date: " 
            <<  (now->tm_year + 1900) << '-' 
            << (now->tm_mon + 1) << '-'
            <<  now->tm_mday
            << "\n\n";

        normfile << "Bin\t Center\t Norm Const.\tNorm Const Error\n";
        //end norm header

        int calibBins = calibRuthHist->GetNbinsX();
        int thetaBins = calibTheta1->GetNbinsX();
        if ((calibBins!=numBins) || (thetaBins!=numBins)){
            cout << "Error: number of bins in calibRuthLab or calibTheta1 unexpected. Aborting.\n";
            return -1;
        }

        for (int i = 1; i <=numBins; i++){

            float calibRuthCenter = calibRuthHist->GetBinCenter(i);
            float calibRuthVal = calibRuthHist->GetBinContent(i);

            int binVal = calibTheta1->GetBinContent(i);
            //float binCenter = calibTheta1->GetBinCenter(i); //used for legacy check with calibNormCalc_orig

            yield_cal[i-1] = binVal;

            if (binVal>0){
                normConst[i-1] = monNormConst*calibRuthVal / (float)binVal;
                normConstErr[i-1] = normConst[i-1] * sqrt( monErrTerm + 
                        (1./ (float)binVal));
                //cout << monNormConst << " " << calibRuthVal << " " << binVal << " " << normConst[i-1] << endl;
            }
            else{
                normConst[i-1]=0.;
                normConstErr[i-1]=0.;
            }

            //cout << calibNormCalc_orig(calibRxn, binCenter, binVal) << " " << binVal << " " << calibRuthVal/binVal << " " << normConst[i-1] << endl;

            if (normConst[i-1]>0){
                //output to screen 

                //cout << i << " " << binCenter << " " << binVal << " " <<normConst[i-1] << endl;

                //output to normfile
                normfile << i << "\t" << calibRuthCenter << "\t" << normConst[i-1];
                normfile << "\t" << normConstErr[i-1] << "\n";

            }
        }

        normfile.close();

        cout << "Normalization constants / bin have been calculated.\n";
        //norm calculations are complete and have been written to 
        //appropriately named output files. 

        return 0;

    } // end calcSAnorms




    //////////////////////// calcAngDist //////////////////////////////

    //calculates angular distribution using histograms (the usual way)
    /*
       int calcAngDist(TH2D* thLthCM, TH2D* calibThLthCM, float* th_cm, 
       float* xf, float* xferr, int numBins,int* validCnt,
       bool calTag, xsecSpecs* xsecSpec, int *yield_cal,
       float* normConst, float monErrTerm, reactPar* fisRxn){

     */
    int calcAngDist(TH2D* thLthCM, TH2D* calibThLthCM, float* th_cm, 
            float* xf, float* xferr, float* Xf, float* Xferr, int numBins,int* validCnt,
            bool calTag, xsecSpecs* xsecSpec, int *yield_cal,
            float* normConst, float monErrTerm, reactPar* fisRxn){

        int labBins = thLthCM->GetNbinsX();
        int cmBins = thLthCM->GetNbinsY();
        int calibLabBins =calibThLthCM->GetNbinsX();

        if (labBins != calibLabBins){
            cout << "Error: fission and calibration binning is not the same!" << endl;
            return -1;
        }



        if (calTag==1){
            cout << "\n\n*Angular Distribution Check (calib)*\n";
            cout << "thetaCM[deg]\tdsig/dth\t[mb/sr]\terror [mb/sr]";
            cout << "\tRutherford [mb/sr]";
        }
        cout << "\n";


        for (int i=1;i<=cmBins;i++){ //begin cm bins

            float thCM=thLthCM->GetYaxis()->GetBinCenter(i);
            //cout << "thetaCM = " << thCM << endl;
            //dsigdOmega
            float xfis=0.0;
            float xfis_tot=0.0;
            float xfiserr_tot=0.0;
            int tot_yield=0;
            //int tot_cal_yield=0;

            //dsigdtheta edited by Yun
            float Xfis=0.0;
            float Xfis_tot=0.0;
            float Xfiserr_tot=0.0;				

            for (int j=1;j<=labBins;j++){ // begin lab bins

                float thL=thLthCM->GetXaxis()->GetBinCenter(j);
                float cThL=calibThLthCM->GetXaxis()->GetBinCenter(j);
                float cal_yield=0;

                if (cThL != thL){
                    cout << "Error: fission and calibration binning is not the same!" << endl;
                    return -1;
                }

                int bin_num = thLthCM->GetBin(j, i);
                int yield = (int)thLthCM->GetBinContent(bin_num);

                int bin_index =  (int) round(thL / xsecSpec->binSize);
                cal_yield = yield_cal[bin_index-1];
                //if (yield>0) tot_cal_yield = cal_yield;

                if (yield >0 && cal_yield>0){
                    tot_yield = tot_yield + yield;

                    // dsig/domega

                    //xfis = (float)yield * normConst[j-1] * dXLab2dXCM(thL,thCM);
                    xfis = (float)yield * normConst[j-1] / (2*pi*sin(thCM*d2r));;//Yes, we don't need conversion factor! 
                    xfis_tot += xfis;
                    //xfiserr_tot += xfis*sqrt( (1./(float)yield) + (1./(float)cal_yield) 
                    //		+ monErrTerm);
                    xfiserr_tot += xfis*xfis*( (1./(float)yield) + (1./(float)cal_yield) 
                            + monErrTerm);


                    // dsig/dtheta 

                    Xfis = (float)yield * normConst[j-1];
                    Xfis_tot += Xfis;
                    Xfiserr_tot += Xfis*Xfis*( (1./(float)yield) + (1./(float)cal_yield) 
                            + monErrTerm);	

                }			

            }// end lab bins (j)

            xfiserr_tot = sqrt(xfiserr_tot);
            //cout << "xfis = " << xfis_tot << "+/-" << xfiserr_tot << endl;
            Xfiserr_tot = sqrt(Xfiserr_tot);// edited by Yun

            //save results in arrays for later use
            if (xfis_tot > 0 ){

                if (calTag==1){
                    cout << thCM << "\t\t" << xfis_tot << "\t\t" << xfiserr_tot ; // For Rutherford calib check
                    cout << "\t\t";
                    cout << Rutherford(fisRxn->Zp, fisRxn->Zt, fisRxn->Ecm, thCM);
                    cout<< "\n";
                }

                //save for plotting
                xf[*validCnt]=xfis_tot;
                xferr[*validCnt]=xfiserr_tot;
                th_cm[*validCnt]=thCM;

                Xf[*validCnt]=Xfis_tot;// Yun
                Xferr[*validCnt]=Xfiserr_tot;//Yun

                (*validCnt)++;
            }


        } // end cm bins (i)
        if (calTag==0){
            cout << "Angular distribution has been calculated.\n";
        }
        if (*validCnt > numBins){
            cout << "Don't trust angular distribution. Binning mismatch.\n";
        }

        return 0;

    }//end calcAngDist

