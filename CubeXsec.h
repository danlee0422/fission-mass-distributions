#ifndef GUARD_CubeXsec
#define GUARD_CubeXsec


//required std headers
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <list>
#include <ctime>

//root headers
#include "TCanvas.h"
#include "TGraphErrors.h"

//dacube headers
#include "CubeStruct.h"
#include "CubeInput.h"
#include "physConst.h"
#include "CubeSort.h"


int xsecCalc(char*);
int deadTimeCalc(xsecInfo*,int monNum);
float Rutherford(int Zp, int Zt, float Ecm, float thetaCM);
float thLab2ThCM_elastics(int, int, float);
float thCM2ThLab_elastics(int, int, float);
float dXCM2dXLab(float thetaLab, float thetaCM);
float dXLab2dXCM(float thetaLab, float thetaCM);
float calibNormCalc_orig(reactPar* calibRxn, float binCenter, int binVal);//computes normalization using calculated lab->cm angles
int fifrangOutput(float *thCM, float *xfis, float *xfiserr, size_t numBins, 
        std::string outputFileName);
int preCalcs(xsecInfo* xsecInput, reactPar* rxn, targetPar* targ, int monNum);
int calcSAnorms(xsecSpecs* xsecSpec, 
        float *normConst, float *normConstErr, int *yield_cal,
        int numBins, int fisRunNum, 
        TH1D *calibTheta1, TH1D* calibRuthHist,
        float monNormConst, float monErrTerm,bool calTest);


int calcAngDist(TH2D* thLthCM, TH2D* calibThLthCM, float* th_cm, 
        float* xf, float* xferr, float* Xf, float* Xferr, int numBins,int* validCnt,
        bool calTag, xsecSpecs* xsecSpec, int *yield_cal,
        float* normConst, float monErrTerm,reactPar* fisRxn);



#endif

