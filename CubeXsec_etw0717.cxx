// Code for calculating cross sections based on user-provided fission and calibration root trees,
// sort parameters, and additional information for deadtime / solid angle corrections.
//
// Does NOT assume a mass split of 0.5!
//
// Liz Williams, July 2017
// Special thanks to Dongyun Jeung for her contributions to the earlier version of this code and her
// suggestions on testing the cross section calibration, which have been implemented below.

/* This replaces the old CubeXsec.cxx. It extends the code modified by Yun in May 2017,
   simplifying the code's approach and introducing capability for the new three detector configuration. 

   Tests that have been performed to verify this code (by X - date):

   2-detector dacube
   - compiles with some modifications to CubeInput, CubeStruct, and reactPar initializers throughout code (etw-7/7/17)
   - runs successfully (no seg faults) (etw-7/7/17)
   - XSEC1 equation and all inputs verified after 1 bug fix (etw-11/7/17)
   --> Note: energy input to dsigdOmega_Ruth_lab is in CM frame, not lab frame!
   - Originally trialed an event-by-event average to compute dsig/dtheta_lab for the
   calibration run. This leads to a mismatch between the theoretical dsig/dtheta_lab
   and the one obtained from the data because the distribution of events in each
   bin is not flat, but we're applying a flat solid angle normalization. Switched this
   out for a simpler histogram-based approach.
   - XSEC4 calculations now verified after simplified histogram-based approach implemented (etw-12/7/17)
   - XSEC3 verified (etw- 12/7/17)
   - XSEC4a verified (etw-12/7/17)
   - XSEC5 verified (etw-12/7/17)
   - event-by-event angular distribution compares well to calcAngDist() and David's original results for SICO.RUN041 (etw-13/7/17)
   --> Minor issues to sort out at detector edge; errors seem to be too small there, too, but match up with David's everywhere else
   --> Small underprediction of lab rutherford cross section using the event-by-event method in calibTest. 
   I suspect this is a statistics / machine precision thing but need to verify.  
   - Fixed difference between event--by-event and histogram methods -- found a bug in my bin calculation. Fission angular distributions 
   behave well and the calculated and lab rutherford dsigma/dtheta values match exactly. (etw 19/7/17)
   - Altered code to accommodate 3rd detector setup with calibrations using different reactions. All tests confirm no new bugs have been
   added with the slight change in method. (etw-20/7/17)
   3-detector dacube
   -compiles with 3-detector code v2 (etw-6/7/17)

   Unless otherwise noted, verified means code output has been checked against values calculated "by hand" 
   (in Excel) for the SICO dataset.
 */

#include "CubeXsec_etw0717.h"
#include "CubeHist.h"
#include <TGraph.h>
using namespace std;

/*------------------------------------------------------------------------------------
  xsecCalc
  : calculate differential cross sections (angular distribuitons)
  ------------------------------------------------------------------------------------*/
int xsecCalc(char* xsecInputFile){

    TTree *fisTree;
    ofstream logfile; //contains information useful to users for error checking
    ofstream normfile; //contains printout of normalization coefficients
    string test;

    //initialize universal parameters
    int errCheck = 0;
    xsecSpecs xsecSpec = {"",0};

    //create structures for storing calibration parameters
    xsecInfo calibInfo1 = {"","",0,0,0,{0,0},0,{0,0},0,0,0,0,0,0};
    sortInfo calibSort1 = {"","","",0,0,0,0,0,0,0,0,"","",""};
    xsecInfo calibInfo3 = {"","",0,0,0,{0,0},0,{0,0},0,0,0,0,0,0};
    sortInfo calibSort3 = {"","","",0,0,0,0,0,0,0,0,"","",""};

    //Implicit assumption: the two detector calibrations are the same
    reactPar calibRxn1 = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //uses 3 det structure //for src_v5 +2
    targetPar calibTarg1 = {0,0,0,0,0,0,0,{0,0}};

    reactPar calibRxn3 = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //uses 3 det structure //for src_v5 +2
    targetPar calibTarg3 = {0,0,0,0,0,0,0,{0,0}};


    //create lists for storing fission parameters for all runs. 
    list<xsecInfo> fisInfoList;
    list<sortInfo> fisSortList;
    list<reactPar> fisRxnList;
    list<targetPar> fisTargList;


    //Read in all relevant file names, trees, and parameters - populates required structures 
    errCheck = readXsecInput(xsecInputFile, &xsecSpec, &fisInfoList, &calibInfo1, &calibInfo3,
            &fisSortList, &calibSort1, &calibSort3, &fisRxnList, &calibRxn1, &calibRxn3,
            &fisTargList, &calibTarg1, &calibTarg3);
    if (errCheck == -1){
        return -1;
    }
    else{
        cout << "Cross section structures now populated...\n";
    }

    if (calibInfo3.monAngleLab!=calibInfo1.monAngleLab){
        cout << "Beware: You should use calibration runs from "
            << "the same experiment; monitor positions aren't precise!\n";
        logfile << "Beware: You should use calibration runs "
            <<"from the same experiment; monitor positions aren't precise!\n";
    }

    //Create a log file name that contains time stamp
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );

    stringstream logtime;
    logtime << (now->tm_year + 1900) << '-' 
        << (now->tm_mon + 1) << '-'
        <<  now->tm_mday << "_" << now->tm_hour << "-" << now->tm_min;
    string logPre = "xsec_";
    string logExt = ".log";

    string logName;
    string logFileName = logPre + logtime.str() + logExt;
    logName = xsecSpec.resultsPath + logFileName;
    //end log filename creation

    //LOGFILE: open and print header
    logfile.open(&logName[0]);
    if (!logfile.is_open()){
        cout << "Error: couldn't open log file. Soldiering on...\n";
    }


    //Print header on log file
    logfile << "Cross section calculation log, Date: " <<  (now->tm_year + 1900) << '-' 
        << (now->tm_mon + 1) << '-'
        <<  now->tm_mday
        << "\n\n";


    //Calculate bin numbers for xsec calcs
    int numBins = (int)round( 180. / xsecSpec.binSize );
    if (numBins <= 0){
        cout << "Error: CubeXsec::xsecCalc: Calculated bin size is zero.\n";
        logfile << "Error - bin size. Calculation aborted.\n";
        return -1;
    }

    cout << "Calculating normalization constants for a bin size of "
        << xsecSpec.binSize << ", corresponding to "
        << numBins << " bins...\n\n";

    // array is used to store the calibration yield per angle bin (lab frame) - will be used in fission
    // loop for error calculation
    float *calibYield_lab = new float[numBins];


    // Define histograms where we'll store the calibration data we need to compute the normalization factors
    // Histograms are filled in the calcCalibData() function.

    //Holds the counts in each lab angle theta bin for the back detector(s)
    TH1D calibThetaBack_lab("calibThetaBack_lab","calibThetaBack_lab",numBins,0,180);
    calibThetaBack_lab.SetTitle("calibThetaBack_lab;thetaback [deg];Counts");

    //Holds the counts in each cm angle theta bin for the back detector(s)
    TH1D calibThetaBack_cm("calibThetaBack_cm","calibThetaBack_cm",numBins,0,180);
    calibThetaBack_cm.SetTitle("calibThetaBack_cm;thetaback [deg];Counts");

    //Will store the counts in each lab angle theta versus center-of-mass angle theta for the back detector(s)
    TH2D calibThetaBack_labVcm("calibThetaBack_labVcm","calibThetaBack_labVcm",numBins,0,180,numBins,0,180);
    calibThetaBack_labVcm.SetTitle("calibThetaBack_labVcm [Counts];thetaback [deg];thetaCMback [deg]");
    calibThetaBack_labVcm.SetOption("COLSCATZ");

    TH1D calibRuthCalc("calibRuthCalc","calibRuthCalc",numBins,0,180);//etw 20-7-17 added to deal with different calibration reactions
    calibRuthCalc.SetTitle("average d#sigma_{Ruth}/d#theta_{lab} for calib1+3; #theta_{lab} [deg]; d#sigma_{Ruth}/d#theta_{lab}");


    float monNormConst_cal = 0.; 

    // Do all calculations and histogram manipulations related to the calibration file in this function
    errCheck = 0;
    errCheck = calcCalibData(&calibInfo1, &calibInfo3,
            &calibRxn1, &calibRxn3, 
            &calibTarg1, &calibTarg3,
            &calibThetaBack_lab,
            &calibThetaBack_cm,
            &calibThetaBack_labVcm,
            &calibRuthCalc,
            &monNormConst_cal,
            numBins);
    if (errCheck != 0){
        return -1;
    }


    /////////// COMPUTE AND SAVE SOLID ANGLE NORMALIZATIONS ////////////
    // I am removing the fission monitor coefficient from the calculation
    // at this stage; this is now taken into account in the fission loop.


    // Create arrays for storing the solid angle normalizations and errors on the stack
    float *solidAngleNorm = new float[numBins];
    float *solidAngleNorm_err = new float[numBins];

    //zero arrays
    for (int i=0;i<numBins;i++){
        solidAngleNorm[i]=0;
        solidAngleNorm_err[i]=0;

    }


    errCheck = 0;
    errCheck = calcSAnorms(&xsecSpec,solidAngleNorm, calibYield_lab,
            numBins, &calibThetaBack_lab, &calibRuthCalc, monNormConst_cal);
    if (errCheck < 0){
        return -1;
    }


    /////////////////// SET UP FISSION DATA PROCESSING ///////////////////

    //create iterator pointing to first element in fission list
    list<xsecInfo>::iterator fisInfoIndex = fisInfoList.begin();
    list<sortInfo>::iterator fisSortIndex = fisSortList.begin();
    list<reactPar>::iterator fisRxnIndex = fisRxnList.begin();
    list<targetPar>::iterator fisTargIndex = fisTargList.begin();

    string normstring, normstring2;

    // create arrays for storing solid angle normalizations with appropriate monitor factor
    // for each fission run
    float* saNorm_fis = new float[numBins];
    float* saNorm_fis_err = new float[numBins];


    //   ///////// LOOP OVER FISSION RUNS //////////  //


    int fisRunNum=0;


    while (fisInfoIndex != fisInfoList.end()){

        cout << "\n********** Fission Run " << fisRunNum << " ***********" << endl;

        if (fisRxnIndex == fisRxnList.end() || fisTargIndex == fisTargList.end()
                || fisSortIndex ==fisSortList.end()){
            cout << "Error: mismatch in fission data structure indices! "
                << "Aborting." << endl;
            logfile << "Error: mismatch in fission data structure indices! "
                << "Aborting." << endl;
            return -1;
        }

        // initialize the saNorm_fis arrays
        for (int i = 0; i<numBins; i++){
            saNorm_fis[i]=0;
            saNorm_fis_err[i]=0;
        }

        //define structures for all the relevant stuff...
        xsecInfo& fisInfo = *fisInfoIndex;
        reactPar& fisRxn = *fisRxnIndex;
        targetPar& fisTarg = *fisTargIndex;

        //Do all initial calcs for fission run
        errCheck = preCalcs(&fisInfo, &fisRxn, &fisTarg, 1); //1 because we want the reaction parameters to be calculated for each fission run
        if ( errCheck != 0 ){
            return -1;
        }

        if (fisInfo.monAngleLab!=calibInfo1.monAngleLab){
            cout << "Beware: You should use calibration and fission runs from "
                << "the same experiment; monitor positions aren't precise!\n";
            logfile << "Beware: You should use calibration and fission runs "
                <<"from the same experiment; monitor positions aren't precise!\n";
        }


        // &&&&&&&&&&&&&&&&&&&&&&& Equation XSEC3 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& //

        //fission run component of the monitor normalization factor. Should come in
        //to the final equation as 1/monNormConst_fis as defined.
        float monNormConst_fis = (float)fisInfo.monSumPeak / fisInfo.ruthMon;

        if (xsecTest == 1 && fisRunNum == 0){
            cout << "\n !!!!!!!!!!!!!!!!!!!!!!!!!! XSEC3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n";
            cout << "TESTING: monNormConst_fis [ " << fisRunNum << " ] = " << monNormConst_fis << "\n\n";
            cout << "TESTING: fisInfo.monSumPeak, fisInfo.ruthMon: " << 
                (float)fisInfo.monSumPeak << ", " << fisInfo.ruthMon << "\n";
            cout << "\n !!!!!!!!!!!!!!!!!!!!!!!!!! XSEC3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\n";
        }

        // &&&&&&&&&&&&&&&&&&&&&& end Equation XSEC3 &&&&&&&&&&&&&&&&&&&&&&&&&&& //




        // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee //
        // Calculate errors associated with monitor counts in both
        // calibration and fission runs

        float monErrTerm = 1./(float)fisInfo.monSumPeak
            + 1./(float)calibInfo1.monSumPeak;

        // Take into account monNormConst_fis term and calculate solid angle normalization
        // errors using calibYield_lab
        if (xsecTest == 1){

            cout << "\n !!!!!!!!!!!!!!!!!!! XSEC4a !!!!!!!!!!!!!!!!!!!!!!!!!!!! \n";
        }

        for (int i = 1; i <= numBins; i++){

            if (calibYield_lab[i-1] > 0){

                if (xsecTest == 1){
                    cout << "TESTING: calibYield_lab[ " << i-1 << "] = " << calibYield_lab[i-1] << "\n";
                }    

                // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& XSEC4a &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& //
                // take into account monitors for each fission run in solid angle normalization
                saNorm_fis[i-1] = solidAngleNorm[i-1] / monNormConst_fis;
                saNorm_fis[i-1] = saNorm_fis[i-1] / fisInfo.cubeEffCal; // takes into account deadtime


                // &&&&&&&&&&&&&&&&&&&&&&&&&&&& end XSEC4a &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& //

                saNorm_fis_err[i-1] = saNorm_fis[i-1] *
                    sqrt( monErrTerm + (1./ (calibYield_lab[i-1])));

                if (xsecTest == 1){

                    cout << "saNorm_fis [ " << i-1 << " ], solidAngleNorm[ " << i-1 <<
                        " ], monNormConst_fis : " << saNorm_fis[i-1] << " ( " << 
                        saNorm_fis_err[i-1] << " ) " << 
                        solidAngleNorm[i-1] << " " << monNormConst_fis << "\n";

                }
            }
            else{
                saNorm_fis[i-1]=0.;
                saNorm_fis_err[i-1]=0.;
            }
        }
        if (xsecTest == 1){

            cout << " !!!!!!!!!!!!!!!!!!!!!!!XSEC4a!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\n";
        }
        // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee //



        /////////////////// OPEN FISSION FILE FOR CURRENT RUN ///////////////////////

        TFile fisFile(&fisInfo.dataFile[0],"update");

        if (!fisFile.IsOpen()){
            cout << "Error: CubeXsec:: xsecCalc. Fission data file "
                << fisInfo.dataFile << " not found."<< endl;
            return -1;
        }
        if (fisFile.GetListOfKeys()->Contains("GatedTree")==true){
            fisTree = (TTree*) fisFile.Get("GatedTree");
        }
        else if (fisFile.GetListOfKeys()->Contains("CubeTreeNew")==true){
            fisTree = (TTree*) fisFile.Get("CubeTreeNew");
        }
        else{
            cout << "VALID TREE NOT FOUND for " << fisInfo.dataFile << endl;
            return -1;
        }

        ///////////
        float Slope = 2*(90-fisRxn.intercept)*1.0;//


        //////////////////// XSEC TREE+HIST CREATION //////////////////////////


        // initialize variables for storing values in our event-by-event loop

        float fisDsigdth=0.0; //place to store event-by-event solid angle normalization
        float fisDsigdomega_cm=0.0;
        float fisDsigdomega_lab=0.0;
        fisDsigdomega_lab = fisDsigdomega_lab; // turns off set but not used error
        float fisTheta_lab=0.0;
        float fisTheta_cm=0.0;
        float binnedTheta_cm=0.0;
        float binnedTheta_lab=0.0;

        float fisMR=0.0;
        float fisTKE=0.0;
        float fisTKEViola=0.0;

        //splice in definitions and related variables for all histograms
#define XSEC_HIST_CREATE
#include "CubeXsec_hist.h"
#undef XSEC_HIST_CREATE

        //define histograms that are used to do the "old" calculation cross check
        TH2D fisThetaBack_labVcm("fisThetaBack_labVcm","fisThetaBack_labVcm",numBins,0,180,numBins,0,180);
        fisThetaBack_labVcm.SetTitle("ThetaLab vs ThetaCM;thetaback[deg];ThetaCMback [deg]");
        fisThetaBack_labVcm.SetOption("COLSCATZ");

        fisFile.cd("/");

        string thetaBranchName_lab;
        string thetaBranchName_cm;

        if (N_MWPC == 2){
            thetaBranchName_lab = "theta1";
            thetaBranchName_cm = "thetaCM1";
        }
        else if (N_MWPC == 3){
            thetaBranchName_lab = "thetaback";
            thetaBranchName_cm = "thetaCMback";
        }
        else{
            cout << "Code has not been updated for N_MWPC != 2 or 3. Abort.\n";
            return -1;
        }


        //Branches from fission tree that I need
        TBranch *ftheta_lab = fisTree->GetBranch(&thetaBranchName_lab[0]);
        TBranch *ftheta_cm = fisTree->GetBranch(&thetaBranchName_cm[0]);
        TBranch *fMR = fisTree->GetBranch("MR");
        TBranch *fTKE = fisTree->GetBranch("TKE");
        TBranch *fTKEViola = fisTree->GetBranch("TKEViola");

        //Create new tree and branch to store solid angle normalizations for each
        // event. You can use this tree as a friend to the fission tree if you
        // want to make histograms on the fly using this information.

        //New tree and branch needed
        TTree saNormTree("saNormTree","saNormTree");

        TBranch *saNormBranch = saNormTree.Branch("fisDsigdth",
                &fisDsigdth,"fisDsigdth/D");
        TBranch *theta_cm = saNormTree.Branch("thetaCMback_binned",
                &fisTheta_cm,"thetaCMback_binned/D");
        TBranch *theta_lab = saNormTree.Branch("thetaback_binned",
                &fisTheta_lab,"thetaback_binned/D");

        // arrays for storing event-by-event angular distributions + errors
        float* xsecCM_o=new float[numBins];
        float* xsecCM_t=new float[numBins];
        float* xsecCM_o_mir = new float[numBins];
        float* xsecCM_t_mir = new float[numBins];

        float* xsecCM_o_err=new float[numBins];
        float* xsecCM_t_err=new float[numBins];
        float* xsecCM_o_mir_err = new float[numBins];
        float* xsecCM_t_mir_err = new float[numBins];

        float* xsecThetaCM = new float[numBins];


        //initialize arrays
        for (int i=0;i<numBins;i++){
            xsecCM_o[i]=0;
            xsecCM_t[i]=0;
            xsecCM_o_mir[i]=0;
            xsecCM_t_mir[i]=0;

            xsecCM_o_err[i]=0;
            xsecCM_t_err[i]=0;
            xsecCM_o_mir_err[i]=0;
            xsecCM_t_mir_err[i]=0;

            xsecThetaCM[i]=0;

        }



        //get total events in fission tree
        Long64_t nentries = fisTree->GetEntries();

        for (Long64_t i=0;i<nentries;i++){

            //set addresses for fission tree
            ftheta_lab->SetAddress(&fisTheta_lab);
            ftheta_cm->SetAddress(&fisTheta_cm);
            fMR->SetAddress(&fisMR);
            fTKE->SetAddress(&fisTKE);
            fTKEViola->SetAddress(&fisTKEViola);

            //set addresses for new tree
            theta_cm->SetAddress(&binnedTheta_cm);
            theta_lab->SetAddress(&binnedTheta_lab);
            saNormBranch->SetAddress(&fisDsigdth);

            //get values we will use from fis tree
            ftheta_lab->GetEvent(i);
            ftheta_cm->GetEvent(i);
            fMR->GetEvent(i);
            fTKE->GetEvent(i);
            fTKEViola->GetEvent(i);

            int binNum_lab=0;
            if (fisTheta_lab >= 0){

                //figure out which bin corresponds to the theta value 
                //for this event - this is the ROOT BIN with first bin = 1

                binNum_lab = findRootBin(fisTheta_lab, 0, 180, numBins);//etw 19-7-17 to fix bug in root bin calc
                int binNum_cm = findRootBin(fisTheta_cm, 0, 180, numBins);//etw 19-7-17 to fix bug in root bin calc

                //used to record value at bin center (effectively used in histogram method)
                float binnedTheta_lab;
                binnedTheta_lab = (binNum_lab-1) * xsecSpec.binSize + 0.5*xsecSpec.binSize;
                binnedTheta_lab = binnedTheta_lab;//just turns off set but not used error
                binnedTheta_cm = (binNum_cm-1) * xsecSpec.binSize + 0.5*xsecSpec.binSize;

                // Trick is used because you need to take into account binning
                // used to derive SA normalization. All angle quantities in 
                // xsec tree are inherently binned quantities because the solid
                // angle normalizations are derived from binned data.

                // &&&&&&&&&&&&&&&&&&&&&&&& EQUATION XSEC5 &&&&&&&&&&&&&&&&&&&&&&&&&& //

                // The sum of fisDsigdth (fisDsigdomega_cm) over all events in the fission run is
                // equal to dsig/dtheta_<ref frame> (dsig/domega_<ref frame>).
                // Note: for fisDsigdth, the reference frame is defined by our choice in creating
                // histograms; if we distribute fisDsigdth in a histogram according to lab angles,
                // this will be dsig/dtheta_lab; if we distribute fisDsigdth in a histogram according
                // to centre-of-mass angles, this will be dsig/dtheta_cm.
                // For fisDsigdomega_cm, we can ONLY distribute these values in histograms according
                // to centre-of-mass angles because of the sin(fisTheta_cm) factor; if we wanted
                // the equivalent in the lab frame, we would need to create the equivalent term
                // with sin(fisTheta_lab).

                if (xsecTest == 1 && i < 10){
                    cout << "TESTING: saNorm_fis [" << binNum_lab-1 << " ] = " << saNorm_fis[binNum_lab-1] << "\n";
                }


                fisDsigdth = saNorm_fis[binNum_lab-1];
                fisDsigdomega_cm = fisDsigdth/(2.*pi*sin(fisTheta_cm*d2r));
                fisDsigdomega_lab = fisDsigdth/(2.*pi*sin(fisTheta_lab*d2r));




                // &&&&&&&&&&&&&&&&&&&&&&&& EQUATION XSEC5 &&&&&&&&&&&&&&&&&&&&&&&&&& //


                //fill angular distributions arrays
                xsecCM_o[binNum_cm-1]+=fisDsigdomega_cm;
                xsecCM_t[binNum_cm-1]+=fisDsigdth;

                //fill angular distribution error arrays
                xsecCM_o_err[binNum_cm-1]+=fisDsigdomega_cm*fisDsigdomega_cm*((1./calibYield_lab[binNum_lab-1])+monErrTerm+1.);//+1 for fis yield
                xsecCM_t_err[binNum_cm-1]+=fisDsigdth*fisDsigdth*((1./calibYield_lab[binNum_lab-1])+monErrTerm+1.);


                // fill mirrored angular distribution arrays 
                //if (fisTheta_cm > 90. + 74.*(fisMR-0.5)){ // linear
                if (fisTheta_cm > 90. + Slope*(fisMR-0.5)){ // linear
                    //if (fisThetaCM >43.125 + 197.917*fisMR - 312.5*fisMR*fisMR + 208.33*fisMR*fisMR*fisMR){ 
                    // curve (mirroing condition from Renju's code)


                    int mirBin_cm = (int) round((180.-fisTheta_cm) / xsecSpec.binSize);

                    xsecCM_o_mir_err[binNum_cm-1]+=fisDsigdomega_cm*fisDsigdomega_cm*((1./calibYield_lab[binNum_lab-1])+monErrTerm+1.);
                    xsecCM_t_mir_err[binNum_cm-1]+=fisDsigdth*fisDsigdth*((1./calibYield_lab[binNum_lab-1])+monErrTerm+1.);

                    xsecCM_o_mir[binNum_cm-1]+=fisDsigdomega_cm;
                    xsecCM_t_mir[binNum_cm-1]+=fisDsigdth;

                    xsecCM_o_mir[mirBin_cm-1]+=fisDsigdomega_cm;
                    xsecCM_t_mir[mirBin_cm-1]+=fisDsigdth;

                    xsecCM_o_mir_err[mirBin_cm-1]+=fisDsigdomega_cm*fisDsigdomega_cm*((1./calibYield_lab[binNum_lab-1])+monErrTerm+1.);
                    xsecCM_t_mir_err[mirBin_cm-1]+=fisDsigdth*fisDsigdth*((1./calibYield_lab[binNum_lab-1])+monErrTerm+1.);

                }



                //fill all histograms except those used for angular distributions
#define XSEC_HIST_FILL
#include "CubeXsec_hist.h"
#undef XSEC_HIST_FILL




                if (xsecTest == 1 && i < 10){
                    cout << " \n !!!!!!!!!!!!!!!!!!!! XSEC5 !!!!!!!!!!!!!!!!!!!!! \n";
                    cout << "TESTING: fisDsigdth [ " << fisRunNum << " ] = " << fisDsigdth << "\n";
                    cout << "TESTING: fisDsigdomega_cm [ " << fisRunNum << " ] = " << 
                        fisDsigdomega_cm << "\n";
                    cout << "TESTING: fisDsigdomega_lab [ " << fisRunNum << " ] = " << 
                        fisDsigdomega_lab << "\n\n";
                    cout << "TESTING: bin number - 1: " << binNum_lab-1 << "\n";


                    cout << "TESTING: fisTheta_cm, fisTheta_lab, (2.*pi*sin(fisTheta_cm*d2r)," <<
                        "(2.*pi*sin(fisTheta_lab*d2r)): " << fisTheta_cm << " " << fisTheta_lab << " " <<
                        (2.*pi*sin(fisTheta_cm*d2r)) << ", " << (2.*pi*sin(fisTheta_lab*d2r)) << "\n";
                    cout << " \n !!!!!!!!!!!!!!!!!!!! XSEC5 !!!!!!!!!!!!!!!!!!!!! \n\n";

                    cout << "TESTING: xsecCM_o[ " << binNum_cm-1 << " ] = " << xsecCM_o[binNum_cm-1] << "\n";
                    cout << "TESTING: xsecCM_t[ " << binNum_cm-1 << " ] = " << xsecCM_t[binNum_cm-1] << "\n";
                }


                //The following histogram is filled and handled separately because we use it in the
                // calcAngDist() function later; user modification might muck things up 
                fisThetaBack_labVcm.Fill(fisTheta_lab,fisTheta_cm);//used to compute error on ang dist later

                }
                else{
                    fisDsigdth = 0.;
                }

                saNormTree.Fill();

            } // end loop over events in current fission tree

            saNormTree.Write("",TObject::kOverwrite);
            cout << " saNormTree written ... \n";

            //fill xsecThetaCM histogram and take square root of error histograms
            for (int i = 0; i<numBins; i++){

                xsecCM_o_err[i]=sqrt(xsecCM_o_err[i]);
                xsecCM_t_err[i]=sqrt(xsecCM_t_err[i]);
                xsecCM_o_mir_err[i]=sqrt(xsecCM_o_err[i]);
                xsecCM_t_mir_err[i]=sqrt(xsecCM_t_err[i]);

                xsecThetaCM[i]= i * xsecSpec.binSize + 0.5*xsecSpec.binSize; //calculates bin center  // updated 19-7-17 etw

            }



            // Create ascii output of the

            // The next five arrays are used in the "old" method of the code, which uses binned histograms
            // to compute an angular distribution from the data. This angular distribution calculation is
            // done within the calcAngDist function. Arrays used solely for this purpose will have names
            // starting with AD.

            // The old method is a good cross check for the new method and allows us to compute
            // errors on the angular distribution we obtain from our data in the usual way

            float* ADtheta_cm = new float[numBins];
            float* ADdsigdomega_cm = new float[numBins];
            float* ADdsigdomega_cm_err = new float[numBins];
            float* ADdsigdtheta_cm = new float[numBins];
            float* ADdsigdtheta_cm_err = new float[numBins];


            // initialize these histograms
            for (int i=0;i<numBins;i++){
                ADtheta_cm[i]=0.;
                ADdsigdomega_cm[i]=0.;
                ADdsigdomega_cm_err[i]=0.;
                ADdsigdtheta_cm[i]=0.;
                ADdsigdtheta_cm_err[i]=0.;
            }

            int numDataPoints=0; //for creating the TGraph objects we'll use to plot angular distributions


            // Compute Angular Distributions the "old" way (using histograms)
            errCheck = calcAngDist(&fisThetaBack_labVcm, &calibThetaBack_labVcm,
                    ADtheta_cm, ADdsigdomega_cm,
                    ADdsigdomega_cm_err, ADdsigdtheta_cm,
                    ADdsigdtheta_cm_err, numBins, &numDataPoints,
                    &xsecSpec, calibYield_lab,
                    saNorm_fis, monErrTerm);

            if (errCheck!=0){
                return -1;
            }


            // OUTPUT ASCII FILE OF EVENT-BY-EVENT ANGULAR DISTRIBUTION
            errCheck = xsecCMascii(&xsecSpec,xsecThetaCM,xsecCM_o,xsecCM_o_err,numBins,0,fisRunNum);

            if (errCheck!=0){
                return -1;
            }


            // OUTPUT FIFRANG-FRIENDLY ASCII FILE OF EVENT-BY-EVENT ANGULAR DISTRIBUTION
            if (fisInfo.fifrangOut == true){

                errCheck = xsecCMascii(&xsecSpec,xsecThetaCM,xsecCM_o,xsecCM_o_err,numBins,1,fisRunNum); //overwrites normal output format

                if (errCheck!=0){
                    return -1;
                }

                errCheck = fifrangOutput(&xsecSpec, ADtheta_cm, ADdsigdomega_cm, ADdsigdomega_cm_err, (size_t)numDataPoints,
                        fisRunNum);

                if (errCheck!=0){
                    return -1;
                }
            }



            // Create TGraph objects to store angular distribution objects - old (histogram) method

            //dsigdOmega
            TGraphErrors *ADgraph_O = new TGraphErrors(numDataPoints,ADtheta_cm,ADdsigdomega_cm,0,ADdsigdomega_cm_err); //# points,x,y,dx,dy
            ADgraph_O->SetMarkerColor(kBlue);
            ADgraph_O->SetLineColor(2);
            ADgraph_O->SetLineWidth(2);
            ADgraph_O->SetMarkerStyle(21);
            ADgraph_O->SetMarkerSize(1.0);
            ADgraph_O->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            ADgraph_O->GetXaxis()->CenterTitle(true);
            ADgraph_O->GetXaxis()->SetTitleOffset(1.0);
            ADgraph_O->GetXaxis()->SetLimits(20,180);
            ADgraph_O->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega}(#theta_{cm}) mb/sr");
            ADgraph_O->GetYaxis()->CenterTitle(true);
            ADgraph_O->GetYaxis()->SetTitleOffset(1.5);
            ADgraph_O->SetTitle("Angular Distribution (Histogram method -- aka old way)");
            ADgraph_O->Draw("acp");

            //dsigdtheta
            TGraphErrors *ADgraph_T = new TGraphErrors(numDataPoints,ADtheta_cm,ADdsigdtheta_cm,0,ADdsigdtheta_cm_err); //# points,x,y,dx,dy
            ADgraph_T->SetMarkerColor(kBlue);
            ADgraph_T->SetLineColor(2);
            ADgraph_T->SetLineWidth(2);
            ADgraph_T->SetMarkerStyle(21);
            ADgraph_T->SetMarkerSize(1.0);
            ADgraph_T->SetFillColor(38);
            ADgraph_T->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            ADgraph_T->GetXaxis()->CenterTitle(true);
            ADgraph_T->GetXaxis()->SetTitleOffset(1.0);
            ADgraph_T->GetXaxis()->SetLimits(20,180);
            ADgraph_T->GetYaxis()->SetTitle("#frac{d#sigma}{d#theta}(#theta_{cm}) mb/rad");
            ADgraph_T->GetYaxis()->CenterTitle(true);
            ADgraph_T->GetYaxis()->SetTitleOffset(1.5);
            ADgraph_T->SetTitle("Angular Distribution (Histogram Method -- aka old way)");
            ADgraph_T->Draw("acp");

            //Create TGraph objects to store event-by-event angular distributions

            //dsigdOmega
            TGraphErrors *EEgraph_O = new TGraphErrors(numBins,xsecThetaCM,xsecCM_o,0,xsecCM_o_err); //# points,x,y,dx,dy
            EEgraph_O->SetMarkerColor(kGray);
            EEgraph_O->SetLineColor(kBlack);
            EEgraph_O->SetLineWidth(2);
            EEgraph_O->SetMarkerStyle(21);
            EEgraph_O->SetMarkerSize(1.0);
            EEgraph_O->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            EEgraph_O->GetXaxis()->CenterTitle(true);
            EEgraph_O->GetXaxis()->SetTitleOffset(1.0);
            EEgraph_O->GetXaxis()->SetLimits(20,180);
            EEgraph_O->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega}(#theta_{cm}) mb/sr");
            EEgraph_O->GetYaxis()->CenterTitle(true);
            EEgraph_O->GetYaxis()->SetTitleOffset(1.5);
            EEgraph_O->SetTitle("Angular Distribution (Event-by-event method)");
            EEgraph_O->Draw("acp");

            //dsigdtheta
            TGraphErrors *EEgraph_T = new TGraphErrors(numBins,xsecThetaCM,xsecCM_t,0,xsecCM_t_err); //# points,x,y,dx,dy
            EEgraph_T->SetMarkerColor(kGray);
            EEgraph_T->SetLineColor(kBlack);
            EEgraph_T->SetLineWidth(2);
            EEgraph_T->SetMarkerStyle(21);
            EEgraph_T->SetMarkerSize(1.0);
            EEgraph_T->SetFillColor(38);
            EEgraph_T->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            EEgraph_T->GetXaxis()->CenterTitle(true);
            EEgraph_T->GetXaxis()->SetTitleOffset(1.0);
            EEgraph_T->GetXaxis()->SetLimits(20,180);
            EEgraph_T->GetYaxis()->SetTitle("#frac{d#sigma}{d#theta}(#theta_{cm}) mb/rad");
            EEgraph_T->GetYaxis()->CenterTitle(true);
            EEgraph_T->GetYaxis()->SetTitleOffset(1.5);
            EEgraph_T->SetTitle("Angular Distribution (Event-by-event method)");
            EEgraph_T->Draw("acp");

            //dsigdOmega - mirrored
            TGraphErrors *EEgraph_O_mir = new TGraphErrors(numBins,xsecThetaCM,xsecCM_o_mir,0,xsecCM_o_mir_err); //# points,x,y,dx,dy
            EEgraph_O_mir->SetMarkerColor(kGray);
            EEgraph_O_mir->SetLineColor(kBlack);
            EEgraph_O_mir->SetLineWidth(2);
            EEgraph_O_mir->SetMarkerStyle(21);
            EEgraph_O_mir->SetMarkerSize(1.0);
            EEgraph_O_mir->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            EEgraph_O_mir->GetXaxis()->CenterTitle(true);
            EEgraph_O_mir->GetXaxis()->SetTitleOffset(1.0);
            EEgraph_O_mir->GetXaxis()->SetLimits(20,180);
            EEgraph_O_mir->GetYaxis()->SetTitle("#frac{d#sigma}{d#Omega}(#theta_{cm}) mb/sr");
            EEgraph_O_mir->GetYaxis()->CenterTitle(true);
            EEgraph_O_mir->GetYaxis()->SetTitleOffset(1.5);
            EEgraph_O_mir->SetTitle("Angular Distribution (Event-by-event method - mirrored)");
            EEgraph_O_mir->Draw("acp");

            //dsigdtheta - mirrored
            TGraphErrors *EEgraph_T_mir = new TGraphErrors(numBins,xsecThetaCM,xsecCM_t_mir,0,xsecCM_t_mir_err); //# points,x,y,dx,dy
            EEgraph_T_mir->SetMarkerColor(kGray);
            EEgraph_T_mir->SetLineColor(kBlack);
            EEgraph_T_mir->SetLineWidth(2);
            EEgraph_T_mir->SetMarkerStyle(21);
            EEgraph_T_mir->SetMarkerSize(1.0);
            EEgraph_T_mir->SetFillColor(38);
            EEgraph_T_mir->GetXaxis()->SetTitle("#theta_{c.m.} deg");
            EEgraph_T_mir->GetXaxis()->CenterTitle(true);
            EEgraph_T_mir->GetXaxis()->SetTitleOffset(1.0);
            EEgraph_T_mir->GetXaxis()->SetLimits(20,180);
            EEgraph_T_mir->GetYaxis()->SetTitle("#frac{d#sigma}{d#theta}(#theta_{cm}) mb/rad");
            EEgraph_T_mir->GetYaxis()->CenterTitle(true);
            EEgraph_T_mir->GetYaxis()->SetTitleOffset(1.5);
            EEgraph_T_mir->SetTitle("Angular Distribution (Event-by-event method - mirrored)");
            EEgraph_T_mir->Draw("acp");


            //********************************************************************
            //Xsec Histogram Directory
            //SAVE HISTOGRAMS TO FISSION ROOT FILE
            cout << "Now writing histograms of data ...\n\n";


            // (create and) write to xsecInput directory
            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_Input")!=true){
                fisFile.mkdir("Xsec_Input");
            }
            fisFile.cd("Xsec_Input");

            calibThetaBack_lab.Write("",TObject::kOverwrite);
            calibThetaBack_cm.Write("",TObject::kOverwrite);
            calibThetaBack_labVcm.Write("",TObject::kOverwrite);
            fisThetaBack_labVcm.Write("",TObject::kOverwrite);

            cout << "  1.  Finished filling Xsec_Input directories ... \n" << endl;


            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_pub")!=true){
                fisFile.mkdir("Xsec_pub");
            }
            fisFile.cd("Xsec_pub");

            ADgraph_T->Write("AD_disigdtheta",TObject::kOverwrite);
            ADgraph_O->Write("AD_dsigdomega",TObject::kOverwrite);
            EEgraph_T->Write("EE_dsigdtheta",TObject::kOverwrite);
            EEgraph_O->Write("EE_dsigdomega",TObject::kOverwrite);
            EEgraph_T_mir->Write("EE_mir_dsigdtheta",TObject::kOverwrite);
            EEgraph_O_mir->Write("EE_mir_dsigdomega",TObject::kOverwrite);


#define XSEC_HIST_PUB_WRITE
#include "CubeXsec_hist.h"
#undef XSEC_HIST_PUB_WRITE

            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_group")!=true){
                fisFile.mkdir("Xsec_group");
            }
            fisFile.cd("Xsec_group");

#define XSEC_HIST_GR_WRITE
#include "CubeXsec_hist.h"
#undef XSEC_HIST_GR_WRITE

            cout << "  2.  Finished filling Xsec_pub and Xsec_group directories ... \n" << endl;


            // (create and) write to Xsec_derived directory
            fisFile.cd("/");
            if (fisFile.GetListOfKeys()->Contains("Xsec_derived")!=true){
                fisFile.mkdir("Xsec_derived");
            }
            fisFile.cd("Xsec_derived");

#define XSEC_HIST_DERIVED
#include "CubeXsec_hist.h"
#undef XSEC_HIST_DERIVED

            cout << "  3.  Finished filling Xsec_derived directories ... \n" << endl;

            cout << "Finished creating histograms ...\n\n";

            //END HISTOGRAM CREATION
            //**********************************************************************


            //increment iteration through all fission run lists
            fisInfoIndex++;
            //fisSortIndex++;
            fisRxnIndex++;
            fisTargIndex++;
            fisRunNum++;

            // clean up dynamically allocated arrays
            delete[] ADtheta_cm;
            delete[] ADdsigdomega_cm;
            delete[] ADdsigdomega_cm_err;
            delete[] ADdsigdtheta_cm;
            delete[] ADdsigdtheta_cm_err;


            }//END FISSION LOOP



            // Run calibration error check (basically, provide graphs of dsig/dtheta_lab extracted using
            // the solid angle normalization coefficients and dsig/dtheta_lab calculated using the
            // Rutherford formula; these plots are provided in the calibCheck.root formula.

            errCheck = calibTest(&xsecSpec, &calibInfo1, &calibInfo3, &calibRuthCalc, &calibThetaBack_lab, solidAngleNorm,
                    numBins, monNormConst_cal);
            if (errCheck != 0){
                return -1;
            }

            //clean up stuff on stack

            delete[] solidAngleNorm;
            delete[] solidAngleNorm_err;
            delete[] saNorm_fis;
            delete[] saNorm_fis_err;
            delete[] calibYield_lab;



            /////////////////////// SUMMARY LOGFILE //////////////////////////////////

            //CALIB LOG FILE OUTPUT //////////////////////////////////////////////

            logfile << "\n Calculations assume a bin size of " 
                << xsecSpec.binSize << " degrees (" << numBins << " bins).\n\n";
            cout << "Calculations assume a bin size of " 
                << xsecSpec.binSize << " degrees (" << numBins << " bins).\n\n";

            logfile << "If three detectors are used, the summary table below";
            logfile << "only provides information on the detector 1 calibration. \n\n";

            char logprint[500];

            //table header
            logfile << "SUMMARY TABLE:\n";
            cout << " *** SUMMARY TABLE ***\n";

            sprintf(logprint,"%-70s %-5s %-15s %-15s %-15s %-20s %-20s %-20s %-20s \n",
                    "File","Type","Run Index","Mon Eff","Cube Eff","MonAngle[deg,lab]",
                    "MonAngle[deg,CM]","MonCounts*","Rutherford [mb]^");
            logfile << logprint;
            cout << logprint;

            //table info

            //calib - det 1
            sprintf(logprint,"%-70s %-5s %-15s %-15f %-15f %-20f %-20f %-20i %-20f \n",
                    &calibInfo1.dataFile[0], "cal det 1", "-", calibInfo1.monEffCal,
                    calibInfo1.cubeEffCal,calibInfo1.monAngleLab, calibInfo1.monAngleCM,
                    calibInfo1.monSumPeak,calibInfo1.ruthMon);
            logfile << logprint;
            cout << logprint;

            if (N_MWPC == 3){

                //calib - det 3
                sprintf(logprint,"%-70s %-5s %-15s %-15f %-15f %-20f %-20f %-20i %-20f \n",
                        &calibInfo3.dataFile[0], "cal det 3", "-", calibInfo3.monEffCal,
                        calibInfo3.cubeEffCal,calibInfo3.monAngleLab, calibInfo3.monAngleCM,
                        calibInfo3.monSumPeak,calibInfo3.ruthMon);
                logfile << logprint;
                cout << logprint;

            }

            //fission loop

            fisRunNum=0;
            fisInfoIndex = fisInfoList.begin();


            while (fisInfoIndex != fisInfoList.end()){

                xsecInfo& fisInfo = *fisInfoIndex;

                sprintf(logprint,"%-70s %-5s %-15i %-15f %-15f %-20f %-20f %-20i %-20f \n",
                        &fisInfo.dataFile[0], "fis", fisRunNum, 
                        fisInfo.monEffCal, 
                        fisInfo.cubeEffCal,
                        fisInfo.monAngleLab, 
                        fisInfo.monAngleCM, 
                        fisInfo.monSumPeak,
                        fisInfo.ruthMon);
                logfile << logprint;
                cout << logprint;

                fisInfoIndex++;
                fisRunNum++;
            };



            //table footer
            logfile << "_________________________________________________\n";
            logfile << "* Corrected for pre-scale and detector efficiency.\n";
            logfile << "^ Calculated Rutherford scattering cross section at "
                <<"monitor angle in lab frame.\n";

            cout << "_________________________________________________\n";
            cout << "* Corrected for pre-scale and detector efficiency.\n";
            cout << "^ Calculated Rutherford scattering cross section at "
                <<"monitor angle in lab frame.\n";

            cout << "\nCross section calculations complete! Calculation summary saved"
                << " in " << logName << " \n";




            logfile.close(); //close log file for all runs

            return 0;
        }// end xsecCalc



        /////////////////////////////////////////////////////////////////////
        ///////////////////// BEGIN OTHER FUNCTIONS HERE ////////////////////
        /////////////////////////////////////////////////////////////////////
        // Last modified + by whom dates are listed in comments


        /*------------------------------------------------------------------------------------
          calcCalibData
          : This is where all calculations related to the calibration run(s) are done.
          - Liz Williams, Jun 2017
          ------------------------------------------------------------------------------------*/
        int calcCalibData(xsecInfo* calibInfo1, xsecInfo* calibInfo3,
                reactPar* calibRxn1, reactPar* calibRxn3, 
                targetPar* calibTarg1, targetPar* calibTarg3,
                TH1D* calibThetaBack_lab,
                TH1D* calibThetaBack_cm,
                TH2D* calibThetaBack_labVcm,
                TH1D* calibRuthCalc,
                float* monNormConst_cal,
                int numBins){

            //Designed to work for both 2- and 3-detector configurations

            cout << "\n ************ CALIBRATION RUN";
            if (N_MWPC==3){
                cout << "(S) **************** \n";
            }
            else{
                cout << " ***************** \n";
            }

            int errCheck = 0;

            // ** Now carry out deadtime / efficiency / monitor lab angle / beam energy calculations
            // and calculated Rutherford cross section dsig/domega_lab at monitor angle
            // relating only to the calibration runs **//

            errCheck = preCalcs(calibInfo1, calibRxn1, calibTarg1, 1); //1 means calculate the energy loss, etc, for this reaction etw 19-7-17

            if (errCheck == -1){
                cout << "Calibration run: Problem with efficiency calibrations. Check input file!\n";
                cout << calibInfo1->monEffCal << "  " << calibInfo1->cubeEffCal << "\n";
            }

            if (N_MWPC==3){
                errCheck = preCalcs(calibInfo3, calibRxn3, calibTarg3, 1); //1 means calculate the energy loss, etc, for this reaction etw 19-7-17

                if (errCheck == -1){
                    cout << "Calibration run: Problem with efficiency calibrations. Check input file!\n";
                    cout << calibInfo3->monEffCal << "  " << calibInfo3->cubeEffCal << "\n";
                }

            }


            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& begin XSEC1 Equation &&&&&&&&&&&&&&&&&&&&&&&&&//

            //monitor normalization factor part 1, including cube efficiency correction. Calib only portion.
            //Uses detector 1 to calculate this.

            *monNormConst_cal =  (float)calibInfo1->monSumPeak / (calibInfo1->ruthMon);

            if (xsecTest == 1){
                cout << "\n !!!!!!!!!!!!!!!!!!!!!!!! XSEC1 !!!!!!!!!!!!!!!!!!!!!!! \n";
                cout << "TESTING: monNormConst_cal = " << *monNormConst_cal << "\n";
                cout << "TESTING: calibInfo1->monSumPeak, calibInfo1->ruthMon: " << 
                    calibInfo1->monSumPeak << ", " << calibInfo1->ruthMon << "\n";
                cout << " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\n";
            }

            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end XSEC1 Equation &&&&&&&&&&&&&&&&&&&&&&&&&&&//





            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& begin XSEC2 Equation &&&&&&&&&&&&&&&&&&&&&&&&//
            float det3ScaleFactor = 0;

            if (N_MWPC==3){


                //calculate the factor the detector 3 calibration data will be scaled by, so the
                //integrated beam current for the two different runs are accounted for.

                det3ScaleFactor = (float)calibInfo1->monSumPeak / (float)calibInfo3->monSumPeak;

                if (xsecTest == 1){
                    cout << "\n !!!!!!!!!!!!!!!!!!!!!!!! XSEC2 !!!!!!!!!!!!!!!!!!!!!!!! \n";
                    cout << "TESTING: det3ScaleFactor = " << det3ScaleFactor << "\n\n";
                    cout << "TESTING: calibInfo1->monSumPeak, calibInfo3->monSumPeak " << calibInfo1->monSumPeak << ", " << calibInfo3->monSumPeak << "\n";
                    cout << " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n\n";
                }

            }

            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& end XSEC2 Equation &&&&&&&&&&&&&&&&&&&&&&&&&&&//


            //Access the first calibration file -- applies for both 2+3 detector configurations

            TFile calibFile1(&calibInfo1->dataFile[0],"update");
            if (!calibFile1.IsOpen()){
                cout << "Error: CubeXsec:: xsecCalc: Calibration data file "
                    << &calibInfo1->dataFile[0] << " not found."<< endl;
                return -1;
            }

            //Access the first tree
            TTree *calibTree1;
            if (calibFile1.GetListOfKeys()->Contains("GatedTree")==true){
                calibTree1 = (TTree*) calibFile1.Get("GatedTree");
            }
            else if (calibFile1.GetListOfKeys()->Contains("CubeTreeNew")==true){
                string input = "";
                char answer={0};
                calibTree1 = (TTree*) calibFile1.Get("CubeTreeNew");
                cout << "Are you sure you want the ungated tree?? We usually ";
                cout << "apply gates to calibration files. ";
                while (true){
                    cout << "Please answer y or n \n";
                    getline(cin, input);
                    if (input.length() == 1){
                        answer = input[0];
                        if (answer == 'y' || answer == 'n'){
                            if (answer == 'n'){
                                cout << "Exiting calculation. Please check your ";
                                cout << "cross section input file and try again.\n";
                                return -1; //abort the code
                            }
                            if (answer == 'y'){
                                break; //break out of while loop
                            }
                        }
                        else{
                            cout << "Please enter a valid character.\n";
                        }
                    }
                }

            }
            else{
                cout << "Calibration file: Tree named GatedTree or CubeTreeNew ";
                cout << "not found. Calculation aborted.\n";
                return -1;
            }


            // create theta1, theta1CM histograms
            TH1D* theta1=new TH1D("theta1","theta1",numBins,0,180);
            calibTree1->Draw("theta1>>theta1","","");
            TH1D* theta1CM=new TH1D("theta1CM","theta1CM",numBins,0,180);
            calibTree1->Draw("thetaCM1>>theta1CM","","");

            TH2D* theta1vtheta1CM=new TH2D("theta1vtheta1CM","theta1vtheta1CM",numBins,0,180,numBins,0,180);
            calibTree1->Draw("theta1:thetaCM1>>theta1vtheta1CM","","");

            // do efficiency correction for histograms - calibration file 1
            theta1->Scale(1./calibInfo1->cubeEffCal);
            theta1CM->Scale(1./calibInfo1->cubeEffCal);
            theta1vtheta1CM->Scale(1./calibInfo1->cubeEffCal);

            //etw 20-7-17 changed to accommodate different calibration reactions. Need average rutherford [angle bin] so
            //this is step one. Note I use theta at bin center because our solid angle norms share the predefined bin structure
            for (int i=1;i<=numBins;i++){

                float ruthCalc = dsigdTheta_Ruth_lab(calibRxn1->Ap, calibRxn1->Zp, calibRxn1->At, 
                        calibRxn1->Zt, calibRxn1->Ecm, theta1->GetBinCenter(i));
                ruthCalc = ruthCalc * theta1->GetBinContent(i);

                calibRuthCalc->SetBinContent(i,ruthCalc);
            }



            //begin calibration file processing for detector 3 (if applicable)
            if (N_MWPC==3){

                //Access the second calibration file -- applies for 3 detector configuration

                TFile calibFile3(&calibInfo3->dataFile[0],"update");
                if (!calibFile3.IsOpen()){
                    cout << "Error: CubeXsec:: xsecCalc: Calibration data file "
                        << &calibInfo3->dataFile[0] << " not found."<< endl;
                    return -1;
                }

                //Access the second calibration tree
                TTree *calibTree3;
                if (calibFile3.GetListOfKeys()->Contains("GatedTree")==true){
                    calibTree3 = (TTree*) calibFile3.Get("GatedTree");
                }
                else if (calibFile3.GetListOfKeys()->Contains("CubeTreeNew")==true){
                    string input = "";
                    char answer={0};
                    calibTree3 = (TTree*) calibFile3.Get("CubeTreeNew");
                    cout << "Are you sure you want the ungated tree?? We usually ";
                    cout << "apply gates to calibration files. ";
                    while (true){
                        cout << "Please answer y or n \n";
                        getline(cin, input);
                        if (input.length() == 1){
                            answer = input[0];
                            if (answer == 'y' || answer == 'n'){
                                if (answer == 'n'){
                                    cout << "Exiting calculation. Please check your ";
                                    cout << "cross section input file and try again.\n";
                                    return -1; //abort the code
                                }
                                if (answer == 'y'){
                                    break; //break out of while loop
                                }
                            }
                            else{
                                cout << "Please enter a valid character.\n";
                            }
                        }
                    }

                }
                else{
                    cout << "Calibration file: Tree named GatedTree or CubeTreeNew "
                        << "not found. Calculation aborted."<< endl;
                    return -1;
                }

                // create theta3 histogram
                TH1D* theta3= new TH1D("theta3","theta3",numBins,0,180);
                calibTree3->Draw("theta3>>theta3","","");

                TH1D* theta3CM=new TH1D("thetaCM3","thetaCM3",numBins,0,180);
                calibTree3->Draw("thetaCM3>>theta3CM","","");

                TH2D* theta3vtheta3CM=new TH2D("theta3vtheta3CM","theta3vtheta3CM",numBins,0,180,numBins,0,180);
                calibTree1->Draw("theta3:thetaCM3>>theta3vtheta3CM","","");

                // do efficiency correction and integrated beam current correction for histograms 
                //- calibration file 3
                theta3->Scale(det3ScaleFactor/calibInfo3->cubeEffCal);
                theta3CM->Scale(det3ScaleFactor/calibInfo3->cubeEffCal);
                theta3vtheta3CM->Scale(det3ScaleFactor/calibInfo3->cubeEffCal);

                //add theta3 histograms to the theta1 histograms
                theta1->Add(theta3);
                theta1CM->Add(theta3CM);
                theta1vtheta1CM->Add(theta3vtheta3CM);


                //etw 20-7-17 changed to accommodate different calibration reactions. Need average rutherford [angle bin] so
                //this is step one. Note I use theta at bin center because our solid angle norms share the predefined bin structure
                for (int i=1;i<=numBins;i++){

                    float ruthBinContent1 = calibRuthCalc->GetBinContent(i);
                    float ruthCalc = dsigdTheta_Ruth_lab(calibRxn3->Ap, calibRxn3->Zp, calibRxn3->At, 
                            calibRxn3->Zt, calibRxn3->Ecm, theta3->GetBinCenter(i));
                    ruthCalc = ruthCalc * theta3->GetBinContent(i);

                    calibRuthCalc->SetBinContent(i,ruthCalc+ruthBinContent1);
                }

            } // end det 3 calibration logic


            //Now divide calibRuthCalc histogram bins by theta1 bins to get average dsig/dtheta_Ruth (since, for N_MWPC=3, the 
            // theta1 histogram now contains the efficiency-corrected number of counts for both theta1 and theta3
            //etw 20-7-17
            calibRuthCalc->Divide(theta1);

            //populate histograms passed back to the main function
            for (int i=1; i<=numBins; i++){

                calibThetaBack_lab->SetBinContent(i,theta1->GetBinContent(i));
                calibThetaBack_cm->SetBinContent(i,theta1CM->GetBinContent(i));

                for (int j=1; j<=numBins; j++){
                    calibThetaBack_labVcm->SetBinContent(i,j, theta1vtheta1CM->GetBinContent(i,j));

                } 

            }//end histogram copies

            // code for error checking - only checks lab hist 
            if (xsecTest == 1){
                int thetatot=0;

                for (int i = 0; i<numBins; i++){
                    cout << "TESTING: THETA1: bin [ " << i+1 << " ] = " << 
                        calibThetaBack_lab->GetBinContent(i+1) << "\n";
                    thetatot += calibThetaBack_lab->GetBinContent(i+1);
                }
                cout << "TESTING: THETA1 TOTAL: " << thetatot << "\n\n";
            }


            cout << "Calibration histograms successfully made.\n";
            //We'll store these calibration file histograms in the fission TFiles later...

            return 0;


        } //end calcCalibData



        /*------------------------------------------------------------------------------------
          deadTimeCalc
          : Calculates deadtimes from mon 1,2 and CUBE detectors
          Here, the efficiency calibration factor cubeEffCal = N_DAQ/N_Scal, so
          the corrected counts are N = N_DAQ / cubeEffCal
          ------------------------------------------------------------------------------------*/
        int deadTimeCalc(xsecInfo* runInput){

            unsigned int totalScal=0;
            unsigned int totalDAQ=0;

            for (int i=0;i<N_mon;i++){
                if (runInput->monScal[i]==0){
                    cout << "Error in CubeXsec::deadTimeCalc. Monitor " << i+1 
                        << " scaler is zero. Check input file.\n";
                    return -1;
                }
                else{
                    totalScal = totalScal + runInput->monScal[i];
                    totalDAQ = totalDAQ + runInput->monDAQ[i];
                }
            }
            runInput->monEffCal = (float) totalDAQ / (float) totalScal;


            if (runInput->cubeScal==0){
                cout << "Error in CubeXsec::deadTimeCalc. Cube scaler is zero."
                    << "Check input file.\n";
                return -1;
            }
            else{

                runInput->cubeEffCal = (float)runInput->cubeDAQ 
                    / (float)runInput->cubeScal;
            }

            return 0;
        }//end deadTimeCalc



        ////////////////////////////// preCalcs //////////////////////////////// updated 6/17 by Liz

        //does all monitor normalization, dead time calc, etc.
        int preCalcs(xsecInfo* xsecInput, reactPar* rxn, targetPar* targ, bool calcReactPar){


            if (calcReactPar == 1){
                cout << "\nNow calculating reaction parameters for the calibration run ...\n";
                // if (rxn->elossBool==true){
                if (rxn->dEcalcType==1 || rxn->dEcalcType==2 || rxn->dEcalcType==3){
                    calcElossBeam(targ,rxn);
                }
                //Immediately calculate the remaining undefined parameters in calib rxn struct. ***
                calcRxn(rxn);
            }

            //Calculate deadtimes / efficiency corrections - calib run **************************
            cout << "\nCalculating detector efficiencies for the calibration run ...\n";
            int errCheck = deadTimeCalc(xsecInput);

            if ( errCheck != 0 ){
                cout << "Problem with efficiency calibrations. Check input file!\n";
                cout << xsecInput->monEffCal << "  " << xsecInput->cubeEffCal << "\n";
                return -1;
            }

            //Monitor Lab->CM calculation *******************************************
            cout << "Transforming monitor angles from lab to CM, calculating Rutherford cross sections...\n";

            xsecInput->monAngleCM = thLab2ThCM_elastics(rxn->Ap, 
                    rxn->At, xsecInput->monAngleLab);


            xsecInput-> ruthMon = dsigdTheta_Ruth_lab(rxn->Ap,rxn->Zp, rxn->At,rxn->Zt,
                    rxn->Ecm, xsecInput->monAngleLab);

            if (xsecTest == 1){

                cout << " !!!!!!!!!!!!!!!!!!! mon inputs !!!!!!!!!!!!!!!!!!! \n";
                cout << " monAngleCM, ruthMon, monEffCal: " << xsecInput->monAngleCM << " " << xsecInput->ruthMon << 
                    "  " << xsecInput->monEffCal << "\n";
                cout << " !!!!!!!!!!!!!!!!!!! mon inputs end !!!!!!!!!!!!!!! \n\n";
            }

            xsecInput->monSumPeak = xsecInput->monSumPeak * xsecInput->preScal / xsecInput->monEffCal;



            return 0;
            }//end preCalcs




            //////////////////////////// calcSAnorms //////////////////////////// updated 6/17 by Liz

            //calculates solid angle normalization parameters for binned lab angles

            int calcSAnorms(xsecSpecs* xsecSpec,
                    float *solidAngleNorm, float *calibYield_lab,
                    int numBins, TH1D *calibThetaBack_lab, 
                    TH1D* calibRuthCalc,
                    float monNormConst_cal){

                //open file to save normalization data to, print header info

                string filepath = xsecSpec->resultsPath;
                string normstring;
                ofstream normfile;

                normstring = filepath + "calibNorm.dat";

                // Get local time to give output file a time stamp
                time_t t = time(0);   // get time now
                struct tm * now = localtime( & t );

                cout << "Calibration portion of solid angle normalization coefficients ";
                cout << "written to " << normstring << "\n";

                normfile.open(&normstring[0]);
                normfile << "Cross section normalization data (calibration portion), Date: ";
                normfile <<  (now->tm_year + 1900) << '-'; 
                normfile << (now->tm_mon + 1) << '-';
                normfile <<  now->tm_mday;
                normfile << "\n\n";

                normfile << "Bin\t Center\t Solid Angle Norm\n";
                //end norm header

                int thetaBins = calibThetaBack_lab->GetNbinsX();

                if (thetaBins!=numBins){
                    cout << "Error: number of bins in calibThetaBack_lab histograms unexpected. Aborting.\n";
                    return -1;
                }

                if (xsecTest == 1){
                    cout << "\n !!!!!!!!!!!!!!!!!!!!!!!XSEC4!!!!!!!!!!!!!!!!!!!!!!!! \n";
                }

                for (int i = 1; i <=numBins; i++){

                    float calibRuthCenter = calibThetaBack_lab->GetBinCenter(i);
                    float calibRuthVal = calibRuthCalc->GetBinContent(i);

                    float binVal = calibThetaBack_lab->GetBinContent(i);
                    calibYield_lab[i-1] = binVal; //I will use this in the fission loop to calculate errors

                    if (binVal>100){ //etw 19-7-17 implements stats check within solid angle norm code; points with less than 10% error in calib bin included

                        // &&&&&&&&&&&&&&&&&&&&&&&&&& Equation XSEC4 &&&&&&&&&&&&&&&&&&&&&&&&&&&& //
                        // This is where the CUBE component of the solid angle normalization is
                        // calculated (at least in the first line). The second line is XSEC1 * XSEC4
                        // and makes sure that all terms relating only to the calibration file(s) are
                        // included in the solidAngleNorm array. I ignore cases where the bin has
                        // no counts to avoid a divide by zero error.

                        solidAngleNorm[i-1] = calibRuthVal / binVal;
                        solidAngleNorm[i-1] = monNormConst_cal * solidAngleNorm[i-1];

                        if (xsecTest == 1){
                            cout << "TESTING: solidAngleNorm [ " << (i-1) << 
                                " ], monNormConst_cal, calibRuthVal, binVal, calibRuthCenter " << 
                                solidAngleNorm[i-1] << "  " << monNormConst_cal << 
                                " " << calibRuthVal << "  " << binVal <<
                                " " << calibRuthCenter << "\n";
                        }

                        // &&&&&&&&&&&&&&&&&&&&&&&&&& end Equation XSEC4 &&&&&&&&&&&&&&&&&&&&&&&& //
                    }
                    else{
                        solidAngleNorm[i-1]=0.;
                    }


                    if (solidAngleNorm[i-1]>0){
                        normfile << i << "\t" << calibRuthCenter << "\t" << solidAngleNorm[i-1] << "\n";
                    }
                }
                if (xsecTest == 1){
                    cout << "\n !!!!!!!!!!!!!!!!!!!!!!!XSEC4!!!!!!!!!!!!!!!!!!!!!!!! \n\n";
                }

                normfile.close();
                cout << "Solid angle normalizations for each lab angle bin have been calculated.\n";

                return 0;

            } // end calcSAnorms




            //////////////////////// calcAngDist ////////////////////////////// updated 6/17 by Liz

            //calculates angular distribution using histograms -- aka the "old way"
            int calcAngDist(TH2D* fisThetaBack_labVcm, TH2D* calibThetaBack_labVcm,
                    float* theta_cm, float* dsigdomega_cm,
                    float* dsigdomega_cm_err, float* dsigdtheta_cm,
                    float* dsigdtheta_cm_err, int numBins,int* numDataPoints,
                    xsecSpecs* xsecSpec, float *calibYield_lab,
                    float* saNorm_fis, float monErrTerm){



                int fisBins_lab = fisThetaBack_labVcm->GetNbinsX();
                int fisBins_cm = fisThetaBack_labVcm->GetNbinsY();
                int calibBins_lab =calibThetaBack_labVcm->GetNbinsX(); //we don't actually use this histogram at all except to check binning

                if (fisBins_lab != calibBins_lab){
                    cout << "Error: fission and calibration binning is not the same!\n";
                    return -1;
                }


                for (int i=1;i<=fisBins_cm;i++){ //begin cm bins

                    float fTh_cm=fisThetaBack_labVcm->GetYaxis()->GetBinCenter(i);

                    //dsigdOmega = dsigdO
                    // final values
                    float dsigDO_cm=0.0;
                    float dsigDO_cm_tot=0.0;
                    float dsigDO_cm_tot_err=0.0;
                    int fisYield_tot=0;

                    //dsigdtheta = dsigDth
                    float dsigDth_cm=0.0;
                    float dsigDth_cm_tot=0.0;
                    float dsigDth_cm_tot_err=0.0;

                    for (int j=1;j<=fisBins_lab;j++){ // begin lab bins

                        float fTh_lab=fisThetaBack_labVcm->GetXaxis()->GetBinCenter(j);
                        float cTh_lab=calibThetaBack_labVcm->GetXaxis()->GetBinCenter(j); 
                        float calYield=0;

                        if (cTh_lab != fTh_lab){
                            cout << "Error: fission and calibration binning is not the same!" << endl;
                            return -1;
                        }

                        int fisBinNum = fisThetaBack_labVcm->GetBin(j, i);
                        int fisYield = (int)fisThetaBack_labVcm->GetBinContent(fisBinNum);

                        int binIndex =  (int) round(fTh_lab / xsecSpec->binSize);
                        calYield = calibYield_lab[binIndex-1];

                        if (fisYield >0 && calYield>0){
                            // if there are counts in this bin in the fission run AND we have a valid solid
                            // angle normalization for this bin
                            fisYield_tot += fisYield;

                            // dsig/domega
                            dsigDO_cm = (float)fisYield * saNorm_fis[j-1] / (2.*pi*sin(fTh_cm*d2r));
                            dsigDO_cm_tot += dsigDO_cm;

                            // dsig/domega - eeeeeeeeeeeeeeeeeeeeeeeeeeeee //
                            dsigDO_cm_tot_err += dsigDO_cm*dsigDO_cm*( (1./(float)fisYield) + (1./(float)calYield)
                                    + monErrTerm); //square of dsigDO_cm because square root of term is taken
                            // once all CM bins are looped over


                            // dsig/dtheta
                            dsigDth_cm = (float)fisYield * saNorm_fis[j-1];
                            dsigDth_cm_tot += dsigDth_cm;

                            // dsig/dtheta - eeeeeeeeeeeeeeeeeeeeeeeeeeeee //
                            dsigDth_cm_tot_err += dsigDth_cm*dsigDth_cm*( (1./(float)fisYield) 
                                    + (1./(float)calYield)
                                    + monErrTerm);	

                        }			

                    }// end lab bins (j)


                    // eeeeeeeeeeeeeeeeeeeeeeeeeeeee //
                    dsigDO_cm_tot_err = sqrt(dsigDO_cm_tot_err);
                    dsigDth_cm_tot_err = sqrt(dsigDth_cm_tot_err);

                    //save results in arrays for later use
                    if (dsigDO_cm_tot > 0 ){

                        //save for plotting
                        dsigdomega_cm[*numDataPoints]=dsigDO_cm_tot;
                        dsigdomega_cm_err[*numDataPoints]=dsigDO_cm_tot_err;
                        theta_cm[*numDataPoints]=fTh_cm;

                        dsigdtheta_cm[*numDataPoints]=dsigDth_cm_tot;
                        dsigdtheta_cm_err[*numDataPoints]=dsigDth_cm_tot_err;
                    }

                    (*numDataPoints)++;


                } // end cm bins (i)



                cout << "Angular distribution has been calculated using the OLD method (calcAngDist function).\n";

                if (*numDataPoints != numBins){
                    cout << "Don't trust angular distribution calculated using OLD method (calcAngDist). Binning mismatch.\n";
                }

                return 0;

            }//end calcAngDist



            /////////////////////////// fifrangOutput /////////////////////////////////// updated by Liz 7/17

            // Function outputs ascii file with angular distribution data, for use with fifrang
            int fifrangOutput(xsecSpecs* xsecSpec, float *ADtheta_cm, float *ADdsigdomega_cm,
                    float *ADdsigdomega_cm_err, size_t numDataPoints, int fisRunNum){


                stringstream ss;
                ss << "fisAngDist_" << fisRunNum << ".dat";
                string outputFileName = xsecSpec->resultsPath + ss.str();

                ofstream fifrang;

                //open output file, now that we have a name
                fifrang.open(&outputFileName[0]);

                for (size_t i=0; i<numDataPoints;i++){

                    if (ADdsigdomega_cm[i] >0){
                        fifrang << setw(5) << ADtheta_cm[i] << setw(5) << "\t"
                            << setw(5) <<  ADdsigdomega_cm[i];
                        fifrang << setw(5) << "\t" << setw(5) <<  ADdsigdomega_cm_err[i]
                            << setw(5) << "\t" << setw(5) << "-42\n";
                    }

                }

                cout << "Finished writing fifrang-friendly data to " << outputFileName << "\n";

                //close output file
                fifrang.close();

                return 0;
            }//end fifrangOutput



            ////////////////////////////// xsecCMascii() ////////////////////////////  updated by Liz 7/17

            // produces a text file with the event-by-event angular distribution
            // modification of Yun's contribution to previous CubeXsec

            int xsecCMascii(xsecSpecs* xsecSpec,float* xsecThetaCM, float* xsecCM_o, float* xsecCM_o_err, 
                    int numBins, bool fifrangFriendly, int fisRunNum){

                ofstream xsecCMfile;
                stringstream ss;
                string xsecstring= xsecSpec->resultsPath + ss.str();



                if (xsecTest == 1){
                    cout << "!!!!!!!!!!! xsecCMascii test !!!!!!!!!!!!!! \n";
                }

                ss << "xsecAngDist_" << fisRunNum << ".dat";

                xsecstring = xsecSpec->resultsPath + ss.str();
                xsecCMfile.open(&xsecstring[0]);
                if (!xsecCMfile.is_open()){
                    cout << "Could not open " << xsecstring << ". xsecCM ascii file will not be produced.\n";
                }

                for (int i=0;i<numBins;i++){


                    if (xsecCM_o[i]>0){
                        //dsigdomega outputs

                        if (xsecTest == 1){
                            cout << "TESTING: xsecCMascii [ " << i << " ]: " << setw(5) << 
                                xsecThetaCM[i] << setw(5) <<"\t" << setw(5) << xsecCM_o[i] << "\t"
                                << setw(5) << "\t" << setw(5) << xsecCM_o_err[i] << endl;
                        }

                        if (fifrangFriendly==0) {
                            //normal format

                            xsecCMfile << setw(5) << xsecThetaCM[i] << setw(5) <<"\t"
                                << setw(5) << xsecCM_o[i] << "\t"
                                << setw(5) << "\t" << setw(5) << xsecCM_o_err[i] << endl;

                        }
                        if (fifrangFriendly==1){

                            //for fifrang input format
                            xsecCMfile << setw(5) << xsecThetaCM[i] << setw(5) <<"\t"
                                << setw(5) << xsecCM_o[i] << "\t"
                                << setw(5) << "\t" << setw(5) << xsecCM_o_err[i]
                                << setw(5) << "\t" << setw(5) << "-42" << endl; // for fifrang input format

                        }


                    }

                }

                if (xsecTest == 1){
                    cout << "!!!!!!!!!!! xsecCMascii end !!!!!!!!!!!!!! \n\n";

                }

                xsecCMfile.close();
                return 0;

            }// end  xsecCMascii()


            //function that finds the bin equivalent to that root would select //etw 19-7-17 new functions due to bug found in bin number id in calibTest
            int findRootBin(float value, float min, float max, int numBins){

                int bin=0;

                if (value < min){
                    bin = 0;//underflow bin
                }
                if (value >= max){
                    bin = numBins+1;//overflow bin
                }
                else{
                    bin = 1+int(numBins * (value - min)/(max - min));
                }

                return bin;

            }//end findRootBin


            //////////////////////// calibTest /////////////////////////////// created 7/17
            // Creates plots that allow a simple check of the solid angle normalization
            // This test is different than the previous test. To avoid influencing the
            // test with the time calibration parameters within dacube, I am using lab
            // frame data and calculations only.


            int calibTest(xsecSpecs* xsecSpec, xsecInfo* calibInfo1, xsecInfo* calibInfo3,
                    TH1D* calibRuthCalc, TH1D* calibThetaBack_lab, float* solidAngleNorm, 
                    int numBins, float monNormConst_cal){


                // Open a root file to store the comparison plots
                string checkName="calibCheck.root";
                string checkPathName=xsecSpec->resultsPath + checkName;

                TFile calCheck(&checkPathName[0],"update");

                float* saNorm_cal = new float[numBins];

                for (int i=0; i<numBins; i++){

                    saNorm_cal[i] = 0;
                }

                ofstream checkCalc;
                checkCalc.open("calibCheck.txt");


                float monNormConst_fis = monNormConst_cal;

                //this is a test so I have copied and pasted code. Obviously there is no fission run in this
                //test!
                //cout << "SA NORM CALIB TEST: " << monNormConst_fis << "\t" << calibInfo1->cubeEffCal << "\n"; 

                for (int i = 1; i <= numBins; i++){

                    saNorm_cal[i-1] = solidAngleNorm[i-1] / monNormConst_fis;
                    saNorm_cal[i-1] = saNorm_cal[i-1] / calibInfo1->cubeEffCal;//take into account deadtime
                }

                float* calibThLab_RuthTest = new float[numBins];
                float* calibdsigdth_RuthTest = new float[numBins];
                float* calibdsigdth_RuthTest_err = new float[numBins];

                float* calc_DsigDth = new float[numBins];

                for (int i=0;i<numBins;i++){
                    calibThLab_RuthTest[i]=0;
                    calibdsigdth_RuthTest[i]=0;
                    calibdsigdth_RuthTest_err[i]=0;
                    calc_DsigDth[i]=0;
                }


                //Use event-by-even method here as proper check of the method applied to fission data


                //Define variables I will use for tree access -- will be used for both detectors
                float cTheta_cm=0.;
                float cTheta_lab=0.;
                float cDsigdth_cm=0.;
                float cDsigdth_lab=0.;
                cDsigdth_cm=cDsigdth_cm;//turn off unused error
                cDsigdth_lab=cDsigdth_lab;


                //Access the first calibration file -- applies for both 2+3 detector configurations

                TFile calibFile1(&calibInfo1->dataFile[0],"update");
                if (!calibFile1.IsOpen()){
                    cout << "Error: CubeXsec:: xsecCalc: Calibration data file "
                        << &calibInfo1->dataFile[0] << " not found."<< endl;
                    return -1;
                }

                //Access the first tree
                TTree *calibTree1;
                if (calibFile1.GetListOfKeys()->Contains("GatedTree")==true){
                    calibTree1 = (TTree*) calibFile1.Get("GatedTree");
                }
                else if (calibFile1.GetListOfKeys()->Contains("CubeTreeNew")==true){
                    string input = "";
                    char answer={0};
                    calibTree1 = (TTree*) calibFile1.Get("CubeTreeNew");
                    cout << "Are you sure you want the ungated tree?? We usually ";
                    cout << "apply gates to calibration files. ";
                    while (true){
                        cout << "Please answer y or n \n";
                        getline(cin, input);
                        if (input.length() == 1){
                            answer = input[0];
                            if (answer == 'y' || answer == 'n'){
                                if (answer == 'n'){
                                    cout << "Exiting calculation. Please check your ";
                                    cout << "cross section input file and try again.\n";
                                    return -1; //abort the code
                                }
                                if (answer == 'y'){
                                    break; //break out of while loop
                                }
                            }
                            else{
                                cout << "Please enter a valid character.\n";
                            }
                        }
                    }

                }
                else{
                    cout << "Calibration file: Tree named GatedTree or CubeTreeNew ";
                    cout << "not found. Calculation aborted.\n";
                    return -1;
                }
                //Define histogram for testing
                TH1D thetaCheck("thetaCheck","thetaCheck",numBins,0,180);//etw 18-7-17
                TH1D dsdtCheck("dsdtCheck","dsdtCheck",numBins,0,180);//etw 18-7-17

                //Define some TBranches; these will allow us to access the data in calibTree1 event-by-event
                TBranch *theta_cm1 = calibTree1->GetBranch("thetaCM1");
                TBranch *theta_lab1 = calibTree1->GetBranch("theta1");

                Long64_t nentries = calibTree1->GetEntries();

                float* checkBins = new float[numBins];//etw 18-7-17 TESTING
                for (int i=0;i<numBins;i++){//etw 18-7-17 TESTING
                    checkBins[i]=0;//etw 18-7-17 TESTING
                }//etw 18-7-17 TESTING

                for (Long64_t i=0;i<nentries;i++){

                    cTheta_cm=0.;
                    cTheta_lab=0.;
                    cDsigdth_cm=0.;
                    cDsigdth_lab=0.;

                    theta_cm1->SetAddress(&cTheta_cm);
                    theta_lab1->SetAddress(&cTheta_lab);

                    theta_cm1->GetEvent(i);
                    theta_lab1->GetEvent(i);

                    //calculate lab bin -- this is root bin, so array bin is actually binNum_lab-1
                    int binNum_lab = findRootBin(cTheta_lab, 0, 180, numBins);



                    if (cTheta_cm>0){

                        calibdsigdth_RuthTest[binNum_lab-1] += saNorm_cal[binNum_lab-1];
                        checkBins[binNum_lab-1]++;//etw 18-7-17 TESTING
                        thetaCheck.Fill(cTheta_lab);//etw 18-7-17
                        dsdtCheck.Fill(cTheta_lab,saNorm_cal[binNum_lab-1]);//etw 18-7-17

                    }


                }//end calibration file processing for detector 1




                //begin calibration file processing for detector 3 (if applicable)
                if (N_MWPC==3){

                    cTheta_cm=0.;
                    cTheta_lab=0.;
                    cDsigdth_cm=0.;
                    cDsigdth_lab=0.;

                    //Access the second calibration file -- applies for 3 detector configuration

                    TFile calibFile3(&calibInfo3->dataFile[0],"update");
                    if (!calibFile3.IsOpen()){
                        cout << "Error: CubeXsec:: xsecCalc: Calibration data file "
                            << &calibInfo3->dataFile[0] << " not found."<< endl;
                        return -1;
                    }

                    //Access the second calibration tree
                    TTree *calibTree3;
                    if (calibFile3.GetListOfKeys()->Contains("GatedTree")==true){
                        calibTree3 = (TTree*) calibFile3.Get("GatedTree");
                    }
                    else if (calibFile3.GetListOfKeys()->Contains("CubeTreeNew")==true){
                        string input = "";
                        char answer={0};
                        calibTree3 = (TTree*) calibFile3.Get("CubeTreeNew");
                        cout << "Are you sure you want the ungated tree?? We usually ";
                        cout << "apply gates to calibration files. ";
                        while (true){
                            cout << "Please answer y or n \n";
                            getline(cin, input);
                            if (input.length() == 1){
                                answer = input[0];
                                if (answer == 'y' || answer == 'n'){
                                    if (answer == 'n'){
                                        cout << "Exiting calculation. Please check your ";
                                        cout << "cross section input file and try again.\n";
                                        return -1; //abort the code
                                    }
                                    if (answer == 'y'){
                                        break; //break out of while loop
                                    }
                                }
                                else{
                                    cout << "Please enter a valid character.\n";
                                }
                            }
                        }

                    }
                    else{
                        cout << "Calibration file: Tree named GatedTree or CubeTreeNew "
                            << "not found. Calculation aborted."<< endl;
                        return -1;
                    }

                    //Define some TBranches; these will allow us to access the data in calibTree1 event-by-event
                    TBranch *theta_cm3 = calibTree3->GetBranch("thetaCM3");
                    TBranch *theta_lab3 = calibTree3->GetBranch("theta3");

                    Long64_t nentries = calibTree3->GetEntries();


                    for (Long64_t i=0;i<nentries;i++){

                        cTheta_cm=0.;
                        cTheta_lab=0.;
                        cDsigdth_cm=0.;
                        cDsigdth_lab=0.;

                        theta_cm3->SetAddress(&cTheta_cm);
                        theta_lab3->SetAddress(&cTheta_lab);

                        theta_cm3->GetEvent(i);
                        theta_lab3->GetEvent(i);

                        int binNum_lab = findRootBin(cTheta_lab, 0, 180, numBins);

                        if (cTheta_cm>0){

                            calibdsigdth_RuthTest[binNum_lab-1] += saNorm_cal[binNum_lab-1];
                            checkBins[binNum_lab-1]++;//etw 18-7-17 TESTING
                            thetaCheck.Fill(cTheta_lab);//etw 18-7-17
                            dsdtCheck.Fill(cTheta_lab,saNorm_cal[binNum_lab-1]);//etw 18-7-17

                        }


                    }//end calibration file processing for detector 3



                }//end calibration file processing for detector 3


                // get bin number,
                int numCalibBins = calibThetaBack_lab->GetXaxis()->GetNbins();
                if (numCalibBins != numBins){
                    cout << "Error: binning mismatch in calibTest! Abort.\n";
                    return -1;
                }

                for (int i = 1; i<=numBins; i++){

                    //get value, and bin center from calibThetaBack_lab
                    float theta_lab = calibThetaBack_lab->GetBinCenter(i);
                    float lab_cts = calibThetaBack_lab->GetBinContent(i); // float because of 3-det normalization

                    //calibdsigdth_RuthTest[i-1] = dsdtCheck.GetBinContent(i);//etw 18-7-17
                    calibThLab_RuthTest[i-1] = theta_lab;

                    if (lab_cts > 0){
                        calibdsigdth_RuthTest_err[i-1] =  (sqrt(lab_cts)/lab_cts) * calibdsigdth_RuthTest[i-1]; 
                        // monitor counts cancel out so all that matters are the number of counts in each calibration bin
                    }

                    else{
                        calibdsigdth_RuthTest_err[i-1] = 0;
                    }


                }


                calCheck.cd();

                thetaCheck.Write("",TObject::kOverwrite);//etw 18-7-17
                dsdtCheck.Write("",TObject::kOverwrite);//etw 18-7-17

                //Make experiment and calibration graphs and write to calibCheck.root file
                TGraphErrors *expt = new TGraphErrors(numBins,calibThLab_RuthTest,calibdsigdth_RuthTest,0,calibdsigdth_RuthTest_err); //# points,x,y,dx,dy
                expt->SetMarkerColor(1);
                expt->SetMarkerStyle(7);
                expt->GetXaxis()->SetTitle("#theta_{lab} deg");
                expt->GetXaxis()->CenterTitle(true);
                expt->GetXaxis()->SetTitleOffset(1.5);
                expt->GetXaxis()->SetLimits(20,180);
                expt->GetYaxis()->SetTitle("#frac{d#sigma}{d#theta_{lab}}(#theta) mb/rad");//DYJ 090118
                expt->GetYaxis()->CenterTitle(true);
                expt->GetYaxis()->SetTitleOffset(1.5);
                expt->SetTitle("#frac{d#sigma}{d#theta} - lab frame (From Calibration Data)");
                expt->Draw("ap");
                expt->Write("dsigDth_expt",TObject::kOverwrite);


                for (int i=1;i<=numBins;i++){
                    calc_DsigDth[i-1] = calibRuthCalc->GetBinContent(i);//etw 20-7-17 changed to deal with different calib rxns
                }

                TGraphErrors *theory = new TGraphErrors(numBins,calibThLab_RuthTest,calc_DsigDth,0,0); //# points,x,y,dx,dy
                theory->SetLineColor(2);
                theory->SetMarkerColor(2);
                theory->SetMarkerStyle(7);
                theory->GetXaxis()->SetTitle("#theta_{lab} deg");
                theory->GetXaxis()->CenterTitle(true);
                theory->GetXaxis()->SetTitleOffset(1.5);
                theory->GetXaxis()->SetLimits(20,180);
                theory->GetYaxis()->SetTitle("#frac{d#sigma}{d#theta}(#theta) mb/rad");
                theory->GetYaxis()->CenterTitle(true);
                theory->GetYaxis()->SetTitleOffset(1.5);
                theory->SetTitle("#frac{d#sigma}{d#theta} - lab frame (Calculated, Rutherford)");
                theory->Draw("ap");
                theory->Write("dsigDth_calc",TObject::kOverwrite);

                checkCalc << "Theta_lab \t dsigdth_calc \t dsigdth_data \t dsigdth_data_err\n"; //etw 18-7-17 added bin count header

                for (int i = 0; i<numBins; i++){

                    checkCalc << calibThLab_RuthTest[i] << "\t" << std::fixed << std::setprecision(8) << calc_DsigDth[i] << "\t" << 
                        calibdsigdth_RuthTest[i] << "\t" << calibdsigdth_RuthTest_err[i] << "\n"; //etw 18-7-17 changed display precision in file for testing

                }


                //clean up dynamically allocated arrays
                delete[] calibThLab_RuthTest;
                delete[] calc_DsigDth;
                delete[] calibdsigdth_RuthTest;
                delete[] calibdsigdth_RuthTest_err;

                checkCalc.close();

                return 0;

            } // end calibTest







            // LEGACY CODE - no longer used


            ///////////////////////calibNormCalc ////////////////////////////////
            //method not used - original part of sa norm calc; uses calculated thetaCM
            // rather than "observed" CM. results with either method are pretty much the same

            float calibNormCalc_orig(reactPar* calibRxn, float binCenter, int binVal){

                //get bin info
                float normBin=0;

                float calThetaCM = thLab2ThCM_elastics(calibRxn->Ap, calibRxn->At,
                        binCenter);
                //calculate rutherford xsec for this point
                float RuthCube = dsigdOmega_Ruth_cm(calibRxn->Zp, calibRxn->Zt, calibRxn->Ecm,
                        calThetaCM);

                //getin histogram contents here
                if (binVal!=0){
                    normBin = (1./binVal) * RuthCube * dXCM2dXLab(binCenter,calThetaCM);
                    //last term: change back to lab frame
                }
                else{
                    normBin = 0.0;
                }
                return normBin;
            } //end calibNormCalc_orig

