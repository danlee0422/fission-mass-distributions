#ifndef GUARD_CubeXsec
#define GUARD_CubeXsec


//required std headers
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <list>
#include <ctime>

//root headers
#include "TCanvas.h"
#include "TGraphErrors.h"

//dacube headers
#include "CubeStruct.h"
#include "CubeInput.h"
#include "physConst.h"
#include "CubeSort.h"
#include "Scattering.h"


int xsecCalc(char*);

int calcCalibData(xsecInfo* calibInfo1, xsecInfo* calibInfo3,
        reactPar* calibRxn1, reactPar* calibRxn3,
        targetPar* calibTarg1, targetPar* calibTarg3,
        TH1D* calibThetaBack_lab,
        TH1D* calibThetaBack_cm,
        TH2D* calibThetaBack_labVcm,
        TH1D* calibRuthCalc,
        float* monNormConst_cal,
        int numBins);

int deadTimeCalc(xsecInfo*);
int preCalcs(xsecInfo* xsecInput, reactPar* rxn, targetPar* targ, bool calcReactPar);
int calcSAnorms(xsecSpecs* xsecSpec, 
        float *solidAngleNorm, float *calibYield_lab,
        int numBins, TH1D *calibThetaBack_lab,
        TH1D *calibRuthCalc,
        float monNormConst_cal);
int calcAngDist(TH2D* fisThetaBack_labVcm, TH2D* calibThetaBack_labVcm,
        float* theta_cm, float* dsigdomega_cm,
        float* dsigdomega_err, float* dsigdtheta_cm,
        float* dsigdtheta_cm_err, int numBins,int* numDataPoints,
        xsecSpecs* xsecSpec, float *calibYield_lab,
        float* solidAngleNorm, float monErrTerm);
int fifrangOutput(xsecSpecs* xsecSpec, float *thCM, float *xfis, 
        float *xfiserr, size_t numBins,int fisRunNum);
int xsecCMascii(xsecSpecs* xsecSpec,float* xsecThetaCM, float* xsecCM_o, float* xsecCM_o_err, 
        int numBins, bool fifrangFriendly, int fisRunNum);

int findRootBin(float value, float min, float max, int numBins);

int calibTest(xsecSpecs* xsecSpec, xsecInfo* calibInfo1, xsecInfo* calibInfo3,
        TH1D* calibRuthCalc, TH1D* calibThetaBack_lab, float* solidAngleNorm, 
        int numBins, float monNormConst_cal);



float calibNormCalc_orig(reactPar* calibRxn, float binCenter, int binVal);//computes normalization using calculated lab->cm angles

#endif

