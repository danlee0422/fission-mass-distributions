//
//  CubeXsec_hist.h stores all histogram definitions related to the fission runs
//  for CubeXsec.cxx. This is done because histogram definitions make the code
//  difficult to read. The code below is spliced into the locations indicated within the
//  code at the pre-compiler stage, so it's as if the lines below are simply a continuation
//  of the code itself.

//  Because of how the histograms are incremented in this code, we can't make a standalone
//  function or code equivalent to CubeHist.cxx.

//  Liz Williams, June 2017
//  updated by Yun for src_v4.3, 04/04/2019
/* As with UserCode.h, if you modify this, make sure to

make clean
make

And watch for errors in your code... */

// * There are two sets of histograms below:
//
//      (1) publication-use histograms and
//      (2) group-use histograms.
//
//   Publication-use histograms take into account the bin width(s) and are to be used for any histograms
//   displayed in a publication. Group-use publications do not take into account the bin widths and can
//   be used for taking projections or integrations without accounting for bin width. See documentation
//   on the wiki about the distinctions made here.

// * Any histograms that do not have a _g equivalent are usable for all purposes. (This should only be
//   true for histograms where bins are incremented by counts.)

// * Any group-use angular distribution histograms (theta versus dsigma/dtheta or dsigma/domega CAN be used
//   in publications if they are displayed as FUNCTIONS rather than histograms. Bin value plotted as a
//   function of bin center yields the angular distribution function for the data in question.

// Users can add histograms. Users can also change binning and range parameters for certain histograms
// within the XSEC_HIST_CREATE #ifdef statement.

#ifdef XSEC_HIST_CREATE

// DEFINE + LABEL HISTOGRAMS AND ANY RELATED VARIABLES HERE. You can use numBins (defined in CubeXsec.cxx)
// to define your histogram binning. If variables exist for defining ranges and bin numbers, please
// change them here and not in the histogram definitions below.

// DO NOT CHANGE ANY ANGLE BINS OR RANGES HERE. If you want different angle bins, change this in the
// cubeXsec input file.

// Finally, for any histograms defined here that involve dsigma/dtheta or dsigma/domega, you cannot
// change the binning of the histograms after the histograms are made!
float slope = 2*(90-fisRxn.intercept)*1.0;

cout<< ""<<endl;
cout << "-------------------SA MAD Mirror Fuction--------------------------------" << endl;
cout << " Intercept = " << fisRxn.intercept << " deg.\t\t" << "slope = " << slope  << endl;
cout << "------------------------------------------------------------------------" << endl;
// **************************************************************************** //
// **************** USER-DEFINED BINNING AND RANGE PARAMETERS ***************** //

float MRmin = 0.;
float MRmax = 1.;
int MRbins = 100;

float TKEVmin = 0.;
float TKEVmax = 5.;
int TKEVbins = 500; // number of bins in TKE/TKEViola plots

float TKEmin = 0.;
float TKEmax = 400.;
int TKEbins = 400; //do not change if you want a TKE bin width of 1 MeV

// *** end user-defined binning and range parameters
// ************************************************************************** //


// lab and cm angle bins are the same - do not change these values as they are set
// within the user input file
float angleMin = 0.;
float angleMax = 180.;
int angleBins = numBins;

float angleBinWidth = (angleMax - angleMin)/(float)angleBins; //same for lab and cm
float MRbinwidth = (MRmax-MRmin)/(float)MRbins;
float TKEVbinwidth = (TKEVmax - TKEVmin)/(float)TKEVbins;
float TKEbinwidth = (TKEmax - TKEmin)/(float)TKEbins;

// *** HISTOGRAMS FOR PUBLICATION ARE DEFINED HERE. ****************************** //
/* These histograms take into account the bin widths in one or both histogram 
   dimensions if they use the solid angle normalization to obtain a cross section. 
   These should be used for publication. If you take projections 
   of these histograms (e.g. to obtain dsigma/dtheta from d^2sigma/(dtheta dMR),
   you will need to multiply your projection by the bin width (in MR and theta, in this 
   example) to get the right answer.

   If bins are incremented with counts, then there's no need to worry about bin width.
 */

//int binNumExpt = 36; // bin size = 5
int binNumExpt = 60; // bin size = 3
TH2D Exp_MAD("Exp_MAD","Exp_MAD",MRbins,MRmin,MRmax,binNumExpt,angleMin, angleMax);
string expt_MAD_title = "MR vs thetaCM (mirrored) [Counts]; M_{R} [bin width " +
to_string(MRbinwidth) + "]; #theta_{CM} [deg] [bin width " +
to_string(angleBinWidth) + " deg]"; 
Exp_MAD.SetTitle(&expt_MAD_title[0]);
Exp_MAD.SetOption("COLZ");

//1D hist
TH1D MR("MR","MR",MRbins,MRmin,MRmax);
string MR_title = "M_{R} [Counts]; M_{R} [bin width " +
to_string(MRbinwidth) + "]; Counts"; 
MR.SetTitle(&MR_title[0]);

TH1D MRxsec_T("MRxsec_T","MRxsec_T",MRbins,MRmin,MRmax);
string MRxsec_T_title = "M_{R} [dsigdtheta_cm]; M_{R} [bin width " + 
to_string(MRbinwidth) + " deg];#frac{d#sigma}{d#theta}(#theta_{cm}) [mb/rad]";
MRxsec_T.SetTitle(&MRxsec_T_title[0]);

//2D hist

TH2D MrTKExsec("MrTKExsec","MrTKExsec",MRbins, MRmin, MRmax, TKEbins,TKEmin,TKEmax);
string MrTKExsec_title = "M_{R} vs TKE [d^{2}#sigma/(dTKE dM_{R})];M_{R} [bin width " + 
to_string(MRbinwidth) + "];TKE [MeV] [bin width " + to_string(TKEbinwidth) + "]";
MrTKExsec.SetTitle(&MrTKExsec_title[0]);
MrTKExsec.SetOption("COLZ");

TH2D MrTKEVxsec("MrTKEVxsec","MrTKEVxsec",MRbins, MRmin, MRmax, TKEVbins,TKEVmin,TKEVmax);
string MrTKEVxsec_title = "M_{R} vs TKE / TKEViola [d^{2}#sigma/(d(TKE/TKEViola) dM_{R})];M_{R} [bin width " + 
to_string(MRbinwidth) + "];TKE/TKEViola [MeV] [bin width " + to_string(TKEVbinwidth) + "]";
MrTKEVxsec.SetTitle(&MrTKEVxsec_title[0]);
MrTKEVxsec.SetOption("COLZ");

TH2D MrThCMxsec_T("MrThCMxsec_T","MrThCMxsec_T",MRbins,MRmin,MRmax,numBins,angleMin, angleMax); 
string MrThCMxsec_T_title = "M_{R} vs ThCM [d^{2}#sigma/(d#theta dM_{R})];M_{R} [bin width " + 
to_string(MRbinwidth) + "];theta_{CM} [deg] [bin width " + to_string(angleBinWidth) + "]";
MrThCMxsec_T.SetTitle(&MrThCMxsec_T_title[0]);
MrThCMxsec_T.SetOption("COLZ");

TH2D MrThCMxsec_T_mir("MrThCMxsec_T_mir","MrThCMxsec_T_mir",MRbins,MRmin,MRmax,numBins,angleMin, angleMax);
string MrThCMxsec_T_mir_title = "M_{R} vs Theta_{CM} [d^{2}#sigma/(d#theta dM_{R}) - mirrored];M_{R} [bin width " + 
to_string(MRbinwidth) + "];theta_{CM} [deg] [bin width " + to_string(angleBinWidth) + "]";
MrThCMxsec_T_mir.SetTitle(&MrThCMxsec_T_mir_title[0]);
MrThCMxsec_T_mir.SetOption("COLZ");

TH2D MrThCMxsec_O("MrThCMxsec_O","MrThCMxsec_O",MRbins,MRmin, MRmax,numBins,angleMin, angleMax); 
string MrThCMxsec_O_title = "M_{R} vs Theta_{CM} [d^{2}#sigma/(d#Omega dM_{R})];M_{R} [bin width " + 
to_string(MRbinwidth) + "];theta_{CM} [deg] [bin width " + to_string(angleBinWidth) + "]";
MrThCMxsec_O.SetTitle(&MrThCMxsec_O_title[0]);
MrThCMxsec_O.SetOption("COLZ");

TH2D MrThCMxsec_O_mir("MrThCMxsec_O_mir","MrThCMxsec_O_mir",MRbins,MRmin, MRmax,numBins,angleMin, angleMax);
string MrThCMxsec_O_mir_title = "M_{R} vs Theta_{CM} [d^{2}#sigma/(d#Omega dM_{R}) - mirrored];M_{R} [bin width " + 
to_string(MRbinwidth) + "];theta_{CM} [deg] [bin width " + to_string(angleBinWidth) + "]";
MrThCMxsec_O_mir.SetTitle(&MrThCMxsec_O_mir_title[0]);
MrThCMxsec_O_mir.SetOption("COLZ");

// *** end publication histograms *** //


// ********* HISTOGRAMS FOR GROUP USE GO HERE ******************************** //
/* "Group use" means use within this group; all these histograms have a _g appended to their name. 
   The bin widths are not taken into account in these histogram definitions, so a projection over, 
   e.g., all MR, of a histogram d^sigma/(dtheta dMR) will be equal to dsigma/dtheta. There's no need 
   for the additional multiplicative factor.

   This is weird, I realize, but it has to do with how we're distributing solid angle normalization
   factors within the histogram.

   For convenience all group use histograms have a _g tag in the histogram name.*/

TH1D MRxsec_T_g("MRxsec_T_g","MRxsec_T_g",MRbins,MRmin,MRmax);
MRxsec_T_g.SetTitle("MR v dsigmadtheta_cm;theta1CM[deg];#frac{d#sigma}{d#theta}(#theta_{cm}) [mb/rad]");

//2D hist

TH2D MrTKExsec_g("MrTKExsec_g","MrTKExsec_g",MRbins, MRmin, MRmax, TKEbins,TKEmin,TKEmax);
MrTKExsec_g.SetTitle("Mr vs TKE [SAnorm - d^{2}#sigma/(dTKE dmr)];MR;TKE [MeV]");
MrTKExsec_g.SetOption("COLZ");

TH2D MrTKEVxsec_g("MrTKEVxsec_g","MrTKEVxsec_g",MRbins, MRmin, MRmax, TKEVbins,TKEVmin,TKEVmax);
MrTKEVxsec_g.SetTitle("Mr vs TKE / TKEViola [SAnorm - d^{2}#sigma/(d(TKE/TKEViola) dMr];MR;TKE /TKEViola");
MrTKEVxsec_g.SetOption("COLZ");

TH2D MrThCMxsec_T_g("MrThCMxsec_T_g","MrThCMxsec_T_g",MRbins,MRmin,MRmax,numBins,angleMin, angleMax); //no mirroring
MrThCMxsec_T_g.SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#theta dmr)] ;MR;ThCM [deg]");
MrThCMxsec_T_g.SetOption("COLZ");

TH2D MrThCMxsec_T_mir_g("MrThCMxsec_T_mir_g","MrThCMxsec_T_mir_g",MRbins,MRmin,MRmax,numBins,angleMin, angleMax);
MrThCMxsec_T_mir_g.SetTitle("MR vs ThCM_mir [ d^{2}#sigma/(d#theta dmr) ];MR; theta1CM[deg]");
MrThCMxsec_T_mir_g.SetOption("COLZ");

TH2D MrThCMxsec_O_g("MrThCMxsec_O_g","MrThCMxsec_O_g",MRbins,MRmin, MRmax,numBins,angleMin, angleMax); //no mirroring
MrThCMxsec_O_g.SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#Omega dmr)] ;MR;ThCM [deg]");
MrThCMxsec_O_g.SetOption("COLZ");

TH2D MrThCMxsec_O_mir_g("MrThCMxsec_O_mir_g","MrThCMxsec_O_mir_g",MRbins,MRmin, MRmax,numBins,angleMin, angleMax);
MrThCMxsec_O_mir_g.SetTitle("MR vs ThCM_mir [ d^{2}#sigma/(d#Omega dmr) ];MR; theta1CM[deg]");
MrThCMxsec_O_mir_g.SetOption("COLZ");

// *** end group use histograms here ***//

#endif




#ifdef XSEC_HIST_FILL

// FILL HISTOGRAMS AND DEFINE ANY VARIABLES RELATED TO FILLING HERE.
// All histograms you fill must be defined above.

// You can use the following variables (as defined in CubeXsec.cxx):

// ANGLES: fisTheta_cm, fisTheta_lab
// SOLID ANGLE NORMALIZATION FACTORS (see doco re: XSEC5): fisDsigdth, fisDsigdomega_<ref frame>
// MASS RATIO: fisMR
// TKE: fisTKE, fisTKEViola

// Do not use fisDsigdomega_<ref frame> in a binned histogram involving an angle NOT corresponding
// to <ref frame>!!!!

// Additional notes:

// * For any publication histograms involving fisDsigdth or fisDsigdomega_<ref frame>, you need to take binning into account by dividing fisDsigdth or fisDsigdomega_<ref frame> by the bin width(s)

// * For MIRRORED histograms, the cross section you get is the particle cross section, not the
//   fission cross section. If you want the fission cross section, you need to divide your
//   solid angle normalization factor by 2.0 (The decimal is important; we don't want to accidentally divide
//   by an integer.)

// * As introduced by Yun, _O_ histograms contain dsigma/domega; _T_ histograms contain dsigma/dtheta

// * As introduced by Liz, _g histograms are for group use only.


// Mirror functions and mirrored histograms

//EXP. MAD
//if (fisTheta_cm > 90. + 74.*(fisMR-0.5)){ // linear
if (fisTheta_cm > 90. + slope*(fisMR-0.5)){
    //if (fisThetaCM >43.125 + 197.917*fisMR - 312.5*fisMR*fisMR + 208.33*fisMR*fisMR*fisMR){ // curve (mirroing condition from Renju's code)

    // IF YOU CHANGE MIRRORING HERE, ALSO CHANGE MIRRORING BELOW XSEC5 EQUATION IN CubeXsec!!!!!!!!!!!!!!!!

    // **** PUBLICATION-USE MIRRORED HISTOGRAMS DEFINED HERE **** //

    // 2D hist

    Exp_MAD.Fill(fisMR,fisTheta_cm);
    Exp_MAD.Fill(1.-fisMR,180.-fisTheta_cm);


/* We do not need to divid by anglebinwidth in a normalised MAD since during the SA normalisation calculation the angle bin width is already taken into account. 
- 31.03.2020
- Yun Jeung */
/*
    MrThCMxsec_T_mir.Fill(fisMR,fisTheta_cm,fisDsigdth/(MRbinwidth*angleBinWidth));
    MrThCMxsec_T_mir.Fill(1.-fisMR,180.-fisTheta_cm,fisDsigdth/(MRbinwidth*angleBinWidth));

    MrThCMxsec_O_mir.Fill(fisMR,fisTheta_cm,fisDsigdomega_cm/(MRbinwidth*angleBinWidth));
    MrThCMxsec_O_mir.Fill(1.-fisMR,180.-fisTheta_cm,fisDsigdomega_cm/(MRbinwidth*angleBinWidth));
*/

    MrThCMxsec_T_mir.Fill(fisMR,fisTheta_cm,fisDsigdth/(MRbinwidth));
    MrThCMxsec_T_mir.Fill(1.-fisMR,180.-fisTheta_cm,fisDsigdth/(MRbinwidth));

    MrThCMxsec_O_mir.Fill(fisMR,fisTheta_cm,fisDsigdomega_cm/(MRbinwidth));
    MrThCMxsec_O_mir.Fill(1.-fisMR,180.-fisTheta_cm,fisDsigdomega_cm/(MRbinwidth));

    // *** end PUBLICATION-USE MIRRORED HISTOGRAMS HERE


    // **** GROUP-USE MIRRORED HISTOGRAMS DEFINED HERE **** //

    // 2D hist

    MrThCMxsec_T_mir_g.Fill(fisMR,fisTheta_cm,fisDsigdth);
    MrThCMxsec_T_mir_g.Fill(1.-fisMR,180.-fisTheta_cm,fisDsigdth);

    MrThCMxsec_O_mir_g.Fill(fisMR,fisTheta_cm,fisDsigdomega_cm);
    MrThCMxsec_O_mir_g.Fill(1.-fisMR,180.-fisTheta_cm,fisDsigdomega_cm);



    // *** end GROUP-USE MIRRORED HISTOGRAM FILL

}


//non-mirrored histograms

//1D hist
MR.Fill(fisMR);

// *** PUBLICATION-USE NON-MIRRORED HISTOGRAMS INCREMENTED HERE ***//

MRxsec_T.Fill(fisMR,fisDsigdth/MRbinwidth);

//2D hist

MrTKExsec.Fill(fisMR,fisTKE,fisDsigdth/(MRbinwidth*TKEbinwidth));
MrTKEVxsec.Fill(fisMR,fisTKE/fisTKEViola,fisDsigdth/(MRbinwidth*TKEVbinwidth));

MrThCMxsec_T.Fill(fisMR,fisTheta_cm,fisDsigdth/(MRbinwidth*angleBinWidth));
MrThCMxsec_O.Fill(fisMR,fisTheta_cm,fisDsigdomega_cm/(MRbinwidth*angleBinWidth));

// *** end PUBLICATION-USE NON-MIRRORED HISTOGRAM FILL

// *** GROUP-USE NON-MIRRORED HISTOGRAMS INCREMENTED HERE ***//

MRxsec_T_g.Fill(fisMR,fisDsigdth);

//2D hist

MrTKExsec_g.Fill(fisMR,fisTKE,fisDsigdth);
MrTKEVxsec_g.Fill(fisMR,fisTKE/fisTKEViola);

MrThCMxsec_T_g.Fill(fisMR,fisTheta_cm,fisDsigdth);
MrThCMxsec_O_g.Fill(fisMR,fisTheta_cm,fisDsigdomega_cm);

// *** end GROUP-USE NON-MIRRORED HISTOGRAM FILL



#endif




#ifdef XSEC_HIST_PUB_WRITE
// WRITE COMMANDS FOR ANY PUB HISTOGRAMS TO THE FISSION FILE GO HERE

MrThCMxsec_T.Write("",TObject::kOverwrite);
MrThCMxsec_T_mir.Write("",TObject::kOverwrite);

Exp_MAD.Write("",TObject::kOverwrite);

MrThCMxsec_O.Write("",TObject::kOverwrite);
MrThCMxsec_O_mir.Write("",TObject::kOverwrite);
MR.Write("",TObject::kOverwrite);

//MRxsec_T.Write("",TObject::kOverwrite);
MrTKExsec.Write("",TObject::kOverwrite);
MrTKEVxsec.Write("",TObject::kOverwrite);


#endif

#ifdef XSEC_HIST_GR_WRITE
// WRITE COMMANDS FOR ANY GROUP HISTOGRAMS TO THE FISSION FILE GO HERE

MrThCMxsec_T_g.Write("",TObject::kOverwrite);
MrThCMxsec_T_mir_g.Write("",TObject::kOverwrite);

MrThCMxsec_O_g.Write("",TObject::kOverwrite);
MrThCMxsec_O_mir_g.Write("",TObject::kOverwrite);

MrTKExsec_g.Write("",TObject::kOverwrite);
MrTKEVxsec_g.Write("",TObject::kOverwrite);
#endif



#ifdef XSEC_HIST_DERIVED
// Create Histograms derived from the above histograms here.

TH2D* SAnorm_MAD_g =(TH2D*)MrThCMxsec_O_mir_g.Clone("SA_MAD");
SAnorm_MAD_g->SetTitle("Mr vs ThCM [ SAnorm - d^{2}#sigma/(d#theta dmr)] ;MR;ThCM [deg]");
SAnorm_MAD_g->SetOption("COLZ");

TH2D* exp =(TH2D*)Exp_MAD.Clone("exp");

TH1D* exp_py=exp->ProjectionY("exp_py",0,-1,"");
exp_py->SetTitle("ThCM [counts] ;ThCM;counts");

TH2D* norm_g =(TH2D*)MrThCMxsec_O_mir_g.Clone("norm_g");
norm_g->SetTitle("Mr vs ThCM [ SA_MAD/EXP_MAD] ;MR;ThCM [deg]");
norm_g->SetOption("COLZ");
norm_g->Divide(exp);

TH1D *sa_py_g=SAnorm_MAD_g->ProjectionY("sa_py_g",0,-1,"");
sa_py_g->SetTitle("SA_NormConst ;ThCM;norm coeff");

TH2D* temp2 =(TH2D*)MrThCMxsec_T_mir_g.Clone("temp2");

TH1D *MrThCMxsec_mir_py_g = temp2->ProjectionY("MrThCMxsec_mir_py_g",0,-1,"[light]");
MrThCMxsec_mir_py_g->SetTitle("MrThCMxsec_mir_py_g;#theta_{cm};#frac{d#sigma}{d#theta} [mb/rad]");
MrThCMxsec_mir_py_g->SetLineWidth(2.0);

exp_py->Write("",TObject::kOverwrite);
SAnorm_MAD_g->Write("",TObject::kOverwrite);
norm_g->Write("",TObject::kOverwrite);
sa_py_g->Write("",TObject::kOverwrite);
SAnorm_MAD_g->Write("",TObject::kOverwrite);
norm_g->Write("",TObject::kOverwrite);
sa_py_g->Write("",TObject::kOverwrite);

#endif

