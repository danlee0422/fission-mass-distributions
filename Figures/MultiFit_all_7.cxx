// Add ascii file output
// Modified by Yun 15/12/2021

//Script to fit MR vs TKE/TKE-Viola (RTKE) data with 1 to 7 Gaussians
//one symmetric and multiple asymmetric (mirrored)
//Annette 21/07/2021
//
//root commands:
//	.L ~/Documents/Scripts/MultiFit_all.cxx
//      multifit("parameter_file_name")
//
//	e.g. multifit("MR_FitPars.dat")
//
//requires parameter file
//	e.g. MR_FitPars.dat
//
//output:
//	terminal screen: fit values
//      root canvas
//	graphic files: MR_2Dfit.png and MR_2Dfit.pdf   or
//                     MR_1Dfit.png and MR_1Dfit.pdf
//
//checks:
//	1. Fit status should be "CONVERGED"
//		if not, reassess starting values and parameter limits
//	2. If any parameter is "** at limit **", reassess parameters and limits
//
//things to change:
//	1D and 2D residue data rebinning, change "res_bins" (twice)
//		(currently set to 4, so 4x4 rebinning)
//	additional 2D residue A and Z axes: set "A" and "Z" in 2D pad4
//
//
//note: has tendency to not work on the second and subsequent use
//      may need to restart root each time
//
//Volume/Area vs Height
//	initially set at volume/area
//	to change to height:
//	- change return lines in functions Gaus2 and Gaus1
//	note: volume/area fraction calculations only work where volume/area are used
//

 #include "TH1.h"
 #include "TH2.h"
 #include "TF1.h"
 #include "TF2.h"

bool even;

 //        ******** Gaussian functions *******
 Double_t Gaus2(Double_t *x, Double_t *par, int mirror) {
   auto pi = TMath::Pi();
   Double_t r1;
   if (mirror==0){
      r1 = Double_t((x[0]-par[1])/par[2]);
   }else{
      r1 = Double_t((x[0]-(1-par[1]))/par[2]);
   }
   Double_t r2 = Double_t((x[1]-par[3])/par[4]);
   //return par[0]*TMath::Exp(-0.5*(r1*r1+r2*r2));	//par[0] is height
   return (par[0]/(2*pi*par[2]*par[4]))*TMath::Exp(-0.5*(r1*r1+r2*r2));  //par[0] is volume
 }

 Double_t multiGaus2(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t *p2 = &par[5];
   Double_t *p3 = &par[10];
   Double_t *p4 = &par[15];
   Double_t result;

   Double_t p1_s [5] = {p1[0],p1[1],p1[2],p1[3],p1[4]}; 
   Double_t p2_s [5] = {p2[0],p2[1],p2[2],p2[3],p2[4]}; 
   Double_t p3_s [5] = {p3[0],p3[1],p2[2],p3[3],p3[4]}; 
   Double_t p4_s [5] = {p4[0],p4[1],p2[2],p4[3],p4[4]}; 
   //cout << p1[6] << endl;
   if (even) {  //even number of Gaussians, no symmetric peak
     result=Gaus2(x,p1_s,0)+Gaus2(x,p1_s,1)+ Gaus2(x,p2_s,0)+Gaus2(x,p2_s,1)+ Gaus2(x,p3_s,0)+Gaus2(x,p3_s,1);
   } else {     //odd number of Gaussians, with symmetric peak
     result=Gaus2(x,p1_s,0)+ Gaus2(x,p2_s,0)+Gaus2(x,p2_s,1)+ Gaus2(x,p3_s,0)+Gaus2(x,p3_s,1)+ Gaus2(x,p4_s,0)+Gaus2(x,p4_s,1);
   }
   return result;
 }

 Double_t singleGaus2(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t result=Gaus2(x,p1,0);
   return result;
 }

 Double_t doubleGaus2(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t result=Gaus2(x,p1,0)+Gaus2(x,p1,1);
   return result;
 }

 Double_t Gaus1(Double_t *x, Double_t *par, int mirror) {
   auto pi = TMath::Pi();
   Double_t r1;
   if (mirror==0){
      r1 = Double_t((x[0]-par[1])/par[2]);
   }else{
      r1 = Double_t((x[0]-(1-par[1]))/par[2]);
   }
   //return par[0]*TMath::Exp(-0.5*(r1*r1));	//par[0] is height
   return (par[0]/(sqrt(2*pi)*par[2]))*TMath::Exp(-0.5*(r1*r1));  //par[0] is area
 }

 Double_t multiGaus1(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t *p2 = &par[3];
   Double_t *p3 = &par[6];
   Double_t *p4 = &par[9];
   Double_t result;

   Double_t p1_s [3] = {p1[0],p1[1],p1[2]}; 
   Double_t p2_s [3] = {p2[0],p2[1],p2[2]}; 
   Double_t p3_s [3] = {p3[0],p3[1],p2[2]}; 
   Double_t p4_s [3] = {p4[0],p4[1],p2[2]}; 

   if (even) {  //even number of Gaussians, no symmetric peak
     result=Gaus1(x,p1,0)+Gaus1(x,p1,1)+ Gaus1(x,p2,0)+Gaus1(x,p2,1)+ Gaus1(x,p3,0)+Gaus1(x,p3,1);
   } else {     //odd number of Gaussians, with symmetric peak
     result=Gaus1(x,p1,0)+ Gaus1(x,p2,0)+Gaus1(x,p2,1)+ Gaus1(x,p3,0)+Gaus1(x,p3,1)+ Gaus1(x,p4,0)+Gaus1(x,p4,1);
   }
   return result;
 }

 Double_t singleGaus1(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t result=Gaus1(x,p1,0);
   return result;
 }

 Double_t doubleGaus1(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t result=Gaus1(x,p1,0)+Gaus1(x,p1,1);
   return result;
 }

 //        ******** plot colours *******
 void newColour(){
  const Int_t NRGBs = 7;
  const Int_t NCont = 20;
  Double_t Stops[NRGBs] = { 0.00, 0.45,0.49,0.50, 0.51, 0.55,1.00};
  Double_t Red[NRGBs] =   { 0.00, 0.41, 1.0, 1.00, 1.0, 1.0, 0.80};
  Double_t Green[NRGBs] = { 0.00, 0.89, 1.0, 1.00, 1.0, 0.89, 0.00};
  Double_t Blue[NRGBs] =  { 0.80, 1.0,  1.0, 1.00, 1.0, 0.41, 0.00};
  TColor::CreateGradientColorTable(NRGBs, Stops,Red, Green, Blue, NCont);
  gStyle->SetNumberContours(NCont);
 }

 void madColour(){
  const Int_t NRGBs = 12;
  const Int_t NCont = 20;
  Double_t stops[NRGBs] = { 0.00, 0.08, 0.17, 0.25, 0.33, 0.42, 0.50, 0.58, 0.67, 0.75, 0.83, 1.00};
  Double_t red[NRGBs]   = {0,0,0,0,0.25,0.55,1,1,1,1,0.86,0.47};
  Double_t green[NRGBs] = {0,0,0.4,0.7,0.9,1,1,0.75,0.5,0.35,0.19,0.0};
  Double_t blue[NRGBs]  = {0.5,0.7,0.55,0.3,0.1,0,0,0,0,0,0,0.18};
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
  gStyle->SetLineWidth(1);
 }

 //        ******** main program *******
 void multifit(const char *parfile) {

    TVirtualFitter::SetDefaultFitter("Minuit");
    gStyle->SetOptStat(0);
    auto pi = TMath::Pi();
    int  Num_pars, Num_pars1, dimensions;
    string runNo;
    string comment, comment1;
    TString rootfile, reaction, Ebeam;
    Double_t MR_left,MR_right,TKE_bottom,TKE_top;
    Double_t MR_left_plot,MR_right_plot,TKE_bottom_plot,TKE_top_plot;
    Double_t range_residual,range_residual1;
    Double_t p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19;
    Double_t p0e,p1e,p5e,p6e,p10e,p11e,p15e,p16e;
    Double_t v1,v2,v3,v4,v1e,v2e,v3e,v4e,v101,v102,v103,v104,v101e,v102e,v103e,v104e;
    int Num_Gaus;
    Double_t v_total;
    ifstream input;

    //        ******** parameter input file *******
    // open parameter input file
    input.open(parfile);
    if (!input.is_open()){
        cout << "Parameter file " << parfile << " not found!" << endl;
        return -1;
    }

    // get root file name
    input >> rootfile;
    input.ignore(1000, '\n');

    input >> runNo;
    input.ignore(1000, '\n');

    // get reaction
    input >> reaction;
    input.ignore(1000, '\n');

    // get beam energy
    input >> Ebeam;
    input.ignore(1000, '\n');

    // get number of Gaussians to fit
    input>>Num_Gaus;
    input.ignore(1000, '\n');

    // get number of dimensions to fit (1=MR (1D), 2=MR vs RKTE (2D))
    input>>dimensions;
    input.ignore(1000, '\n');

    // get fit range
    input >> comment;
    input >> MR_left>>MR_right;
    input.ignore(1000, '\n');
    input >> TKE_bottom>>TKE_top;
    input.ignore(1000, '\n');

    // get plot range
    input >> comment;
    input >> MR_left_plot>>MR_right_plot;
    input.ignore(1000, '\n');
    input >> TKE_bottom_plot>>TKE_top_plot;
    input.ignore(1000, '\n');

    if (Num_Gaus %2 ==0) {
       even=true;
    } else {
       even=false;
    }

    // get fit parameters and constraints
    if (even) {
         Num_pars=5*(Num_Gaus/2);
         Num_pars1=3*(Num_Gaus/2);
         input >> comment;
         Double_t temp1, temp2, temp3;
         string temp4;
         for (int j=0; j<5; j++){
             input>>temp1>>temp2>>temp3>>temp4;
             input.ignore(1000, '\n');
         } //end j
    } else {
         Num_pars=5*(Num_Gaus+1)/2;
         Num_pars1=3*(Num_Gaus+1)/2;
    }
    Double_t ipar[Num_pars], imin[Num_pars], imax[Num_pars];
    string itype[Num_pars];
    for (int i=0; i<Num_pars/5; i++){
         input >> comment;
         for (int j=0; j<5; j++){
             //cout<<"i*5+j="<<i*5+j<<endl;
             input>>ipar[i*5+j]>>imin[i*5+j]>> imax[i*5+j]>>itype[i*5+j];
             input.ignore(1000, '\n');
         } //end j
    } //end i

    // close parameter input file
    input.close();
    // for generating output ascii file - DYJ 15/12/2021
    TString txtfile = "outputs_7/output_" +  runNo + ".dat";
    ofstream fileout(txtfile);


    //        ******** data file *******
    //  open root file and extract MR vs TKE/TKE_Viola
    TFile* input1 = new TFile(rootfile);
    if (!input1->IsOpen() ){
        cout << "Root file " << rootfile << " not found!" << endl;
        return -1;
    }

    //  get data
    TH2D *mr2=(TH2D *)input1->Get("Hist2D/mrTKEViola");
    TH1D *mr1 =mr2->ProjectionX();

    //  set up fit and residuals histograms
    double NbinsX = mr2->GetNbinsX();
    double NbinsY = mr2->GetNbinsY();
    double NbinsX1 = mr1->GetNbinsX();

    // get total number of data events
    int data_events=mr2->Integral();

    //        ******** set functions *******
    // set 2D function
    TF2* Gaus2D=new TF2("Gaus2D",multiGaus2,MR_left,MR_right,TKE_bottom,TKE_top,Num_pars);
    TF2* G1=new TF2("G1",singleGaus2,MR_left_plot,MR_right_plot,TKE_bottom_plot,TKE_top_plot,5);
    TF2* G2=new TF2("G2",doubleGaus2,MR_left_plot,MR_right_plot,TKE_bottom_plot,TKE_top_plot,5);
    TF2* G3=new TF2("G3",doubleGaus2,MR_left_plot,MR_right_plot,TKE_bottom_plot,TKE_top_plot,5);
    TF2* G4=new TF2("G4",doubleGaus2,MR_left_plot,MR_right_plot,TKE_bottom_plot,TKE_top_plot,5);
    // set residual functions
    TH2D *residual= new TH2D("residual","residual",NbinsX,0,1,NbinsY,0,5);
    TH1D *residual1= new TH1D("residual1","residual1",NbinsX1,0,1);
    // set 1D function
    TF1* Gaus1D=new TF1("Gaus1D",multiGaus1,MR_left,MR_right,Num_pars1);
    TF1* G101=new TF1("G101",singleGaus1,MR_left_plot,MR_right_plot,3);
    TF1* G102=new TF1("G102",doubleGaus1,MR_left_plot,MR_right_plot,3);
    TF1* G103=new TF1("G103",doubleGaus1,MR_left_plot,MR_right_plot,3);
    TF1* G104=new TF1("G104",doubleGaus1,MR_left_plot,MR_right_plot,3);

    // screen output
    fileout<<"======================    file    ======================"<<endl;
    fileout<<"root file:      "<<rootfile<<endl;
    fileout<<"parameter file: "<<parfile<<endl;
    fileout<<"Number of Gaussians = "<<Num_Gaus<<endl;
    fileout<<"Fit Area: MR   = "<<MR_left<<" to "<<MR_right<<endl;
    if (dimensions==2) {
        fileout<<"Fit Area: RTKE = "<<TKE_bottom<<" to "<<TKE_top<<endl;
    }

    // get maximum counts/bin from data (for plotting)
    Double_t max_data_bin, max_data;
    max_data_bin = mr2->GetMaximumBin();
    max_data =mr2->GetBinContent(max_data_bin);

    Double_t max_data_bin1, max_data1;
    max_data_bin1 = mr1->GetMaximumBin();
    max_data1 =mr1->GetBinContent(max_data_bin1);

    //
    //        ******** 2D MR vs TKE/TKE_Viola fitting *******
    //
    if (dimensions==2) {
    cout<<"======================    fitting  2D  ======================"<<endl;

    // set 2D function
    Double_t pars[20] = {ipar[0], ipar[1], ipar[2], ipar[3], ipar[4], ipar[5], ipar[6], ipar[7], ipar[8], ipar[9], ipar[10], ipar[11], ipar[7], ipar[13], ipar[14], ipar[15], ipar[16], ipar[7], ipar[18], ipar[19]};
    Gaus2D->SetParameters(pars);

    // set parameter limits
    for (int i=0; i<Num_pars; i++){
             if (itype[i]=="A") {
                Gaus2D->SetParLimits(i,imin[i],imax[i]);
             }
             if (itype[i]=="R") {
                Gaus2D->SetParLimits(i,ipar[i]-imin[i],ipar[i]+imax[i]);
             }
             if (itype[i]=="F") {
                Gaus2D->SetParLimits(i,ipar[i]*imin[i],ipar[i]*imax[i]);
             }
    } //end i

    /*
    Double_t parmin,parmax;
    for (int i=0; i<Num_pars; i++){
        Gaus2D->GetParLimits(i,parmin,parmax);
        cout<<"i="<<i<<", par="<<Gaus2D->GetParameter(i)<<", min="<<parmin<<", max="<<parmax<<endl;
    } //end i
    */



    // set parameter names for output
    //Gaus2D->SetParName(0,  "1: height ");
    Gaus2D->SetParName(0,  "1: vol      ");
    Gaus2D->SetParName(1,  "  MR        ");
    Gaus2D->SetParName(2,  "  sigma_MR  ");
    Gaus2D->SetParName(3,  "  RTKE      ");
    Gaus2D->SetParName(4,  "  sigma_RTKE");
    //Gaus2D->SetParName(5,  "2:height");
    Gaus2D->SetParName(5,  "2: vol      ");
    Gaus2D->SetParName(6,  "  MR        ");
    Gaus2D->SetParName(7,  "  sigma_MR  ");
    Gaus2D->SetParName(8,  "  RTKE      ");
    Gaus2D->SetParName(9,  "  sigma_RTKE");
    //Gaus2D->SetParName(10,  "3:height");
    Gaus2D->SetParName(10, "3: vol      ");
    Gaus2D->SetParName(11, "  MR        ");
    Gaus2D->SetParName(12, "  sigma_MR  ");
    Gaus2D->SetParName(13, "  RTKE      ");
    Gaus2D->SetParName(14, "  sigma_RTKE");
    //Gaus2D->SetParName(15,  "4:height");
    Gaus2D->SetParName(15, "4: vol      ");
    Gaus2D->SetParName(16, "  MR        ");
    Gaus2D->SetParName(17, "  sigma_MR  ");
    Gaus2D->SetParName(18, "  RTKE      ");
    Gaus2D->SetParName(19, "  sigma_RTKE");

    // fit with combined Gaussians
        //"R"=use fit range from Gaus2D definition
        //"0"=suppress plotting of fit
    mr2->Fit("Gaus2D","0R");			// do actual fit

    // screen output
    cout<<"====================    how good a 2D fit    ===================="<<endl;
    cout<<"chisqr = "<<Gaus2D->GetChisquare()<<endl;
    cout<<"number of degrees of freedom = "<<Gaus2D->GetNDF()<<endl;

    // save 2D fit parameters
    p0 = Gaus2D->GetParameter(0);
    p0e = Gaus2D->GetParError(0);
    p1 = Gaus2D->GetParameter(1);
    p1e = Gaus2D->GetParError(1);
    p2 = Gaus2D->GetParameter(2);
    p3 = Gaus2D->GetParameter(3);
    p4 = Gaus2D->GetParameter(4);
    p5 = Gaus2D->GetParameter(5);
    p5e = Gaus2D->GetParError(5);
    p6 = Gaus2D->GetParameter(6);
    p6e = Gaus2D->GetParError(6);
    p7 = Gaus2D->GetParameter(7);
    p8 = Gaus2D->GetParameter(8);
    p9 = Gaus2D->GetParameter(9);
    p10 = Gaus2D->GetParameter(10);
    p10e = Gaus2D->GetParError(10);
    p11 = Gaus2D->GetParameter(11);
    p11e = Gaus2D->GetParError(11);
    p12 = Gaus2D->GetParameter(12);
    p13 = Gaus2D->GetParameter(13);
    p14 = Gaus2D->GetParameter(14);
    p15 = Gaus2D->GetParameter(15);
    p15e = Gaus2D->GetParError(15);
    p16 = Gaus2D->GetParameter(16);
    p16e = Gaus2D->GetParError(16);
    p17 = Gaus2D->GetParameter(17);
    p18 = Gaus2D->GetParameter(18);
    p19 = Gaus2D->GetParameter(19);

    // set parameters for individual 2D Gaussians
    if (even) {
      G2->SetParameters(p0,p1,p2,p3,p4);
      G3->SetParameters(p5,p6,p7,p8,p9);
      G4->SetParameters(p10,p11,p7,p13,p14);
    } else {
      G1->SetParameters(p0,p1,p2,p3,p4);
      G2->SetParameters(p5,p6,p7,p8,p9);
      G3->SetParameters(p10,p11,p7,p13,p14);
      G4->SetParameters(p15,p16,p7,p18,p19);
    }

    // Volume 2D calculations
    if (even==true) {
       v_total=2*(p0 + p5 + p10);
       v2=2*p0*100/v_total;
       v2e=2*p0e*100/v_total;
       v3=2*p5*100/v_total;
       v3e=2*p5e*100/v_total;
       v4=2*p10*100/v_total;
       v4e=2*p10e*100/v_total;
    } else {
       v_total=p0 + 2*(p5 + p10 + p15);
       v1=p0*100/v_total;
       v1e=p0e*100/v_total;
       v2=2*p5*100/v_total;
       v2e=2*p5e*100/v_total;
       v3=2*p10*100/v_total;
       v3e=2*p10e*100/v_total;
       v4=2*p15*100/v_total;
       v4e=2*p15e*100/v_total;
    }
    fileout<<"==================== 2D Fission  ===================="<<endl;
    fileout<<"Number of experimental data events = "<<data_events<<endl;
    fileout<<"Fit: total volume = "<<v_total<<endl;
    if (even==false) {
        fileout<<"Fit: Symmetric Fission    = "<<v1<<" +/- "<<v1e<<" %"<<endl;
    }
    fileout<<"Fit: Asymmetric Fission 1 = "<<v2<<" +/- "<<v2e<<" %"<<endl;
    if (Num_Gaus>=4){
      fileout<<"Fit: Asymmetric Fission 2 = "<<v3<<" +/- "<<v3e<<" %"<<endl;
    }
    if (Num_Gaus>=6){
      fileout<<"Fit: Asymmetric Fission 3 = "<<v4<<" +/- "<<v4e<<" %"<<endl;
    }

    // 2D Residuals
    double temp;
    Double_t max_residual_bin, min_residual_bin, max_residual,min_residual ;
    int res_bins= 4;
    NbinsX=NbinsX/res_bins;
    NbinsY=NbinsY/res_bins;
    for(int i=0; i < NbinsX; i++){
       for(int j=0; j < NbinsY; j++){
         temp=0;
         for (int k=1; k<=res_bins; k++){
           for (int l=1; l<=res_bins; l++){
               temp = temp + mr2->GetBinContent(i*res_bins+k,j*res_bins+l) - Gaus2D->Eval(mr2->GetXaxis()->GetBinCenter(i*res_bins+k),mr2->GetYaxis()->GetBinCenter(j*res_bins+l));
           }//end l
         }//end k
         temp=temp/(res_bins*res_bins);
         for (int k=1; k<=res_bins; k++){
           for (int l=1; l<=res_bins; l++){
              residual->SetBinContent(i*res_bins+k,j*res_bins+l,temp);
           } //end l
         }//end k

        }//end j
     }//end i

    // residual plotting parameters
    max_residual_bin = residual->GetMaximumBin();
    min_residual_bin = residual->GetMinimumBin();
    max_residual =residual->GetBinContent(max_residual_bin);
    min_residual =residual->GetBinContent(min_residual_bin);
    if (max_residual-min_residual>1) {
           range_residual = max_residual;
           }else{
           range_residual = -min_residual;
    }
    } //end 2D fitting

    //
    //        ******** 1D MR fitting *******
    //
    cout<<"======================    fitting  1D  ======================"<<endl;
    if (dimensions==2) {
       Double_t pars1[12] = {ipar[0], p1, p2,  ipar[5], p6, p7, ipar[10], p11, p7, ipar[15], p16, p7};
       Gaus1D->SetParameters(pars1);
    } else {
        Double_t pars1[12] = {ipar[0], ipar[1], ipar[2],  ipar[5], ipar[6], ipar[7], ipar[10], ipar[11], ipar[7], ipar[15], ipar[16], ipar[7]};
        Gaus1D->SetParameters(pars1);
    }

    // set parameter limits
    if (dimensions==2) {
      Gaus1D->SetParLimits(0,0,p0*1000);
      Gaus1D->SetParLimits(1,p1,p1);
      Gaus1D->SetParLimits(2,p2,p2);
      Gaus1D->SetParLimits(3,0,p5*1000);
      Gaus1D->SetParLimits(4,p6,p6);
      Gaus1D->SetParLimits(5,p7,p7);
      Gaus1D->SetParLimits(6,0,p10*1000);
      Gaus1D->SetParLimits(7,p11,p11);
      Gaus1D->SetParLimits(8,p7,p7);
      Gaus1D->SetParLimits(9,0,p15*1000);
      Gaus1D->SetParLimits(10,p16,p16);
      Gaus1D->SetParLimits(11,p7,p7);
    }else{
    for (int i=0; i<Num_pars/5; i++){
         for (int j=0; j<3; j++){
             if (itype[i*5+j]=="A") {
                Gaus1D->SetParLimits(i*3+j,imin[i*5+j],imax[i*5+j]);
             }
             if (itype[i*5+j]=="R") {
                Gaus1D->SetParLimits(i*3+j,ipar[i*5+j]-imin[i*5+j],ipar[i*5+j]+imax[i*5+j]);
             }
             if (itype[i*5+j]=="F") {
                Gaus1D->SetParLimits(i*3+j,ipar[i*5+j]*imin[i*5+j],ipar[i*5+j]*imax[i*5+j]);
             }
         } //end j
    } //end i
    }

    /*
    Double_t parmin,parmax;
    for (int i=0; i<Num_pars; i++){
        Gaus1D->GetParLimits(i,parmin,parmax);
        cout<<"i="<<i<<", par="<<Gaus1D->GetParameter(i)<<", min="<<parmin<<", max="<<parmax<<endl;
    } //end i
    */

    // set parameter names for output
    //Gaus1D->SetParName(0,  "1:height");
    Gaus1D->SetParName(0,  "1: area ");
    Gaus1D->SetParName(1,  "   MR   ");
    Gaus1D->SetParName(2,  "   sigma");
    //Gaus1D->SetParName(3,  "2:height");
    Gaus1D->SetParName(3,  "2:area");
    Gaus1D->SetParName(4,  "   MR   ");
    Gaus1D->SetParName(5,  "   sigma");
    //Gaus1D->SetParName(6,  "3:height");
    Gaus1D->SetParName(6, "3:area");
    Gaus1D->SetParName(7, "   MR   ");
    Gaus1D->SetParName(8, "   sigma");
    //Gaus1D->SetParName(9,  "4:height");
    Gaus1D->SetParName(9, "4:area");
    Gaus1D->SetParName(10, "   MR   ");
    Gaus1D->SetParName(11, "   sigma");

    // 1D fit with combined Gaussians
    mr1->Fit("Gaus1D","0R");			// do actual fit

    // screen output
    cout<<"====================    how good a 1D fit    ===================="<<endl;
    cout<<"chisqr = "<<Gaus1D->GetChisquare()<<endl;
    cout<<"number of degrees of freedom = "<<Gaus1D->GetNDF()<<endl;

    // save 1D fit parameters
    Double_t p100 = Gaus1D->GetParameter(0);
    Double_t p100e = Gaus1D->GetParError(0);
    Double_t p101 = Gaus1D->GetParameter(1);
    Double_t p101e = Gaus1D->GetParError(1);
    Double_t p102 = Gaus1D->GetParameter(2);
    Double_t p103 = Gaus1D->GetParameter(3);
    Double_t p103e = Gaus1D->GetParError(3);
    Double_t p104 = Gaus1D->GetParameter(4);
    Double_t p104e = Gaus1D->GetParError(4);
    Double_t p105 = Gaus1D->GetParameter(5);
    Double_t p106 = Gaus1D->GetParameter(6);
    Double_t p106e = Gaus1D->GetParError(6);
    Double_t p107 = Gaus1D->GetParameter(7);
    Double_t p107e = Gaus1D->GetParError(7);
    Double_t p108 = Gaus1D->GetParameter(8);
    Double_t p109 = Gaus1D->GetParameter(9);
    Double_t p109e = Gaus1D->GetParError(9);
    Double_t p110 = Gaus1D->GetParameter(10);
    Double_t p110e = Gaus1D->GetParError(10);
    Double_t p111 = Gaus1D->GetParameter(11);

    // get individual 1D Gaussians
    if (even) {
      G102->SetParameters(p100,p101,p102);
      G103->SetParameters(p103,p104,p105);
      G104->SetParameters(p106,p107,p105);
    } else {
      G101->SetParameters(p100,p101,p102);
      G102->SetParameters(p103,p104,p105);
      G103->SetParameters(p106,p107,p105);
      G104->SetParameters(p109,p110,p105);
    }

    // Volume 1D calculations
    if (even==true) {
      v_total=2*(p100 + p103 + p106);
      v102=2*p100*100/v_total;
      v102e=2*p100e*100/v_total;
      v103=2*p103*100/v_total;
      v103e=2*p103e*100/v_total;
      v104=2*p106*100/v_total;
      v104e=2*p106e*100/v_total;
    } else {
      v_total=p100 + 2*(p103 + p106 + p109);
      v101=p100*100/v_total;
      v101e=p100e*100/v_total;
      v102=2*p103*100/v_total;
      v102e=2*p103e*100/v_total;
      v103=2*p106*100/v_total;
      v103e=2*p106e*100/v_total;
      v104=2*p109*100/v_total;
      v104e=2*p109e*100/v_total;
    }
    fileout<<"==================== 1D Fission ===================="<<endl;
    fileout<<"Number of experimental data events = "<<data_events<<endl;
    fileout<<"Fit: total area = "<<v_total<<endl;
    if (even==false) {
       fileout<<"Fit: Symmetric Fission    = "<<v101<<" +/- "<<v101e<<" %"<<endl;
    }
    fileout<<"Fit: Asymmetric Fission 1 = "<<v102<<" +/- "<<v102e<<" %"<<endl;
    if (Num_Gaus>=4){
      fileout<<"Fit: Asymmetric Fission 2 = "<<v103<<" +/- "<<v103e<<" %"<<endl;
    }
    if (Num_Gaus>=6){
      fileout<<"Fit: Asymmetric Fission 3 = "<<v104<<" +/- "<<v104e<<" %"<<endl;
    }

    // 1D Residuals
    double temp1;
    Double_t max_residual1_bin, min_residual1_bin, max_residual1,min_residual1 ;
    int res_bins= 4;
    NbinsX1=NbinsX1/res_bins;
    for(int i=0; i < NbinsX1; i++){
         temp1=0;
         for (int k=1; k<=res_bins; k++){
               temp1 = temp1 + mr1->GetBinContent(i*res_bins+k) - Gaus1D->Eval(mr1->GetXaxis()->GetBinCenter(i*res_bins+k));
         }//end k
         temp1=temp1/(res_bins*res_bins);
         for (int k=1; k<=res_bins; k++){
              residual1->SetBinContent(i*res_bins+k,temp1);
         }//end k

     }//end i


    // residual plotting parameters
    //int  xx1=mr1->FindBin(MR_left_plot);
    //int  xx2=mr1->FindBin(MR_right_plot);
    //residual1->GetXaxis()->SetRange(xx1,xx2);
    max_residual1_bin = residual1->GetMaximumBin();
    min_residual1_bin = residual1->GetMinimumBin();
    max_residual1 =residual1->GetBinContent(max_residual1_bin);
    min_residual1 =residual1->GetBinContent(min_residual1_bin);
    if (max_residual1-min_residual1>1) {
           range_residual1 = max_residual1;
           }else{
           range_residual1 = -min_residual1;
    }
    range_residual1=range_residual1*1.05;

    TLine *line1 = new TLine(MR_left_plot,0,MR_right_plot,0);
    line1->SetLineColor(kRed+1);
    line1->SetLineWidth(1);

    //
    //        ******** Plot results *******
    //
    cout<<"=============================================================="<<endl;
    cout<<""<<endl;

    // set palettes
    TExec *colour1 = new TExec("colour1","madColour();");
    TExec *colour2 = new TExec("colour2","newColour();");

   if (dimensions==2) {	    	// start 2D and 1D fits - plots
    // set canvas size
    TCanvas *c1 = new TCanvas("c1","Fitting plots",0,0,1800,900);

    // set individual plots
    TPad *pad1 = new TPad("pad1","pad1",   0.00, 0.75, 0.25, 1.00);
    TPad *pad2 = new TPad("pad2","pad2",   0.00, 0.50, 0.25, 0.75);
    TPad *pad3 = new TPad("pad3","pad3",   0.00, 0.25, 0.25, 0.50);
    //TPad *pad4 = new TPad("pad4","pad4",   0.00, 0.00, 0.25, 0.25);
    TPad *pad4 = new TPad("pad4","pad4",   0.00, 0.00, 0.50, 0.50); //larger residual
    TPad *pad5 = new TPad("pad5","pad5",   0.25, 0.75, 0.50, 1.00);
    TPad *pad6 = new TPad("pad6","pad6",   0.25, 0.50, 0.50, 0.75);
    TPad *pad7 = new TPad("pad7","pad7",   0.25, 0.25, 0.50, 0.50);
    TPad *pad8 = new TPad("pad8","pad8",   0.25, 0.00, 0.50, 0.25);
    TPad *pad9 = new TPad("pad9","pad9",   0.50, 0.75, 0.75, 1.00);
    TPad *pad10 = new TPad("pad10","pad10",0.50, 0.50, 0.75, 0.75);
    TPad *pad11 = new TPad("pad11","pad11",0.50, 0.25, 0.75, 0.50);
    TPad *pad12 = new TPad("pad12","pad12",0.50, 0.00, 0.75, 0.25);
    TPad *pad13 = new TPad("pad13","pad13",0.75, 0.75, 1.00, 1.00);
    TPad *pad14 = new TPad("pad14","pad14",0.75, 0.50, 1.00, 0.75);
    TPad *pad15 = new TPad("pad15","pad15",0.75, 0.25, 1.00, 0.50);
    TPad *pad16 = new TPad("pad16","pad16",0.75, 0.00, 1.00, 0.25);
    pad1->Draw();
    pad2->Draw();
    //pad3->Draw();
    pad4->Draw();
    pad5->Draw();
    pad6->Draw();
    //pad7->Draw();
    //pad8->Draw();
    pad9->Draw();
    pad10->Draw();
    pad11->Draw();
    pad12->Draw();
    pad13->Draw();
    pad14->Draw();
    pad15->Draw();
    pad16->Draw();

    colour1->Draw();		//set initial colour

    auto pl = new TPaveLabel();
    TPaveText *pt = new TPaveText(0.05,0.75,0.95,0.95);
    TPaveText *pt1 = new TPaveText(0.05,0.05,0.95,0.70);
    TPaveText *pt2 = new TPaveText(0.05,0.05,0.95,0.70);

    // individual plots (pads)
    pad1->cd();
    pad1->SetLogz(0);
    mr2->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    mr2->GetYaxis()->SetRangeUser(TKE_bottom_plot,TKE_top_plot);
    mr2->SetMaximum(max_data);
    mr2->Draw();		//data
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"data","brNDC");

    pad2->cd();
    pad2->SetLogz(0);
    mr2->Draw();
    //Gaus2D->Draw("CONT3 same");
    auto clone = (TF2*)Gaus2D->DrawClone("CONT3 same");
    clone->SetContour(5);  //set number of contours on total fit overlay
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"data & fit","brNDC");

    // plot residuals
    pad4->cd();
    pad4->SetLogz(0);
    residual->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    residual->GetYaxis()->SetRangeUser(TKE_bottom_plot,TKE_top_plot);
    residual->GetXaxis()->SetTitle("MR");
    residual->GetYaxis()->SetTitle("TKE/TKE Viola");
    residual->SetMaximum(range_residual);
    residual->SetMinimum(-range_residual);
    residual->Draw("axis");
    colour2->Draw();
    residual->Draw("colz same"); //residuals
    residual->Draw("axis same");
    //clone->Draw("cont3 same");
    auto clone1 = (TF2*)G1->DrawClone("CONT3 same");
    auto clone2 = (TF2*)G2->DrawClone("CONT3 same");
    auto clone3 = (TF2*)G3->DrawClone("CONT3 same");
    auto clone4 = (TF2*)G4->DrawClone("CONT3 same");
    clone1->SetContour(4);  //set number of contours on individual fit overlay
    clone2->SetContour(4);
    clone3->SetContour(4);
    clone4->SetContour(4);
    //pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"2D residuals","brNDC");

    Double_t scaleZ=0.03*(TKE_top_plot-TKE_bottom_plot);
    Double_t scaleA=0.07*(TKE_top_plot-TKE_bottom_plot);
    int A=249;    // for p + 232Th
    int Z=97;    // for p + 232Th
    TGaxis *axis1 = new TGaxis(MR_left_plot,TKE_bottom_plot-scaleA,MR_right_plot,
         TKE_bottom_plot-scaleA,MR_left_plot*A,MR_right_plot*A,505,"");
    axis1->SetTitle("A           ");
    axis1->SetTitleOffset(0.3);
    axis1->Draw();
    TGaxis *axis2 = new TGaxis(MR_left_plot,TKE_top_plot+scaleZ,MR_right_plot,
         TKE_top_plot+scaleZ,MR_left_plot*Z,MR_right_plot*Z,505,"-");
    axis2->SetTitle("Z");
    axis2->SetTitleOffset(0.5);
    axis2->Draw();


    pad5->cd();
    pad5->SetLogz(1);
    mr2->Draw("axis");
    colour1->Draw();
    mr2->Draw("colz same");		//data.q
    auto clone5 = (TF2*)Gaus2D->DrawClone("CONT3 same");
    clone5->SetMinimum(1);
    clone5->SetContour(5);  //set number of contours on total fit overlay
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"data (log)","brNDC");

    pad6->cd();
    pt->AddText(Ebeam+" MeV,   "+reaction+",   "+rootfile);
    pt->Draw();
    if (even) {
      pt1->AddText(Form("Asym 1: Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v2,v2e,p1,p1e));
      pt1->AddText(Form("Asym 2: Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v3,v3e,p6,p6e));
      pt1->AddText(Form("Asym 3: Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v4,v4e,p11,p11e));
    } else {
      pt1->AddText(Form("Sym:      Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v1,v1e,p1,p1e));
      pt1->AddText(Form("Asym 1: Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v2,v2e,p6,p6e));
      pt1->AddText(Form("Asym 2: Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v3,v3e,p11,p11e));
      pt1->AddText(Form("Asym 3: Volume = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v4,v4e,p16,p16e));
    }
    pt1->AddText(Form("Chi Squared = %g         Degrees of Freedom= %d",Gaus2D->GetChisquare(),Gaus2D->GetNDF()));
    pt1->SetFillColor(0);
    pt1->SetTextAlign(12);
    pt1->Draw();

    pad9->cd();
    pad9->SetLogz(0);
    G1->SetMaximum(max_data);
    G1->SetMinimum(1);
    G1->Draw("axis");
    G1->Draw("CONT1 same");		//symmetric Gaussian
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"fit Sym","brNDC");

    pad10->cd();
    pad10->SetLogz(0);
    G2->SetMaximum(max_data);
    G2->SetMinimum(1);
    G2->Draw("axis");
    colour1->Draw();
    G2->Draw("CONT1 same");	//first asymmetric Gaussians
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"fit Asym1","brNDC");

    pad11->cd();
    pad11->SetLogz(0);
    G3->SetMaximum(max_data);
    G3->SetMinimum(1);
    G3->Draw("CONT1");		//second asymmetric Gaussians
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"fit Asym2","brNDC");

    pad12->cd();
    pad12->SetLogz(0);
    G4->SetMaximum(max_data);
    G4->SetMinimum(1);
    G4->Draw("CONT1");		//third asymmetric Gaussians
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"fit Asym3","brNDC");

    pad13->cd();
    G2->Draw("CONT1");		//second asymmetric Gaussians
    G1->Draw("CONT1 SAME");	//symmetric Gaussian
    G3->Draw("CONT1 SAME");	//third asymmetric Gaussians
    G4->Draw("CONT1 SAME");	//fourth asymmetric Gaussians
    pl->DrawPaveLabel(0.65,0.92,0.90,0.99,"fit (lin)","brNDC");

    pad14->cd();
    pad14->SetLogz(0);
    mr1->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    mr1->Draw();
    Gaus1D->Draw("same");
    G101->SetLineColor(4);
    G101->Draw("same");
    G102->SetLineColor(8);
    G102->Draw("same");
    G103->SetLineColor(7);
    G103->Draw("same");
    G104->SetLineColor(6);
    G104->Draw("same");
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"1D MR fit","brNDC");

    pad15->cd();
    residual1->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    residual1->GetYaxis()->SetRangeUser(-range_residual1,range_residual1);
    residual1->Draw();
    line1->Draw();
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"1D residuals","brNDC");

    pad16->cd();
    if (even) {
      pt2->AddText(Form("Asym 1: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v102,v102e,p101,p101e));
      pt2->AddText(Form("Asym 2: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v103,v103e,p104,p104e));
      pt2->AddText(Form("Asym 3: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v104,v104e,p107,p107e));
    } else {
      pt2->AddText(Form("Sym:      Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v101,v101e,p101,p101e));
      pt2->AddText(Form("Asym 1: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v102,v102e,p104,p104e));
      pt2->AddText(Form("Asym 2: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v103,v103e,p107,p107e));
      pt2->AddText(Form("Asym 3: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v104,v104e,p110,p110e));
    }
    pt2->AddText(Form("Chi Squared = %g         Degrees of Freedom= %d",Gaus1D->GetChisquare(),Gaus1D->GetNDF()));
    pt2->SetFillColor(0);
    pt2->SetTextAlign(12);
    pt2->Draw();

    //        ******** output to files *******
    //c1->SaveAs("MR_2Dfit_.png");
    //c1->SaveAs("MR_2Dfit.pdf");

   TString fileName1 = "captures_7/MR_2Dfit_" + runNo + ".png";
   TString fileName2 = "captures_7/MR_2Dfit_" + runNo + ".pdf";

    c1->SaveAs(fileName1);
    c1->SaveAs(fileName2);

   }  	// end 2D and 1D fits - plots


   if (dimensions==1) {	    	// start 1D fits - plots
    // set canvas size
    TCanvas *c1 = new TCanvas("c1","Fitting plots",0,0,900,900);

    // set individual plots
    TPad *pad1 = new TPad("pad1","pad1",   0.00, 0.67, 0.50, 1.00);
    TPad *pad2 = new TPad("pad2","pad2",   0.00, 0.33, 0.50, 0.67);
    TPad *pad3 = new TPad("pad3","pad3",   0.00, 0.00, 0.50, 0.33);
    TPad *pad4 = new TPad("pad4","pad4",   0.50, 0.67, 1.00, 1.00);
    TPad *pad5 = new TPad("pad5","pad5",   0.50, 0.33, 1.00, 0.67);
    TPad *pad6 = new TPad("pad6","pad6",   0.50, 0.00, 1.00, 0.33);

    pad1->Draw();
    pad2->Draw();
    pad3->Draw();
    pad4->Draw();
    pad5->Draw();
    pad6->Draw();

    colour1->Draw();		//set initial colour

    auto pl = new TPaveLabel();
    TPaveText *pt = new TPaveText(0.05,0.75,0.95,0.95);
    TPaveText *pt1 = new TPaveText(0.05,0.05,0.95,0.70);
    TPaveText *pt2 = new TPaveText(0.05,0.05,0.95,0.70);

    // individual plots (pads)
    pad1->cd();
    pad1->SetLogz(0);
    mr2->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    mr2->GetYaxis()->SetRangeUser(TKE_bottom_plot,TKE_top_plot);
    mr2->SetMaximum(max_data);
    mr2->Draw("axis");		//data
    colour1->Draw();
    mr2->Draw("colz same");
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"data","brNDC");

    pad2->cd();
    pad2->SetLogz(1);
    mr2->Draw("axis");
    colour1->Draw();
    mr2->Draw("colz same");		//data
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"data (log)","brNDC");

    pad4->cd();
    pad4->SetLogz(0);
    mr1->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    mr1->SetMaximum(max_data1*1.05);
    mr1->Draw();
    Gaus1D->Draw("same");
    G101->SetLineColor(4);
    G101->Draw("same");
    G102->SetLineColor(8);
    G102->Draw("same");
    G103->SetLineColor(7);
    G103->Draw("same");
    G104->SetLineColor(6);
    G104->Draw("same");
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"1D MR fit","brNDC");

    pad5->cd();
    //pad12->SetLogz(0);
    //Gaus2D->Draw("SURF2");	//total fit in 3D
    residual1->GetXaxis()->SetRangeUser(MR_left_plot,MR_right_plot);
    residual1->GetYaxis()->SetRangeUser(-range_residual1,range_residual1);
    residual1->Draw();
    line1->Draw();
    pl->DrawPaveLabel(0.75,0.92,0.95,0.99,"1D residuals","brNDC");

    pad6->cd();
    pt->AddText(Ebeam+" MeV,   "+reaction+",   "+rootfile);
    pt->Draw();
    if (even) {
      pt2->AddText(Form("Asym 1: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v102,v102e,p101,p101e));
      pt2->AddText(Form("Asym 2: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v103,v103e,p104,p104e));
      pt2->AddText(Form("Asym 3: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v104,v104e,p107,p107e));
    } else {
      pt2->AddText(Form("Sym:      Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v101,v101e,p101,p101e));
      pt2->AddText(Form("Asym 1: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v102,v102e,p104,p104e));
      pt2->AddText(Form("Asym 2: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v103,v103e,p107,p107e));
      pt2->AddText(Form("Asym 3: Area = %3.3g +/- %3.2g %%  MR = %3.3g +/- %3.2g",v104,v104e,p110,p110e));
    }
    pt2->AddText(Form("Chi Squared = %g         Degrees of Freedom= %d",Gaus1D->GetChisquare(),Gaus1D->GetNDF()));
    pt2->SetFillColor(0);
    pt2->SetTextAlign(12);
    pt2->Draw();

    //        ******** output to files *******
    //c1->SaveAs("MR_1Dfit.png");
    //c1->SaveAs("MR_1Dfit.pdf");

   TString fileName3 = "captures_7/MR_1Dfit_" + runNo + ".png";
   TString fileName4 = "captures_7/MR_1Dfit_" + runNo + ".pdf";

    c1->SaveAs(fileName3);
    c1->SaveAs(fileName4);

   }  	// end 1D fits - plots

 }  //end main program
