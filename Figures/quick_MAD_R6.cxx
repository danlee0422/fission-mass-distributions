/*-------------------------------------
  Checking velocity plots
  Useage:
    .L quick_mad_R6.cxx
    quick_mad_R6("XXX.PEG.root","runNo")
  Yun Jeung. 04.02.2021
--------------------------------------*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;
void madColour();

void quick_mad_R6(const char *rootfile, string runNo){
  gStyle->SetErrorX(0);
  gStyle->SetOptStat(0);
  gStyle->SetEndErrorSize(0);
  gStyle->SetLineWidth(2);

  //------------GATES----------------------------------

   //For MR-distribution
   TCutG *cutg2 = new TCutG("cutg2",5); //gating on 0.1 < MR < 0.9 && 15 < thetaCM < 90
   cutg2->SetPoint(0,0.02,90);
   cutg2->SetPoint(1,0.98,90);
   //cutg2->SetPoint(2,0.98,130);
   //cutg2->SetPoint(3,0.02,130);
   //cutg2->SetPoint(4,0.02,90);
   //cutg2->SetPoint(0,0.02,115);
   //cutg2->SetPoint(1,0.98,115);
   cutg2->SetPoint(2,0.98,175);
   cutg2->SetPoint(3,0.02,175);
   cutg2->SetPoint(4,0.02,90);

   //---------SETTING FILES/TITLE NAMES------------------

   TString saveFilePath = "figures/"; // figures saved in captures directory
   TString format = "png"; // e.g pdf, eps, jpg etc..

   TString fileName = "232Th_" + runNo;
   TString title = "p + ^{232}Th  at E_{lab} = 24.9928 MeV";

   //--------------CALL FILE and HISTOGRAMS-------------------
   TFile* input = new TFile(rootfile); // Input sorted root file

   TH2D *plot=(TH2D *)input->Get("Hist2D/mrthcm13_mir");   //3 Detectors conf.
   //TH2D *plot=(TH2D *)input->Get("Hist2D/mrthcm13");   //3 Detectors conf.

   plot->RebinX(5); // binning factor; 1000/10=100
   plot->RebinY(20); // binning factor; 1800/30=60

   TH1D *px =plot->ProjectionX();

   TH1D *px_gated = plot->ProjectionX("px_gated",0,-1,"[cutg2]");
   TH1D *px_gated_log = plot->ProjectionX("px_gated_log",0,-1,"[cutg2]");
   //px_gated->Rebin(10);
   //px_gated_log->Rebin(10);

   //-------------DEFINE PLOT RANGES------------------------------
   plot->SetMinimum(1);           // MAD min
   plot->SetMaximum(100000);      // MAD max, depeding on statistics
   px->SetMinimum(0);             // MR-dist min
   px->SetMaximum(16000);         // MR-dist max, depeding on statistics
   px_gated->SetMinimum(0);       // Gated MR-dist
   px_gated->SetMaximum(24000);    // variable value depeding on statistics
   px_gated_log->SetMinimum(1);   // Gated MR-dist (logscale)
   px_gated_log->SetMaximum(100000); // variable value depeding on statistics

   plot->GetXaxis()->SetRangeUser(0,1);

   px_gated->GetXaxis()->SetRangeUser(0,1);
   px_gated->SetNdivisions(306,"X");
   // px_gated->GetXaxis()->SetRangeUser(0.2,0.8);
   // px_gated->SetNdivisions(-206,"X");

   px_gated_log->GetXaxis()->SetRangeUser(0,1);
//--------------------------------------------------------------------------------

  //-------------SETTING STYLE-------------------

   plot->SetTitle(title);
   plot->SetMinimum(1);
   plot->GetXaxis()->SetTitle("M_{R}");
   plot->GetXaxis()->CenterTitle(true);
   plot->GetXaxis()->SetTitleOffset(1.1);//0.87
   plot->GetYaxis()->SetTitle("#theta_{c.m.} [deg]");
   plot->GetYaxis()->SetTitleOffset(1.2);
   plot->GetYaxis()->CenterTitle(true);
   plot->SetNdivisions(-304,"Y");
  // plot->SetNdivisions(-205,"X");
   plot->SetNdivisions(306,"X");
   plot->SetTickLength(0.03,"XY");

   px->SetTitle(title);
   px->GetXaxis()->SetTitle("M_{R}");
   px->GetXaxis()->CenterTitle(true);
   px->GetXaxis()->SetTitleOffset(1.1);
   px->SetLineColor(kBlue+1);
   px->SetMarkerColor(kBlue+1);
   px->SetMarkerStyle(20);
   px->SetMarkerSize(1.2);
   px->SetNdivisions(-304,"Y");
   //px->SetNdivisions(-205,"X");
   px->SetNdivisions(306,"X");

   px->SetTickLength(0.03,"XY");

   px_gated->SetTitle(title);
   px_gated->GetXaxis()->SetTitle("M_{R}");
   px_gated->GetXaxis()->CenterTitle(true);
   px_gated->GetXaxis()->SetTitleOffset(1.1);
   px_gated->GetYaxis()->SetTitle("Counts");
   px_gated->GetYaxis()->CenterTitle(true);
   px_gated->GetYaxis()->SetTitleOffset(2.2);
   px_gated->SetLineColor(kBlue+1);
   px_gated->SetMarkerColor(kBlue+1);
   px_gated->SetMarkerStyle(20);
   px_gated->SetMarkerSize(1.2);
   //px_gated->SetNdivisions(-304,"Y");

   px_gated->SetTickLength(0.03,"XY");

   px_gated_log->SetTitle(title);
   px_gated_log->GetXaxis()->SetTitle("M_{R}");
   px_gated_log->GetXaxis()->CenterTitle(true);
   px_gated_log->GetXaxis()->SetTitleOffset(1.1);
   px_gated_log->GetYaxis()->SetTitle("Counts");
   px_gated_log->GetYaxis()->CenterTitle(true);
   px_gated_log->GetYaxis()->SetTitleOffset(2.2);
   px_gated_log->SetLineColor(kBlue+1);
   px_gated_log->SetMarkerColor(kBlue+1);
   px_gated_log->SetMarkerStyle(20);
   px_gated_log->SetMarkerSize(1.2);
   px_gated_log->SetNdivisions(-304,"Y");
   px_gated_log->SetNdivisions(-205,"X");
   px_gated_log->SetTickLength(0.03,"XY");

   //-------------DEFINE CANVAS--------------------------------------------
   //Canvas
    TCanvas *c1 = new TCanvas("c1","c1",10,10,920,950);//2D Hist.  Exp. MAD
  //  TCanvas *c2 = new TCanvas("c2","c2",920,950);//1D MR
    TCanvas *c3 = new TCanvas("c3","c3",1000,10,920,950);//1D gated MR
    TCanvas *c4 = new TCanvas("c4","c4",1800,10,920,950);//1D gated MR (logscale)

    c1->SetRightMargin(0.1100218);
    //c2->SetLeftMargin(0.1590414);
    //c2->SetRightMargin(0.04466231);
    c3->SetLeftMargin(0.1448802);
    c3->SetRightMargin(0.05446623);
    c4->SetLeftMargin(0.1448802);
    c4->SetRightMargin(0.05446623);

    // For MAD plot color z
    TPaletteAxis *palette = new TPaletteAxis(1.005, 0,1.05,180,plot);
      palette->SetLabelColor(1);
      palette->SetLabelFont(42);
      palette->SetLabelOffset(0.005);
      palette->SetLabelSize(0.035);
      palette->SetTitleOffset(1);
      palette->SetTitleSize(0.035);

    Int_t ci;   // for color index setting
    ci = TColor::GetColor("#e3e3f6");
      palette->SetFillColor(ci);
      palette->SetFillStyle(1001);
      palette->SetLineWidth(3);

    plot->GetListOfFunctions()->Add(palette,"br");

    // -----------DRAW PLOTS ----------------------------
    c1->cd();
    c1->SetRightMargin(1);
      gPad->SetLogz();// if you're using log scale, Minimum should be greater than 0.
      plot->Draw();
      madColour();

     cutg2->SetLineColor(kRed);
     cutg2->Draw("same");

    //-----
/*
    c2->cd();
    px->SetFillColor(ci);
    px->Draw("EBAR");
*/
    //-----
    c3->cd();
      px_gated->SetFillColor(ci);
      px_gated->Draw("EBAR");


    //-----
    c4->cd();
      c4->SetLogy();//
      px_gated_log->SetFillColor(ci);
      px_gated_log->Draw("EBAR");

    //-----------------------SAVE FIGURES----------------

     c1->SaveAs(saveFilePath + fileName + "_mad." + format);
    // c2->SaveAs(saveFilePath + fileName + "_mr." + format);
     c3->SaveAs(saveFilePath + fileName + "_mr_gated." + format);
     c4->SaveAs(saveFilePath + fileName + "_mr_gated_log." + format);


    //----------------------------------------------------
/*
    c4->Close();

    c1->Close();
    c2->Close();
    c3->Close();
    c4->Close();
*/

}//end quick_madplot



void madColour(){
  const Int_t NRGBs = 20;
  const Int_t NCont = 20;
  // Colour scheme to match "Hinde special" in dagui
  Double_t stops[NRGBs] = { 0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 1.00};

  Double_t red[NRGBs]   = {0,0,0,0,0.25,0.55,1,1,1,1,0.86,0.67,0.47,0.47,0.525,0.57,0.62,0.75,0.855,0.93};
  Double_t green[NRGBs] = {0,0,0.4,0.7,0.9,1,1,0.75,0.5,0.35,0.19,0.12,0,0.1,0.27,0.43,0.55,0.72,0.855,0.93};
  Double_t blue[NRGBs]  = {0.5,0.7,0.55,0.3,0.1,0,0,0,0,0,0,0,0.18,0.48,0.59,0.7,0.81,0.88,0.94,0.99};
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
  gStyle->SetLineWidth(2);
}
