/*-------------------------------------
TKE and RTKE plots
  Useage:
    .L quick_tke.cxx
    quick_tke("XXX.PEG.root", "runNo")
  Yun Jeung. 25.06.2021
--------------------------------------*/
//#include "iostream.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

void madColour();

void tke_plot(const char *rootfile, string runNo){
  gStyle->SetOptStat(0);  // 1: show statistics
  gStyle->SetEndErrorSize(0);
  gStyle->SetLineWidth(2);

  gStyle->SetTextFont(42);
  gStyle->SetTextSize(0.035);

  gStyle->SetStatX(0.40);		//Stat box x position (top right corner)
  gStyle->SetStatY(0.90); 	//Stat box y position
  gStyle->SetStatW(0.30);	 	//Stat box width as fraction of pad size
  gStyle->SetStatH(0.15);	 	//Size of each line in stat box

  //---------SETTING FILES/TITLE NAMES------------------
   TString saveFilePath = "figures/";
   TString format = ".png"; // e.g pdf, eps, jpg etc..

   TString fileName = "232Th_" + runNo;
   TString title = "p + ^{232}Th  at E_{lab} = 24.9928 MeV";

   int zmin = 1; int zmax = 10000;          // z-range
   double xmin = 0.2; double xmax = 0.8;    // Mr-range
   int ymin = 0; int ymax = 350;            // TKE-range
   double rymin = 0.0; double rymax = 2.5;  // RKTE-range

  //--------------CALL FILE and HISTOGRAMS-------------------
   TFile* input1 = new TFile(rootfile); // Input sorted root file

   TH2D *plot3=(TH2D *)input1->Get("Hist2D/mrTKE");
   plot3->RebinX(4); plot3->RebinY(3);

   TH2D *plot4=(TH2D *)input1->Get("Hist2D/mrTKEViola");
   plot4->RebinX(4); plot4->RebinY(1);

  // TGraph *tke_gr = (TGraph *)input1->Get("Calc/mrTKE_calc");
  TGraph *tke_gr = (TGraph *)input1->Get("Calc/mrTKE_calc_Viola");
   tke_gr->SetLineColor(kBlack);

  //-------------DEFNE PLOT RANGES------------------------------
   plot3->SetMinimum(zmin); plot3->SetMaximum(zmax);
   plot4->SetMinimum(zmin); plot4->SetMaximum(zmax);

   plot3->GetXaxis()->SetRangeUser(xmin,xmax);
   plot4->GetXaxis()->SetRangeUser(xmin,xmax);
   //plot3->GetYaxis()->SetRangeUser(75,300);
   plot3->GetYaxis()->SetRangeUser(ymin,ymax);
   plot4->GetYaxis()->SetRangeUser(rymin,rymax);

  //-------------SETTING STYLE-------------------
   plot3->SetTitle(title);
   // plot3->SetTitle("M_{R} vs TKE");
   plot3->SetMinimum(1);
   plot3->GetXaxis()->SetTitle("M_{R}");
   plot3->GetXaxis()->CenterTitle(true);
   plot3->GetXaxis()->SetTitleOffset(1.3);//0.87
   plot3->GetYaxis()->SetTitle("TKE   (MeV)");
   plot3->GetYaxis()->SetTitleOffset(1.45);
   plot3->GetYaxis()->CenterTitle(true);
   plot3->SetNdivisions(406,"Y");
   plot3->SetNdivisions(206,"X");
   plot3->SetTickLength(0.03,"XY");

   plot4->SetTitle(title);
   // plot4->SetTitle("M_{R} vs TKE/TKEViola");
   plot4->SetMinimum(1);
   plot4->GetXaxis()->SetTitle("M_{R}");
   plot4->GetXaxis()->CenterTitle(true);
   plot4->GetXaxis()->SetTitleOffset(1.3);//0.87
   plot4->GetYaxis()->SetTitle("TKE/TKEViola");
   plot4->GetYaxis()->SetTitleOffset(1.5);
   plot4->GetYaxis()->CenterTitle(true);
   plot4->SetNdivisions(307,"Y");
   plot4->SetNdivisions(206,"X");
   plot4->SetTickLength(0.03,"XY");

   plot3->GetXaxis()->SetDecimals(true);
   plot4->GetXaxis()->SetDecimals(true);

  //---------------DEFINE CANVAS---------------------------
   TLine *line1 = new TLine(xmin,1,xmax,1);
   line1->SetLineColor(kBlack);
   line1->SetLineStyle(2); line1->SetLineWidth(2);

   TCanvas *c1 = new TCanvas("c1","c1",10,10,920,950); c1->SetRightMargin(0.1100218);
   TCanvas *c2 = new TCanvas("c2","c2",600,10,920,950); c2->SetRightMargin(0.1100218);

   madColour();

  //--------------------------------------------------------
    c1->cd(0);
    gPad->SetLogz();
    plot4->Draw();
    line1->Draw("same");

    c2->cd(0);
    gPad->SetLogz();

    plot3->Draw();
    tke_gr->SetLineWidth(2);
    tke_gr->Draw("same");

  //-----------------------SAVE FIGURES----------------
    c1->SaveAs(saveFilePath + fileName + "_rtke." + format);
    c2->SaveAs(saveFilePath + fileName + "_tke." + format);

  //----------------------------------------------------

}//end quick_madplot

void madColour(){
  const Int_t NRGBs = 20;
  const Int_t NCont = 20;
  // Colour scheme to match "Hinde special" in dagui
  Double_t stops[NRGBs] = {0.00,0.05,0.10,0.15,0.20,0.25,0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.90,1.00};
  Double_t red[NRGBs]   = {0,0,0,0,0.25,0.55,1,1,1,1,0.86,0.67,0.47,0.47,0.525,0.57,0.62,0.75,0.855,0.93};
  Double_t green[NRGBs] = {0,0,0.4,0.7,0.9,1,1,0.75,0.5,0.35,0.19,0.12,0,0.1,0.27,0.43,0.55,0.72,0.855,0.93};
  Double_t blue[NRGBs]  = {0.5,0.7,0.55,0.3,0.1,0,0,0,0,0,0,0,0.18,0.48,0.59,0.7,0.81,0.88,0.94,0.99};
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
  gStyle->SetLineWidth(2);
}
