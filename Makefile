SHELL = /bin/sh

.SUFFIXES:
.SUFFIXES: .c .o

CPP=g++

FLAGS=-g -Wall -Wextra -std=c++11 `root-config --cflags` -O3 -ggdb3

LIB=-lm -ldl -lImt

ROOT=$ROOTSYS

ROOTLIB=`root-config --cflags --glibs` -L$(ROOTSYS)/lib

OBJS =  MassTable.o eloss.o plot_kin.o Reaction.o Nucleus.o Scattering.o CubeXsec_etw0717.o CubeInput.o CubeReact.o CubeDet.o CubeFis.o CubeSort.o CubeGate.o tCalib.o CubeHistEss.o CubeHist.o mainCube.o CubeLoss.o

dacube: $(OBJS)
	$(CPP) -o dacube $(OBJS) $(LIB) $(ROOTLIB)

mainCube.o: mainCube.cxx
	$(CPP) $(LIB) $(FLAGS) -c mainCube.cxx $(ROOTLIB)

CubeHist.o: CubeHist.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeHist.cxx $(ROOTLIB)

CubeHistEss.o: CubeHistEss.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeHistEss.cxx $(ROOTLIB)

tCalib.o: tCalib.cxx
	$(CPP) -lm $(FLAGS) -c tCalib.cxx $(ROOTLIB)

CubeGate.o: CubeGate.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeGate.cxx $(ROOTLIB)

CubeSort.o: CubeSort.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeSort.cxx $(ROOTLIB)

CubeLoss.o: CubeLoss.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeLoss.cxx $(ROOTLIB)

CubeFis.o: CubeFis.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeFis.cxx $(ROOTLIB)

CubeDet.o: CubeDet.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeDet.cxx $(ROOTLIB)

CubeReact.o: CubeReact.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeReact.cxx $(ROOTLIB)
	
Scattering.o: Scattering.cxx
	$(CPP) $(LIB) $(FLAGS) -c Scattering.cxx $(ROOTLIB)

CubeInput.o: CubeInput.cxx
	$(CPP) $(LIB) $(FLAGS) -c CubeInput.cxx $(ROOTLIB)

CubeXsec_etw0717.o: CubeXsec_etw0717.cxx
	$(CPP) -lm $(FLAGS) -c CubeXsec_etw0717.cxx $(ROOTLIB)

Nucleus.o: Nucleus.cxx
	$(CPP) $(LIB) $(FLAGS) -c $(PWD)/Nucleus.cxx $(ROOTLIB)

Reaction.o: Reaction.cxx
	$(CPP) $(LIB) $(FLAGS) -c $(PWD)/Reaction.cxx $(ROOTLIB)

plot_kin.o: plot_kin.cxx
	$(CPP) $(LIB) $(FLAGS) -c plot_kin.cxx $(ROOTLIB)

eloss.o: eloss.cxx
	$(CPP) -lm $(FLAGS) -c eloss.cxx $(ROOTLIB)

MassTable.o: MassTable.cpp
	$(CPP) -lm $(FLAGS) -c $(PWD)/MassTable.cpp $(ROOTLIB)

.PHONY : clean
clean :
	rm dacube $(OBJS)




