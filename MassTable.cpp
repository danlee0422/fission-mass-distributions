//MassTable.cpp : my first foray into classes. Used in dacube for Q value, Excitation energy calculations.
//returns an array with mass excess values for all A,Z values available in mass.mas03 from NNDC
//REQUIRES A MODIFIED FORMAT OF mass.mas03, because I suck at FORTRAN->c++ formatting.
// by Liz Williams
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include "MassTable.h"

using namespace std;


//constructor: initializes all variables
MassTable::MassTable()
{

    //std::cout << "Constructing MassTable" << std::endl;
    maxN=getMaxN();
    maxZ=getMaxZ();

    for (int i=0;i<maxN;i++){
        for (int j=0;j<maxZ;j++){
            massExcess[i][j]=0;
        }
    }

    MassTable::readMassTable();

}


//function for reading in the MassTable:
int MassTable::readMassTable(){

    ifstream input;
    string header;
    int N=0;
    int Z=0;
    float delta=0;
    int maxN=MassTable::getMaxN();
    int maxZ=MassTable::getMaxZ();

    //cout << maxN << " " << maxZ << endl;

    // let's grab the path here
    std::string filepath(__FILE__);
    size_t pos = filepath.find_last_of("/");
    std::string mass_path = filepath.substr(0, pos) + "/mass03.etw";

    // input.open("src_3det_v5.0/mass03.etw");
    input.open(mass_path);
    //mass03.etw is my modified version of the NNDC mass.mas03; only has 6 word header, then columns of N, Z, delta[keV]

    if (!input.is_open()){
        cout << "Mass table input file (mass.mas03, modified for dacube) " <<
            "not found in working directory ." << endl;
        return -1;
    }


    input >> header >> header >> header >> header >> header >>  header;//ignores header

    while (!input.eof())
    {
        input >> N >> Z >> delta;
        if (N>(maxN-1)||Z>(maxZ-1))
        {
            cout << "Error: MassTable N,Z values exceed maximum possible values: "
                << N << "," << Z << endl;
            return -1;
        }
        else if (N==0&&Z==0)
        {
            cout << "Error: There are no nucleons in your nucleus!"
                << endl;
            return -1;
        }
        else
        {

            massExcess[N][Z]=delta/1000.; //conversion to MeV
        }
    }
    //cout << "Mass table object now defined." << endl;
    return 0;
}



//function for getting Mass excess values here
float MassTable::getMassExcess(int A, int Z)
{
    float excess;
    int maxN=getMaxN();
    int maxZ=getMaxZ();

    //cout << "Now getting mass excess for A,Z = " << A << ", " << Z << " = ";

    if ( A<=(maxN+maxZ) && (Z<=maxZ) ){
        excess = massExcess[(A-Z)][Z]; //returnsMassExcess in MeV
        //cout << excess << endl;
    }
    else
    {
        cout << "Invalid fission fragment (A,Z) = (" << A << ", " << Z << "); massExcess set to zero" << endl;
        excess = 0.;
    }
    return excess;
}


float MassTable::calcQval(int A1, int Z1, int A2, int Z2, int A3, int Z3, int A4, int Z4){

    float Qvalgs=0.0;

    if (A4>0){
        Qvalgs-=MassTable::getMassExcess(A4,Z4);
    }
    if (A3>0){
        Qvalgs-=MassTable::getMassExcess(A3,Z3);
    }
    if (A2>0){
        Qvalgs+=MassTable::getMassExcess(A2,Z2);
    }
    if (A1>0){
        Qvalgs+=MassTable::getMassExcess(A1,Z1);
    }

    return Qvalgs;

}

/*
   int main(){

   int A, Z;
   float delta;
   MassTable m;

   m.readMassTable();

   A=40;
   Z=20;
   cout << "Getting mass excess for 40Ca..." << endl;
   delta = m.getMassExcess(A,Z);
   cout << "The mass excess for 40Ca is " << delta << " MeV" << endl;
   return 0;

   }*/
