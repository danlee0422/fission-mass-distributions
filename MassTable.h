#ifndef MassTable_h
#define MassTable_h

//MassTable class header: for reading mass table from file
// Liz Williams, August 2014

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <string>

class MassTable
{
    public:

        MassTable();
        int getMaxZ(){
            //  return 119; //Original
            return 119;
        }
        int getMaxN(){
            return 183;
        }

        float getMassExcess(int,int);
        float calcQval(int,int,int,int,int,int,int,int); //A1(Z1)+Z2(Z2)->A3(Z3)+A4(Z4); set A, Z to zero to ignore one
        int readMassTable();

    protected:
        //protected variables
        float massExcess[183][119];
        int maxN;
        int maxZ;
};



#endif
