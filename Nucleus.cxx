// Nucleus.cxx

// See Nucleus.h for explanations

#include "Nucleus.h"
#include <sstream>

using namespace std;

Nucleus::Nucleus() {
    // default ctor
}

Nucleus::Nucleus(int A, int Z) {
    mA = A;
    mZ = Z;
    mElement = "";
    mZKnown = MatchElementZ(mZ,mElement);
    mExcitationEnergy = 0;
    if (mZKnown)
        GetParameters();
    if (mZ>112) {
        std::stringstream buf;
        buf << mZ;
        mElement = buf.str();
        buf.str("");
    }
}

Nucleus::Nucleus(int A, std::string Element) {
    mA = A;
    mZ = 0;
    mElement = Element;
    mZKnown = MatchElementZ(mZ,mElement);
    mExcitationEnergy = 0;
    if (mZKnown)
        GetParameters();
}

Nucleus::Nucleus(int Z) {
    if (Z>0) {
        mA = GetAFromZ(Z,kBetaMass);
        mZ = Z;
        mElement = "";
        mZKnown = MatchElementZ(mZ,mElement);
        mExcitationEnergy = 0;
    }
    else
        mZKnown = false;
    if (mZKnown)
        GetParameters();
}

Nucleus::Nucleus(std::string Symbol) {
    //std::cout<<"OLA"<<endl;

    unsigned int i1 = Symbol.size();
    unsigned int i2 = 0;
    std::string tmp;
    for (unsigned int i=0; i<Symbol.size(); i++) {
        if (isdigit(Symbol[i])) {
            i < i1 ? i1 = i : 0;  // i1 = min(i1,i)
            i > i2 ? i2 = i : 0;  // i2 = max(i2,i)
        }
    }
    tmp = Symbol.substr(i1,i2-i1+1);
    mA = atoi(tmp.c_str());
    mZ = 0;
    mElement = i1==0 ? Symbol.substr(i2+1,Symbol.size()-i2+1) : Symbol.substr(0,i1);

    //std::cout<<"mElement = "<<mElement<<endl;
    mZKnown = MatchElementZ(mZ,mElement);

    // std::cout<<"mZKnown = "<<mZKnown<<endl;

    mExcitationEnergy = 0;
    if (mZKnown)
        GetParameters();
}


Nucleus::~Nucleus() {
    // default dtor
}


Nucleus& Nucleus::operator=(const Nucleus& rhs)
{
    if (this == &rhs) {
        return *this;
    }
    mA = rhs.GetA();
    mZ = rhs.GetZ();
    mElement = rhs.GetElement();
    mMassExcess = rhs.GetMassExcess();
    mBindingEnergy = rhs.GetBindingEnergy();
    mAtomicMass = rhs.GetMass();
    mExcitationEnergy = rhs.GetExcitEnergy();
    return *this;
}


Nucleus Nucleus::operator+(const Nucleus& rhs)
{
    Nucleus& lhs = *this;
    int atot = lhs.GetA() + ((Nucleus &) rhs).GetA();
    int ztot = lhs.GetZ() + rhs.GetZ();
    float extot = lhs.GetExcitEnergy() + rhs.GetExcitEnergy();
    Nucleus temp(atot, ztot);
    float estar = extot + lhs.GetMassExcess() + rhs.GetMassExcess() - temp.GetMassExcess();
    temp.SetExcitEnergy(estar);
    return temp;
}

Nucleus Nucleus::operator-(const Nucleus& rhs)
{
    Nucleus& lhs = *this;
    int zres = lhs.GetZ() - rhs.GetZ();
    int ares = lhs.GetA() - ((Nucleus &) rhs).GetA();
    float exres = lhs.GetExcitEnergy() - rhs.GetExcitEnergy();
    if (zres <= 0 || ares <= 0) {
        printf("Cannot subtract nuclei, resulting A=%i, Z=%i",ares,zres);
        Nucleus temp(0,0);
        temp.SetExcitEnergy(0.0);
        return temp;
    } else {
        Nucleus temp(ares, zres);
        temp.SetExcitEnergy(exres);
        return temp;
    }
}


float Nucleus::kAMU = 931.494043;
float Nucleus::kAMU_SI = 1.66054e-27;
float Nucleus::kkeV_SI = 1.782662e-33;
float Nucleus::kMeV_SI = 1.e3*Nucleus::kkeV_SI;

float Nucleus::u() {
    return kAMU;
}


int Nucleus::GetA() const {
    return mA;
}


int Nucleus::GetZ() const {
    return mZ;
}


int Nucleus::GetN() const {
    return mA-mZ;
}


std::string Nucleus::GetElement() const {
    return mElement;
}


std::string Nucleus::GetNucleus() const {
    std::stringstream temp;
    temp << mA;
    return temp.str() + mElement;
}

float Nucleus::GetMassExcess() const {
    // Mass excess in keV
    return mMassExcess;
}

float Nucleus::GetBindingEnergy() const {
    // Binding energy in keV / A
    return mBindingEnergy;
}

float Nucleus::GetMass() const {
    // Mass in keV
    return mAtomicMass * 1.e-3 * kAMU;
}

float Nucleus::GetExcitEnergy() const {
    return mExcitationEnergy;
}

bool Nucleus::IsKnown() const {
    return mNucleusKnown;
}


bool Nucleus::MatchElementZ(int &Z, std::string &element) {
    bool val;
    //std::cout<<"numberofelements = "<<NumberOfElements<<endl;
    /*std::string periodic_table[NumberOfElements] = {"H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe","Cs", "Ba","La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb","Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn","Fr", "Ra","Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No","Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", "Uut", "Uuq", "Uup", "Uuh", "Uus", "Uuo" };*/
    std::string periodic_table[NumberOfElements] = {
      "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne",
     "Na", "Mg", "Al", "Si", "P", "S", "Cl","Ar", "K", "Ca",
     "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
      "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr",
      "Nb", "Mo",
     "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te",
     "I", "Xe","Cs", "Ba","La", "Ce", "Pr", "Nd", "Pm", "Sm",
      "Eu", "Gd", "Tb", "Dy", "Ho",  "Er", "Tm", "Yb","Lu", "Hf",
      "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb",
      "Bi", "Po", "At", "Rn","Fr", "Ra","Ac","Th", "Pa", "U",
      "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No",
      "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn",
      "Nh", "Fl", "Mc", "Lv", "Ts", "Og" ,"119", "120"};

    if (element=="") {
        // return element name
        if ((Z>NumberOfElements)||(Z<0)) {
            val=false;
        }
        else {

            //std::cout<<"Z = "<<endl;
            element = periodic_table[Z-1];
        }
    }
    else if (element=="p") {
        Z = 1;
    }
    else {
        // return atomic number
        for (int i=0; i<NumberOfElements; i++) {

            //std::cout<<"noelements checked : "<<(i+1)<<" periodictable("<<i<<") = "<<periodic_table[i]<<" size = "<<periodic_table[i].size()<<endl;

            //std::cout<<"element = "<<element<<" size = "<<element.size()<<endl;

            if (periodic_table[i]==element){
                //std::cout<<"SUCCESS"<<endl;
                Z = i+1;
            }

        }
    }
    //if ((Z>0)&&(Z<119)) //original
    if ((Z>0)&&(Z<500))
        val=true;
    else
        val=false;
    return val;
}


void Nucleus::Print() const {
    if (mNucleusKnown) {
        printf("=====================================\n");
        printf("Details for %i%s\n",GetA(),GetElement().c_str());
        printf("Mass excess    = %11.5f MeV\n",GetMassExcess()*1.e-3);
        printf("Binding energy = %11.5f MeV / A\n",GetBindingEnergy()*1.e-3);
        printf("Atomic mass    = %11.5f MeV\n",GetMass()*1.e-3);
        printf("E*             = %11.5f MeV\n",GetExcitEnergy()*1.e-6);
        printf("=====================================\n");
    }
    return;
}

void Nucleus::SetExcitEnergy(float Ex) {
    // Excitation energy Ex in MeV
    mExcitationEnergy = Ex*1.0e3;
}

void Nucleus::SetA(int A) {
    mA = A;
    GetParameters();
}

void Nucleus::SetZ(int Z, bool AFromZ) {
    mZ = Z;
    mElement = "";
    mZKnown = MatchElementZ(mZ,mElement);
    if (mZKnown) {
        if (AFromZ)
            mA = GetAFromZ(Z,kBetaMass);
        GetParameters();
    }
}

void Nucleus::SetZ(std::string Element, bool AFromZ) {
    mZ = 0;
    mElement = Element;
    mZKnown = MatchElementZ(mZ,mElement);
    if (mZKnown) {
        if (AFromZ)
            mA = GetAFromZ(mZ,kBetaMass);
        GetParameters();
    }
}

void Nucleus::SetAZ(int A, int Z) {
    mA = A;
    mZ = Z;
    mElement = "";
    mZKnown = MatchElementZ(mZ,mElement);
    if (mZKnown)
        GetParameters();
}

void Nucleus::SetAZ(int A, std::string Element) {
    mA = A;
    mZ = 0;
    mElement = Element;
    mZKnown = MatchElementZ(mZ,mElement);
    if (mZKnown)
        GetParameters();
}

void Nucleus::SetNucleus(std::string Symbol) {
    unsigned int i1 = Symbol.size();
    unsigned int i2 = 0;
    std::string tmp;
    for (unsigned int i=0; i<Symbol.size(); i++) {
        if (isdigit(Symbol[i])) {
            i < i1 ? i1 = i : 0;  // i1 = min(i1,i)
            i > i2 ? i2 = i : 0;  // i2 = max(i2,i)
        }
    }
    tmp = Symbol.substr(i1,i2-i1+1);
    mA = atoi(tmp.c_str());
    mZ = 0;
    mElement = i1==0 ? Symbol.substr(i2+1,Symbol.size()-i2+1) : Symbol.substr(0,i1);
    mZKnown = MatchElementZ(mZ,mElement);
    mExcitationEnergy = 0;
    if (mZKnown)
        GetParameters();
}

int Nucleus::GetAFromZ(int Z, int mt) {
    int A;
    switch (Z) {
        case 1 :
            A = 1;
            break;
        case 2 :
            A = 4;
            break;
        case 3 :
            A = 7;
            break;
        case 4 :
            A = 9;
            break;
        case 5 :
            A = 11;
            break;
        case 13 :
            A = 27;
            break;
        default :
            A = Nucleus::GetRealAFromZ(Z,mt) + 1;
    }
    return A;
}

int Nucleus::GetRealAFromZ(int Z, int mt) {
    int A;
    switch (mt) {
        case kVedaMass :
            A = (int)(1.867*Z+0.016*Z*Z-1.07e-4*Z*Z*Z);
            break;
        case kBetaMass :
            A = 0.2875+1.7622*Z+0.013879*Z*Z-0.000054875*Z*Z*Z;
            break;
        case kEALMass :
            A = 2.072*Z+2.32e-3*Z*Z;
            break;
        case kEALResMass :
            A = 2.045*Z+3.57e-3*Z*Z;
            break;
        default :
            A = 2*Z;
    }
    return A;
}

void Nucleus::GetParameters(std::string filename) {
    // Check if mass file exists, download if necessary
    int ret=0;
    ret=ret;
    // let's grab the path here
    std::string filepath(__FILE__);
    size_t pos = filepath.find_last_of("/");
    mMassFilePath = filepath.substr(0, pos);

    std::string location_file = mMassFilePath + "/" + filename;

    // Read in input file
    std::string line;
    std::ifstream inputfile(location_file.c_str(),std::ios::in);
    int l = 0;
    int n=0,z=0,a=0;
    n=n,z=z;a=a;
    float massx,err_massx;
    float be,err_be,amass;
    (void) err_massx;
    (void) err_be;

    std::string str_massx,str_err_massx;
    std::string str_be,str_err_be,str_amass;

    std::string el;
    bool kFound = false;
    while (!inputfile.eof()) {
        getline(inputfile,line);
        if (l>38) {
            if (line.length()>10) {
                n = atoi(line.substr(6,3).c_str());
                z = atoi(line.substr(11,3).c_str());
                a = atoi(line.substr(16,3).c_str());
                el = line.substr(20,2);
                for (unsigned int i = 0; i<el.length(); i++) {
                    if (el[i]==' ')
                        el.erase(i,1);
                }
                str_massx = line.substr(29,12);
                str_err_massx = line.substr(42,10);
                for (unsigned int i = 0; i<str_massx.length(); i++) {
                    if (str_massx[i]=='#')
                        str_massx[i]='.';
                }
                for (unsigned int i = 0; i<str_err_massx.length(); i++) {
                    if (str_err_massx[i]=='#')
                        str_err_massx[i]='.';
                }
                massx = atof(str_massx.c_str());
                err_massx = atof(str_err_massx.c_str());
                str_be = line.substr(54,9);
                str_err_be = line.substr(65,7);
                for (unsigned int i = 0; i<str_be.length(); i++) {
                    if (str_be[i]=='#')
                        str_be[i]='.';
                }
                for (unsigned int i = 0; i<str_err_be.length(); i++) {
                    if (str_err_be[i]=='#')
                        str_err_be[i]='.';
                }
                str_amass = line.substr(96,16);
                for (unsigned int i = 0; i<str_amass.length(); i++) {
                    if (str_amass[i]=='#')
                        str_amass[i]='.';
                    if (str_amass[i]==' ')
                        str_amass.erase(i,1);
                }
                be = atof(str_be.c_str());
                err_be = atof(str_err_be.c_str());
                amass = atof(str_amass.c_str());
                if ((z==mZ)&&(a==mA)) {
                    mMassExcess=massx;
                    mBindingEnergy=be;
                    mAtomicMass = amass;
                    kFound = true;
                    mNucleusKnown = true;
                }
            }
        }
        l++;
    }
    inputfile.close();
    if (kFound == false) {
        std::cout << "(WW) Nucleus not found in mass table." << std::endl;
        mNucleusKnown = false;
        mMassExcess = 0.0;
        mBindingEnergy = 0.0;
        mAtomicMass = 0.0;
    }
    return;
}

bool Nucleus::LocationExists(std::string &location) {
    bool val;
    struct stat tmp;
    int res = stat(location.c_str(),&tmp);
    if (res==0)
        val = true;
    else val = false;
    return val;

}
