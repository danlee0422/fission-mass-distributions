//Nucleus.h

//
// Class Nucleus: Define nucleus in terms of mass A and charge Z or Element symbol. 
// Get mass excess, binding energy, mass, symbol from file mass.mas03 from National Nuclear Data Center (NNDC)
// Overladed operators =,+,- to allow for "Nucleus arithmetics"
// 
// Author:  Maurits Evers
// Version: 1.0
//

#ifndef _NUCLEUS_H_
#define _NUCLEUS_H_

#include <iostream>
#include <sstream>          // for stringstreams
#include <fstream>          // for file input
#include <cstdlib>          // for atoi,atof
#include <sys/stat.h>       // for file checking
#include <unistd.h>
#include <stdio.h>
class Nucleus {

    public:

        // ctors
        Nucleus();
        Nucleus(int A, int Z);
        Nucleus(int A, std::string Element);
        Nucleus(int Z);
        Nucleus(std::string Symbol);

        // dtor
        ~Nucleus();

        Nucleus& operator=(const Nucleus& rhs);
        Nucleus operator+(const Nucleus& rhs);
        Nucleus operator-(const Nucleus& rhs);

        // Statics
        static float kAMU;
        static float kAMU_SI;
        static float kkeV_SI;
        static float kMeV_SI;
        static float u();

        // Inspectors
        int GetA() const;
        int GetZ() const;
        int GetN() const;
        std::string GetElement() const;
        std::string GetNucleus() const;
        float GetMassExcess() const;
        float GetBindingEnergy() const;
        float GetMass() const;
        float GetExcitEnergy() const;
        bool IsKnown() const;
        void Print() const;

        // Mutators
        void SetExcitEnergy(float Ex);
        void SetA(int Z);
        void SetZ(int Z, bool AFromZ = false);
        void SetZ(std::string Element, bool AFromZ = false);
        void SetAZ(int A, int Z);
        void SetAZ(int A, std::string Element);
        void SetNucleus(std::string Symbol);

    private:

        enum {NumberOfElements = 120, kBetaMass, kVedaMass, kEALMass, kEALResMass, kEPAXMass};

        int mA,mZ;
        std::string mElement;
        std::string mMassFilePath;
        float mMassExcess,mBindingEnergy,mAtomicMass;
        float mExcitationEnergy;
        bool mZKnown,mNucleusKnown;

        int GetAFromZ(int Z, int mt);
        int GetRealAFromZ(int Z, int mt);
        bool MatchElementZ(int &Z, std::string &element);
        void GetParameters(std::string filename = "mass.mas03");
        bool LocationExists(std::string &location);
};

#endif // end _NUCLEUS_H_
