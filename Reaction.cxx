// Reaction.cxx

// See Reaction.h for explanations

#include <sstream>
#include <math.h>
#include <cstdio> // for gcc >= 4.4
#include "Reaction.h"

using namespace std;

Reaction::Reaction() {
    mNucleus1 = NULL;
    mNucleus3 = NULL;
    mNucleus2 = NULL;
    mNucleus4 = NULL;
    mInitializedNuclei = 0;
    Initialize();
}

Reaction::Reaction(Nucleus *n1, Nucleus *n2, Nucleus *n3, Nucleus *n4) {
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = n3;
    mNucleus4 = n4;
    mInitializedNuclei = 0;
    Initialize();
}

Reaction::Reaction(Nucleus *n1, Nucleus *n2) {
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = n1;
    mNucleus4 = n2;
    mInitializedNuclei = 0;
    Initialize();
}

Reaction::Reaction(Nucleus *n1, Nucleus *n2, Nucleus *n3) {
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = n3;
    mNucleus4 = new Nucleus(mNucleus1->GetA()+mNucleus2->GetA()-mNucleus3->GetA(),mNucleus1->GetZ()+mNucleus2->GetZ()-mNucleus3->GetZ());
    mInitializedNuclei = 1;
    Initialize();
}

Reaction::Reaction(Nucleus *n1, Nucleus *n2, int DeltaN, int DeltaZ) {
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = new Nucleus(mNucleus1->GetA()+DeltaN+DeltaZ,mNucleus1->GetZ()+DeltaZ);
    mNucleus4 = new Nucleus(mNucleus2->GetA()-DeltaN-DeltaZ,mNucleus2->GetZ()-DeltaZ);
    mInitializedNuclei = 2;
    Initialize();
}

Reaction::Reaction(Nucleus *n1, Nucleus *n2, const std::string &transfer) {
    mNucleus1 = n1;
    mNucleus2 = n2;
    std::string temp = transfer;
    int DeltaN = 0;
    int DeltaZ = 0;
    ConvertDeltaNDeltaZTransfer(temp,DeltaN,DeltaZ);
    mNucleus3 = new Nucleus(mNucleus1->GetA()+DeltaN+DeltaZ,mNucleus1->GetZ()+DeltaZ);
    mNucleus4 = new Nucleus(mNucleus2->GetA()-DeltaN-DeltaZ,mNucleus2->GetZ()-DeltaZ);
    mInitializedNuclei = 2;
    Initialize();
}

Reaction::Reaction(const std::string &n1, const std::string &n2, const std::string &n3, const std::string &n4) {
    mNucleus1 = new Nucleus(n1);
    mNucleus2 = new Nucleus(n2);
    mNucleus3 = new Nucleus(n3);
    mNucleus4 = new Nucleus(n4);
    mInitializedNuclei = 4;
    Initialize();
}

Reaction::Reaction(const std::string &n1, const std::string &n2) {
    mNucleus1 = new Nucleus(n1);
    mNucleus2 = new Nucleus(n2);
    mNucleus3 = new Nucleus(n1);
    mNucleus4 = new Nucleus(n2);
    mInitializedNuclei = 4;
    Initialize();
}

Reaction::Reaction(const std::string &n1, const std::string &n2, const std::string &n3) {
    mNucleus1 = new Nucleus(n1);
    mNucleus2 = new Nucleus(n2);
    mNucleus3 = new Nucleus(n3);
    mNucleus4 = new Nucleus(mNucleus1->GetA()+mNucleus2->GetA()-mNucleus3->GetA(),mNucleus1->GetZ()+mNucleus2->GetZ()-mNucleus3->GetZ());
    mInitializedNuclei = 4;
    Initialize();
}

Reaction::Reaction(const std::string &str_reaction) {
    mInitializedNuclei = 0;
    std::string str_n1,str_n2,str_n3,str_n4;
    ConvertReactionString(str_reaction,str_n1,str_n2,str_n3,str_n4);
    this->SetReaction(str_n1,str_n2,str_n3);
    Initialize();
}

Reaction::~Reaction() {
    // default dtor
    CleanUp();
}

float Reaction::kESQUARED = 1.43996;
float Reaction::kHBARC = 197.327;
float Reaction::kAMU = 931.49;

bool Reaction::IsValid() const {
    return ((mNucleus1->GetZ()+mNucleus2->GetZ()==mNucleus3->GetZ()+mNucleus4->GetZ())&&(mNucleus1->GetN()+mNucleus2->GetN()==mNucleus3->GetN()+mNucleus4->GetN()));
}

int Reaction::GetDeltaN() const {
    return mNucleus3->GetN()-mNucleus1->GetN();
}

int Reaction::GetDeltaZ() const {
    return mNucleus3->GetZ()-mNucleus1->GetZ();
}

float Reaction::GetDeltaRelativeMasses() const {
    float val;
    float mu_i = (mNucleus1->GetA()*mNucleus2->GetA())/(1.0*mNucleus1->GetA()+mNucleus2->GetA());
    int DN = GetDeltaN();
    int DZ = GetDeltaZ();
    int n = 0;
    int m = 0;
    if (DN<0)
        n+=abs(DN);
    else
        m+=DN;
    if (DZ<0)
        n+=abs(DZ);
    else
        m+=DZ;
    float mu_f1 = n*(mNucleus2->GetA()-m)/(1.0*mNucleus4->GetA());
    float mu_f2 = m*(mNucleus1->GetA()-n)/(1.0*mNucleus3->GetA());
    val = mu_f1/mu_i+mu_f2/mu_i;
    return val;
}

float Reaction::GetQ() const {
    // Q value in keV of reaction (takes care of excitations in nuclei)
    return GetQgs()+mNucleus1->GetExcitEnergy()+mNucleus2->GetExcitEnergy()-mNucleus3->GetExcitEnergy()-mNucleus4->GetExcitEnergy();
}

float Reaction::GetQgs() const {
    // gs Q value in keV of the reaction
    return mNucleus1->GetMassExcess()+mNucleus2->GetMassExcess()-mNucleus3->GetMassExcess()-mNucleus4->GetMassExcess();
}

float Reaction::GetQeff() const {
    // gs Qeff value in keV of reaction
    // Qeff definition from Brink, PL40B,37(1972)
    return GetQ() - (mNucleus4->GetZ()*mNucleus3->GetZ() - mNucleus2->GetZ()*mNucleus1->GetZ())*kESQUARED*1.0e3 / (1.2 * (pow(mNucleus2->GetA(),1./3.)+pow(mNucleus1->GetA(),1./3.)));
}

float Reaction::GetQopt(int type) {
    // optimum Q value in keV of reaction
    // type = 0 : Buttle, Goldfarb, NPA176,229(1971) (no recoil)
    // type = 1 : Schiffer et al., PLB44,47(1973) (with recoil) --- NOT IMPLEMENTED ---
    // type = 2 : Wilczynski et al., PRC39,2475(1989)
    // type = 3 : Siemens et al., PLB36,24(1971) (includes recoil)
    float val = 0;
    if (mInputEnergy!=noEnergyGiven) {
        switch (type) {
            case 0 :
                val = GetEProjectile(0)*(mNucleus3->GetZ()*mNucleus4->GetZ()/(1.0*mNucleus1->GetZ()*mNucleus2->GetZ())-1.0);
                break;
            case 1 : {
                         //      float rmin = mNucleus1->GetZ()*mNucleus2->GetZ()/(2*GetEProjectile(0))*(1.0+1.0/sin(mThetacm/2.0));
                         float xi = 2.0/(1+1.0/sin(mThetacm/2.0));
                         float di = mNucleus1->GetZ()*mNucleus2->GetZ()/(GetEProjectile(0)*xi);
                         float df = mNucleus3->GetZ()*mNucleus4->GetZ()/(GetEProjectile(0)*xi);
                         val = GetEProjectile(0)*xi*(mNucleus3->GetZ()*mNucleus4->GetZ()/(1.0*mNucleus1->GetZ()*mNucleus2->GetZ())/(1+(df-di)/di)-1.0);
                         val = 0.0;
                         break;
                     }
            case 2 : {
                         float mu_i = (mNucleus1->GetA()*mNucleus2->GetA())/(1.0*mNucleus1->GetA()+mNucleus2->GetA());
                         int DN = GetDeltaN();
                         int DZ = GetDeltaZ();
                         int n = 0;
                         int m = 0;
                         if (DN<0)
                             n+=abs(DN);
                         else
                             m+=DN;
                         if (DZ<0)
                             n+=abs(DZ);
                         else
                             m+=DZ;
                         float mu_f1 = n*(mNucleus2->GetA()-m)/(1.0*mNucleus4->GetA());
                         float mu_f2 = m*(mNucleus1->GetA()-n)/(1.0*mNucleus3->GetA());
                         val = GetDeltaVb(1) - (GetEProjectile(0)-GetVb(1))*(mu_f1/mu_i+mu_f2/mu_i); 
                         break;
                     }
            case 3 : {
                         float vAa = sqrt(2*GetEProjectile(1)*1.e-3/(mNucleus1->GetA()*kAMU));
                         int DN = GetDeltaN();
                         int DZ = GetDeltaZ();
                         int n = 0;
                         int m = 0;
                         if (DN<0)
                             n+=abs(DN);
                         else
                             m+=DN;
                         if (DZ<0)
                             n+=abs(DZ);
                         else
                             m+=DZ;
                         float vBb = (1.0 - 1.0*n/mNucleus4->GetA() - 1.0*m/mNucleus3->GetA()) * vAa;
                         float Ti = 0.5*mui()*vAa*vAa;
                         float Tf = 0.5*muf()*vBb*vBb;
                         val = Tf-Ti + GetDeltaVb(1);
                         break;
                     }
            default :
                     break;
        }
    }
    return val;
}

float Reaction::GetExopt() {
    // optimum Ex in keV
    //
    float val = 0;
    if (mInputEnergy!=noEnergyGiven) {
        val = GetQ() - GetQopt();
    }
    return val;
}

float Reaction::GetVb(int type, int mt) const {
    // Barrier energy of entrance channel in cm (mt = 0) or lab (mt = 1) in keV
    // type = 0 : Swiatecki et al., PRC71, 014602 (2005)
    // type = 1 : Christensen, Winther, PLB65, 19 (1976)
    float val;
    switch (mt) {
        case 0 :
            if (mBarrierGiven)
                val = mVbcm;
            else
                val = Vbi(type) * 1.e3;
            break;
        case 1 :
        default :
            if (mBarrierGiven)
                val = mVbcm * cm2lab();
            else
                val = Vbi(type) * cm2lab() * 1.e3;
            break;
    }
    return val;
}

float Reaction::GetDeltaVb(int type) const {
    // Barrier energy difference in keV between incoming and outgoing channel
    // type = 0 : Swiatecki et al., PRC71, 014602 (2005)
    // type = 1 : Christensen, Winther, PLB65, 19 (1976)
    return (Vbf(type)-Vbi(type)) * 1.e3;
}

float Reaction::GetEProjectile(int mt) {
    // energy in keV of ejectile in cm (mt = 0) or lab (mt = 1) frame
    float val;
    switch (mt) {
        case 0 :
            val = mE1 * lab2cm();
            break;
        case 1 :
        default :
            val = mE1;
            break;
    }
    return val;
}

float Reaction::GetEnergyPerNucleon() {
    // kinetic energy of nucleus per nucleon in keV/nucleon
    return mE1 / mNucleus1->GetA();
}

float Reaction::GetETarget(int mt) {
    // energy in keV of target in cm (mt = 0) or lab (mt = 1) frame
    float val;
    switch (mt) {
        case 0 :
            val = GetEProjectile(Reaction::kLabFrame) - GetEProjectile(Reaction::kCMFrame);
            break;
        case 1 :
        default :
            val = 0;
            break;
    }
    return val;
}

float Reaction::GetEEjectile(int mt) {
    // energy in keV of ejectile in cm (mt = 0) or lab (mt = 1) frame
    float val;
    // UpdateKinematics();
    switch (mt) {
        case 0 :
            val = mE3 * lab2cm();
            break;
        case 1 :
        default :
            val = mE3;
            break;
    }
    return val;
}

float Reaction::GetERecoil(int mt) {
    // recoil energy in keV in cm (mt = 0) or lab (mt = 1) frame
    float val;
    // UpdateKinematics();
    switch (mt) {
        case 0 :
            val = mE4 * lab2cm();
            break;
        case 1 :
        default :
            val = mE4;
            break;
    }
    return val;
}

float Reaction::GetTheta(int mt) {
    // scattering angle in rad in cm (mt = 0) or lab (mt = 1) frame
    float val;
    // if (mE1==0.0)
    //   UpdateKinematics();
    switch (mt) {
        case 0 :
            val = mThetacm;
            break;
        case 1 :
        default :
            val = mThetalab;
            break;
    }
    return val;
}

float Reaction::GetPhi(int mt) const {
    // recoil angle in rad in cm (mt = 0) or lab (mt = 1) frame
    float val;
    // UpdateKinematics();
    switch (mt) {
        case 0 :
            val = mPhicm;
            break;
        case 1 :
        default :
            val = mPhilab;
            break;
    }
    return val;
}

float Reaction::GetBrhoEjectile() {
    float val;
    std::cout << "E_projectile = " << Reaction::GetEProjectile(Reaction::kLabFrame)*1.e-3 << std::endl;
    std::cout << "E_ejectile = " << Reaction::GetEEjectile(Reaction::kLabFrame)*1.e-3 << std::endl;
    val = sqrt(2*mNucleus3->GetA()*kAMU*Reaction::GetEEjectile(Reaction::kLabFrame))/mNucleus3->GetZ();
    return val;
}

float Reaction::GetBrhoRecoil() {
    float val;
    val = sqrt(2*mNucleus4->GetA()*kAMU*Reaction::GetERecoil(Reaction::kLabFrame))/mNucleus4->GetZ();
    return val;
}

float Reaction::Getrmin() {
    // distance of closest approach for a particle on a Coulomb trajectory
    float val;
    val = Reaction::kESQUARED * mNucleus1->GetZ()*mNucleus2->GetZ()/(2.0*GetEProjectile(kCMFrame)*1.e-3)*(1.0+1.0/sin(GetTheta(kCMFrame)/2.0));
    return val;
}

float Reaction::Geteta() {
    // Sommerfeld parameter
    return etai();
}

float Reaction::Getk() {
    // Wave number in 1/fm
    float val = sqrt(2*mNucleus1->GetMass()*1.e-3*GetEProjectile(kLabFrame)*1.e-3)/kHBARC;
    return val;
}

float Reaction::Getb() {
    // Impact parameter assuming Coulomb trajectory
    //  float val = Geteta()/Getk()*cos(GetTheta(kCMFrame)/2.0)/sin(GetTheta(kCMFrame)/2.0);
    float val = mNucleus1->GetZ()*mNucleus2->GetZ()*Reaction::kESQUARED/2.0/(GetEProjectile(kCMFrame)*1.e-3)*1.0/tan(GetTheta(kCMFrame)/2.0);
    return val;
}

float Reaction::GetScalingPar() {
    // Scaling parameter p according to "Quantum world in nuclear physics", ...
    // p << 1 -> Fraunhofer scattering
    // p >= 1 -> Fresnel scattering
    float Rtarg = 1.2 * pow(mNucleus2->GetA(),1./3.);
    float a = pow(Getk()*Rtarg - Geteta(),2.0)/(2*Geteta()*Getk());
    float val = (Getk()*Rtarg-2*Geteta())*Rtarg/a;
    return val;
}

float Reaction::GetThetaGrazing() {
    // Grazing angle in deg (assuming Rutherford trajectories)
    float val = 0;

    float Rcrit = 1.2*(pow(mNucleus1->GetA(),1./3.)+pow(mNucleus2->GetA(),1./3.));
    float asinarg = Geteta()/(Getk()*Rcrit-Geteta());
    // while (asinarg>1)
    //   asinarg-=1;
    // while (asinarg<-1)
    //   asinarg+=1;
    val = 2*asin(asinarg)*R2D;
    // SATCHLER: DIRECT NUCLEAR REACTIONS, p. 394
    // val = 2.0*atan(Geteta()/(Getk()*GetbGrazing()))*R2D;
    return val;
}

float Reaction::GetbGrazing() {
    // Grazing (classical) impact parameter (assuming Rutherford trajectories)
    float val;
    val = Geteta()/Getk()*cos(GetThetaGrazing()/2.0)/sin(GetThetaGrazing()/2.0);
    // SATCHLER: DIRECT NUCLEAR REACTIONS, p. 394
    // val = GetLclassGrazing()/Getk();
    return val;
}

float Reaction::GetLclassGrazing() {
    // Classical grazing angular momentum in units of hbar
    float val = 0;
    float Rcrit = 1.2*(pow(mNucleus1->GetA(),1./3.)+pow(mNucleus2->GetA(),1./3.));
    val = Getk()*Rcrit*sqrt(1.0-2.0*Geteta()/(Getk()*Rcrit));
    return val;
}

float Reaction::GetlGrazing() {
    // Grazing angular momentum number
    float val = 0;
    // OLD METHOD
    float Lclas = sqrt(2*mNucleus1->GetMass()*1.e-3*GetEProjectile(kLabFrame)*1.e-3)*GetbGrazing();
    // unit of Lclas is fm * MeV / c
    float lquant = 0.5*sqrt(1.+4*pow(Lclas/kHBARC,2.0))-0.5;
    val = lquant;
    // SATCHLER: DIRECT NUCLEAR REACTIONS, p. 394
    // val = 0.5*sqrt(1.+4*pow(GetLclassGrazing(),2.0))-0.5;
    return val;
}

std::string Reaction::GetReaction(int fmt) {
    // reaction string either as "208Pb(16O,16O)208Pb"               fmt = 0
    //                        or "^{208}Pb(^{16}O,^{16}O)^{208}Pb"   fmt = 1
    std::string val;
    // UpdateKinematics();
    std::stringstream buf;
    if (fmt==0) {
        val = mNucleus2->GetNucleus()+"("+mNucleus1->GetNucleus()+","+mNucleus3->GetNucleus()+")"+mNucleus4->GetNucleus();
    }
    else {
        val="^{";
        buf << mNucleus2->GetA();
        val.append(buf.str());
        val+="}" + mNucleus2->GetElement() + "(^{";
        buf.str("");
        buf << mNucleus1->GetA();
        val.append(buf.str());
        val+="}" + mNucleus1->GetElement() + ",^{";
        buf.str("");
        buf << mNucleus3->GetA();
        val.append(buf.str());
        val+="}" + mNucleus3->GetElement() + ")^{";
        buf.str("");
        buf << mNucleus4->GetA();
        val.append(buf.str());
        val+="}" + mNucleus4->GetElement();
    }
    return val;
}

std::string Reaction::GetNucleonTransfer() {
    // nucleon transfer string like "+2p-1n"
    //  UpdateKinematics();
    int n_proton = mNucleus3->GetZ()-mNucleus1->GetZ();
    int n_neutron = mNucleus3->GetN()-mNucleus1->GetN();
    std::string temp="";
    ConvertDeltaNDeltaZTransfer(temp,n_neutron,n_proton);
    if (temp=="-2p-2n")
        temp="-alpha";
    return temp;
}

Nucleus* Reaction::GetProjectile() {
    return mNucleus1;
}

Nucleus* Reaction::GetTarget() {
    return mNucleus2;
}

Nucleus* Reaction::GetEjectile() {
    return mNucleus3;
}

Nucleus* Reaction::GetRecoil() {
    return mNucleus4;
}

void Reaction::Print(int mt) {
    // Print all information, mainly for debugging
    // UpdateKinematics();
    printf("\n");
    printf("Details for reaction %s\n",GetReaction().c_str());
    printf("Process: %s\n",GetNucleonTransfer().c_str());
    if (!IsValid()) 
        printf("Not a valid reaction\n");
    switch (mt) {
        case 0 :
            printf("All particle and barrier energies and angles in cm frame\n");
            break;
        case 1 :
        default :
            printf("All particle and barrier energies and angles in lab frame\n");
            break;
    }
    printf(" Ex (prj)   = %7.3f MeV\n",mNucleus1->GetExcitEnergy()*1.e-3);
    printf(" Ex (trg)   = %7.3f MeV\n",mNucleus2->GetExcitEnergy()*1.e-3);
    printf(" Ex (ejc)   = %7.3f MeV\n",mNucleus3->GetExcitEnergy()*1.e-3);
    printf(" Ex (res)   = %7.3f MeV\n",mNucleus4->GetExcitEnergy()*1.e-3);
    printf("Q values\n");
    printf(" Qgs value  = %7.3f MeV\n",GetQgs()*1.e-3);
    //  printf(" Qeff value = %7.3f MeV [Brink,PLB40,37(1972)]\n",GetQeff()*1.e-3);
    if (mInputEnergy!=noEnergyGiven) {
        printf(" (1) Qopt value = %7.3f MeV [Buttle, Goldfarb,NPA176,229(1971)]\n",GetQopt(0)*1.e-3);
        printf("     Ex        = %7.3f MeV [Toepffer, Z.Phys.253,78(1972)], = Qgs - Qopt\n",(GetQ()-GetQopt(0))*1.e-3);
        printf("     Ex_opt    = %7.3f MeV [ibidem], = (Qgs - Qopt) * M4/(M3+M4)\n",(GetQ()-GetQopt(0))*1.e-3*cm2labf());
        if (mInputAngle!=NoAngleGiven)
            printf("     Delta L   = %3i         [ibidem]\n",(int)DeltaL());
        // printf(" (2) Qopt value = %7.3f MeV [Schiffer et al.,PLB44,47(1973)]\n",GetQopt(1)*1.e-3);
        // printf("     Ex        = %7.3f MeV [ibidem], = Qgs - Qopt\n",(GetQ()-GetQopt(1))*1.e-3);
        // printf("     Ex_opt    = %7.3f MeV [ibidem], = (Qgs - Qopt) * M4/(M3+M4)\n",(GetQ()-GetQopt(1))*1.e-3*cm2labf());
        printf(" (3) Qopt value = %7.3f MeV [Wilczynski et al.,PRC39,2475(1989)]\n",GetQopt(2)*1.e-3);
        printf("     Ex        = %7.3f MeV = Qgs - Qopt\n",(GetQ()-GetQopt(2))*1.e-3);
        printf("     Ex_opt    = %7.3f MeV = (Qgs - Qopt) * M4/(M3+M4)\n",(GetQ()-GetQopt(2))*1.e-3*cm2labf());
        printf(" (4) Qopt value = %7.3f MeV [Siemens et al.,PLB36,24(1971)]\n",GetQopt(3)*1.e-3);
        printf("     Ex        = %7.3f MeV = Qgs - Qopt\n",(GetQ()-GetQopt(3))*1.e-3);
        printf("     Ex_opt    = %7.3f MeV = (Qgs - Qopt) * M4/(M3+M4)\n",(GetQ()-GetQopt(3))*1.e-3*cm2labf());
    }
    printf("Fusion barrier\n");
    printf(" Vb         = %7.3f MeV [Swiatecki et al.,PRC71,014602(2005)]\n",GetVb(0,mt)*1.e-3);
    printf(" Vb         = %7.3f MeV [Christensen,Winther,PLB65,19(1976)]\n",GetVb(1,mt)*1.e-3);
    printf("Projectile nucleus energies and angles\n");
    printf(" E1      = %9.3f MeV\n",GetEProjectile(mt)*1.e-3);
    printf(" E3      = %9.3f MeV\n",GetEEjectile(mt)*1.e-3);
    printf(" Theta   = %8.2f deg\n",GetTheta(mt)*R2D);
    printf("Target nucleus energies and angles\n");
    printf(" E2      = %9.3f MeV\n",GetETarget(mt)*1.e-3);
    printf(" E4      = %9.3f MeV\n",GetERecoil(mt)*1.e-3);
    printf(" Phi     = %8.2f deg\n",GetPhi(mt)*R2D);
    printf("\n");
}

void Reaction::SetTheta(float Theta, int mt) {
    // Set scattering angle in cm (mt = 0) or lab (mt = 1) frame, th in deg, mThetacm in rad
    switch (mt) {
        case 0 :
            mThetacm = Theta*D2R;
            mInputAngle = cmAngleGiven;
            break;
        case 1 :
            mThetalab = Theta*D2R;
            mInputAngle = labAngleGiven;
            break;
    }
    UpdateKinematics();
}

void Reaction::SetEProjectile(float E, int mt) {
    // Set projectile energy, E in MeV, mE1 in keV
    // mt = 0 -> E in cm frame
    // mt = 1 -> E in lab frame
    switch (mt) {
        case 0 :
            mE1 = E*1.e3 * cm2lab();
            break;
        case 1 :
        default :
            mE1 = E*1.e3;
            break;
    }
    mInputEnergy = EnergyGiven;
    UpdateKinematics();
}

void Reaction::SetVb(float Vb, int mt) {
    // Set barrier energy in cm (mt = 0) or lab (mt = 1) frame, Vb in MeV
    switch (mt) {
        case 0 :
            mVbcm = Vb;
            break;
        case 1 :
        default :
            mVbcm = Vb * lab2cm();
            break;
    }
    mBarrierGiven = true;
    return;
}

void Reaction::SetProjectile(Nucleus *n1) {
    // Set projectile nucleus
    mNucleus1 = n1;
}

void Reaction::SetReaction(Nucleus *n1, Nucleus *n2, Nucleus *n3, Nucleus *n4) {
    CleanUp();
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = n3;
    mNucleus4 = n4;
    mInitializedNuclei = 0;
    Initialize();
}

void Reaction::SetReaction(Nucleus *n1, Nucleus *n2) {
    CleanUp();
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = n1;
    mNucleus4 = n2;

    mInitializedNuclei = 0;
    Initialize();
}

void Reaction::SetReaction(Nucleus *n1, Nucleus *n2, Nucleus *n3) {
    CleanUp();
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = n3;
    mNucleus4 = new Nucleus(mNucleus1->GetA()+mNucleus2->GetA()-mNucleus3->GetA(),mNucleus1->GetZ()+mNucleus2->GetZ()-mNucleus3->GetZ());
    mInitializedNuclei = 1;
    Initialize();
}

void Reaction::SetReaction(const std::string &n1, const std::string &n2, const std::string &n3, const std::string &n4) {
    CleanUp();
    mNucleus1 = new Nucleus(n1);
    mNucleus2 = new Nucleus(n2);
    mNucleus3 = new Nucleus(n3);
    mNucleus4 = new Nucleus(n4);
    mInitializedNuclei = 4;
    Initialize();
}

void Reaction::SetReaction(const std::string &n1, const std::string &n2) {
    CleanUp();
    mNucleus1 = new Nucleus(n1);
    mNucleus2 = new Nucleus(n2);
    mNucleus3 = new Nucleus(n1);
    mNucleus4 = new Nucleus(n2);
    mInitializedNuclei = 4;
    Initialize();
}

void Reaction::SetReaction(const std::string &n1, const std::string &n2, const std::string &n3) {
    CleanUp();
    mNucleus1 = new Nucleus(n1);
    mNucleus2 = new Nucleus(n2);
    mNucleus3 = new Nucleus(n3);
    mNucleus4 = new Nucleus(mNucleus1->GetA()+mNucleus2->GetA()-mNucleus3->GetA(),mNucleus1->GetZ()+mNucleus2->GetZ()-mNucleus3->GetZ());
    mInitializedNuclei = 4;
    Initialize();
}

void Reaction::SetReaction(Nucleus *n1, Nucleus *n2, int DeltaN, int DeltaZ) {
    CleanUp();
    mNucleus1 = n1;
    mNucleus2 = n2;
    mNucleus3 = new Nucleus(mNucleus1->GetA()+DeltaN+DeltaZ,mNucleus1->GetZ()+DeltaZ);
    mNucleus4 = new Nucleus(mNucleus2->GetA()-DeltaN-DeltaZ,mNucleus2->GetZ()-DeltaZ);
    mInitializedNuclei = 2;
    Initialize();
}

void Reaction::SetReaction(Nucleus *n1, Nucleus *n2, const std::string &transfer) {
    CleanUp();
    mNucleus1 = n1;
    mNucleus2 = n2;
    std::string temp = transfer;
    int DeltaN = 0;
    int DeltaZ = 0;
    ConvertDeltaNDeltaZTransfer(temp,DeltaN,DeltaZ);
    mNucleus3 = new Nucleus(mNucleus1->GetA()+DeltaN+DeltaZ,mNucleus1->GetZ()+DeltaZ);
    mNucleus4 = new Nucleus(mNucleus2->GetA()-DeltaN-DeltaZ,mNucleus2->GetZ()-DeltaZ);
    mInitializedNuclei = 2;
    Initialize();
}

void Reaction::Initialize() {
    mE1 = 0.0;
    mE3 = 0.0;
    mE4 = 0.0;
    mThetalab = 0.0;
    mThetacm = 0.0;
    mPhilab = 0.0;
    mPhicm = 0.0;
    mInputAngle = NoAngleGiven;
    mInputEnergy = noEnergyGiven;
    mBarrierGiven = false;
    return;
}

void Reaction::CleanUp() {
    switch (mInitializedNuclei) {
        case 1 :
            delete mNucleus4;
            break;
        case 2 :
            delete mNucleus3;
            delete mNucleus4;
            break;
        case 4:
            delete mNucleus1;
            delete mNucleus2;
            delete mNucleus3;
            delete mNucleus4;
            break;
        default:
            break;
    }
    return;
}

void Reaction::UpdateKinematics() {
    // Do the math
    float m1 = mNucleus1->GetMass();
    float m2 = mNucleus2->GetMass();
    float m3 = mNucleus3->GetMass();
    float m4 = mNucleus4->GetMass();
    float q = GetQ();
    float et = mE1 + q;
    float a = m1*m4*mE1/et/((m1+m2)*(m3+m4));
    float b = m1*m3*mE1/et/((m1+m2)*(m3+m4));
    float c = m2*m3/((m1+m2)*(m3+m4))*(1+m1*q/m2/et);
    float d = m2*m4/((m1+m2)*(m3+m4))*(1+m1*q/m2/et);
    mE3 = 0;
    mE4 = 0;
    mPhicm = 0;
    mPhilab = 0;
    if (mE1>0) {
        if (mInputAngle==cmAngleGiven) {
            mE3 = (b+d+2*sqrt(a*c)*cos(mThetacm))*et;
            mPhicm = PI - mThetacm;
            mE4 = (a+c+2*sqrt(a*c)*cos(mPhicm))*et;
            mPhilab = asin(sqrt(m3*mE3/m4/mE4)*sin(mThetalab));
            mThetalab = asin(sin(mThetacm)/sqrt(mE3/et/d));
            if (mThetacm>PI/2.) 
                mThetalab = PI - mThetalab;
        }
        else {
            float db = d/b-sin(mThetalab)*sin(mThetalab);
            if (db<0) {
                mE3 = 0;
                mE4 = 0;
                mThetacm = 0;
                mPhicm = 0;
                mPhilab = 0;
            }
            else {
                //cout << db << endl;
                mE3 = b*pow((cos(mThetalab)+sqrt(db)),2)*et;
                mThetacm = asin(sqrt(mE3/et/d)*sin(mThetalab));
                if (mThetalab>PI/2.) 
                    mThetacm = PI - mThetacm;
                mPhicm = PI - mThetacm;
                //  mE4 = (a+c+2*sqrt(a*c)*cos(mPhicm))*et;
                mE4 = mE1 - mE3 + q;
                mPhilab = asin(sqrt(m3*mE3/m4/mE4)*sin(mThetalab));
            }
        }
    }
    return;
}

void Reaction::ConvertDeltaNDeltaZTransfer(std::string &transfer, int &DeltaN, int &DeltaZ) {
    if (transfer.length()<1) {
        std::string temp="";
        std::stringstream buf;
        if (DeltaZ!=0) {
            if (DeltaZ>0) 
                temp+="+";
            buf << DeltaZ;
            temp.append(buf.str());
            buf.str("");
            temp+="p";
        }
        if (DeltaN!=0) {
            if (DeltaN>0) 
                temp+="+";
            buf << DeltaN;
            temp.append(buf.str());
            buf.str("");
            temp+="n";
        }
        if ((DeltaN==0)&&(DeltaZ==0))
            temp="elastic scattering";
        transfer=temp;
    }
    else {
        if ((transfer=="el")||(transfer=="elastic scattering")) {
            DeltaN = 0;
            DeltaZ = 0;
        }
        size_t found;
        found=transfer.find("n");
        if (found!=std::string::npos)
            DeltaN = atoi((transfer.substr((int)found-2,2)).c_str());
        else
            DeltaN = 0;
        found=transfer.find("p");
        if (found!=std::string::npos)
            DeltaZ = atoi((transfer.substr((int)found-2,2)).c_str());
        else
            DeltaZ = 0;
    }
    return;
}

void Reaction::ConvertReactionString(const std::string &str_reaction, std::string &str_n1, std::string &str_n2, std::string &str_n3, std::string &str_n4) {
    // Convert reaction string to obtain Nuclei
    if (str_reaction.length()>1) {
        if (std::string::npos != str_reaction.find("+")) {
            int pos_plus = str_reaction.find_first_of("+");
            str_n1 = str_reaction.substr(0,pos_plus);
            str_n2 = str_reaction.substr(pos_plus+1,str_reaction.length());
            str_n3 = str_n1;
            str_n4 = str_n2;
        }
        else {
            int pos_bracket1 = str_reaction.find_first_of("(");
            str_n1 = str_reaction.substr(0,pos_bracket1);
            int pos_comma = str_reaction.find(',');
            str_n2 = str_reaction.substr(pos_bracket1+1,pos_comma-pos_bracket1-1);
            int pos_bracket2 = str_reaction.find_first_of(")");
            str_n3 = str_reaction.substr(pos_comma+1,pos_bracket2-pos_comma-1);
            str_n4 = str_reaction.substr(pos_bracket2+1,str_reaction.length()-pos_bracket2-1);
        }
    }
    return;
}

float Reaction::Vbi(int type) const {
    // Barrier energy in entrance channel in MeV (in cm frame)
    // type = 0 : Swiatecki et al., PRC71, 014602 (2005)
    // type = 1 : Christensen, Winther, PLB65, 19 (1976)
    float val = 0;
    switch (type) {
        case 0 : {
                     float z = mNucleus1->GetZ()*mNucleus2->GetZ()/(pow(mNucleus1->GetA(),(1.0/3.0))+pow(mNucleus2->GetA(),(1.0/3.0)));    
                     val = 0.85247 * z + 0.001361 * pow(z,2.0) - 0.00000223 * pow(z,3.0); 
                     break;
                 }
        case 1 : {
                     float Rb = 1.07*(pow(mNucleus1->GetA(),1./3.)+pow(mNucleus2->GetA(),1./3.)) + 2.72;
                     val = mNucleus1->GetZ()*mNucleus2->GetZ()*kESQUARED/Rb*(1.0-0.63/Rb);
                     break;
                 }
        default :
                 break;
    }
    return val;
}

float Reaction::Vbf(int type) const {
    // Barrier energy in exit channel in MeV (in cm frame)
    // type = 0 : Swiatecki et al., PRC71, 014602 (2005)
    // type = 1 : Christensen, Winther, PLB65, 19 (1976)
    float val = 0;
    switch (type) {
        case 0 : {
                     float z = mNucleus3->GetZ()*mNucleus4->GetZ()/(pow(mNucleus3->GetA(),(1.0/3.0))+pow(mNucleus4->GetA(),(1.0/3.0)));    
                     val = 0.85247 * z + 0.001361 * pow(z,2.0) - 0.00000223 * pow(z,3.0); 
                     break;
                 }
        case 1 : {
                     float Rb = 1.07*(pow(mNucleus3->GetA(),1./3.)+pow(mNucleus4->GetA(),1./3.)) + 2.72;
                     val = mNucleus3->GetZ()*mNucleus4->GetZ()*kESQUARED/Rb*(1.0-0.63/Rb);
                     break;
                 }
        default :
                 break;
    }
    return val;
}

float Reaction::mui() const {
    // Reduced mass in incoming channel
    return (mNucleus1->GetA()+mNucleus2->GetA())/(1.0*mNucleus1->GetA()*mNucleus2->GetA()) * kAMU;
}

float Reaction::muf() const {
    // Reduced mass in outgoing channel
    return (mNucleus3->GetA()+mNucleus4->GetA())/(1.0*mNucleus3->GetA()*mNucleus4->GetA()) * kAMU;
}

float Reaction::etai() {
    // Sommerfeld parameter in incoming channel
    return mNucleus1->GetZ()*mNucleus2->GetZ() * kESQUARED *sqrt(kAMU) / kHBARC * sqrt(1.0*mNucleus1->GetA()/(2.0*GetEProjectile(1)*1.e-3));
}

float Reaction::etaf() {
    // Sommerfeld parameter in outgoing channel
    // This assumes the trajectory matching of rmin = rmin' and theta = theta',
    // see Toepffer, Z.Phys.253,78(1972)
    return etai() * sqrt(muf()/mui() * mNucleus3->GetZ()*mNucleus4->GetZ() / (mNucleus1->GetZ()*mNucleus2->GetZ()));
}

float Reaction::DeltaL() {
    // Units of angular momentum that have to absorbed by the residual nuclei,
    // see Toepffer, Z.Phys.253,78(1972) for details
    float val = etai() - etaf();
    val = val * cos(mThetacm/2.0)/sin(mThetacm/2.0);
    return val;
}

float Reaction::lab2cm() const {
    return (float)mNucleus2->GetA() / (1.0 * mNucleus1->GetA() + mNucleus2->GetA());
}

float Reaction::cm2lab() const {
    return 1.0/lab2cm();
}

float Reaction::lab2cmf() const {
    return (float)mNucleus4->GetA() / (1.0 * mNucleus3->GetA() + mNucleus4->GetA());
}

float Reaction::cm2labf() const {
    return 1.0/lab2cmf();
}
