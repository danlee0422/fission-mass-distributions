//Reaction.h

//
// Class Reaction: Define reaction in terms of 4 nuclei
// Calculate Q value and Vb (see PRC71, 014602 (2005))
// Given beam energy and scattering angle (either in cm or lab) calcute kinematics (non-relativistic)
// 
// Author:  Maurits Evers
// Version: 1.0
//

#ifndef _REACTION_H_
#define _REACTION_H_

#include <iostream>
#include <math.h>
#include "Nucleus.h"

#define PI 3.141592654
#define R2D (180.0/PI)
#define D2R (PI/180.0)

class Reaction {

    public:

        // ctors
        Reaction();
        Reaction(Nucleus *n1, Nucleus *n2, Nucleus *n3, Nucleus *n4);
        Reaction(Nucleus *n1, Nucleus *n2);
        Reaction(Nucleus *n1, Nucleus *n2, Nucleus *n4);
        Reaction(Nucleus *n1, Nucleus *n2, int DeltaN, int DeltaZ);
        Reaction(Nucleus *n1, Nucleus *n2, const std::string &transfer);
        Reaction(const std::string &n1, const std::string &n2, const std::string &n3, const std::string &n4);
        Reaction(const std::string &n1, const std::string &n2);
        Reaction(const std::string &n1, const std::string &n2, const std::string &n3);
        Reaction(const std::string &str_reaction);

        // dtor
        ~Reaction();

        // Statics
        static float kESQUARED;
        static float kHBARC;
        static float kAMU;

        enum {kCMFrame,kLabFrame};

        // Inspectors
        bool IsValid() const;
        int GetDeltaN() const;
        int GetDeltaZ() const;
        float GetDeltaRelativeMasses() const;
        // Q values and optimum excitation energies
        float GetQ() const;
        float GetQgs() const;
        float GetQeff() const;
        float GetQopt(int type = 0);
        float GetExopt();
        // Barrier energies
        float GetVb(int type = 0, int mt = 0) const;
        float GetDeltaVb(int type = 0) const;
        float GetVbi() const;
        float GetVbf() const;
        // Projectile(-like) and target(-like) nuclei energies
        float GetEProjectile(int mt = 1);
        float GetEnergyPerNucleon();
        float GetETarget(int mt = 1);
        float GetEEjectile(int mt = 1);
        float GetERecoil(int mt = 1);
        // Scattering angles
        float GetTheta(int mt = 0);
        float GetPhi(int mt = 0) const;
        // Magnetic rigidities
        float GetBrhoEjectile();
        float GetBrhoRecoil();
        // rmin for Coulomb trajectory
        float Getrmin();
        // Kinematic quantities (assuming Coulomb trajectories)
        float Geteta();
        float Getk();
        float Getb();
        float GetScalingPar();
        float GetThetaGrazing();
        float GetbGrazing();
        float GetLclassGrazing();
        float GetlGrazing();
        // Other
        std::string GetReaction(int fmt = 0);
        std::string GetNucleonTransfer();
        Nucleus* GetProjectile();
        Nucleus* GetTarget();
        Nucleus* GetEjectile();
        Nucleus* GetRecoil();
        void Print(int mt = 1);

        // Mutators
        void SetTheta(float Theta, int mt = 1);
        void SetEProjectile(float E, int mt = 1);
        void SetVb(float Vb, int mt = 0);
        void SetProjectile(Nucleus *n1);
        void SetReaction(Nucleus *n1, Nucleus *n2, Nucleus *n3, Nucleus *n4);
        void SetReaction(Nucleus *n1, Nucleus *n2);
        void SetReaction(Nucleus *n1, Nucleus *n2,  Nucleus *n4);
        void SetReaction(const std::string &n1, const std::string &n2, const std::string &n3, const std::string &n4);
        void SetReaction(const std::string &n1, const std::string &n2);
        void SetReaction(const std::string &n1, const std::string &n2, const std::string &n3);
        void SetReaction(Nucleus *n1, Nucleus *n2, int DeltaN, int DeltaZ);
        void SetReaction(Nucleus *n1, Nucleus *n2, const std::string &transfer);

    private:

        enum {NoAngleGiven, labAngleGiven, cmAngleGiven, noEnergyGiven, EnergyGiven};

        Nucleus *mNucleus1,*mNucleus2,*mNucleus3,*mNucleus4;
        int mInitializedNuclei;
        int mInputAngle;
        int mInputEnergy;
        bool mBarrierGiven;

        float mThetalab,mThetacm;
        float mPhilab,mPhicm;
        float mE1,mE3,mE4;
        float mVbcm;

        void Initialize();
        void CleanUp();
        void UpdateKinematics();
        void ConvertDeltaNDeltaZTransfer(std::string& transfer, int& DeltaN, int& DeltaZ);
        void ConvertReactionString(const std::string &str_reaction, std::string &str_n1, std::string &str_n2, std::string &str_n3, std::string &str_n4);

        float Vbi(int type) const;
        float Vbf(int type) const;
        float mui() const;
        float muf() const;
        float etai();
        float etaf();
        float DeltaL();
        float lab2cm() const;
        float cm2lab() const;
        float lab2cmf() const;
        float cm2labf() const;

};

#endif // end _REACTION_H_
