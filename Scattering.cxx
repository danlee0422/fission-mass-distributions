// Rutherford and reference frames calculations for CubeXsec.cxx
// Liz Williams 07/17


#include "Scattering.h"
using namespace std;

///////////////////// dsigdOmega_Ruth_cm ////////////////////////////  /name change Liz 7/17
// function name updated 7/17 (orig: Rutherford)
// verified by Liz 11/7/17

// calculate Rutherford scattering cross section dsigma/domega
// in cm frame - units of mb/sr

float dsigdOmega_Ruth_cm(int Zp, int Zt, float Ecm, float theta_cm){

    float dsigdOmega_cm = ((kp*kp)/(16.))*(Zp*Zt / Ecm)*(Zp*Zt / Ecm)
        *(1./pow(sin(0.5*theta_cm*d2r),4)) * fmsq2mb;

    if (xsecTest2 == 1){
        cout << "TESTING: dsigdOmega_Ruth_cm ( " << Zp << "," << Zt << "," << Ecm << "," << 
            theta_cm << ") = " << dsigdOmega_cm << "\n";
    }

    return dsigdOmega_cm;

}//end dsigdOmega_Ruth_cm

///////////////////// dsigdOmega_Ruth_lab ////////////////////////////    /updated by Liz 7/17

//calculate Rutherford scattering cross section dsigma/domega
// in lab frame- units of mb/sr
// verified by Liz 11/7/17

float dsigdOmega_Ruth_lab(int Ap, int Zp, int At, int Zt, float Ecm, float theta_lab){

    float theta_cm = thLab2ThCM_elastics(Ap, At, theta_lab);  //ASSUMES WE'RE LOOKING AT ELASTICS - formula for fission events is the same / different??? CHECK /TODO
    float dsigdomega_lab = dsigdOmega_Ruth_cm(Zp,Zt,Ecm,theta_cm);
    dsigdomega_lab = dsigdomega_lab * dXCM2dXLab(theta_lab, theta_cm);

    if (xsecTest2 == 1){
        cout << "TESTING: dsigdOmega_Ruth_lab ( " << Ap << "," << Zp << "," << At << "," << 
            Zt << "," << Ecm << "," << theta_lab << ") = " << dsigdomega_lab << "\n";
    }    


    return dsigdomega_lab;

}//end dsigdOmega_Ruth_lab



///////////////////// dsigdTheta_Ruth_cm //////////////////////////// /created by Liz 7/17
// function name updated 7/17 (orig: Rutherford)
// verified by Liz 11/7/17

// calculate Rutherford scattering cross section dsigma/dtheta
// in cm frame - units of mb/rad

float dsigdTheta_Ruth_cm(int Zp, int Zt, float Ecm, float theta_cm){

    float dsigdTheta_cm = dsigdOmega_Ruth_cm(Zp, Zt, Ecm, theta_cm);
    dsigdTheta_cm = dsigdTheta_cm * 2. * pi * sin(theta_cm * d2r);


    if (xsecTest2 == 1){
        cout << "TESTING: dsigdTheta_Ruth_cm ( " << Zp << "," << Zt << "," << 
            Ecm << "," << theta_cm << ") = " << dsigdTheta_cm << "\n";
    }


    return dsigdTheta_cm;


}//end dsigdTheta_Ruth_cm


///////////////////// dsigdTheta_Ruth_lab ////////////////////////////  /created by Liz 7/17

//calculate Rutherford scattering cross section dsigma/dtheta
// in lab frame- units of mb/rad
// verified by Liz 11/7/17

float dsigdTheta_Ruth_lab(int Ap, int Zp, int At, int Zt, float Ecm, float theta_lab){

    float dsigdTheta_lab = dsigdOmega_Ruth_lab(Ap, Zp, At, Zt,Ecm,theta_lab);
    dsigdTheta_lab = dsigdTheta_lab * 2. * pi * sin(theta_lab * d2r); 


    if (xsecTest2 == 1){
        cout << "TESTING: dsigdTheta_Ruth_lab ( " << Ap << "," << Zp << "," << At << "," << 
            Zt << "," << Ecm << "," << theta_lab << ") = " << dsigdTheta_lab << "\n";
    } 


    return dsigdTheta_lab;

}//end dsigdTheta_Ruth_lab


/////////////////// thLab2ThCM_elastics ///////////////////////

//thLab2ThCM_elastics: Transforms theta from lab to CM frame - 
//elastic scattering. Consistent with MMA results for Ni+Ni
// verified by Liz 11/7/17

float thLab2ThCM_elastics(int Ap, int At, float theta_lab){

    float theta_cm = 0.0;
    float x = (float)Ap / (float)At;
    theta_lab = theta_lab * d2r;

    theta_cm = ( theta_lab + asin( x*sin( theta_lab ) ) );
    theta_cm = theta_cm * r2d;

    if (xsecTest2 == 1){
        cout << "TESTING: thLab2ThCM_elastics ( " << Ap << "," << At << "," << 
            theta_lab * r2d << ") = " << theta_cm << "\n";
    } 

    return theta_cm;
}//end thLab2ThCM_elastics



////////////////// thCM2ThLab_elastics ///////////////////////////

//thCM2ThLab_elastics: Transforms theta from CM to lab frame - elastic scattering
float thCM2ThLab_elastics(int Ap, int At, float theta_cm){

    float theta_lab = 0.0;
    float x = (float)Ap / (float)At;
    theta_cm = theta_cm*d2r;

    theta_lab = sin(theta_cm) / ( x + cos(theta_cm));
    theta_lab = atan(theta_lab) * r2d;

    if (xsecTest2 == 1){
        cout << "TESTING: thCM2ThLab_elastics ( " << Ap << "," << At << "," << 
            theta_cm * r2d << ") = " << theta_lab << "\n";
    }

    return theta_lab;
}//end thCM2ThLab_elastics



/////////////////////// dXCM2dXLab ///////////////////////////////
// solid angle cm->lab normalization
//dXCM2dXLab - from Marion and Young. Same for reactions and scattering.
// verified by Liz 11/7/17
float dXCM2dXLab(float theta_lab, float theta_cm){

    float factor=0.0;

    theta_cm = theta_cm * d2r;
    theta_lab = theta_lab * d2r;

    factor = ( sin(theta_cm) * sin(theta_cm) )/( sin(theta_lab) * sin(theta_lab));
    factor = factor / cos(theta_cm - theta_lab);


    if (xsecTest2 == 1){
        cout << "TESTING: dXCM2dXLab ( " << theta_lab * r2d << "," << theta_cm * r2d << 
            ") = " << factor << "\n";
    }


    return factor;

} // end dXCM2dXLab



///////////////////////// dXLab2dXcm ////////////////////////////
// solid angle lab->cm normalization
//dXLab2dXcm - from Marion and Young. Same for reactions and scattering.
float dXLab2dXCM(float theta_lab, float theta_cm){

    float factor=0.0;

    theta_cm = theta_cm * d2r;
    theta_lab = theta_lab * d2r;

    factor = sin(theta_lab) * sin(theta_lab) / (sin(theta_cm) * sin(theta_cm));
    factor = factor * cos(theta_cm - theta_lab);

    if (xsecTest2 == 1){
        cout << "TESTING: dXLab2dXCM ( " << theta_lab << "," << theta_cm << 
            ") = " << factor << "\n";
    }

    return factor;

} // end dXLab2dXCM



// &&&&&&&&&&&&&&&&&&&&&& END RUTHERFORD AND REF FRAME FUNCTIONS
