#ifndef GUARD_Scattering
#define GUARD_Scattering

//header for Scattering.cxx

//required std headers
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <list>
#include <ctime>

//root headers
#include "TCanvas.h"
#include "TGraphErrors.h"

//dacube headers
#include "CubeStruct.h"
#include "CubeInput.h"
#include "physConst.h"
#include "CubeSort.h"



float dsigdOmega_Ruth_cm(int Zp, int Zt, float Ecm, float theta_cm);
float dsigdOmega_Ruth_lab(int Ap, int Zp, int At, int Zt, float Ecm, float theta_lab);
float dsigdTheta_Ruth_cm(int Zp, int Zt, float Ecm, float theta_cm);
float dsigdTheta_Ruth_lab(int Ap, int Zp, int At, int Zt, float Ecm, float theta_lab);

float thLab2ThCM_elastics(int, int, float);
float thCM2ThLab_elastics(int, int, float);
float dXCM2dXLab(float thetaLab, float thetaCM);
float dXLab2dXCM(float thetaLab, float thetaCM);

#endif
