/*User-specific code for dacube. If you modify this, make sure to

make clean
make

And watch for errors in your code... */

/**************** Position anomaly corrections **********/

#ifdef USER_DET //code inserted into CubeDet.cxx
//corrects for problems in position reconstruction due to time
//mismatch in CF signals.
//May be applicable to position data derived from time difference between CF
//and one end of the delay line

//det1
  if ((i==0)&& mwpcEvt->y_raw<580){
        //mwpcEvt->x_raw=mwpcEvt->x_raw-5;
   }

// if((i==0)&& mwpcEvt ->x_raw>565){
  // }

//det2
  if ((i==1) && mwpcEvt->x_raw>35){
     //mwpcEvt->y_raw=mwpcEvt->y_raw+6;
    //mwpcEvt->x_raw=mwpcEvt->x_raw-5;
  }
  if ((i==1) && mwpcEvt->y_raw>40){
    // mwpcEvt->y_raw=mwpcEvt->y_raw-10;
  } 

    mwpcEvt->x_mm = mwpcGeo->ax_mm * mwpcEvt->x_raw + mwpcGeo->bx_mm;
    mwpcEvt->y_mm = mwpcGeo->ay_mm * mwpcEvt->y_raw + mwpcGeo->by_mm;

#endif

/**************** Gate definitions *********************/

#ifdef USER_GATEFUNC //code inserted into CubeGate.cxx


#endif


#ifdef USER_GATE //code inserted into CubeGate.cxx


#endif

/*************** Histogram definitions ***************/

#ifdef USER_HIST //Code inserted into CubeHist.cxx (Stored in UHist file in root tree)

/* Add user-defined histogram code here: */

  TH1D* Mratio=new TH1D("Mratio","Mratio",100,0,1);
  tree->Draw("MR>>Mratio","","");
//Draw arguments: Draw("yvalue:xvalue>>histname","gates to apply","");
  Mratio->SetTitle("Mratio [Counts]; MR; Counts");
  Mratio->Write("",TObject::kOverwrite);

  TH2D* fdthphi=new TH2D("fdthphi","fdthphi",1000,50,200,1000,50,300);
  fdthphi->SetOption("COLSCATZ");
  fdthphi->SetMinimum(0);
  tree->Draw("phi12:theta12>>fdthphi","","");
  fdthphi->SetTitle("fdthphi;theta1+theta2 [deg];phi1-phi2 [deg]");
  fdthphi->Write("",TObject::kOverwrite);
  delete fdthphi;

#endif
