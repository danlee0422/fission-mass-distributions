//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul  3 19:32:38 2017 by ROOT version 5.34/15
// from TTree CubeTree/CubeTree
// found on file: YSSm.run028.root
//////////////////////////////////////////////////////////


#ifndef ZClass_h
#define ZClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "CubeStruct.h"
// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class ZClass {
    public :
        TTree          *fChain;   //!pointer to the analyzed TTree or TChain
        Int_t           fCurrent; //!current Tree number in a TChain

        // Declaration of leaf types
        UShort_t        Raw_rs_x1b;
        UShort_t        Raw_rs_x1f;
        UShort_t        Raw_rs_y1b;
        UShort_t        Raw_rs_y1t;
        UShort_t        Raw_rs_x2b;
        UShort_t        Raw_rs_x2f;
        UShort_t        Raw_rs_y2b;
        UShort_t        Raw_rs_y2t;
        UShort_t        Raw_rs_x3b;
        UShort_t        Raw_rs_x3f;
        UShort_t        Raw_rs_y3b;
        UShort_t        Raw_rs_y3t;
        UShort_t        Raw_rs_t1;
        UShort_t        Raw_rs_t2;
        UShort_t        Raw_rs_t3;
        UShort_t        Raw_rs_t3a;
        UShort_t        Raw_rs_t3b;
        UShort_t        Raw_rs_t1a;
        UShort_t        Raw_rs_t1b;
        UShort_t        Raw_rs_t1c;
        UShort_t        Raw_rs_t1d;
        UShort_t        Raw_RF;
        Short_t         Cube_EBack;
        Short_t         Cube_EFront;
        Short_t         Cube_EBackSmall;
        Short_t         Cube_TBack;
        Short_t         Cube_TFront;
        Short_t         Cube_TBackSmall;
        Short_t         Cube_XBack;
        Short_t         Cube_YBack;
        Short_t         Cube_XFront;
        Short_t         Cube_YFront;
        Short_t         Cube_XBackSmall;
        Short_t         Cube_YBackSmall;
        Short_t         Cube_TBackSmallA;
        Short_t         Cube_TBackSmallB;
        Short_t         Cube_TBack_A;
        Short_t         Cube_TBack_B;
        Short_t         Cube_TBack_C;
        Short_t         Cube_TBack_D;
        UShort_t        Monitors_FissionPulser;
        UShort_t        Monitors_Monitor1;
        UShort_t        Monitors_Monitor2;
        UShort_t        Monitors_Mon_Tac;
        UShort_t        Monitors_T1T2_Tac;

        //  Double_t        Time_recTime;
        //  Double_t        Time_deltaTime;
        // List of branches
        TBranch        *b_Raw;   //!
        TBranch        *b_Cube;   //!
        TBranch        *b_Monitors;   //!

        ZClass(TTree *tree=0);
        virtual ~ZClass();
        virtual Int_t    Cut(Long64_t entry);
        virtual Int_t    GetEntry(Long64_t entry);
        virtual Long64_t LoadTree(Long64_t entry);
        virtual void     Init(TTree *tree);
        //virtual void     Loop();
        virtual void    Loop(int minEntry,int maxEntry,MWPCgeo* mwpcGeo,MWPCcorr* mwpcCorr,reactPar* rxn, targetPar* targ, char* datafile);
        virtual Bool_t   Notify();
        virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ZClass_cxx
ZClass::ZClass(TTree *tree) : fChain(0) 
{
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("YSSm.run028.root");
        if (!f || !f->IsOpen()) {
            f = new TFile("YSSm.run028.root");
        }
        f->GetObject("CubeTree",tree);

    }
    Init(tree);
}

ZClass::~ZClass()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t ZClass::GetEntry(Long64_t entry)
{
    // Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t ZClass::LoadTree(Long64_t entry)
{
    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void ZClass::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("Raw", &Raw_rs_x1b, &b_Raw);
    fChain->SetBranchAddress("Cube", &Cube_EBack, &b_Cube);
    fChain->SetBranchAddress("Monitors", &Monitors_FissionPulser, &b_Monitors);
    Notify();
}

Bool_t ZClass::Notify()
{
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.

    return kTRUE;
}

void ZClass::Show(Long64_t entry)
{
    // Print contents of entry.
    // If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}
Int_t ZClass::Cut(Long64_t entry)
{
    std::cout << entry << std::endl;
    // This function may be called from Loop.
    // returns  1 if entry is accepted.
    // returns -1 otherwise.
    return 1;
}
#endif // #ifdef ZClass_cxx
