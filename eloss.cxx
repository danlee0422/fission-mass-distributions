/*
   For src_v5.0 updated -Yun Jeung, May 2019

   Extened up to Z=98 -Yun Oct.2016

   Code for calculating energy loss of heavy ions in a simple target material. Assumes natural target abundance.
   Uses procedure and tables from J. F. Ziegler, J. P. Biersack, and U. Littmarck, The Stopping and Range of Ions in Solids
   Vol. 1 1985; any modifications to Ziegler are consistent with STROP3.
   Liz Williams, January 2014
 */

/***********************************/
/*         HEADER FILES            */
/***********************************/

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

/* Specialized header files */
#include "eloss.h"
#include "physConst.h"

using namespace std;

//define some useful constants - specified in physConst.h
extern const float v0; // //Bohr velocity = speed of light * fine structure constant in keV/u. Alternative units: 2.188 [mm/ns]
extern const float Bv0; //2.188 [mm/ns]
extern const float speed_of_light; // [mm/ns]
extern const float m_u; // [MeV/c^2]
extern const float Econv; // multiplication factor + unit conversion in velocity calculation from K=0.5mv^2 (E [MeV/amu] -> [mm/ns]^2 )
extern const float pi;
extern const float a0;

int debug = 0;
int toggle=0;

// arrays used in eloss.C

//not all items in elementdat used. These are taken from TRIM.
//Z, A (most abundant isotope), Atomic Weight (most abundant isotope), 
//Atomic weight of solid with normal isotopes, density (g/cm^3), 
//atomic density (atoms/cm^3), fermi velocity (units of Bohr velocity), 
//factor determining ion screening length (from scoef.dat, TRIM85)
//
// The last column, screening length values are changed to make the curve smooth against Z. 
// Introduced due to a problem at Z=54; the latest SRIM text 
// suggests the screening length data may be anomalous for this Z and is responsible for
// a gap in the mass ratio corresponding to fission fragments including this Z in thick target
// experiments. -- change by Gayatri Mohanto, 20 Aug 2014. 

static float elementdat[98][8]= {
    {1, 1, 1.0078, 1.008, 0.071486, 4.271, 1.0309, 1.},
    {2, 4, 4.0026, 4.0026, 0.12588, 1.894, 0.15976, 1.},
    {3, 7, 7.016, 6.939, 0.52969, 4.597, 0.59782, 1.05},
    {4, 9, 9.012, 9.0122, 1.8024, 12.046, 1.0781, 1.06},
    {5, 11, 11.009, 10.811, 2.3502, 13.093, 1.0486, 1.03},
    {6, 12, 12., 12.011, 2.2662, 11.364, 1., 1.03},
    {7, 14, 14.003, 14.007, 0.80963, 3.481, 1.058, 1.02},
    {8, 16, 15.995, 15.999, 1.1429, 4.302, 0.93942, 0.99},
    {9, 19, 18.998, 18.998, 1.1111, 3.522, 0.74562, 0.95},
    {10, 20, 19.992, 20.183, 1.2015, 3.585, 0.3424, 0.89},
    {11, 23, 22.99, 22.989, 0.97, 2.541, 0.45259, 0.84},
    {12, 24, 23.985, 24.312, 1.7366, 4.302, 0.71074, 0.82},
    {13, 27, 26.982, 26.981, 2.6981, 6.023, 0.90519, 0.84},
    {14, 28, 27.977, 28.086, 2.3212, 4.977, 0.97411, 0.9},
    {15, 31, 30.974, 30.973, 1.8219, 3.542, 0.97184, 0.94},
    {16, 32, 31.972, 32.064, 2.0686, 3.885, 0.89852, 0.97},
    {17, 35, 34.969, 35.453, 1.8956, 3.22, 0.70827, 0.97},
    {18, 40, 39.962, 39.948, 1.6504, 2.488, 0.39816, 0.98},
    {19, 39, 38.96, 39.102, 0.86318, 1.329, 0.36552, 0.98},
    {20, 40, 39.96, 40.08, 1.54, 2.014, 0.62712, 0.98},
    {21, 45, 44.956, 44.956, 2.9971, 4.015, 0.81707, 0.97},
    {22, 48, 47.95, 47.9, 4.5189, 5.682, 0.9943, 0.97},
    {23, 51, 50.94, 50.942, 6.1008, 7.213, 1.1423, 0.95},
    {24, 52, 51.94, 51.996, 7.1917, 8.33, 1.2381, 0.93},
    {25, 55, 54.94, 54.938, 7.4341, 8.15, 1.1222, 0.91},
    {26, 56, 55.94, 55.847, 7.8658, 8.483, 0.92705, 0.9},
    {27, 59, 58.93, 58.933, 8.796, 8.989, 1.0047, 0.89},
    {28, 58, 57.94, 58.71, 8.8955, 9.125, 1.2, 0.89},
    {29, 63, 62.93, 63.54, 8.9493, 8.483, 1.0661, 0.9},
    {30, 64, 63.93, 65.37, 7.1054, 6.546, 0.97411, 0.9},
    {31, 69, 68.93, 69.72, 5.9085, 5.104, 0.84912, 0.88},
    {32, 74, 73.92, 72.59, 5.3375, 4.428, 0.95, 0.88},
    {33, 75, 74.92, 74.922, 5.7192, 4.597, 1.0903, 0.88},
    {34, 80, 79.92, 78.96, 4.7855, 3.65, 1.0429, 0.9},
    {35, 79, 78.92, 79.909, 3.3995, 2.562, 0.49715, 0.91},
    {36, 84, 83.91, 83.8, 2.6021, 1.87, 0.37755, 0.91},
    {37, 85, 84.91, 85.47, 1.529, 1.077, 0.35211, 0.91},
    {38, 88, 87.91, 87.62, 2.6, 1.787, 0.57801, 0.9},
    {39, 89, 88.91, 88.905, 4.4902, 3.041, 0.77773, 0.9},
    {40, 90, 89.9, 91.22, 6.4695, 4.271, 1.0207, 0.9},
    {41, 93, 92.91, 92.906, 8.6024, 5.576, 1.029, 0.89},
    {42, 96, 95.9, 95.94, 10.206, 6.407, 1.2542, 0.89},
    {43, 99, 99., 99., 0., 0., 1.122, 0.89},
    {44, 102, 101.9, 101.07, 12.177, 7.256, 1.1241, 0.89},
    {45, 103, 102.9, 102.91, 12.399, 7.256, 1.0882, 0.89},
    {46, 106, 105.9, 106.4, 11.955, 6.767, 1.2709, 0.89},
    {47, 107, 106.9, 107.87, 10.473, 5.847, 1.2542, 0.89},
    {48, 114, 113.9, 112.4, 8.5802, 4.597, 0.90094, 0.89},
    {49, 115, 114.9, 114.82, 7.3134, 3.836, 0.74093, 0.89},
    {50, 120, 119.9, 118.69, 7.2816, 3.695, 0.86054, 0.9},
    {51, 121, 120.9, 121.75, 6.6168, 3.273, 0.93155, 0.9},
    {52, 130, 129.9, 127.6, 6.2244, 2.938, 1.0047, 0.9},
    {53, 127, 126.9, 126.9, 4.9373, 2.343, 0.55379, 0.9}, 
    {54, 132, 131.9, 131.3, 3.0589, 1.403, 0.43289, 0.9},
    {55, 134, 133.9, 132.91, 1.8986, 0.86, 0.32636, 0.9},
    {56, 138, 137.9, 137.34, 3.5215, 1.544, 0.5131, 0.89},
    {57, 139, 138.9, 138.91, 6.1738, 2.676, 0.695, 0.87},
    {58, 140, 139.9, 140.12, 6.6724, 2.868, 0.72591, 0.88},
    {59, 141, 140.9, 140.91, 6.7744, 2.895, 0.71202, 0.88},
    {60, 142, 141.9, 144.24, 7.0019, 2.923, 0.67413, 0.91},
    {61, 147, 147., 147., 0., 0., 0.71418, 0.92},
    {62, 152, 151.9, 150.35, 7.5553, 3.026, 0.71453, 0.95},
    {63, 153, 152.9, 151.96, 5.2581, 2.084, 0.5911, 0.99},
    {64, 158, 157.9, 157.25, 7.902, 3.026, 0.70263, 1.02},
    {65, 159, 158.9, 158.92, 8.2773, 3.136, 0.68049, 1.05},
    {66, 164, 163.9, 162.5, 8.5526, 3.17, 0.68203, 1.07},
    {67, 165, 164.9, 164.93, 8.8198, 3.22, 0.68121, 1.08},
    {68, 166, 165.9, 167.26, 9.0902, 3.273, 0.68532, 1.09},
    {69, 169, 168.9, 168.93, 9.3331, 3.327, 0.68715, 1.09},
    {70, 174, 173.9, 173.04, 6.9774, 2.428, 0.61884, 1.08},
    {71, 175, 174.9, 174.97, 9.8298, 3.383, 0.71801, 1.08},
    {72, 180, 179.9, 178.49, 13.124, 4.428, 0.83048, 1.08},
    {73, 181, 180.9, 180.95, 16.601, 5.525, 1.1222, 1.09},
    {74, 184, 184., 183.85, 19.292, 6.32, 1.2381, 1.09},
    {75, 187, 187., 186.2, 21.04, 6.805, 1.045, 1.1},
    {76, 192, 192., 190.2, 22.562, 7.144, 1.0733, 1.11},
    {77, 193, 193., 192.2, 22.506, 7.052, 1.0953, 1.12},
    {78, 195, 195., 195.09, 21.438, 6.618, 1.2381, 1.13},
    {79, 197, 197., 196.97, 19.311, 5.904, 1.2879, 1.14},
    {80, 202, 202., 200.59, 13.553, 4.069, 0.78654, 1.15},
    {81, 205, 205., 204.37, 11.882, 3.501, 0.66401, 1.17},
    {82, 208, 208., 207.19, 11.322, 3.291, 0.84912, 1.18},
    {83, 209, 209., 208.98, 9.8113, 2.827, 0.88433, 1.18},
    {84, 210, 210., 210., 9.2511, 2.653, 0.80746, 1.17},
    {85, 210, 210., 210., 0., 0., 0.43357, 1.17},
    {86, 222, 222., 222., 0., 0., 0.41923, 1.16},
    {87, 223, 223., 223., 0., 0., 0.43638, 1.16},
    {88, 226, 226., 226., 5.0222, 1.338, 0.51464, 1.16},
    {89, 227, 227., 227., 0., 0., 0.73087, 1.16},
    {90, 232, 232., 232., 11.658, 3.026, 0.81065, 1.16},
    {91, 231, 231., 231., 15.4, 4.015, 1.9578, 1.16},
    {92, 238, 238.04, 238.04, 19.043, 4.818, 1.0257, 1.16},//U
    {93, 238, 238.04, 238.04, 19.043, 4.818, 1.0257, 1.16},//Np
    /*
       {94, 244, 244, 244, 19.81, 6.54067, 1.294723, 1.16},//Pu
       {95, 238, 238.04, 238.04, 19.043, 4.818, 1.0257, 1.16},//Am
       {96, 248, 248., 248., 13.51, 8.2806, 1.550253, 1.16}//Cm
     */
    {94, 244, 244, 244, 19.81, 6.54067, 1.294723, 1.16},//Pu        // derived value
    {95, 238, 238.04, 238.04, 19.043, 4.818, 1.0257, 1.16},//Am     // 
    {96, 248, 248., 248., 13.51, 8.28067, 1.550253, 1.16},//Cm      // derived value

    {97, 248, 248., 248., 13.51, 8.2806, 1.549773, 1.16},//Bk       //
    {98, 249, 249., 249., 15.1, 10.02067, 1.805783, 1.16}//Cf       // derived value

};

//proton stopping power coefficients from TRIM (scoef.dat)
static float Acoeff[98][8] = {
    {0.0091827, 0.0053496, 0.69741, 0.48493, 316.07, 1.0143, 9329.3, 0.053989},
    {0.11393, 0.0051984, 1.0822, 0.39252, 1081., 1.0645, 4068.5, 0.017699},
    {0.85837, 0.0050147, 1.6044, 0.38844, 1337.3, 1.047, 2659.2, 0.01898},
    {0.8781, 0.0051049, 5.4232, 0.2032, 1200.6, 1.0211, 1401.8, 0.038529},
    {1.4608, 0.0048836, 2.338, 0.44249, 1801.3, 1.0352, 1784.1, 0.02024},
    {3.2579, 0.0049148, 2.7156, 0.36473, 2092.2, 1.0291, 2643.6, 0.018237},
    {0.59674, 0.0050837, 4.2073, 0.30612, 2394.2, 1.0255, 4892.1, 0.016006},
    {0.75253, 0.0050314, 4.0824, 0.30067, 2455.8, 1.0181, 5069.7, 0.017426},
    {1.226, 0.0051385, 3.2246, 0.32703, 2525., 1.0142, 7563.6, 0.019469},
    {1.0332, 0.0051645, 3.004, 0.33889, 2338.6, 0.99997, 6991.2, 0.021799},
    {6.0972, 0.0044292, 3.1929, 0.45763, 1363.3, 0.95182, 2380.6, 0.081835},
    {14.013, 0.0043646, 2.2641, 0.36326, 2187.4, 0.99098, 6264.8, 0.0462},
    {0.039001, 0.0045415, 5.5463, 0.39562, 1589.2, 0.95316, 816.16, 0.047484},
    {2.072, 0.0044516, 3.5585, 0.53933, 1515.2, 0.93161, 1790.3, 0.035198},
    {17.575, 0.0038346, 0.078694, 1.2388, 2806., 0.97284, 1037.6, 0.012879},
    {16.126, 0.0038315, 0.054164, 1.3104, 2813.3, 0.96587, 1251.4, 0.011847},
    {3.217, 0.0044579, 3.6696, 0.5091, 2734.6, 0.96253, 2187.5, 0.016907},
    {2.0379, 0.0044775, 3.0743, 0.54773, 3505., 0.97575, 1714., 0.011701},
    {0.74171, 0.0043051, 1.1515, 0.95083, 917.21, 0.8782, 389.93, 0.18926},
    {9.1316, 0.0043809, 5.4611, 0.31327, 3891.8, 0.97933, 6267.9, 0.015196},
    {7.2247, 0.0043718, 6.1017, 0.37511, 2829.2, 0.95218, 6376.1, 0.020398},
    {0.147, 0.0048456, 6.3485, 0.41057, 2164.1, 0.94028, 5292.6, 0.050263},
    {5.0611, 0.0039867, 2.6174, 0.57957, 2218.9, 0.92361, 6323., 0.025669},
    {0.53267, 0.0042968, 0.39005, 1.2725, 1872.7, 0.90776, 64.166, 0.030107},
    {0.47697, 0.0043038, 0.31452, 1.3289, 1920.5, 0.90649, 45.576, 0.027469},
    {0.027426, 0.0035443, 0.031563, 2.1755, 1919.5, 0.90099, 23.902, 0.025363},
    {0.16383, 0.0043042, 0.073454, 1.8592, 1918.4, 0.89678, 27.61, 0.023184},
    {4.2562, 0.0043737, 1.5606, 0.72067, 1546.8, 0.87958, 302.02, 0.040944},
    {2.3508, 0.0043237, 2.882, 0.50113, 1837.7, 0.89992, 2377., 0.04965},
    {3.1095, 0.0038455, 0.11477, 1.5037, 2184.7, 0.89309, 67.306, 0.016588},
    {15.322, 0.0040306, 0.65391, 0.67668, 3001.7, 0.92484, 3344.2, 0.016366},
    {3.6932, 0.0044813, 8.608, 0.27638, 2982.7, 0.9276, 3166.6, 0.030874},
    {7.1373, 0.0043134, 9.4247, 0.27937, 2725.8, 0.91597, 3166.1, 0.025008},
    {4.8979, 0.0042937, 3.7793, 0.50004, 2824.5, 0.91028, 1282.4, 0.017061},
    {1.3683, 0.0043024, 2.5679, 0.60822, 6907.8, 0.9817, 628.01, 0.0068055},
    {1.8301, 0.0042983, 2.9057, 0.6038, 4744.6, 0.94722, 936.64, 0.0092242},
    {0.42056, 0.0041169, 0.01695, 2.3616, 2252.7, 0.89192, 39.752, 0.027757},
    {30.78, 0.0037736, 0.55813, 0.76816, 7113.2, 0.97697, 1604.4, 0.0065268},
    {11.576, 0.0042119, 7.0244, 0.37764, 4713.5, 0.94264, 2493.2, 0.01127},
    {6.2406, 0.0041916, 5.2701, 0.49453, 4234.6, 0.93232, 2063.9, 0.011844},
    {0.33073, 0.0041243, 1.7246, 1.1062, 1930.2, 0.86907, 27.416, 0.038208},
    {0.017747, 0.0041715, 0.14586, 1.7305, 1803.6, 0.86315, 29.669, 0.032123},
    {3.7229, 0.0041768, 4.6286, 0.56769, 1678., 0.86202, 3094., 0.06244},
    {0.13998, 0.0041329, 0.25573, 1.4241, 1919.3, 0.86326, 72.797, 0.032235},
    {0.2859, 0.0041386, 0.31301, 1.3424, 1954.8, 0.86175, 115.18, 0.029342},
    {0.76002, 0.0042179, 3.386, 0.76285, 1867.4, 0.85805, 69.994, 0.036448},
    {6.3957, 0.0041935, 5.4689, 0.41378, 1712.6, 0.85397, 18493., 0.056471},
    {3.4717, 0.0041344, 3.2337, 0.63788, 1116.4, 0.81959, 4766., 0.1179},
    {2.5265, 0.0042282, 4.532, 0.53562, 1030.8, 0.81652, 16252., 0.19722},
    {7.3683, 0.0041007, 4.6791, 0.51428, 1160., 0.82454, 17965., 0.13316},
    {7.7197, 0.004388, 3.242, 0.68434, 1428.1, 0.83398, 1786.7, 0.066512},
    {16.78, 0.0041918, 9.3198, 0.29568, 3370.9, 0.90289,7431.7, 0.02616},
    {4.2132, 0.0042098, 4.6753, 0.57945, 3503.9, 0.89261, 1468.9, 0.014359},
    {4.0818, 0.004214, 4.4425, 0.58393, 3945.3, 0.90281, 1340.5, 0.013414},
    {0.18517, 0.0036215, 0.00058788, 3.5315, 2931.3, 0.88936, 26.18, 0.026393},
    {4.8248, 0.0041458, 6.0934, 0.57026, 2300.1, 0.86359, 2980.7, 0.038679},
    {0.49857, 0.0041054, 1.9775, 0.95877, 786.55, 0.78509, 806.6, 0.40882},
    {3.2754, 0.0042177, 5.768, 0.54054, 6631.3, 0.94282, 744.07, 0.0083026},
    {2.9978, 0.0040901, 4.5299, 0.62025, 2161.2, 0.85669, 1268.6, 0.043031},
    {2.8701, 0.004096, 4.2568, 0.6138, 2130.4, 0.85235, 1704.1, 0.039385},
    {10.853, 0.0041149, 5.8907, 0.46834, 2857.2, 0.8755, 3654.2, 0.029955},
    {3.6407, 0.0041782, 4.8742, 0.57861, 1267.7, 0.82211, 3508.2, 0.24174},
    {17.645, 0.0040992, 6.5855, 0.32734, 3931.3, 0.90754, 5156.7, 0.036278},
    {7.5309, 0.0040814, 4.9389, 0.50679, 2519.7, 0.85819, 3314.6, 0.030514},
    {5.4742, 0.0040829, 4.897, 0.51113, 2340.1, 0.85296, 2342.7, 0.035662},
    {4.2661, 0.0040667, 4.5032, 0.55257, 2076.4, 0.84151, 1666.6, 0.040801},
    {6.8313, 0.0040486, 4.3987, 0.51675, 2003., 0.83437, 1410.4, 0.03478},
    {1.2707, 0.0040553, 4.6295, 0.57428, 1626.3, 0.81858, 995.68, 0.055319},
    {5.7561, 0.0040491, 4.357, 0.52496, 2207.3, 0.83796, 1579.5, 0.027165},
    {14.127, 0.0040596, 5.8304, 0.37755, 3645.9, 0.87823, 3411.8, 0.016392},
    {6.6948, 0.0040603, 4.9361, 0.47961, 2719., 0.85249, 1885.8, 0.019713},
    {3.0619, 0.0040511, 3.5803, 0.59082, 2346.1, 0.83713, 1222., 0.020072},
    {10.811, 0.0033008, 1.3767, 0.76512, 2003.7, 0.82269, 1110.6, 0.024958},
    {2.7101, 0.0040961, 1.2289, 0.98598, 1232.4, 0.79066, 155.42, 0.047294},
    {0.52345, 0.0040244, 1.4038, 0.8551, 1461.4, 0.79677, 503.34, 0.036789},
    {0.4616, 0.0040203, 1.3014, 0.87043, 1473.5, 0.79687, 443.09, 0.036301},
    {0.97814, 0.0040374, 2.0127, 0.7225, 1890.8, 0.81747, 930.7, 0.02769},
    {3.2086, 0.004051, 3.6658, 0.53618, 3091.2, 0.85602, 1508.1, 0.015401},
    {2.0035, 0.0040431, 7.4882, 0.3561, 4464.3, 0.88836, 3966.5, 0.012839},
    {15.43, 0.0039432, 1.1237, 0.70703, 4595.7, 0.88437, 1576.5, 0.0088534},
    {3.1512, 0.0040524, 4.0996, 0.5425, 3246.3, 0.85772, 1691.8, 0.015058},
    {7.1896, 0.0040588, 8.6927, 0.35842, 4760.6, 0.88833, 2888.3, 0.011029},
    {9.3209, 0.004054, 11.543, 0.32027, 4866.2, 0.89124, 3213.4, 0.011935},
    {29.242, 0.0036195, 0.16864, 1.1226, 5688., 0.89812, 1033.3, 0.0071303},
    {1.8522, 0.0039973, 3.1556, 0.65096, 3755., 0.86383, 1602., 0.012042},
    {3.222, 0.0040041, 5.9024, 0.52678, 4040.2, 0.86804, 1658.4, 0.011747},
    {9.3412, 0.0039661, 7.921, 0.42977, 5180.9, 0.88773, 2173.2, 0.0092007},
    {36.183, 0.0036003, 0.58341, 0.86747, 6990.2, 0.91082, 1417.1, 0.0062187},
    {5.9284, 0.0039695, 6.4082, 0.52122, 4619.5, 0.88083, 2323.5, 0.011627},
    {5.2454, 0.0039744, 6.7969, 0.48542, 4586.3, 0.87794, 2481.5, 0.011282},
    {33.702, 0.0036901, 0.47257, 0.89235, 5295.7, 0.8893, 2053.3, 0.0091908},
    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816},
    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816},
    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816},
    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816},
    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816},

    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816},
    {2.7589, 0.0039806, 3.2092, 0.66122, 2505.4, 0.82863, 2065.1, 0.022816}

};


//* BEGIN ACTUAL CODE *//

/*------------------------------------------------------------------------------------
  protonSP
  : Calculates stopping power
  ------------------------------------------------------------------------------------*/
float protonSP(float E, int Zt){ // E = MeV/nucleon[amu], Zt=proton number of target material

    extern float Acoeff[98][8]; //Acoeff defined in eloss.h; required extern def for this function to "see" defined array.
    float A[12]={0,0,0,0,0,0,0,0,0,0,0,0};
    float Se,Seinv,Slow,Shigh;
    float pe0,pe;
    float term;
    int i;

    pe0=25.;//for velocity proportional stopping (see below). consistent with strop3 implementation 
    pe=max(pe0,E);

    for (i=0;i<8;i++){
        //cout << Zt << " " << i;
        A[i]=Acoeff[Zt-1][i];
    }

    Slow  = (A[0] * pow(pe,A[1])) + (A[2] * pow(pe,A[3]));

    term = (A[6]/pe) + (A[7]*pe);
    Shigh = A[4] * log(term);
    Shigh = Shigh / pow(pe,A[5]);

    Seinv = (1.0/Slow) + (1.0/Shigh);
    Se = 1.0/Seinv;

    if (E <= pe0){ // velocity proportional stopping below E<pe0

        if (Zt <= 6){
            Se = Se*pow((E/pe0),0.25);
        }
        else{
            Se = Se*pow((E/pe0),0.45);
        }
    }

    //if ( E>10000) {
    //  if (toggle==0){
    //    cout << "High energy stopping power calculations (E/A>10MeV) have not been implemented. Results will not be completely accurate for higher energies."<< endl;
    // }
    // toggle=1;
    //}

    return Se;
}//end protonSP
// compared to Strop3 - matches

/*------------------------------------------------------------------------------------
  heliumSP
  : Calculates stopping power
  ------------------------------------------------------------------------------------*/
float heliumSP(int Zt, float E){

    int i;
    float hE0, hE, b, sum, gamsq, c, S, term, Sp;
    float a[6] = {0.2865,0.1266,-0.001429,0.02402,-0.01135,0.001475}; // coeffs from JF Ziegler et al Vol 1 1985

    hE0=1.;
    hE=max(hE0,E); // velocity proportional stopping below 1 keV/AMU
    b = log(hE);

    sum = 0.0;

    for (i=0;i<6;i++){
        sum = sum + a[i]*pow(b,i);
    }
    gamsq = 1. - exp(-min(float(30.),sum)); // not sure why the 30 is here, but is consistently used in STROP3 and Ziegler codes

    //correct for polarization of target atoms by projectile (Z^3 effect)
    term = 7.6-max(0.,double(log(hE)));
    c = 1. + (0.007 + 0.00005 * (float)Zt) * exp( -term*term );
    gamsq = c*c*gamsq;

    Sp=protonSP(hE,Zt); // from TRIM function (1985);
    S = 4. * gamsq * Sp; // Factor of 4 is from 2*Zp, eq (3-9) in Ziegler

    if (E<hE0){
        S=S*sqrt(E/hE0);
    }

    return S;
}//end heliumSP

//heliumSP: checked against current SRIM version and STROP3 for a range of test cases
//Results agree precisely with STROP3, as expected (method and inputs are the same); modern SRIM results in slightly 
//higher Eloss for projectile energies E<2MeV. 

/*------------------------------------------------------------------------------------
  HeavyIonSP
  : Calculates stopping power
  ------------------------------------------------------------------------------------*/
float HeavyIonSP(int Zp, int Zt, float E){ // direct translation of TRIM85 code. Still not right -- overpredicts compared to STROP3 and SRIM at lower energies!!!

    float Z, S, Sp;
    float v, vmin, vr, vF;
    float yr,yr1,yr2,yr3;
    float L0, L0lim1, L0lim2, L, Lfctr, L1;
    float q, power;
    float gamma;
    float term, a,b,eee;

    Z = (float)Zp;
    S = 0.; // initialize returned value

    float yrmin = 0.13;
    float vrmin = 1.;

    //calculate velocity of the projectile nucleus
    vF = elementdat[Zt-1][6]; // pull vF= fermi velocity from array defined in eloss.h
    v = sqrt( E / v0 ) / vF; 

    //compute relative ion velocity vr
    if (v < 1.){
        vr = 0.75 * vF * (1. + v*v/1.5 - pow(v,4)/15.);
    }
    else{
        vr = v*vF*(1. + 0.2/(v*v));
    }

    //compute effective ion velocity yr (see explanation on p. 220 of JF Ziegler vol 1 1985)
    yr1 = yrmin;
    yr2 = vr*pow(Z,-0.6667);
    yr3 = vrmin*pow(Z,-0.6667);
    yr = max(yr1,yr2); // yr is the maximum value of yr1,yr2,yr3
    yr = max(yr,yr3);

    //compute ionization level q of the ion at effective velocity yr
    a = -0.803*pow(yr,0.3) + 1.3167*pow(yr,0.6) + 0.38157*yr + 0.008983*yr*yr;
    term = 1.-exp(-min(a,float(50.)));
    q = min(float(1.),max(float(0.),term));

    //convert ionization level to effective charge
    b = (min(0.43,max(0.32,0.12+0.025*Z)))/pow(Z,0.3333);
    L0 = (0.8 - q*(min(1.2,0.6 + Z/30.)))/pow(Z,0.3333);
    L0lim1 = max(0.,(0.9-0.025*Z));
    L0lim2 = max(0.,1.-0.025*min(float(16.),Z));
    L1=0.;

    if (q<0.2){
        L1 = 0.;
    }
    else if (q<L0lim1){
        L1=b*(q-0.2)/fabs(max(float(0.),float(0.9-0.025*Z))-0.2000001);
    }
    else if (q<L0lim2){
        L1=b;
    }
    else{
        L1=b*(1.-q)/(0.025*min(float(16.),Z));
    }

    Lfctr = elementdat[Zp-1][7]; // pull Lfctr from array defined in eloss.h
    L = max(L1,L0*Lfctr);

    gamma = q + (0.5/(vF*vF))*(1.-q)*log(1.+(4.*L*vF/1.919)*(4.*L*vF/1.919));

    // add in Z^3 effect
    a = -(7.6-max(0.,double(log(E))))*(7.6-max(0.,double(log(E))));

    gamma = gamma*(1.+(1./(Z*Z))*(0.18+0.0015*(float)Zt)*exp(a)); //original

    //*********************************************gayatri
    /*

       if((Z>52)&&(Z<54)){
       gamma = gamma*(1.+(1./(Z*Z))*(0.18+0.0015*(float)Zt)*exp(a))*0.96;
    //cout<<"new gamma="<<gamma<<endl;
    }
    else if((Z>53)&&(Z<55)){
    gamma = gamma*(1.+(1./(Z*Z))*(0.18+0.0015*(float)Zt)*exp(a))*0.94;
    //cout<<"new gamma="<<gamma<<endl;
    }
    else{
    gamma = gamma*(1.+(1./(Z*Z))*(0.18+0.0015*(float)Zt)*exp(a));
    }
    //cout<<Z<<"   gamma="<<gamma<<endl;//gayatri

     */
    //*********************************************gayatri

    //cout<<"gamma="<<gamma<<endl;//gayatri
    //finally: Calculate scaled stopping power values using appropriate proton stopping power
    if (yr <= max(yrmin,float(vrmin*pow(Z,-0.6667)))){
        //calculate velocity stopping for yr < yrmin
        vrmin = max(vrmin,float(yrmin*pow(Z,0.6667)));
        vmin = 0.5*(vrmin + sqrt(max(0.,vrmin*vrmin-0.8*vF*vF)));
        eee=v0*vmin*vmin;
        Sp=protonSP(eee,Zt); // from TRIM function (1985);
        power = 0.5;
        if ( (Zt==6||Zt==14||Zt==32) && Zp<=19){
            power = 0.375;
        }
        S = Sp*gamma*gamma*Z*Z*pow(E/eee,power);
    }
    else{
        Sp=protonSP(E,Zt); // from TRIM function (1985);
        S=Sp*gamma*gamma*Z*Z;
    }

    //cout<<Z<<"   S="<<S<<endl;//gayatri

    return S;
}//end HeavyIonSP

// tested against STROP3 (where this code has been adapted from); results are identical

/*------------------------------------------------------------------------------------
  nuclearSP
  : Calculates stopping power
  ------------------------------------------------------------------------------------*/
float nuclearSP(int Zp, int Ap, int Zt, float At, float E){

    float epsil, a, Sn;
    float m1, m2, z1,z2;

    m1=(float)Ap;
    m2=(float)At;
    z1=(float)Zp;
    z2=(float)Zt;

    E=E*m1;

    epsil = 32.53*m2*E/(z1*z2*(m1+m2)*(pow(z1,0.23)+pow(z2,0.23)));
    if (debug == 1){
        cout << "epsil =" << epsil << endl;
    }
    if (epsil<30.){
        a = 0.01321*pow(epsil,.21226) + .19593*pow(epsil,.5);
        Sn = 0.5*log(1.+1.1383*epsil)/(epsil+a);
        if (debug == 1){
            cout << "a[nuc]="<< a << endl;
        }
    }
    else{
        Sn=log(epsil)/(2.*epsil);
    }
    Sn=Sn*z1*z2*m1*8.462/((m1+m2)*(pow(z1,0.23)+pow(z2,0.23)));

    return Sn;
}//end nuclearSP


/*------------------------------------------------------------------------------------
  eloss
  : Calculates the amount of energy loss
  eloss(Zp,Aproj,Zt,Eproj) returns stopping power in MeV/(mg/cm^2)
  ------------------------------------------------------------------------------------*/
float eloss(int Zp, int Aproj, int Zt, int At, float Eproj){

    // Zp = projectile Z
    // Zt = target Z
    // E = energy in MeV / nucleon (amu)

    float E;
    float S,Sn;
    float At_temp;
    extern float elementdat[98][8];


    //calculate energy per nucleon [MeV/amu]
    E = Eproj/(float)Aproj;

    if (E<1e-10){
        return 0.;
    }

    //change from E [MeV/amu]->E[keV/amu] to match with Ziegler's parameterization
    E=1000*E;

    //cout << Zp << endl;

    if (Zp==1){
        S=protonSP(E,Zt); // from TRIM function (1985);
    }

    else if (Zp==2){
        S = heliumSP(Zt, E);
    }

    //  else if (Zp > 2 && Zp < 93){
    else if (Zp > 2 && Zp < 99){//2016 Linac 
        S = HeavyIonSP(Zp, Zt, E);
    }

    else{
        Zp=92;
        S = HeavyIonSP(Zp, Zt, E);
        //cout << "Stopping powers for Zproj>92 not implemented. Energy loss set to zero." << endl;
        //cout << "Zp = " << Zp << endl;
        //return 0;
    }

    // if At is given as zero to the eloss function then we should use the
    // natural atomic weights for the isotope, otherwise use the given isotopic
    // mass number (this enables enriched targets to be used).
    if (At == 0) {
        At_temp = elementdat[Zt-1][3];
    } else {
        At_temp = (float) At;
    }


    Sn = nuclearSP(Zp, Aproj, Zt, At_temp, E); // calculate nuclear stopping

    /* std::cout << Zt << " " << At_temp << " " << S << " "  << Sn << std::endl; */
    S = S + Sn;

    //At = elementdat[Zt-1][3];//assumes atomic weight for natural substance 

    S = S * 0.60222/At_temp; // converts to units of MeV / (mg/cm^2)

    return S; 

}//end eloss
// matches STROP3 


/*------------------------------------------------------------------------------------
  eloss

  rxn->dEcalcType == 1;

  eloss(Zp,Aproj,Zt,Eproj,dx) returns PROJECTILE ENERGY LOSS in MeV for a target of thickness dx[ug/cm^3]
  Currently assumes target is not enriched, NOT ANY MORE SUCKERS! - JMB
  ------------------------------------------------------------------------------------*/
float eloss(int Zp, int Aproj, int Zt, int At, float Eproj, float dx){

    // Zp = projectile Z
    // Zt = target Z
    // E = beam energy in MeV 
    // dx = thickness of target material in ug/cm^2
    // Aproj = projectile mass

    float S,dE;

    if (dx>0){
        //calculate energy loss 
        S = eloss(Zp,Aproj,Zt, At, Eproj);
        S=S/1000;//converts to MeV/(ug/cm^2)
        dE=S*dx;
    }
    else{
        dE=0;
    }

    //cout<<" dEMeV="<<dE<<endl; //gayatri
    return dE;

}//end eloss


/*------------------------------------------------------------------------------------
  elossBT
  rxn->dEcalcType == 2;

  Followed the descriptions of FF loss given in
  B. K. Srivastava and Shankar Mukherji, Phys. Rev. A 14, 718 (1976).
  Shankar Mukherji and Brijesh K. Srivastava, Phys. Rev. B 9, 3708 (1974).
  N. Bohr, Kgl. Danske Videnskab. Selskab, Mat.-Fys. Medd. 18, 8 (1948)
  It returns heavy ion energy loss in MeV for a target of thickness dx[ug/cm^2]
  - Tathagata Banerjee, Nov. 2018
  - Yun Jeung, May 2019 update
  ------------------------------------------------------------------------------------*/
float elossBT(int Zp, int Aproj, int Zt, float Eproj, float dx){
    float zp,mion,zion,atr,ztr,Eion,vion,chi;
    float fzt1,fzt2,fzt;
    float fzi1,fzi2,fzi,vlim1,vlim2,vlim3,vlim4;
    float fac,Ibar,s;
    float ET,SN,ST,dE;

    float Conv = m_u/(speed_of_light*speed_of_light);    //0.0104
    float unity = 1.0;

    zp = (float)Zp;
    mion = (float)Aproj;
    ztr = (float)Zt;
    Eion = Eproj; //[MeV]
    atr = elementdat[Zt-1][3];

    if (Eion<1e-10){
        return 0.;
    }

    vion = sqrt((2.0*Eion)/(Conv*mion)); //[mm/ns]

    chi = (2.0*zp*Bv0)/vion;  
    fzi1 = 0.28*pow(zp,0.66667);
    fzi2 = pow(zp,0.33333);
    fzt1 = 0.28*pow(ztr,0.66667);
    fzt2 = pow(ztr,0.33333);
    vlim1 = 0.5*ztr*Bv0*chi;
    vlim2 = 0.5*ztr*Bv0*pow(chi,0.33333);
    vlim3 = 0.5*ztr*Bv0;

    if (zp<=45.5){
        fzi = fzi1;
    }
    else{
        fzi = fzi2;
    }

    if (ztr<=45.5){
        fzt = fzt1;
    }
    else{
        fzt = fzt2;
    }

    vlim4 = (0.5*zp*Bv0)/fzi;

    if (vion<=vlim4){
        zion = (fzi*vion)/Bv0;
    }
    else{
        zion = zp*sqrt(1.0-(2.03*exp((-2.0*vion*fzi)/(zp*Bv0))));
    }

    //Calculating ionization potential of the target material

    fac = (ztr-2.0)*log(13.6*pow(((ztr-2.0)/(2.717*fzt)),2.0));
    fac = (fac+(2.0*log(13.6*pow(ztr,2.0))))/ztr;
    Ibar = exp(fac); // in [eV]

    s = 0.0;

    //for heavy ions in heavy medium
    if (ztr>=45.5){   

        if (chi>unity){

            if (vion>=vlim1){
                s = ((63.65*pow(zion,2.0)*ztr)/(atr*vion));          
                s = s*log10((11.39*pow(vion,2.0))/(Ibar*chi));
            }
            else if (vion>=vlim2 && vion<vlim1){
                s = (3.0*(ztr-2.0))*log((2.0*fzt*vion)/((ztr-2.0)*Bv0));
                s = s+(6.0*log((2.0*vion)/(ztr*Bv0)))-(ztr*log(chi));
                s = s+(3.0*(ztr-2.0)+((2.0*fzt*vion)/(Bv0*chi)));
                s = s*((13.79*pow(zion,2.0))/(atr*pow(vion,2.0)));
            }
            else if (vion<vlim2){
                s = ((12.68*fzt*pow(zion,2.0))/(atr*vion));
                s = s*((3.0*pow(chi,-0.33333))+pow(chi,-1.0));
            }
        }
        else if (chi<unity){

            if (vion>=vlim3){
                s = log10((11.39*pow(vion,2.0))/Ibar);
                s = s*((63.65*pow(zion,2.0)*ztr)/(atr*pow(vion,2.0)));
            }
            else{
                s = (50.6*fzt*pow(zion,2.0))/(atr*vion);
            }
        }
    }

    // for heavy ions in light medium
    else{          
        s = 1.327*(fzt/atr)*vion*((4.7622*pow(fzi,1.6667))+fzi);
    }

    ET = Eion/mion;
    //change from E [MeV/amu]->E[keV/amu] to match with Ziegler's parameterization
    ET=1000*ET;

    SN = nuclearSP(Zp, Aproj, Zt, atr, ET); // calculate nuclear stopping

    SN = SN * 0.60222/atr; // converts to units of MeV / (mg/cm^2)

    ST = s + SN;

    if (dx>0){
        ST=ST/1000;//converts to MeV/(ug/cm^2)
        dE=ST*dx;
    }
    else{
        dE=0;
    }
    return dE;

}//end elossBT

/*------------------------------------------------------------------------------------
  elossBT
  rxn->dEcalcType == 3;

  Followed the semi-empirical description of FF loss given by 
  G.N. Knyazheva et al., NIM B 248, 13-14 (2006).
  There is a misprint in the equation-13 in her paper.
  This one mainly works for 0.05<E/u<6.0, as suggested by Knyazheva.
  It returns heavy ion energy loss in MeV for a target of thickness dx[ug/cm^2]
  - Tathagata Banerjee, Nov. 2018
  - Yun Jeung, May 2019 update
  ------------------------------------------------------------------------------------*/
float elossT(int Zp, int Aproj, int Zt, float Eproj, float dx){

    float mprj,zprj,Atr,ztr,Eion,vion,beta,ET;
    float zeff; 
    extern float elementdat[98][8];
    float xi,L1,L,ST,SN,dE;
    float lmd[6] = {0.568,0.562,0.560,0.570,0.567,0.667}; //adjustable param. for{C,Ni,Au,Myler,Al2O3,Others}


zprj = (float)Zp; 
mprj = (float)Aproj;
ztr = (float)Zt;
Eion = Eproj;
Atr = elementdat[Zt-1][3];

ET = Eion/mprj;

if (Eion<1e-10){
    return 0.;
}

vion = 1.389*sqrt(ET); // E in MeV/amu
beta = vion/(speed_of_light* 0.1);

xi = (7.866*pow(10.0,7.0)*pow(beta,3.0))/(zprj*ztr*10.0);

L1 = log(xi);

//taking into account the equilibrium charge
if (ztr==6){
    zeff = zprj*(1.-exp(-vion/(0.1*Bv0*pow(zprj,lmd[0]))));
    L=L1+1.1678-0.5126*L1+0.055886*pow(L1,2.0);
}
else if (ztr==28){
    zeff = zprj*(1.-exp(-vion/(0.1*Bv0*pow(zprj,lmd[1]))));
    L=L1+1.05-0.5632*L1+0.053614*pow(L1,2.0);
}
else if (ztr==79){
    zeff = zprj*(1.-exp(-vion/(0.1*Bv0*pow(zprj,lmd[2]))));
    L=L1+0.9735-0.6130*L1+0.061688*pow(L1,2.0)+0.0042028*pow(L1,3.0);
}
else if (ztr==4||ztr==5){
    zeff = zprj*(1.-exp(-vion/(0.1*Bv0*pow(zprj,lmd[3]))));
    L=L1+1.1408-0.4565*L1+0.040390*pow(L1,2.0);
}
else if (ztr==13||ztr==10){
    zeff = zprj*(1.-exp(-vion/(0.1*Bv0*pow(zprj,lmd[4]))));
    L=L1+1.0933-0.5189*L1+0.048160*pow(L1,2.0);
}
else {
    zeff = zprj*(1.-exp(-vion/(0.1*Bv0*pow(zprj,lmd[5]))));
    L=L1+0.9735-0.6130*L1+0.061688*pow(L1,2.0)+0.0042028*pow(L1,3.0); //keeping same as Au
}

ST = (3.0705*pow(10.0,-4.0)*ztr*zeff*zeff*L)/(Atr*beta*beta); //MeV / (mg/cm^2)

// change from ET [MeV/amu]->ET[keV/amu] to match with Ziegler's parameterization
ET=1000*ET;

SN = nuclearSP(Zp, Aproj, Zt, Atr, ET); // calculate nuclear stopping

SN = SN * 0.60222/Atr; // converts to units of MeV / (mg/cm^2)

ST = ST + SN;

if (dx>0){
    ST=ST/1000;//converts to MeV/(ug/cm^2)
    dE=ST*dx;
}
else{
    dE=0;
}
return dE;

}//end elossT


/*------------------------------------------------------------------------------------
  dedx
  ------------------------------------------------------------------------------------*/
float dedx(int Zp, int Aproj, int Zt,float Eproj){

    float S=0;

    S=eloss(Zp,Aproj,Zt,0,Eproj);
    cout << "Stopping power: " << S << endl;
    return S;
}

/*------------------------------------------------------------------------------------
  dedxtable
  : Produces output table a la srim
  ------------------------------------------------------------------------------------*/
void dedxtable(int Zp, int Ap, int Zt, float Emin, float Emax, float Estep){//in MeV

    float E;
    float val;
    char buf[300];
    ofstream output;

    toggle=0; // turns off some error messages after first pass
    //create string with unique name
    //open output file
    sprintf(buf,"eloss_%d-%d_%d.dat",Zp,Ap,Zt);
    output.open(buf);

    output << "Zp,Ap,Zt: " << Zp << ", " << Ap << ", " << Zt << endl;
    cout << "Zp,Ap,Zt: " << Zp << ", " << Ap << ", " << Zt << endl;

    output << "E[MeV]\t dE/dx_tot[MeV/mg/cm^2]" << endl;

    E=Emin;

    while (E<Emax){
        val=eloss(Zp, Ap, Zt, 0, E);
        output << E << "\t" << val << endl;

        E=E+Estep;
    }
    output.close();

}//end dedxtable
