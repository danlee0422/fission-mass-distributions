#ifndef ELOSS_H
#define ELOSS_H

/* header accompanying eloss.C */
float protonSP(float, int);
float heliumSP(int, float);
float HeavyIonSP(int, int, float);
float nuclearSP(int, int, int, float, float);
float eloss(int, int, int, int, float);
float eloss(int, int, int, int, float, float);
float elossT(int, int, int, float, float);
float elossBT(int, int, int, float, float);
float dedx(int,int,int,float);
void dedxtable(int,int,int,float,float,float);

#endif
