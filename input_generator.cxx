/* Input file generator for barrier distribution measurements
 * in CUBE. 
 * Liz Williams, March 2017
 * Expects syntax: input_generator("global_input_file")
 * Need to fix for src_v4.3 ...(will do it later) - Yun 04/04/2019
 */

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <sstream>
#include <iomanip>

using namespace std;

# ifndef __CINT__
int main(int argc, char* argv[])
{

    string globalFileName;

    ifstream globalI;
    ofstream outputO;
    ofstream bashOut;
    
    string parseTxt;
 
     
    //parse user inputs
    if (argc < 2 || argc > 2)
    {
        cout << "Syntax error. Expected usage: input_generator ";
        cout << "<path_relative_to_executable/global_file_name.csv> \n";
        return -1;
    }
   
   //save the user-provided name of the global input file
   globalFileName = argv[1];
   
   //open the global input file
   globalI.open( globalFileName.c_str() );
   
   if (globalI.fail() ) {
    cout << "File " << globalFileName << " not found\n";
   }
   
   bashOut.open("allsort.com");
   
   
   //line 1 - Experiment name
   string exptName;
   getline(globalI, exptName, '\t'); //reads in experiment name
   cout << "Creating input files for " << exptName << "\n";
   getline(globalI, parseTxt, '\n'); //ignore rest of line
   
   //line 2 - tcalib path
   string tcalibPath;
   getline(globalI, tcalibPath, '\t'); //reads in tcalib path
   getline(globalI, parseTxt, '\n'); //ignore rest of line
   
   //line 3 - input file path
   string inputPath;
   getline(globalI, inputPath,'\t');
   getline(globalI, parseTxt, '\n');
   
   //line 4 - raw data prefix 
   string uRootPrefix;
   getline(globalI, uRootPrefix, '\t'); //reads in unprocessed root file prefix
   getline(globalI, parseTxt, '\n'); //ignore rest of line
   
   //line 5 - processed data prefix
   string pRootPrefix;
   getline(globalI, pRootPrefix, '\t');
   getline(globalI, parseTxt, '\n');                                          
                                             
   //line 6 - standard detector file name
   string detFile;
   getline(globalI, detFile, '\t'); //Get standard detector file name
   getline(globalI, parseTxt, '\n'); //ignore rest of line
   
   //line 7 - empty line
   getline(globalI, parseTxt, '\n'); //ignore line 
   
   //line 8 + 9 comment lines
   getline(globalI, parseTxt, '\n'); //ignore line
   getline(globalI, parseTxt, '\n'); //ignore line  
   
   //line 10 - gate string
   stringstream parseLine;
   string univGateString = "";
   int gateTog = 0;
   
   getline(globalI, parseTxt, '\n');
   istringstream gateLine(parseTxt); //associate stringstream will parseTxt line
   string gateCommand;
   
   while (gateLine >> gateCommand, !gateLine.eof() )
   {
   
        
        if ( gateCommand.compare("#endUnivGates")!=0 )
        {
            if (gateTog == 1)
            {
            
                univGateString = univGateString + gateCommand + "\n"; //check root syntax here
                
            }
            else
            {
                gateTog = 1;
                univGateString = gateCommand + "\n";
            }
                
        }
   }
   
   //lines 11-15 - empty and comment lines
   getline(globalI, parseTxt, '\n'); //ignore line
   getline(globalI, parseTxt, '\n'); //ignore line
   getline(globalI, parseTxt, '\n'); //ignore line
   getline(globalI, parseTxt, '\n'); //ignore line
   getline(globalI, parseTxt, '\n'); //ignore line  
    
   //lines 16 and beyond
   string junk;
   

   while (!globalI.eof())
   {
    getline(globalI, parseTxt, '\n');
    
    if ( parseTxt.compare("#endRuns")!=0 )
    {
    
        //extract run number and name input file
        int fileNum;
        ostringstream fileNumStream;
        
        
        istringstream inputLine(parseTxt);
        inputLine >> fileNum;

        fileNumStream << internal << setfill('0') << setw(3) << fileNum;
        
        
        string outFileName = inputPath + "/sort_" + exptName + fileNumStream.str() + ".inp";
        outputO.open( outFileName.c_str() );
        cout << "Creating " << outFileName << "...\n";
        bashOut << outFileName << "\n";
        
        string uRootFile;
        uRootFile = uRootPrefix + fileNumStream.str() + ".root";
        
        //skip label
        inputLine >> junk;
        
        //look at processed file name (if 0, use standard)
        string pFileName;
        inputLine >> pFileName;
        if ( pFileName.compare("0")==0 ) 
        {
            pFileName = pRootPrefix + fileNumStream.str() + ".PE.root";
        }
        //no else because if compare test is false you just use pFileName as is

        //look at gated file name (if 0, use standard)
        string gFileName; 
        inputLine >> gFileName;
        if ( gFileName.compare("0")==0 )
        {
            gFileName = pRootPrefix + fileNumStream.str() + ".PEG.root";
        }
        //no else because if compare test is false use gFileName as is
        
        //look at detector file name (if 0, use standard)
        string detFileName;
        inputLine >> detFileName;
        if ( detFileName.compare("0")==0 ) 
        {
            detFileName = detFile;
        }
        //no else because if compare test is false use detFileName as is
        
        //Get reaction parameters
        uint Zp, Ap, Zt, At;
        float Elab;
        
        inputLine >> Zp;
        inputLine >> Ap;
        inputLine >> Zt;
        inputLine >> At;
        inputLine >> Elab;
        
        //Get sort boundaries
        int eventI, eventF;
        
        inputLine >> eventI;
        inputLine >> eventF;
        
        //Get time parameters
        float T0, dT, dT3;
        
        inputLine >> T0;
        inputLine >> dT;
        inputLine >> dT3;
        
        //Get beam type
        uint beamType;
        
        inputLine >> beamType;
        
        //Get target parameters
        float targThick, targAngle;
        float uBackThick;
        uint uBackZ;
        float dBackThick;
        uint dBackZ;
        
        inputLine >> targThick;
        inputLine >> targAngle;
        inputLine >> uBackThick;
        inputLine >> uBackZ;
        inputLine >> dBackThick;
        inputLine >> dBackZ;       
        
        //Get sort booleans
        uint eloss, tsharp;
        
        inputLine >> eloss;
        inputLine >> tsharp;
        
        
        //Get unique gate items
    
        stringstream parseGateLine;
        string gateString;
        string dumString;


        gateString = univGateString;
   
        while (inputLine)
        {
            inputLine >> dumString;
            if ( dumString.compare("#endGates")!=0 )
            {
                if (gateTog == 1){
                    gateString = gateString + dumString + "\n"; //check root syntax here
                }
                else
                {
                    gateTog = 1;
                    gateString = dumString + "n"; //check root syntax here
                }
            }
        }      

        
        
        //output values in appropriate format to sort file
        outputO << "####### Sort information ###################################################\n";
        outputO << uRootFile << "\t #input file\n";
        outputO << detFileName << "\t #detector file\n";
        outputO << tcalibPath << "\t #tcalib sorts saved here\n";
        outputO << eventI << "\t" << eventF << "\t #sort bounds by event number (0=all)\n";
        outputO << Zp << "\t" << Ap << "\t" << Zt << "\t" << At << " \t #Zp Ap Zt At\n";
        outputO << Elab << "\t #Elab [MeV]\n";
        outputO << T0 << "\t" << dT << "\t" << dT3 << "\t #T0, dT, dT3 (Time calibration)\n";
        outputO << T0 << "\t" << T0 << "\t 1 \t" << dT << "\t" << dT << "\t 1";         
        outputO << "#T0min,T0max,Tstep,dTmin,dTmax,dTstep\n";
        outputO << beamType << "\t #Beam type 1=AC, 2=AC tdiff, 3=DC tdiff-, 4=DC tdiff+\n";
        outputO << targThick << "\t" << targAngle << "\t #Target thickness (ug/cm^2),";
        outputO << " target angle theta (deg)\n";
        outputO << uBackThick << "\t" << uBackZ << "\t 	#Backing(upstream) thickness (ug/cm^2), Z \n";
        outputO << dBackThick << "\t" << dBackZ << "\t #Backing (downstream) thickness (ug/cm^2), Z \n";
        outputO << eloss << "\t #Enable energy loss (0=no,1=yes)\n";
        outputO << tsharp << "\t #Enable time sharpening (0=no, 1=yes)\n";
        outputO << "######### Gate information ####################################################\n";
        outputO << pFileName << "\t #Ungated file\n";
        outputO << gFileName << "\t #Desired name of gated files\n";
        outputO << gateString;
        outputO << "#Gates to be applied (comment must follow last gate)\n";
        outputO << "################################################################################\n";
        

        
        //close output file
        outputO.close();
    }
    else
    {
    cout << parseTxt << "\n";
    }

   }
   
   
   cout << "Input file creation complete.\n";
   globalI.close();
   bashOut.close();
   return 0;
   
}




#endif
