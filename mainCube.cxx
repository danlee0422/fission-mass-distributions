//mainCube.cxx: main control program for dacube
// updated for src_3det_v4.3

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

#include "ZClass.h"
#include "CubeSort.h"
#include "CubeGate.h"
#include "CubeHist.h"
#include "CubeHistEss.h"
#include "tCalib.h"
#include "CubeXsec.h"
#include "CubeLoss.h"

#include "TROOT.h"

using namespace std;

# ifndef __CINT__
int main(int argc, char* argv[])
{

    ROOT::EnableImplicitMT(4);
    int i=1;
    string runfile, runlist;
    char* sortfile;
    char* listfile;
    int runcount=1;
    int listcount=0;
    int raw=0;
    int gate=0;
    int calc=0;
    int hist=0;
    int histg=0;
    int histe=0; // Yun
    int histge=0;// Yun
    int tcal=0;
    int count=0;
    int nini=0;
    int xsec=0;

    //for batch sorts
    ifstream input;

    int temp;

    // *** BEGIN PARSING USER INPUTS ***//
    /*
       cout << " *****************************************************" << endl; 
       cout << " *        You are using dacube src_3det_v5.0         *" << endl;
       cout << " *        released 2019. 05. 15                      *" << endl;
       cout << " *****************************************************" << endl;
     */
    if (argc < 2){

        cout << " *****************************************************" << endl; 
        cout << " *        You are using dacube src_3det_v5.0         *" << endl;
        cout << " *        released 2019. 05. 15                      *" << endl;
        cout << " *****************************************************" << endl;

        cout << " Error: For all options, a sort file or list of sort files must " << endl;
        cout << "       be specified." << endl;
        cout << " For all options:  dacube <listfile> -b <opts> OR dacube <sortfile> <opts>" << endl;
        cout << " Valid <opts>:                 -r (sorts raw file)" << endl;
        cout << "                               -g (turns on gating)," << endl;
        cout << "                               -c (folding angle calculations without histogram generation)," << endl;
        cout << "                               -h (histograms ungated data)" << endl;
        cout << "                               -he (essential histograms ungated data)" << endl;    
        cout << "                               -hg (histograms gated data)" << endl;
        cout << "                               -hge (essential histograms gated data)" << endl;    
        cout << "                               -tc (iterative sort over range of time params)" << endl;
        cout << "                               -a (same as -r -g -h -hg)" << endl;
        cout << "                               -ae (same as -r -g -he -hge)" << endl;  
        cout << "                               -n (special options for Ni+Ni reactions)" << endl;
        cout << "For cross section calculations:" << endl;
        cout << " dacube <xsecFile.inp> -x (Cross section calculations)" << endl;
        return -1;
    }

    //save first argument as sortfile by default
    runfile = argv[1];
    sortfile = &runfile[0];
    listfile = &runfile[0];

    i=2;
    while (i<argc){

        count = 0;

        //-b switch: enables batch sort and stores name of listfile
        temp = strcmp(argv[i],"-b");
        if (temp == 0){
            listcount = 1;
            runcount = 0;
        }
        else{
            count++;
        }

        //-r switch: enables sort of raw data
        temp = strcmp(argv[i],"-r");
        if (temp == 0){
            raw = 1;
        }
        else{
            count++;
        }

        //-g switch: enables gating of sorted data
        temp = strcmp(argv[i],"-g");
        if (temp==0){
            gate = 1;
        }
        else{
            count++;
        }

        //-c switch: folding angle calculations without histogram generation
        temp = strcmp(argv[i],"-c");
        if (temp==0){
            calc = 1;
        }
        else{
            count++;
        }

        //-h switch: enables histogram creation for ungated data
        temp = strcmp(argv[i],"-h");
        if (temp==0){
            hist = 1;
        }
        else{
            count++;
        }

        //-hg switch: enables histogram creation for gated data
        temp = strcmp(argv[i],"-hg");
        if (temp==0){
            histg = 1;
        }
        else{
            count++;
        }

        //-he switch: enables essential histogram creation for ungated data
        temp = strcmp(argv[i],"-he");
        if (temp==0){
            histe = 1;
        }
        else{
            count++;
        }

        //-hge switch: enables essential histogram creation for gated data
        temp = strcmp(argv[i],"-hge");
        if (temp==0){
            histge = 1;
        }
        else{
            count++;
        }

        //-tc switch: enables iterative sorts over a range of time calibration parameters
        temp = strcmp(argv[i],"-tc");
        if (temp == 0){
            tcal = 1;
        }
        else{
            count++;
        }

        temp = strcmp(argv[i],"-a");
        if (temp == 0){
            raw=1;
            gate=1;
            hist=1;
            histg=1;
        }
        else{
            count++;
        }

        temp = strcmp(argv[i],"-ae");
        if (temp == 0){
            raw=1;
            gate=1;
            histe=1;
            histge=1;
        }
        else{
            count++;
        }

        temp = strcmp(argv[i],"-n");
        if (temp == 0){
            nini = 1;
        }
        else{
            count++;
        }

        temp = strcmp(argv[i],"-x");
        if (temp == 0){
            xsec = 1;
        }
        else{
            count++;
        }



        if (count > 12){
            cout << "******************************************************" << endl;
            cout << "Error (dacube): It looks like you mistyped an option." << endl;
            cout << "                Check your entry and try again." << endl;
            cout << "                Unrecognized option: " << argv[i] << endl;
            cout << "******************************************************" << endl;
        }
        i++;
    } // **** end parsing user arguments **** //

    //do more error checking
    if (runcount != 1 && listcount != 1){
        cout << " Error: For all options, a sort file or list of sort files must " << endl;
        cout << "       be specified." << endl;
        cout << " For all options:  dacube <listfile> -b <opts> OR dacube <sortfile> <opts>" << endl;
        cout << " Valid <opts>:                 -r (sorts raw file)" << endl;
        cout << "                               -g (turns on gating)," << endl;
        cout << "                               -c (folding angle calculations without histogram generation)," << endl;
        cout << "                               -h (histograms ungated data)" << endl;
        cout << "                               -hg (histograms gated data)" << endl;
        cout << "                               -he (histograms ungated data)" << endl;
        cout << "                               -hge (histograms gated data)" << endl;
        cout << "                               -tc (iterative sort over range of time params)" << endl;
        cout << "                               -a (same as -r -h -g -hg)" << endl;
        cout << "                               -ae (same as -r -he -g -hge)" << endl;   
        cout << "                               -n (special options for Ni+Ni reactions)" << endl;
        cout << "For cross section calculations:" << endl;
        cout << " dacube <xsecFile.inp> -x (Cross section calculations)" << endl;

        return -1;
    }
    if (raw==0 && gate==0 && calc==0 && hist==0 && histg==0 && histe==0 && histge==0 && tcal==0 && xsec==0 ){
        cout << "\nWARNING: You haven't asked dacube to do anything." << endl;
        cout << "         Sort of raw data commencing by default.\n" << endl;
        raw=1;
    }


    // END INPUT ERROR CHECKING

    //BEGIN ACTUALLY DOING STUFF


    //for single sort
    if (runcount == 1){

        cout << "Opening " << sortfile << "... \n";

        if (raw == 1){
            cout << "Raw data is being sorted ... \n";
            CubeSort(sortfile,nini);
            cout << endl;
        }
        if (gate == 1){
            cout << "Gates are being applied ... \n";
            ApplyGate(sortfile);
            cout << endl;
        }
        if (calc == 1){
            cout << "Folding angle calculations are beginning ... \n";
            if (calc == 1 && gate == 1){
                calc_kin(sortfile,1);
                }
            else {calc_kin(sortfile,0);}
            cout << endl;
        }
        if (hist == 1){
            cout << "UNGATED histograms are being created ... \n";
            CreateHist(sortfile,0);
            cout << endl;
        }
        if (histg == 1){
            cout << "GATED histograms are being created ... \n";
            CreateHist(sortfile,1);
            cout << endl;
        }
        if (histe == 1){
            cout << "UNGATED essential histograms are being created ... \n";
            CreateHistEss(sortfile,0);
            cout << endl;
        }
        if (histge == 1){
            cout << "GATED essential histograms are being created ... \n";
            CreateHistEss(sortfile,1);
            cout << endl;
        }
        if (tcal == 1){
            cout << "Time calibration sort beginning ... \n";
            tCalib(sortfile);
        }
        if (xsec == 1){
            cout << "Cross section calculations beginning ...\n";
            xsecCalc(sortfile);
        }


        return 0;
    }//end single sort loop

    //Do batch sort
    if (listcount ==1){
        input.open(listfile);

        if (!input.is_open()){
            cout << "Error: Could not find " << listfile << endl;
            return -1;
        }

        while (!input.eof()){
            input >> runfile;
            sortfile = &runfile[0];

            cout << "Sortfile: " << sortfile << endl;
            if (raw == 1){
                cout << "Raw data is being sorted ... " << endl;
                CubeSort(sortfile,nini);
                cout << endl;
            }
            if (gate == 1){
                cout << "Gates are being applied ... " << endl;
                ApplyGate(sortfile);
                cout << endl;
            }
            if (hist == 1){
                cout << "UNGATED histograms are being created for... " << endl;
                CreateHist(sortfile,0);
                cout << endl;
            }
            if (histg == 1){
                cout << "GATED histograms are being created for... " << endl;
                CreateHist(sortfile,1);
                cout << endl;
            }
            if (histe == 1){
                cout << "UNGATED essential histograms are being created for... " << endl;
                CreateHistEss(sortfile,0);
                cout << endl;
            }
            if (histge == 1){
                cout << "GATED essential histograms are being created for... " << endl;
                CreateHistEss(sortfile,1);
                cout << endl;
            }
            if (tcal == 1){
                cout << "Time calibration sort beginning ... " << endl;
                tCalib(sortfile);
            }
        }
        cout << "Batch processing complete!"<< endl;
        input.close();
        return 0;
    }//end batch loop

    return 0;
}
# endif
