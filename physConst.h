#ifndef PHYSCONST_H
#define PHYSCONST_H

//define some useful constants
const float v0=25.; // //Bohr velocity = speed of light * fine structure constant in keV/u. Alternative units: 2.188 [mm/ns]
const float Bv0=2.188; // [mm/ns]
const float speed_of_light=299.8; // [mm/ns]
const float m_u = 931.494; // [MeV/c^2]
const float Econv = 13.8914; // multiplication factor + unit conversion in velocity calculation from K=0.5mv^2 (E [MeV/amu] -> [mm/ns]^2 )
const float pi=3.14159;
const float a0 = 0.529117; //Bohr radius, angstroms
const float kp = 1.44; // e^2/(4 pi eps_0) in MeV fm
const float fmsq2mb = 10.; //multiplicative factor

const float d2r= pi / 180.;
const float r2d= 180. / pi;

#endif
