#include "plot_kin.h"

using namespace std;

/*------------------------------------------------------------------------------------
  calcRxn
  : Calculates elastic folding angles
  - Yun Jeung, May 2019 update
  ------------------------------------------------------------------------------------*/

/* Called when user wishes to have folding angle calculations without histogram generation */
int calc_kin(char* inputfile, bool gatebool){

    char *procfile;
    char *gatefile;

    sortInfo sortStruct ={};
    reactPar rxn ={};
    targetPar targ ={};

    int errCheck = readInput(inputfile,&sortStruct,&rxn,&targ);

    if ( errCheck == -1 ){
        cout << "Input file read-in error. Sort failed." << endl;
        return -1;
    }

    if (rxn.dEcalcType==1 || rxn.dEcalcType==2 || rxn.dEcalcType==3 ){
        calcElossBeam(&targ,&rxn);
    }

    string proc;
    proc=sortStruct.procfile;
    procfile=&proc[0];
    string gatf;
    gatf=sortStruct.gatefile;
    gatefile=&gatf[0];

    if (gatebool){
        plot_kin(gatefile,&rxn);
    }
    else{
        plot_kin(procfile,&rxn);
    }

    return 0;

}

/* Called from calc_kin, and also during histogram generation. Refer to CubeHist.cxx */

int plot_kin(char* filename,reactPar* rxn){

    //Define nucleus 

    Nucleus* proj = new Nucleus(rxn->Ap,rxn->Zp); //projectile
    Nucleus* tar = new Nucleus(rxn->At,rxn->Zt); //target

    Nucleus* product = new Nucleus(rxn->Ap,rxn->Zp); //product
    Nucleus* product2 = new Nucleus(rxn->At,rxn->Zt); //product

    Reaction* reaction = new Reaction(proj, tar, product); //reaction
    Reaction* reaction2 = new Reaction(proj, tar, product2); //reaction

    reaction->SetEProjectile(rxn->Elab);
    reaction2->SetEProjectile(rxn->Elab);

  //Get Mulgin TKE values 

/*
 TKE as function of mass ratio, where Acn=233,
 from S.I. Mulgin et al. Nuc Plys A 824 (2009), p1-23
 {mass ratio, TKE(E_proton=10.3MeV), TKE(E_proton=18MeV), TKE(E_proton=30MeV)}
*/
     static float TKEMulgin[47][4]= {
	{0.300 ,0 ,0 ,144.70},
	{0.305 ,0 ,148.69 ,145.08},
	{0.309 ,0 ,149.08 ,145.87},
	{0.313 ,148.19 ,149.46 ,147.40},
	{0.318 ,149.73 ,149.82 ,148.71},
	{0.322 ,150.36 ,150.75 ,149.99},
	{0.326 ,151.76 ,151.96 ,150.97},
	{0.330 ,152.90 ,152.81 ,151.99},
	{0.335 ,154.35 ,153.75 ,153.09},
	{0.339 ,155.75 ,154.81 ,154.36},
	{0.343 ,156.83 ,155.95 ,155.32},
	{0.348 ,157.89 ,156.91 ,156.59},
	{0.352 ,158.83 ,157.78 ,157.68},
	{0.356 ,159.60 ,158.63 ,158.52},
	{0.361 ,160.40 ,159.33 ,159.56},
	{0.365 ,161.20 ,160.12 ,160.39},
	{0.369 ,162.11 ,160.84 ,161.22},
	{0.373 ,162.91 ,161.54 ,161.98},
	{0.378 ,163.56 ,162.26 ,162.71},
	{0.382 ,164.22 ,162.84 ,163.34},
	{0.386 ,164.85 ,163.34 ,163.87},
	{0.391 ,165.56 ,163.88 ,164.43},
	{0.395 ,166.22 ,164.45 ,164.94},
	{0.399 ,166.87 ,164.96 ,165.55},
	{0.403 ,167.64 ,165.49 ,165.95},
	{0.408 ,168.41 ,165.85 ,166.36},
	{0.412 ,169.21 ,166.12 ,166.70},
	{0.416 ,170.01 ,166.51 ,167.04},
	{0.421 ,170.69 ,167.02 ,167.04},
	{0.425 ,171.38 ,167.40 ,167.16},
	{0.429 ,171.78 ,167.33 ,166.70},
	{0.434 ,171.81 ,167.00 ,166.38},
	{0.438 ,171.66 ,166.49 ,165.78},
	{0.442 ,171.18 ,165.49 ,165.00},
	{0.447 ,170.10 ,164.40 ,164.15},
	{0.450 ,168.73 ,162.96 ,163.32},
	{0.455 ,166.90 ,161.88 ,162.54},
	{0.459 ,164.90 ,160.67 ,161.88},
	{0.464 ,162.97 ,159.73 ,161.26},
	{0.468 ,161.68 ,158.80 ,160.71},
	{0.472 ,160.40 ,158.10 ,160.33},
	{0.476 ,159.23 ,157.63 ,160.20},
	{0.481 ,158.66 ,157.27 ,159.90},
	{0.485 ,158.12 ,157.02 ,159.76},
	{0.489 ,157.66 ,156.97 ,159.63},
	{0.494 ,157.75 ,156.85 ,159.56},
	{0.498 ,157.58 ,156.85 ,159.52}
  };

//Data from 246Cm TKE
//  J.P. Unik et al. 
//  Third IAEA Symposium on Physics and Chemistry of Fission
//  Rochester N.Y., 1973, paper IAEA/SM-174/209
     static float TKEUnik[37][2]= {
         {0.3527 ,163.4211},
         {0.3569 ,163.8158},
         {0.3615 ,165},
         {0.3651 ,166.1842},
         {0.3693 ,167.3684},
         {0.3733 ,168.4211},
         {0.3775 ,169.4737},
         {0.3817 ,170.2632},
         {0.3856 ,171.8421},
         {0.3898 ,173.1579},
         {0.394 ,173.9474},
         {0.398 ,175.1316},
         {0.4022 ,176.7105},
         {0.406 ,178.5526},
         {0.4102 ,180.1316},
         {0.4143 ,180.3947},
         {0.4185 ,181.1842},
         {0.4223 ,181.9737},
         {0.4263 ,183.2895},
         {0.4305 ,184.2105},
         {0.4345 ,185},
         {0.4384 ,185.3947},
         {0.4426 ,186.1842},
         {0.4466 ,186.9737},
         {0.4508 ,188.1579},
         {0.4548 ,189.0789},
         {0.459 ,190.2632},
         {0.4627 ,191.3158},
         {0.4671 ,192.6316},
         {0.4711 ,193.0263},
         {0.4751 ,192.6316},
         {0.4793 ,191.5789},
         {0.4835 ,189.8684},
         {0.4875 ,188.4211},
         {0.4915 ,185.1316},
         {0.4959 ,181.7105},
         {0.5 ,181.1842},
  };

  float Mulgin_plus=1.0175;   //percentage increase to apply to Mulgin TKE
  TString Mulgin_plus_txt1="Mulgin*1.0175_Ep=10.3MeV";
  TString Mulgin_plus_txt2="Mulgin*1.0175_Ep=18MeV";
  TString Mulgin_plus_txt3="Mulgin*1.0175_Ep=30MeV";


    //Define Tgraph
    TGraph* gh=new TGraph();
    TGraph* gh2 = new TGraph();
    TGraph* gh3 = new TGraph();
    TGraph* gh4 = new TGraph();
    TGraph* gh5 = new TGraph();
    TGraph* gh6 = new TGraph();  
    TGraph* gh7 = new TGraph();//TKE
    TGraph* gh8 = new TGraph();//Vcm1Vcm2
  TGraph* gh9 = new TGraph();
  TGraph* gh10 = new TGraph();
  TGraph* gh11 = new TGraph();
  TGraph* gh12 = new TGraph();
  TGraph* gh13 = new TGraph();
  TGraph* gh14 = new TGraph();
  TGraph* gh15 = new TGraph();

    //Calculate a range of scatter angles
    const int n=180; //max. theta1 angle

    float phi,phi2;//Scattering angles!!!

    float m1=product->GetA();
    float m2=product2->GetA();

    float mp=proj->GetA();
    float mt=tar->GetA();

    float mr1,mr2;

    mr1=m1/(mp+mt);
    mr2=m2/(mp+mt);

    float thcm1,thcm2;

    //***************************************
    //edited by Yun src_3det_v4
    float tke[100], mr[100];
    float vcm1[100], vcm2[100];
    float K=0.0;

    //CN
    float acn= rxn->Ap + rxn->At;
    float zcn= rxn->Zp + rxn->Zt;

    //cout << "Acn = " << acn << "\t Zcn = " << zcn << endl;
    const int m=100;

    for (int j=1;j<m;j++){
        mr[j] = j*0.01;
        tke[j] = (0.789*mr[j]*(1.0-mr[j])*zcn*zcn)/((pow(mr[j],(1.0/3.0))+pow((1-mr[j]),(1.0/3.0)))*pow(acn,(1.0/3.0)));
        //TKE is in units of MeV 

        K = 10*0.982*sqrt(((1.51*(pow(zcn,2.0)))/(pow(acn,(4.0/3.0))*(pow(mr[j],(1.0/3.0))+pow((1-mr[j]),(1.0/3.0)))))+(14.6/(acn*mr[j]*(1-mr[j]))));
        //K(=vcm1+vcm2) is in units of mm/ns
        vcm2[j] = K*mr[j];
        vcm1[j] = K-vcm2[j];

        gh7->SetPoint(j-1,mr[j],tke[j]);
        gh8->SetPoint(j-1,vcm1[j],vcm2[j]);
        //cout << "TKE=" << tke[j] << " MeV \t mr=" << mr[j] << endl; 
    }

   // TKE from Mulgin for p+232Th reaction

  for (int j=0;j<44;j++){
    gh9->SetPoint(j,TKEMulgin[j+3][0],TKEMulgin[j+3][1]);
    gh9->SetPoint(j+44,1-TKEMulgin[46-j][0],TKEMulgin[46-j][1]);
    gh12->SetPoint(j,TKEMulgin[j+3][0],Mulgin_plus*TKEMulgin[j+3][1]);
    gh12->SetPoint(j+44,1-TKEMulgin[46-j][0],Mulgin_plus*TKEMulgin[46-j][1]);
  }
  
  for (int j=0;j<46;j++){
    gh10->SetPoint(j,TKEMulgin[j+1][0],TKEMulgin[j+1][2]);
    gh10->SetPoint(j+46,1-TKEMulgin[46-j][0],TKEMulgin[46-j][2]);
    gh13->SetPoint(j,TKEMulgin[j+1][0],Mulgin_plus*TKEMulgin[j+1][2]);
    gh13->SetPoint(j+46,1-TKEMulgin[46-j][0],Mulgin_plus*TKEMulgin[46-j][2]);
  }

  for (int j=0;j<47;j++){
    gh11->SetPoint(j,TKEMulgin[j][0],TKEMulgin[j][3]);
    gh11->SetPoint(j+47,1-TKEMulgin[46-j][0],TKEMulgin[46-j][3]);
    gh14->SetPoint(j,TKEMulgin[j][0],Mulgin_plus*TKEMulgin[j][3]);
    gh14->SetPoint(j+47,1-TKEMulgin[46-j][0],Mulgin_plus*TKEMulgin[46-j][3]);
  }

  for (int j=0;j<37;j++){
    gh15->SetPoint(j,TKEUnik[j][0],TKEUnik[j][1]);
    gh15->SetPoint(j+37,1-TKEUnik[36-j][0],TKEUnik[36-j][1]);
  }



    //***************************************
    //NOTE. Reaction.cxx
    //recoil angle in rad in cm (mt = 0) or lab (mt = 1) frame

    for (int i=1;i<n;i++){
        reaction -> SetTheta(i*1.0); // ejectile angle
        phi = (reaction -> GetPhi(1))*R2D;// deg
        gh->SetPoint(i-1,i*1.0,phi); //(no.of points, x-axis, y-axis);

        reaction2 -> SetTheta(i*1.0); // ejectile angle
        phi2 = (reaction2 -> GetPhi(1))*R2D;// deg
        gh2->SetPoint(i-1,i*1.0,phi2);

        gh3->SetPoint(i-1,i*1.0,i*1.0+phi2);
        gh6->SetPoint(i-1,i*1.0,i*1.0+phi);

        //CM Frame
        thcm1=(reaction->GetPhi(0))*R2D;
        thcm2=(reaction2->GetPhi(0))*R2D;

        gh4->SetPoint(i-1,mr1,thcm1);
        gh5->SetPoint(i-1,mr2,thcm2);

    }


    TFile sfile(filename,"UPDATE");
    if (!sfile.IsOpen()){
        std::cout << "CubeHist: Root file " << filename << " not found!" << std::endl;
        return -1;
    }
    else{
        std::cout << "Generating calculated theta1_theta2 plot..." << std::endl;
    }

    if (sfile.GetListOfKeys()->Contains("Calc")!=true){
        sfile.mkdir("Calc");
    }

    // Graph options
    gh->SetLineColor(kBlue+2);
    gh->SetLineWidth(2);
    gh->SetMarkerColor(4);
    gh->SetMarkerStyle(21);
    gh->SetMarkerSize(0.1);
    gh->SetTitle("ejectile_recoil angle");
    gh->GetXaxis()->SetTitle("theta1[deg]");//ejectile angle
    gh->GetXaxis()->SetRange(0,180);
    gh->GetYaxis()->SetTitle("theta2[deg]");//recoile angle
    gh->GetYaxis()->SetRange(0,180);
    //gh->Draw();
    // c1-> Update(); 
    //gh->Print();

    // Graph options
    gh2->SetLineColor(kBlack);
    gh2->SetLineWidth(2);
    gh2->SetMarkerStyle(21);
    gh2->SetMarkerSize(0.2);
    gh2->SetTitle("ejectile_recoil angle");
    gh2->GetXaxis()->SetTitle("theta1[deg]");//ejectile angle
    gh2->GetXaxis()->SetRange(0,180);
    gh2->GetYaxis()->SetTitle("theta2[deg]");//recoile angle
    gh2->GetYaxis()->SetRange(0,180);
    //gh2->Draw();
    //c1-> Update();
    //gh->Print();

    // Graph options
    gh3->SetLineColor(kBlack);
    gh3->SetLineWidth(2);
    gh3->SetMarkerStyle(21);
    gh3->SetMarkerSize(0.2);
    gh3->SetTitle("calculated folding angle_p");
    gh3->GetXaxis()->SetTitle("theta1[deg]");//ejectile angle
    gh3->GetXaxis()->SetRange(0,180);
    gh3->GetYaxis()->SetTitle("theta1+theta2[deg]");//recoile angle
    gh3->GetYaxis()->SetRange(0,180);
    // gh3->Draw();
    //c1-> Update();
    //gh->Print();
    // Graph options
    gh6->SetLineColor(kBlue);
    gh6->SetLineWidth(2);
    gh6->SetMarkerStyle(21);
    gh6->SetMarkerSize(0.2);
    gh6->SetTitle("calculated folding angle_t");
    gh6->GetXaxis()->SetTitle("theta1[deg]");//ejectile angle
    gh6->GetXaxis()->SetRange(0,180);
    gh6->GetYaxis()->SetTitle("theta1+theta2[deg]");//recoile angle
    gh6->GetYaxis()->SetRange(0,180);
    //gh6->Draw();
    //c1-> Update();
    //gh->Print();


    // Graph options
    gh4->SetLineColor(kRed);
    gh4->SetMarkerColor(kRed);
    gh4->SetMarkerStyle(21);
    gh4->SetMarkerSize(0.1);
    gh4->SetTitle("mr1_thcm: calculated");
    gh4->GetXaxis()->SetTitle("MR");//ejectile angle
    gh4->GetXaxis()->SetRangeUser(0,1);
    gh4->GetYaxis()->SetTitle("theta_cm[deg]");//recoile angle
    gh4->GetYaxis()->SetRangeUser(0,180);
    //gh4->Draw();
    //c1-> Update(); 
    //gh->Print();

    // Graph options
    gh5->SetLineColor(kBlue);
    gh5->SetMarkerStyle(kBlue);
    gh5->SetMarkerSize(0.2);
    gh5->SetTitle("mr2_thcm: calculated");
    gh5->GetXaxis()->SetTitle("MR[deg]");//ejectile angle
    gh5->GetXaxis()->SetRangeUser(0,1);
    gh5->GetYaxis()->SetTitle("theta_cm[deg]");//recoile angle
    gh5->GetYaxis()->SetRangeUser(0,180);
    //gh5->Draw();
    //c1-> Update();
    //gh->Print();

    //*************************
    gh7->SetLineColor(kBlack);
    gh7->SetLineWidth(2);
    gh7->SetTitle("mr_TKE: calculated");
    gh7->GetXaxis()->SetTitle("MR");//ejectile angle
    gh7->GetXaxis()->SetRangeUser(0,1);
    gh7->GetYaxis()->SetRangeUser(0,400);
    gh7->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
    // gh7->Draw();

    gh8->SetLineColor(kBlack);
    gh8->SetLineWidth(2);
    gh8->SetTitle("vcm1_vcm2: calculated");
    gh8->GetXaxis()->SetTitle("Vcm1");//ejectile angle
    gh8->GetXaxis()->SetRangeUser(0,40);
    gh8->GetYaxis()->SetRangeUser(0,40);
    gh8->GetYaxis()->SetTitle("Vcm2 [MeV]");//recoile angle
    //gh8->Draw();

  gh9->SetLineColor(kViolet);
  gh9->SetLineWidth(2);
  gh9->SetTitle("mr_TKE: Mulgin_Ep=10.3MeV");
  gh9->GetXaxis()->SetTitle("MR");//ejectile angle
  gh9->GetXaxis()->SetRangeUser(0,1);
  gh9->GetYaxis()->SetRangeUser(0,400);
  gh9->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh9->Draw();

  gh10->SetLineColor(kRed);
  gh10->SetLineWidth(2);
  gh10->SetTitle("mr_TKE: Mulgin_Ep=18MeV");
  gh10->GetXaxis()->SetTitle("MR");//ejectile angle
  gh10->GetXaxis()->SetRangeUser(0,1);
  gh10->GetYaxis()->SetRangeUser(0,400);
  gh10->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh10->Draw();

  gh11->SetLineColor(kGreen);
  gh11->SetLineWidth(2);
  gh11->SetTitle("mr_TKE: Mulgin_Ep=30MeV");
  gh11->GetXaxis()->SetTitle("MR");//ejectile angle
  gh11->GetXaxis()->SetRangeUser(0,1);
  gh11->GetYaxis()->SetRangeUser(0,400);
  gh11->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh11->Draw();

  gh12->SetLineColor(kViolet);
  gh12->SetLineWidth(2);
  gh12->SetTitle(Mulgin_plus_txt1);
  //gh12->SetTitle("mr_TKE: Mulgin*1.02_Ep=10.3MeV");
  gh12->GetXaxis()->SetTitle("MR");//ejectile angle
  gh12->GetXaxis()->SetRangeUser(0,1);
  gh12->GetYaxis()->SetRangeUser(0,400);
  gh12->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh12->Draw();

  gh13->SetLineColor(kRed);
  gh13->SetLineWidth(2);
  //gh13->SetTitle("mr_TKE: Mulgin*1.02_Ep=18MeV");
  gh13->SetTitle(Mulgin_plus_txt2);
  gh13->GetXaxis()->SetTitle("MR");//ejectile angle
  gh13->GetXaxis()->SetRangeUser(0,1);
  gh13->GetYaxis()->SetRangeUser(0,400);
  gh13->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh13->Draw();

  gh14->SetLineColor(kGreen);
  gh14->SetLineWidth(2);
  //gh14->SetTitle("mr_TKE: Mulgin*1.02_Ep=30MeV");
  gh14->SetTitle(Mulgin_plus_txt3);
  gh14->GetXaxis()->SetTitle("MR");//ejectile angle
  gh14->GetXaxis()->SetRangeUser(0,1);
  gh14->GetYaxis()->SetRangeUser(0,400);
  gh14->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh14->Draw();

  gh15->SetLineColor(kRed);
  gh15->SetLineWidth(2);
  gh15->SetTitle("mr_TKE_Unik");
  gh15->GetXaxis()->SetTitle("MR");//ejectile angle
  gh15->GetXaxis()->SetRangeUser(0,1);
  gh15->GetYaxis()->SetRangeUser(0,400);
  gh15->GetYaxis()->SetTitle("TKE [MeV]");//recoile angle
  // gh15->Draw();

    //******************************


    sfile.cd("/");
    sfile.cd("Calc");

    gh->Write("th1_th2_pcalc",TObject::kOverwrite);
    gh2->Write("th1_th2_tcalc",TObject::kOverwrite);
    gh3->Write("folding_calc",TObject::kOverwrite);
    gh6->Write("folding2_calc",TObject::kOverwrite);
    gh4->Write("mrthcm_pcalc",TObject::kOverwrite);
    gh5->Write("mrthcm_tcalc",TObject::kOverwrite);
    gh7->Write("mrTKE_calc_Viola",TObject::kOverwrite);
    gh8->Write("vcm1vcm2_calc",TObject::kOverwrite);  

  gh9->Write("mrTKE_Mulgin_10.3MeV",TObject::kOverwrite);
  gh10->Write("mrTKE_Mulgin_18MeV",TObject::kOverwrite);
  gh11->Write("mrTKE_Mulgin_30MeV",TObject::kOverwrite);
  //gh12->Write("mrTKE_1.02*Mulgin_10.3MeV",TObject::kOverwrite);
  //gh13->Write("mrTKE_1.02*Mulgin_18MeV",TObject::kOverwrite);
  //gh14->Write("mrTKE_1.02*Mulgin_30MeV",TObject::kOverwrite);
  gh12->Write(Mulgin_plus_txt1,TObject::kOverwrite);
  gh13->Write(Mulgin_plus_txt2,TObject::kOverwrite);
  gh14->Write(Mulgin_plus_txt3,TObject::kOverwrite);
  gh15->Write("mrTKEUnik",TObject::kOverwrite);


    std::cout << "Calculated plots complete." << std::endl;

    return 0;

}
