#ifndef PLOT_KIN_H
#define PLOT_KIN_H



#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "TCanvas.h"
#include "TMath.h"
#include "TGraph.h"
#include "TFile.h"
#include "TAxis.h"

#include "Nucleus.h"
#include "Reaction.h"
#include "CubeInput.h"
#include "CubeStruct.h"
#include "CubeReact.h"
#include "eloss.h"

int plot_kin(char*,reactPar*);
int calc_kin(char*,bool);

#endif
