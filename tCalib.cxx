/*
   For src_3det_v5  - Yun Jeung, May 2019 update 

   tCalib.cxx: Used to automate time calibration sorts with dacube. See dacube documentation for usage.
dependencies: CubeSort.C, CubeSort.h, CubeGate.C, CubeGate.h, tCalib.h, root libraries

Liz Williams, Jan 2014

 */

/* general purpose header files*/
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <math.h>


/* ROOT header files */
#include "TTree.h"
#include "TH2D.h"
#include "TH2I.h"
#include "TCutG.h"
#include "TStyle.h"

/*specialized header files*/
#include "tCalib.h"

using namespace std;

/*------------------------------------------------------------------------------------
  thist
  : makes histograms for tcalib sort
  ------------------------------------------------------------------------------------*/
void thist(char* procfile, fitpar* FIT){

    cout << "Now producing relevant histograms..." << endl;

    TFile f2(procfile,"update");
    TTree* tree;
    f2.GetObject("GatedTree",tree);


    if (tree->IsZombie()){
        cout << "TCalib assumes that gates are being applied." << endl;
        cout << "No GatedTree found; histograms will not be made" << endl;

    }
    else{

        TH2I T1_T2raw("T1_T2raw","T1_T2raw",2048,0,2048,2048,0,2048);
        tree->Draw("raw_T2:raw_T1>>T1_T2raw","(raw_y1>0||raw_y1<0)&&(raw_x1>0||raw_x1<0)&&(raw_y2>0||raw_y2<0)&&(raw_x2>0||raw_x2<0)&&(raw_T1>0||raw_T2<0)","colz");
        T1_T2raw.SetTitle("raw_T1 vs raw_T2 [Counts];T1 [ch];T2 [ch]");
        T1_T2raw.Write("",TObject::kOverwrite);

        TH2D vdvp("vdvp","Vpar-Vcn:vperp",1000,-5,5,1000,-5,5);
        tree->Draw("vperp:vpar-Vcn>>vdvp","(raw_y1>0||raw_y1<0)&&(raw_x1>0||raw_x1<0)&&(raw_y2>0||raw_y2<0)&&(raw_x2>0||raw_x2<0)&&(raw_T1>0||raw_T2<0)","colz");
        vdvp.SetTitle("vpar-Vcn vs vperp [Counts];vpar-vcn [mm/ns]; vperp[mm/ns];");
        vdvp.Write("",TObject::kOverwrite);

        FIT->vmean_x=vdvp.GetMean(1);
        FIT->vmean_y=vdvp.GetMean(2);
        FIT->vRMS_x=vdvp.GetRMS(1);
        FIT->vRMS_y=vdvp.GetRMS(2);


        TH2D mrvr("mrvr","MR:Vpar/Vcn",1000,0,1,1000,0,2);
        tree->Draw("vpar/Vcn:MR>>mrvr","(raw_y1>0||raw_y1<0)&&(raw_x1>0||raw_x1<0)&&(raw_y2>0||raw_y2<0)&&(raw_x2>0||raw_x2<0)&&(raw_T1>0||raw_T2<0)","colz");
        mrvr.SetTitle("MR vs vpar/Vcn [Counts];Mass Ratio; vpar/Vcn [mm/ns]");
        mrvr.Write("",TObject::kOverwrite);

        FIT->mmean_x=mrvr.GetMean(1);
        FIT->mmean_y=mrvr.GetMean(2);
        FIT->mRMS_x=mrvr.GetRMS(1);
        FIT->mRMS_y=mrvr.GetRMS(2);

        TH1D tdifference("tdifference","tdifference",1000,-500,500);
        tree->Draw("tdiff>>tdifference","","");
        tdifference.SetTitle("tdiff;tdiff[ns];Counts");
        tdifference.Write("",TObject::kOverwrite);


        FIT->dTmean_x=tdifference.GetMean(1);
        FIT->dTRMS_x=tdifference.GetMean(1);

        TH2D mrthcm1("mrthcm1","MR:ThetaCM1",100,0,1,180,0,180);
        tree->Draw("thetaCM1:MR>>mrthcm1","(raw_y1>0||raw_y1<0)&&(raw_x1>0||raw_x1<0)&&(raw_y2>0||raw_y2<0)&&(raw_x2>0||raw_x2<0)&&(raw_T1>0||raw_T2<0)","colz");
        mrthcm1.SetTitle("MR vs ThetaCM [Counts];Mass Ratio; ThetaCM");
        mrthcm1.Write("",TObject::kOverwrite);

        TH2D temp("temp","temp",100,0,1,180,0,180);
        tree->Draw("(180.-thetaCM1):(1.-MR)>>temp","thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR &&(raw_y1>0||raw_y1<0)&&(raw_x1>0||raw_x1<0)&&(raw_y2>0||raw_y2<0)&&(raw_x2>0||raw_x2<0)&&(raw_T1>0||raw_T2<0) "," "); //condition from Renju's code

        TH2D temp2("temp2","temp2",100,0,1,180,0,180);
        tree->Draw("thetaCM1:MR>>temp2","thetaCM1 > 43.125 + 197.917*MR - 312.5*MR*MR + 208.33*MR*MR*MR && (raw_y1>0||raw_y1<0)&&(raw_x1>0||raw_x1<0)&&(raw_y2>0||raw_y2<0)&&(raw_x2>0||raw_x2<0)&&(raw_T1>0||raw_T2<0) "," "); //condition from Renju's code

        TH2D mrthcm1_mir("mrthcm1_mir","mrthcm1_mir",100,0,1,180,0,180);
        mrthcm1_mir.SetOption("COLSCATZ");
        mrthcm1_mir.SetMinimum(1);
        mrthcm1_mir.Add(&mrthcm1_mir, &temp);
        mrthcm1_mir.Add(&mrthcm1_mir, &temp2);
        mrthcm1_mir.SetTitle("MR vs thetaCM1 (mirrored) [Counts]; Mass Ratio; thetaCM1 [deg]");
        mrthcm1_mir.Write("",TObject::kOverwrite);



    }



}

/*------------------------------------------------------------------------------------
  CalibSort
  : Sorting the data (similar to CubeSort)
  ------------------------------------------------------------------------------------*/
int CalibSort(sortInfo* sortStruct, char* procfile, MWPCgeo* mwpcGeo,MWPCcorr* mwpcCorr,reactPar* rxn, targetPar* targ, float T0, float dT){

    char* datafile;

    datafile = &sortStruct->rawdata[0];

    rxn->T0=T0;
    rxn->dT=dT;//use program inputs for T0 and dT rather than sort file inputs

    //Now do the calibrations:

    //Position (using detector edges):
    for (int i=0;i<N_MWPC;i++){
        mwpcPosCalib(&mwpcGeo[i]);
        calcMwpcCenter(&mwpcGeo[i]);
        calcQuadrantCenter(&mwpcGeo[i],&mwpcCorr[i]);
    }

    //actually begin the sort

    TFile *f1;
    f1=new TFile(datafile);  // File to be sorted
    if (!f1->IsOpen()){
        cout << "CubeSort: Error: could not find sort file" << endl;
        return -1;
    }

    // Assumes CubeTree structure - define TTree name, get object
    TTree* CubeTree;
    f1->GetObject("CubeTree",CubeTree);

    //create new CubeClass type
    ZClass sort(CubeTree);

    //initialize sort for CubeTree
    sort.Init(CubeTree);

    //complete sort as defined in Loop
    sort.Loop(sortStruct->minEntry,sortStruct->maxEntry,mwpcGeo,mwpcCorr,rxn,targ,procfile);

    //Delete CubeTree object after sort is complete
    delete CubeTree;
    delete f1;
    return 0;

}// ***** end CalibSort ******//

/*------------------------------------------------------------------------------------
  tCalib
  ------------------------------------------------------------------------------------*/

int tCalib(char* sortfile){

    float T0, dT;
    char buffer[300];
    ofstream lookup;
    ifstream input;
    char* datafile;
    string procdata;
    char* procfile;
    char* gateF;
    char* buf;
    TString gates;
    string junk, finf;
    int errCheck;

    fitpar FIT={0,0,0,0,0,0,0,0,0,0};

    //define+initialize detector structures; see CubeStruct.h
    MWPCgeo mwpcGeo[N_MWPC]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
    MWPCcorr mwpcCorr[N_MWPC]={{0,0,0,0,{0,0,0,0},0,0}};
    //monitor mon[N_mon]={{0,0,0,0,0}};

    //define+initialize structures to store reaction+target params; see CubeStruct.h
    reactPar rxn={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//src_v5.0 +2
    targetPar targ={0,0,0,0,0,0,0,{0,0}};


    //define and initialize input structures (see CubeStruct.h)
    sortInfo sort={"","","",0,0,0,0,0,0,0,0,"","",""};

    // read in input file information and store in appropriate place
    errCheck=readInput(sortfile,&sort,&rxn,&targ);

    if ( errCheck == -1 ){
        cout << "Input file read-in error. Sort failed." << endl;
        return -1;
    }

    datafile = &sort.rawdata[0];//create char array with file name to be sorted
    cout << "Sorting " << datafile << " ... " << endl;

    procdata = AutoFileExtension(&sort,&rxn); //figure out name of processed file
    procfile = &procdata[0];
    cout << "Processed file " << procfile << " will be produced ... " << endl;

    gateF = &sort.gatefile[0];


    ////\\\\ end sort file input \\\\////


    //calculate energy loss correction first - this assumes interactions halfway through EACH target layer
    //if (rxn.elossBool==true){
    if (rxn.dEcalcType==1 || rxn.dEcalcType==2 || rxn.dEcalcType==3 ){
        calcElossBeam(&targ,&rxn);
    }

    //immediately calculate the remaining undefined parameters in rxn struct
    calcRxn(&rxn);

    //next: read in the detector file contents and populate the relevant structs in CubeDet.h
    errCheck = readDetInfo(&sort,mwpcGeo,mwpcCorr);

    if (errCheck==-1){
        return -1;
    }


    int i=0;
    int j=0;

    //change file path from string to character pointer for use in sprintf
    buf=&sort.calibpath[0];

    sprintf(buffer,"%s/tcalib_table.dat",buf);
    lookup.open(buffer);

    if (!lookup.is_open()){
        cout << "tCalib: Lookup table not open. Make sure the time calibration folder specified in sort file exists."<< endl;
        return -1;
    }

    lookup << "i j T0 dT" << endl;


    T0=sort.T0init;


    while (T0<=sort.T0fin){
        j=0;
        dT=sort.dTinit;

        while (dT<=sort.dTfin){

            cout << sortfile << endl;
            CalibSort(&sort,procfile,mwpcGeo,mwpcCorr,&rxn,&targ,T0,dT);
            ApplyGate(sortfile);

            cout << "Creating histograms for..."  << gateF << endl;
            thist(gateF,&FIT);


            sprintf(buffer,"mv %s %s/calib_%d_%d.root",gateF,buf,i,j);
            cout << buffer << endl;
            int k=system(buffer);
            if (k!=0){
                return -1;
            }

            lookup << i << " " << j << " " << T0 << " " << dT << "\t vdvpFit: x: " <<  FIT.vmean_x << " (+-) " << FIT.vRMS_x << "\t y: " << FIT.vmean_y << " (+-) " << FIT.vRMS_y;
            lookup << "\t mrvrFit: x: "<< FIT.mmean_x << " (+/-) " << FIT.mRMS_x << "\t y: " << FIT.mmean_y <<" (+/-) " << FIT.mRMS_y;
            lookup << "\t tdiffFit: x: "<< FIT.dTmean_x << " (+/-) " << FIT.dTRMS_x << endl;

            j++;
            dT=dT+sort.dTstep;
        }
        i++;
        T0=T0+sort.T0step;
    }


    return 0;
}// end tcalib







