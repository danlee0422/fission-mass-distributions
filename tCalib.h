#ifndef GUARD_tCalib_h
#define GUARD_tCalib_h

#include "CubeStruct.h"
#include "CubeInput.h"
#include "CubeSort.h"
#include "CubeGate.h"


//stores mean, rms from root simple fit
struct fitpar {
    float vmean_x,vmean_y,vRMS_x,vRMS_y; // for vdvp
    float mmean_x,mmean_y,mRMS_x,mRMS_y; // for mrvr
    float dTmean_x,dTRMS_x; // for t2-t1
};

void thist(char*,fitpar*);
int CalibSort(sortInfo*, char*,MWPCgeo*,MWPCcorr*,reactPar*, targetPar*, float, float);
int tCalib(char*);


#endif
