/*User-specific code for dacube. If you modify this, make sure to

make clean
make

And watch for errors in your code... */

/**************** Position anomaly corrections **********/

#ifdef USER_DET //code inserted into CubeDet.cxx
//corrects for problems in position reconstruction due to time
//mismatch in CF signals.
//May be applicable to position data derived from time difference between CF
//and one end of the delay line

//det1
  if ((i==0)&& mwpcEvt->y_raw<580){
        //mwpcEvt->x_raw=mwpcEvt->x_raw-5;
   }

// if((i==0)&& mwpcEvt ->x_raw>565){
  // }

//det2
  if ((i==1) && mwpcEvt->x_raw>35){
     //mwpcEvt->y_raw=mwpcEvt->y_raw+6;
    //mwpcEvt->x_raw=mwpcEvt->x_raw-5;
  }
  if ((i==1) && mwpcEvt->y_raw>40){
    // mwpcEvt->y_raw=mwpcEvt->y_raw-10;
  } 


    mwpcEvt->x_mm = mwpcGeo->ax_mm * mwpcEvt->x_raw + mwpcGeo->bx_mm;
    mwpcEvt->y_mm = mwpcGeo->ay_mm * mwpcEvt->y_raw + mwpcGeo->by_mm;

#endif

/**************** Gate definitions *********************/

#ifdef USER_GATEFUNC //code inserted into CubeGate.cxx

tcfunc();
tttt();
vdsinevp_run004_test();
e52func();
e56func();
e562func();
e522func();
e77func();
e492func();
e782func();
e502func();
e58func();
tke48func();
tke52func();
tke56func();
tke77func();
tke49func();
tke50func();
tke53func();
tke54func();
tke57func();
tke78func();
tke58func();
tke79func();

//--------------------------------
vdvp_centre();

rawt1t2_run047();
rawt3t2_run047();

t2tback_run047();
t2tback_run022();

vdsinevp_run004();
vdsinevp_run022();

tar12();
tar32();

timegate();
timegate12();
timegate32();
timegate12_050();
timegate12_051();
timegate12_055();
timegate32_047();
timegate32_050();
timegate32_051();
timegate32_055();

Det1_Edges();
Det2_Edges();
Det3_Edges();

SF12LadderPos3();
SF32LadderPos3();

SF12LadderPos5();
SF32LadderPos5();

SF12LadderPos7();
SF32LadderPos7();

//pdx gates
run022_T1E1();
run022_vdvp();
raw_T1E1cut_pdx_run036();
vdvp_036();
raw_test4();
vdvp();

#endif


#ifdef USER_GATE //code inserted into CubeGate.cxx

#include "user/cuts/tcfin.C"
#include "user/cuts/test.C"
#include "user/cuts/vdsinevp_run004_test.C"
#include "user/cuts/fmte52.C"
#include "user/cuts/fmte56.C"
#include "user/cuts/fmte562.C"
#include "user/cuts/fmte522.C"
#include "user/cuts/fmte77.C"
#include "user/cuts/fmte492.C"
#include "user/cuts/fmte782.C"
#include "user/cuts/fmte502.C"
#include "user/cuts/fmte58.C"
#include "user/cuts/final_tke48.C"
#include "user/cuts/final_tke52.C"
#include "user/cuts/final_tke56.C"
#include "user/cuts/final_tke77.C"
#include "user/cuts/final_tke49.C"
#include "user/cuts/final_tke50.C"
#include "user/cuts/final_tke53.C"
#include "user/cuts/final_tke54.C"
#include "user/cuts/final_tke57.C"
#include "user/cuts/final_tke78.C"
#include "user/cuts/final_tke58.C"
#include "user/cuts/final_tke79.C"

//--------------------------------
#include "user/cuts/vdcentre.C"

#include "user/cuts/raw_t1t2_run066.C"
#include "user/cuts/raw_t3t2_run066.C"

#include "user/cuts/tbackt2_run047.C"
#include "user/cuts/t2tback_run022.C"

#include "user/cuts/vdsinevp_run004.C"
#include "user/cuts/vdsinevp_run022.C"

#include "user/cuts/target12_gate.C"
#include "user/cuts/target32_gate.C"

#include "user/cuts/trans_tbackt2.C"
#include "user/cuts/trans_t1t2.C"
#include "user/cuts/trans_t3t2.C"
#include "user/cuts/trans_t1t2_run050.C"
#include "user/cuts/trans_t1t2_run051.C"
#include "user/cuts/trans_t1t2_run055.C"
#include "user/cuts/trans_t3t2_run047.C"
#include "user/cuts/trans_t3t2_run050.C"
#include "user/cuts/trans_t3t2_run051.C"
#include "user/cuts/trans_t3t2_run055.C"

#include "user/cuts/fissy_rawXY1.C"
#include "user/cuts/fissy_rawXY2.C"
#include "user/cuts/fissy_rawXY3.C"

#include "user/cuts/ladder_SF12_pos3.C"
#include "user/cuts/ladder_SF32_pos3.C"

#include "user/cuts/ladder_SF12_pos5.C"
#include "user/cuts/ladder_SF32_pos5.C"

#include "user/cuts/ladder_SF12_pos7.C"
#include "user/cuts/ladder_SF32_pos7.C"

//pdx gates
#include "user/cuts/trans_T1E1_run022.C"
#include "user/cuts/trans_vdvp_run022.C"
#include "user/cuts/trans_T1E1cut_pdx_run036.C"
#include "user/cuts/trans_vdvpcut_pdx_run036.C"
#include "user/cuts/trans_test4.C"
#include "user/cuts/trans_ellipse2.C"

#endif

/*************** Histogram definitions ***************/

#ifdef USER_HIST //Code inserted into CubeHist.cxx (Stored in UHist file in root tree)

/* Add user-defined histogram code here: */

  TH1D* Mratio=new TH1D("Mratio","Mratio",100,0,1);
  tree->Draw("MR>>Mratio","","");
//Draw arguments: Draw("yvalue:xvalue>>histname","gates to apply","");
  Mratio->SetTitle("Mratio [Counts]; MR; Counts");
  Mratio->Write("",TObject::kOverwrite);

  TH2D* fdthphi=new TH2D("fdthphi","fdthphi",1000,50,200,1000,50,300);
  fdthphi->SetOption("COLSCATZ");
  fdthphi->SetMinimum(0);
  tree->Draw("phi12:theta12>>fdthphi","","");
  fdthphi->SetTitle("fdthphi;theta1+theta2 [deg];phi1-phi2 [deg]");
  fdthphi->Write("",TObject::kOverwrite);
  delete fdthphi;

  TH2D* targpos12=new TH2D("targpos12","targpos",1000,-200,200,1000,-200,500);
  targpos12->SetOption("COLSCATZ");
  targpos12->SetMinimum(0);
  tree->Draw("(y2det+y1det)/2:(x1det+180*(195-x2det)/(195+x2det))/2>>targpos12","","");
  targpos12->SetTitle("targpos12;(x1det+180*(195-x2det)/(195+x2det))/2 [mm];(y2det+y1det)/2 [mm]");
  targpos12->Write("",TObject::kOverwrite);
  delete targpos12;

  TH2D* targpos32=new TH2D("targpos32","targpos",1000,-200,200,1000,-200,500);
  targpos32->SetOption("COLSCATZ");
  targpos32->SetMinimum(0);
  tree->Draw("(y2det+y3det)/2:(x2det-x3det)/2>>targpos32","","");
  targpos32->SetTitle("targpos32;(x2det-x3det)/2 [mm];(y2det+y3det)/2 [mm]");
  targpos32->Write("",TObject::kOverwrite);
  delete targpos32;

  TH1D* MR01=new TH1D("MR01","MR",1000,0,1);
  //MR01->SetOption("COLSCATZ");
  MR01->SetMinimum(0);
  tree->Draw("MR>>MR01","","");
  MR01->SetTitle("MR;MR;counts");
  MR01->Write("",TObject::kOverwrite);

  delete MR01;
#endif
