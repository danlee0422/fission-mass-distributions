#include "TCutG.h"

TCutG *testcsort;

void testcfunc(){

   testcsort = new TCutG("testcsort",10);
   testcsort->SetVarX("MR");
   testcsort->SetVarY("TKE");
   testcsort->SetTitle("Graph");
   testcsort->SetFillStyle(1000);
   testcsort->SetPoint(0,0.509328,282.77);
   testcsort->SetPoint(1,0.411381,274.324);
   testcsort->SetPoint(2,0.264459,220.27);
   testcsort->SetPoint(3,0.285448,152.703);
   testcsort->SetPoint(4,0.341418,130.743);
   testcsort->SetPoint(5,0.721549,122.297);
   testcsort->SetPoint(6,0.754198,174.662);
   testcsort->SetPoint(7,0.639925,262.5);
   testcsort->SetPoint(8,0.525653,276.014);
   testcsort->SetPoint(9,0.509328,282.77);
   testcsort->Draw();
}
