#include "TCutG.h"

TCutG *rawXY1;

void Det1_Edges(){

   rawXY1 = new TCutG("rawXY1",5);
   rawXY1->SetVarX("raw_x1");
   rawXY1->SetVarY("raw_y1");
   rawXY1->SetTitle("Graph");
   rawXY1->SetFillStyle(1000);
   rawXY1->SetPoint(0,2720,3820);
   rawXY1->SetPoint(1,2720,-3820);
   rawXY1->SetPoint(2,-2650,-3820);
   rawXY1->SetPoint(3,-2650,3820);
   rawXY1->SetPoint(4,2720,3820);
   rawXY1->Draw();
}
