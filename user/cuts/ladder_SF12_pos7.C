#include "TCutG.h"

TCutG *SF12pos7;

void SF12LadderPos7(){

   SF12pos7 = new TCutG("SF12pos7",5);
   SF12pos7->SetVarX("(x1det+180*(195-x2det)/(195+x2det))/2");
   SF12pos7->SetVarY("(y2det+y1det)/2");
   SF12pos7->SetTitle("Graph");
   SF12pos7->SetFillStyle(1000);
   SF12pos7->SetPoint(0,-8,74);
   SF12pos7->SetPoint(1,8,74);
   SF12pos7->SetPoint(2,8,45);
   SF12pos7->SetPoint(3,-8,45);
   SF12pos7->SetPoint(4,-8,74);
   SF12pos7->Draw();
}
