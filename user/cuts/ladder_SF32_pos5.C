#include "TCutG.h"

TCutG *SF32pos5;

void SF32LadderPos5(){

   SF32pos5 = new TCutG("SF32pos5",5);
   SF32pos5->SetVarX("x2det-x3det");
   SF32pos5->SetVarY("y2det+y3det");
   SF32pos5->SetTitle("Graph");
   SF32pos5->SetFillStyle(1000);
   SF32pos5->SetPoint(0,-20,210);
   SF32pos5->SetPoint(1,20,210);
   SF32pos5->SetPoint(2,20,170);
   SF32pos5->SetPoint(3,-20,170);
   SF32pos5->SetPoint(4,-20,210);
   SF32pos5->Draw();
}
