#include "TCutG.h"

TCutG *time022;

void t2tback_run022(){

   time022 = new TCutG("time022",11);
   time022->SetVarX("tback");
   time022->SetVarY("t2");
   time022->SetTitle("Graph");
   time022->SetFillStyle(1000);
   time022->SetLineWidth(2);
   time022->SetPoint(0,9.23047,96.6475);
   time022->SetPoint(1,2.74492,87.7663);
   time022->SetPoint(2,2.4855,33.7393);
   time022->SetPoint(3,7.41452,6.35567);
   time022->SetPoint(4,16.4943,5.61558);
   time022->SetPoint(5,56.9641,10.7963);
   time022->SetPoint(6,41.3988,17.8272);
   time022->SetPoint(7,26.8712,25.2281);
   time022->SetPoint(8,14.9378,47.4311);
   time022->SetPoint(9,12.8624,93.6871);
   time022->Draw();
}
