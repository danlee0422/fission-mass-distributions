#include "TCutG.h"

TCutG *T1E1_run022;

void run022_T1E1(){

   T1E1_run022 = new TCutG("T1E1_run022",10);
   T1E1_run022->SetVarX("raw_T1");
   T1E1_run022->SetVarY("raw_E1");
   T1E1_run022->SetTitle("Graph");
   T1E1_run022->SetFillColor(1);
   T1E1_run022->SetPoint(0,-1306.09,169.512);
   T1E1_run022->SetPoint(1,-1186.98,153.026);
   T1E1_run022->SetPoint(2,-1134.29,74.9927);
   T1E1_run022->SetPoint(3,-1134.29,50.0806);
   T1E1_run022->SetPoint(4,-1205.3,35.4264);
   T1E1_run022->SetPoint(5,-1372.51,35.4264);
   T1E1_run022->SetPoint(6,-1438.94,49.7142);
   T1E1_run022->SetPoint(7,-1448.1,79.7553);
   T1E1_run022->SetPoint(8,-1418.32,151.561);
   T1E1_run022->SetPoint(9,-1306.09,169.512);
   //T1E1_run022->Draw();
}
