#include "TCutG.h"

TCutG *t1t2_055;

void timegate12_055(){

   t1t2_055 = new TCutG("t1t2_055",5);
   t1t2_055->SetVarX("t1");
   t1t2_055->SetVarY("t2");
   t1t2_055->SetTitle("Graph");
   t1t2_055->SetFillStyle(1000);
   t1t2_055->SetPoint(0,17.6,42.8);
   t1t2_055->SetPoint(1,45.6,11.1);
   t1t2_055->SetPoint(2,11.2,10.7);
   t1t2_055->SetPoint(3,11.2,34.2);
   t1t2_055->SetPoint(4,17.6,42.8);
   t1t2_055->Draw();
}
