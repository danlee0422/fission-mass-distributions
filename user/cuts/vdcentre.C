#include "TCutG.h"

TCutG *vdcentre;

void vdvp_centre(){

   vdcentre = new TCutG("vdcentre",6);
   vdcentre->SetVarX("vpar-Vcn");
   vdcentre->SetVarY("vperp");
   vdcentre->SetTitle("Graph");
   vdcentre->SetFillStyle(1000);
   vdcentre->SetPoint(0,-0.999491,0.581928);
   vdcentre->SetPoint(1,0.806025,0.581928);
   vdcentre->SetPoint(2,0.806025,-0.562529);
   vdcentre->SetPoint(3,-0.957624,-0.550289);
   vdcentre->SetPoint(4,-0.978557,0.557448);
   vdcentre->SetPoint(5,-0.999491,0.581928);
   vdcentre->Draw();
}
